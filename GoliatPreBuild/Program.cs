﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Dynamic;
using System.Runtime;
using System.Reflection;
using System.IO;
using System.Windows.Forms;
using Goliat.Serialization;



namespace GoliatPreBuild
{
	using System;
	using System.Drawing;
	using OpenTK;
	using OpenTK.Graphics;
	using OpenTK.Graphics.OpenGL;
	using OpenTK.Input;
	using Vector2 = Goliat.Vector2;
	using Vector3 = OpenTK.Vector3;
	using Color = System.Drawing.Color;
	using Key = OpenTK.Input.Key;
	using Size = System.Drawing.Size;

	using Goliat;

	namespace Example
	{
		public class Test
		{
			public int machintruc = 6;
		}
		public class TestKey
		{
			public int machinchose = 5;
			public Test test = null;
			public TestKey(int machinchose, int machintruc)
			{
				this.machinchose = machinchose;
				this.test = new Test();
				this.test.machintruc = machintruc;
			}
		}
		public class SubObject
		{
			public int machin = 0;
			public string blurp = "fdqd";
			public SubObject(int machin, string blurp)
			{
				this.machin = machin;
				this.blurp = blurp;
			}
		}
		public class TestClassWithDictionary
		{
			public Dictionary<TestKey, SubObject> dictionary = new Dictionary<TestKey, SubObject>();
			public TestClassWithDictionary()
			{
				;
			}
		}
		class GoliatGame : GameWindow
		{
			public static void Test(Vector3 vector)
			{
				Console.WriteLine("vector");
				;
			}
			public static void Test(float nb)
			{
				Console.WriteLine("float");
			}
			public static void Test(string str)
			{
				Console.WriteLine("string");
				;
			}
			public static void Test(int test)
			{
				Console.WriteLine("int");
				;
			}
			public static void Initialize()
			{
				
				Stream myStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("GoliatPreBuild.Resources.chientexture2.png");
				string[] names = Assembly.GetExecutingAssembly().GetManifestResourceNames();
				for (int i = 0; i < names.Length; i++)
				{
					Console.WriteLine("found ressoruce:" + names[i]);
				}
				if (myStream == null)
				{
					Console.WriteLine("resource not found");
				}
				Bitmap image = new Bitmap(myStream);
				var myForm = new Form();
				myForm.ClientSize = new Size(image.Width, image.Height);

				PictureBox pb = new PictureBox();
				pb.Image = image;
				pb.Dock = DockStyle.Fill;
				myForm.Controls.Add(pb);
				myForm.Show();

				OpenTK.ToolkitOptions options = new OpenTK.ToolkitOptions();
				options.Backend = PlatformBackend.PreferNative;
				Toolkit.Init(options);
				GoliatGame game = new GoliatGame();
				game.Load += (sender, e) =>
				{
					// setup settings, load textures, sounds
					game.VSync = VSyncMode.On;
				};

				game.Resize += (sender, e) =>
				{
					GL.Viewport(0, 0, game.Width, game.Height);
				};

				game.UpdateFrame += (sender, e) =>
				{
					// add game logic, input handling
					if (game.Keyboard[Key.Escape])
					{
						game.Exit();
					}
				};

				game.RenderFrame += (sender, e) =>
				{
					// render graphics
					GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

					GL.MatrixMode(MatrixMode.Projection);
					GL.LoadIdentity();
					GL.Ortho(-1.0, 1.0, -1.0, 1.0, 0.0, 4.0);

					GL.Begin(PrimitiveType.Triangles);

					GL.Color3(Color.MidnightBlue);
					GL.Vertex2(-1.0f, 1.0f);
					GL.Color3(Color.SpringGreen);
					GL.Vertex2(0.0f, -1.0f);
					GL.Color3(Color.Ivory);
					GL.Vertex2(1.0f, 1.0f);

					GL.End();

					game.SwapBuffers();
				};

				// Run the game at 60 updates per second
				game.Run(60.0);
			}
			public static void affsubnodes(System.Xml.XmlNode node)
			{
				
				if (node.ChildNodes.Count == 0)
				{
					Console.WriteLine("TEXT:" + node.InnerText);
				}
				else
				{
					Console.WriteLine(node.Name);
				}
				for (int i = 0; i < node.ChildNodes.Count; i++)
				{
					affsubnodes(node.ChildNodes[i]);
				}
			}
			public static bool SameSide(Vector2 p1, Vector2 p2, Vector2 a, Vector2 b)
			{
				Vector2 cp1 = Goliat.Vector2.Cross((b - a), (p1 - a));
				Vector2 cp2 = Goliat.Vector2.Cross((b - a), (p2 - a));
				if (Vector2.Dot(cp1, cp2) >= 0)
					return (true);
				return (false);
			}
			public static bool TriangleContains(List<Vector2> triangle, Vector2 p)
			{

				Vector2 a = triangle[0];
				Vector2 b = triangle[1];
				Vector2 c = triangle[2];
				if (SameSide(p, a, b, c) && SameSide(p, b, a, c) && SameSide(p, c, a, b))
					return (true);
				else
					return (false);
			}
			[STAThread]
			public static void Main(string[] argv)
			{
				Vector2 a = new Vector2(487, 143);
				Vector2 b = new Vector2(720, 384);
				Vector2 c = new Vector2(839, -235);
				List<Vector2> triangle = new List<Vector2>(){a, b, c};
				Vector2 min = Vector2.zero;
				Vector2 max = Vector2.zero;
				List<Vector2> vectors = triangle;
				if (vectors.Count > 0)
				{
					min = new Vector2(vectors[0].x, vectors[0].y);
					max = new Vector2(vectors[0].x, vectors[0].y);
				}

				for (int i = 0; i < vectors.Count; i++)
				{
					if (vectors[i].x < min.x)
					{
						min.x = vectors[i].x;
					}
					if (vectors[i].y < min.y)
					{
						min.y = vectors[i].y;
					}
					if (vectors[i].x > max.x)
					{
						max.x = vectors[i].x;
					}
					if (vectors[i].y > max.y)
					{
						max.y = vectors[i].y;
					}
				}
				List<Vector2> foundPPls = new List<Vector2>();
				for (int y = (int)min.y; y < (int)max.y; y++)
				{
					for (int x = (int)min.x; x < (int)max.x; x++)
					{
						
						Vector2 test = new Vector2(x, y);
						if (TriangleContains(triangle, test))
						{
							if (test != a && test != b && test != c)
							{
								foundPPls.Add(test);
							}
						}
					}
				}
				Console.WriteLine("foundppls:" + foundPPls.Count);
				float time = 1000;
				Vector2 direction = Vector2.right;
				float speed = 0.1f;
				int count = 0;
				for (int i = 0; i < foundPPls.Count; i++)
				{
					foundPPls[i] += time * speed * direction;
					if (!TriangleContains(triangle, foundPPls[i]))
					{
						count++;
					}
				}
				Console.WriteLine("Found moved out of triangle:" + count);
				//Bounds2D bounds =  
				/*System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
				customCulture.NumberFormat.NumberDecimalSeparator = ".";

				System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
				//dynamic test = (double)5;
				//Test(test);
				Goliat.Scene scn = new Goliat.Scene("alien2.dae");
				if (scn.children != null && scn.children.Count > 0)
				{
					Console.WriteLine("Imported alien");
				}
				scn.ExportCollada("testouille.dae");
				Console.WriteLine("begin test");
				//on rajoute underscore au cas ou nom connu. genre vector3_ et quand on le get, si on ne trouve pas, on lui enleve son underscore seulement dans le cas ou il est ecrit exactement comme ça.
				Console.WriteLine("test:" + ((float)(2.455f)).ToString() );
				string value = "<CustomLine><machin><truc>fsdsdfsd</truc><oy>bloup</oy></machin></CustomLine>";
				
				System.Xml.XmlDocument elem = new System.Xml.XmlDocument();
				elem.LoadXml(value);

				//elem.ChildNodes;

				for (int i = 0; i < elem.ChildNodes.Count; i++)
				{
					//Console.WriteLine(elem.Name);
					affsubnodes(elem.ChildNodes[i]);
				}*/


				//<CustomLine><machin>fsdsdfsd</machin></CustomLine>
				/*TestClassWithDictionary test = new TestClassWithDictionary();
				test.dictionary.Add(new TestKey(12, 25), new SubObject(54, "ffsdfdfds"));
				test.dictionary.Add(new TestKey(24, 13), new SubObject(12, "mlgdssd"));
				Serializer.Serialize("test.serialization", test);

				TestClassWithDictionary test2 = (TestClassWithDictionary)Serializer.UnSerialize("test.serialization");
				if (test2.dictionary != null)
				{
					Console.WriteLine("ouai il existe bien");
				}
				foreach (TestKey key in test2.dictionary.Keys)
				{
					Console.WriteLine("key found! :" + key.machinchose + "; machintruc:" + key.test.machintruc);
					Console.WriteLine("and the string in subobject:" + test2.dictionary[key].blurp);
				}
				*/
				//Initialize();

				
			}
		}
	}
}
