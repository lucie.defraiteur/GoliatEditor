﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Goliat;
using Transform = Goliat.Transform;
using Goliat.Serialization;
using Goliat.Engine.GUI;
namespace GoliatEditor
{
	/// <summary>
	/// Logique d'interaction pour Inspector.xaml
	/// </summary>
	public partial class Inspector : UserControl
	{
		public Inspector()
		{
			InitializeComponent();
			this.Loaded += Load;
		}
		private void Load(object sender, System.Windows.RoutedEventArgs e)
		{
			;
		}

		private void Property_ChangedValue(object sender, System.Windows.RoutedEventArgs e)
		{
			;
		}
		private Expander generateExpander(CustomProperty root)
		{

			Expander res = new Expander();
			res.Header = root.name;
			//Console.WriteLine("HEader:" + res.Header);
			//res.IsExpanded = false;
			//Console.WriteLine("header:" + res.Header);
			//res.Content = "yoyo";

			res.Tag = root;
			res.Expanded += Property_Expand;
			if (root.expanded)
			{
				Console.WriteLine("IM EXPANDED WTF??");
				//res.IsExpanded = true;
			}
			return (res);
		}
		private UIElement generateProperty(CustomProperty root)
		{
			if (root.value is IEditable)
			{
				InspectorGUI.InspectorCard card = InspectorGUI.GetCard(root.value.GetType());
				card.Populate((IEditable)root.value);
				Grid res = new Grid();
				Label name = new Label();
				name.Content = root.name;
				Grid.SetColumn(name, 0);
				res.Children.Add(name);
				UIElement value = card.root;
				Grid.SetColumn(value, 1);
				res.Children.Add(value);
				ColumnDefinition def = new ColumnDefinition();
				def.Width = new GridLength(80, GridUnitType.Pixel);

				ColumnDefinition def2 = new ColumnDefinition();
				def2.Width = new GridLength(0, GridUnitType.Auto);
				res.ColumnDefinitions.Add(def);
				res.ColumnDefinitions.Add(def2);

				
				//Grid grid = new Grid();
				Console.WriteLine("adding:" + root.value.GetType() + "to inspector with:" + card.root.GetType());
				return (res);
			}
			else if (root.value.isNumber() || root.value is bool || root.value is string)
			{
				Grid res = new Grid();
				Label name = new Label();
				name.Content = root.name;
				Grid.SetColumn(name, 0);
				res.Children.Add(name);
				TextBox value = new TextBox();
				value.Width = 45;
				value.Height = 20;
				value.Tag = root;
				value.Text = root.value.ToString();
				value.TextChanged += Property_ChangedValue;
				Grid.SetColumn(value, 1);
				res.Children.Add(value);
				ColumnDefinition def = new ColumnDefinition();
				def.Width = new GridLength(0, GridUnitType.Auto);

				ColumnDefinition def2 = new ColumnDefinition();
				def2.Width = new GridLength(0, GridUnitType.Auto);
				res.ColumnDefinitions.Add(def);
				res.ColumnDefinitions.Add(def2);
				return (res);
			}
			else
			{
				//Console.WriteLine("generating expander:");
				return (generateExpander(root));
			}
		}
		private void Property_Expand(Object sender, System.Windows.RoutedEventArgs e)
		{
			//((Expander)sender).IsExpanded = true;
			CustomProperty root = (CustomProperty)((Expander)sender).Tag;
			//root.expanded = true;
			if (root.sons == null || root.sons.Count == 0)
			{
				root.generateSons();
			}
			if (root.sons == null || root.sons.Count == 0)
			{
				Console.WriteLine("sons null???");
				return;
			}
			
			Expander current = ((Expander)sender);
			Console.WriteLine("expanding:" + (current.Header));
			Grid description = new Grid();

			current.Content = description;
			for (int i = 0; i < root.sons.Count; i++)
			{
				Console.WriteLine("son name:" + root.sons[i].name);
				UIElement son = generateProperty(root.sons[i]);
				
				Grid.SetRow(son, i);
				RowDefinition def = new RowDefinition();
				def.Height = new GridLength(1, GridUnitType.Auto);
				description.RowDefinitions.Add(def);
				description.Children.Add(son);
				;
			}
			e.Handled = true;
			//((Expander)sender).IsExpanded = true;
		

			/*if (root.value.IsValueType() && (root.value is string || root.value is bool || root.value.isNumber()))
			{
				;
			}
			else
			{
				;
			}*/
		}
		private Expander generateExpander(CustomProperty root, bool expanded)
		{
			Expander res = new Expander();
			res.Header = root.name;
			Console.WriteLine("header:" + res.Header);
			res.Tag = root;
			//res.IsExpanded = false;
			res.Expanded += Property_Expand;
			if (expanded || root.expanded)
			{
				res.IsExpanded = true;
			}
			return (res);
		}
		public void Update()
		{

			;
		}
		public void LoadTransformInfo(Transform tr)
		{
			if (tr == null)
			{
				Console.WriteLine("tr null");
			}
			List<Expander> expanders = new List<Expander>();
			List<GameBehaviour> components = tr.getAllComponents();
			Grid baseGrid = new Grid();
			this.InspectorContent.Content = baseGrid;
			for (int i = 0; i < components.Count; i++)
			{
				if (components[i] is IEditable)
				{
					InspectorGUI.InspectorCard card = new InspectorGUI.InspectorCard();
					card.Map((IEditable)components[i]);
					card.Populate((IEditable)components[i]);
					Grid res = new Grid();
					Label name = new Label();
					name.Content = "" + components[i].GetType().Name;
					Grid.SetRow(name, 0);
					res.Children.Add(name);
					UIElement value = card.root;
					Grid.SetRow(value, 1);
					res.Children.Add(value);
					RowDefinition def = new RowDefinition();
					//def.Width = new GridLength(120, GridUnitType.Pixel);
					def.Height = new GridLength(1, GridUnitType.Auto);
					res.RowDefinitions.Add(def);
					def = new RowDefinition();
					//def.Width = new GridLength(120, GridUnitType.Pixel);
					def.Height = new GridLength(1, GridUnitType.Auto);
					res.RowDefinitions.Add(def);

					Grid.SetRow(res, i);
					RowDefinition def3 = new RowDefinition();
					def3.Height = new GridLength(1, GridUnitType.Auto);
					baseGrid.RowDefinitions.Add(def3);
					baseGrid.Children.Add(res);
					
					//Grid grid = new Grid();
					Console.WriteLine("adding:" + components[i].GetType() + "to inspector with:" + card.root.GetType());
					//return (res);
				}
				else
				{
					CustomProperty root = new CustomProperty(null, components[i].GetType().Name, components[i]);

					Expander son = generateExpander(root, true);
					expanders.Add(son);
					Grid.SetRow(son, i);
					RowDefinition def = new RowDefinition();
					def.Height = new GridLength(1, GridUnitType.Star);
					baseGrid.Children.Add(son);
					baseGrid.RowDefinitions.Add(def);
				}
			}
		

		}
		private void LoadMappedRessourceInfo(SharedResource.MappedRessourceInfo obj)
		{
			;
		}
		private void LoadInfo(object obj)
		{
			if (obj.GetType() == typeof(Transform))
			{
				LoadTransformInfo((Transform)obj);
			}
			else if (obj.GetType() == typeof(SharedResource.MappedRessourceInfo))
			{
				LoadMappedRessourceInfo((SharedResource.MappedRessourceInfo)obj);
			}
		}
		
	}
}
