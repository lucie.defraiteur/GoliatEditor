﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms.Integration;
using System;
using System.Windows;
using System.Windows.Threading;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Size = System.Drawing.Size;

using Window = System.Windows.Window;
using Transform = Goliat.Transform;
using Vector3 = Goliat.Vector3;
using Quaternion = Goliat.Quaternion;
using Matrix = Goliat.Matrix;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;
using System.Data;
using Path = System.IO.Path;
using System.Runtime;
using System.Reflection;
using System.Dynamic;
using Goliat;
using Goliat.Serialization;
using Color = Goliat.Color;
using Goliat.Utils;
namespace GoliatEditor
{

	/// <summary>
	/// Logique d'interaction pour MainWindow.xaml
	/// </summary>
	/// 
	public static class CompositionTargetEx
	{
		private static TimeSpan _last = TimeSpan.Zero;
		private static event EventHandler<RenderingEventArgs> _FrameUpdating;
		public static event EventHandler<RenderingEventArgs> Rendering
		{
			add
			{
				if (_FrameUpdating == null)
					CompositionTarget.Rendering += CompositionTarget_Rendering;
				_FrameUpdating += value;
			}
			remove
			{
				_FrameUpdating -= value;
				if (_FrameUpdating == null)
					CompositionTarget.Rendering -= CompositionTarget_Rendering;
			}
		}
		static void CompositionTarget_Rendering(object sender, EventArgs e)
		{
			RenderingEventArgs args = (RenderingEventArgs)e;
			if (args.RenderingTime == _last)
				return;
			_last = args.RenderingTime; _FrameUpdating(sender, args);
		}
	}
	public partial class MainWindow : Window
	{

		private void Scene_Handle(Object sender, EventArgs args)
		{
			Microsoft.Win32.FileDialog fileView = new Microsoft.Win32.OpenFileDialog();



			// Set filter for file extension and default file extension 
		
			//////////////
			List<Transform> roots = Transform.roots;
			//FileDialog fileView = new OpenFileDialog();
			//bool saveCurrentScene = false;
			//bool emptyCurrentScene = false;
			//bool loadSelectedScene = false;
			Console.WriteLine("HEADER:-->" + ((MenuItem)sender).Header + "<--");
			if (!Directory.Exists("Assets"))
			{
				Directory.CreateDirectory("Assets");
			}
			//newSceneToolStripMenuItem
			if ((string)((MenuItem)sender).Header == "Save Scene")
			{
				//saveCurrentScene = true;
				fileView = new Microsoft.Win32.SaveFileDialog();
				;//Debug.Log("ok");
			}
			if ((string)((MenuItem)sender).Header == "Save Scene As")
			{
				//saveCurrentScene = true;
				fileView = new Microsoft.Win32.SaveFileDialog();
				Console.WriteLine("ITS A SAVE FILE DIALOG?");
				;
			}
			if ((string)((MenuItem)sender).Header == "Open Scene")
			{
				//emptyCurrentScene = true;
				//loadSelectedScene = true;
				;
			}
			if ((string)((MenuItem)sender).Header == "New Scene")
			{
				//emptyCurrentScene = true;
				//saveCurrentScene = true;
				fileView = new Microsoft.Win32.SaveFileDialog();
				;
			}
			fileView.Title = (string)((MenuItem)sender).Header;
			fileView.Filter = "Goliat Scene Files (*.scn)|*.scn|All Files (*.*)|*.*";
			fileView.RestoreDirectory = false;
			//fileView.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + "..\\..\\Assets"; //+ "../../Assets";
			fileView.InitialDirectory = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "Assets");// "*.*");
			Console.WriteLine("initial directory:" + fileView.InitialDirectory);

			//fileView.AutoUpgradeEnabled = true;
			//fileView.DefaultExt = ".png";
			//dlg.Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";


			// Display OpenFileDialog by calling ShowDialog method 
			Nullable<bool> result = fileView.ShowDialog();

			string filename = "";
			// Get the selected file name and display in a TextBox 
			if (result == true)
			{
				// Open document 
				filename = fileView.FileName;
				//textBox1.Text = filename;
			}


			//DialogResult result = fileView.ShowDialog();
		
			if (result == true)
			{
				string file = fileView.FileName;
				//if (saveCurrentScene)
				switch ((string)((MenuItem)sender).Header)
				{
					case "Save Scene":
						Serializer.Serialize(fileView.FileName, Transform.roots);
						break;
					case "Save Scene As":
						Serializer.Serialize(fileView.FileName, Transform.roots);
						break;
					case "Open Scene":
						List<Transform> newRoots = (List<Transform>)Serializer.UnSerialize(fileView.FileName);
						Transform.roots = newRoots;
						HierarchyTree_Populate();
						break;
					case "New Scene":
						Transform.roots = new List<Transform>();
						HierarchyTree_Populate();
						break;
				}

				//Debug.Log(file);
			}


			//Serializer.Serialize("")
		}

		[DllImport("kernel32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		static extern bool AllocConsole();

		[StructLayout(LayoutKind.Sequential)]
		public struct NativeMessage
		{
			public IntPtr Handle;
			public uint Message;
			public IntPtr WParameter;
			public IntPtr LParameter;
			public uint Time;
			public Point Location;
		}

		bool IsApplicationIdle()
		{
			NativeMessage result;
			return PeekMessage(out result, IntPtr.Zero, (uint)0, (uint)0, (uint)0) == 0;
		}

		[DllImport("user32.dll")]
		public static extern int PeekMessage(out NativeMessage message, IntPtr window, uint filterMin, uint filterMax, uint remove);

		private void Resize(object sender, System.Windows.SizeChangedEventArgs e)
		{
			Update();
		}
		public void FileList_Update()
		{
			//Console.WriteLine("FileList actual height:" + FileList.ActualHeight);
			//Console.WriteLine("parent height" + (((AvalonDock.ResizingPanel)FileList.Parent)).ActualHeight);

		}
		

		public TreeViewItem FromID(GameBehaviour itemId, TreeViewItem rootNode)
		{
			if (((GameBehaviour)(rootNode.Tag)).id == (itemId.id))
			{
				//Debug.Log("found node with id" + ((GameBehaviour)(rootNode.Tag)).id);
				return rootNode;
			}
			foreach (TreeViewItem node in rootNode.Items)
			{
				if (((GameBehaviour)(node.Tag)).id == (itemId.id))
				{
					//	Debug.Log("found node with id" + ((GameBehaviour)(node.Tag)).id);
					return node;
				}
				TreeViewItem next = FromID(itemId, node);
				if (next != null)
				{
					//	Debug.Log("returned next");
					return next;
				}
			}
			//Debug.Log("returned null");
			return null;
		}

		public TreeViewItem FromID(GameBehaviour itemId, TreeView tv)
		{
			TreeViewItem itemNode = null;
			foreach (TreeViewItem node in tv.Items)
			{
				itemNode = FromID(itemId, node);
				if (itemNode != null) break;
			}
			return (itemNode);
		}
		private bool activatedArrows;
		private Vector3 moveVect = Vector3.zero;
		public void PickMatrix(double x, double y, double deltax, double deltay, int[] viewport)
		{
			if (deltax <= 0 || deltay <= 0)
			{
				return;
			}
			GL.Translate((viewport[2] - 2 * ((x + deltax) - viewport[0])) / deltax,
				(viewport[3] - 2 * ((y) - viewport[1])) / deltay, 0);
			GL.Scale(viewport[2] / deltax, viewport[3] / deltay, 1.0);
		}

		public void RSTMODE_switch(object sender, System.Windows.RoutedEventArgs e)
		{

			;
		}

		private bool done = false;
		void GetSelection(int mouseX, int mouseY, GLControl control)
		{
			//Debug.Log("" + mouseX + "  " + mouseY);
			Console.WriteLine("hey00");
			float sw = 1;
			float sh = 1;
			mouseX = mouseX - 1;
			control.MakeCurrent();
			int[] selectBuffer = new int[1024];
			int hits;
			int[] viewport = new int[4];

			GL.SelectBuffer(1024, selectBuffer);
			GL.RenderMode(RenderingMode.Select);
			GL.Viewport(0, 0, control.Size.Width, control.Size.Height);
			GL.GetInteger(GetPName.Viewport, viewport);
			GL.InitNames();

			float aspectRatio = control.Size.Width / (float)control.Size.Height;
			Console.WriteLine("hey11");
			Matrix4 perspective = ((Transform)(control.Tag)).GetComponent<Camera>().perspectiveMatrix.gl();
			Console.WriteLine("hey22");
			GL.MatrixMode(MatrixMode.Projection);
			GL.PushMatrix();
			GL.LoadIdentity();
			PickMatrix(mouseX, viewport[3] - mouseY, sw, sh, viewport);
			GL.MultMatrix(ref perspective);
			Transform.RenderAll(null);
			//scn.OnRenderFrame();
			GL.Flush();
			control.SwapBuffers();
			
			float distance = -1;
			int selectedIndex = -1;
			hits = GL.RenderMode(RenderingMode.Render);
			if (hits != 0)
			{
				for (int n = 0; n < hits; n++)
				{
					uint newDist = (uint)selectBuffer[n * 4 + 1];
					Console.WriteLine("found:" + selectBuffer[n * 4 + 3]);
					if (distance >= newDist || distance == -1 || distance <= Math.E)
					{
						distance = newDist;
						selectedIndex = selectBuffer[n * 4 + 3];
					}
				}
			}
			if (distance != -1)
			{
				//Transform.Selection = new List<Transform>();
				/*if (((Transform)(GameBehaviour.GameBehaviours[selectedIndex])).root == Transform.arrowsTransform)
				{
					if (((Transform)(GameBehaviour.GameBehaviours[selectedIndex])).name == "redArrow")
					{
						moveVect = Vector3.right;
					}
					if (((Transform)(GameBehaviour.GameBehaviours[selectedIndex])).name == "greenArrow")
					{
						moveVect = Vector3.up;
					}
					if (((Transform)(GameBehaviour.GameBehaviours[selectedIndex])).name == "blueArrow")
					{
						moveVect = Vector3.forward;
					}
				}
				else
				{*/
					//Debug.Log(((Transform)(GameBehaviour.GameBehaviours[selectedIndex])).name + " found with id:" + ((Transform)(GameBehaviour.GameBehaviours[selectedIndex])).id);
				try
				{
					if (GameBehaviour.GameBehaviours[selectedIndex].GetType().IsSameOrSubclass(typeof(Transform)))
					{
						((Transform)(GameBehaviour.GameBehaviours[selectedIndex])).isSelected = true;
						TreeViewItem result = FromID(GameBehaviour.GameBehaviours[selectedIndex], HierarchyTree);
						//hierarchyTree.SelectedNode = result;
						result.IsSelected = true;
					}
				}
				catch (Exception e)
				{
					Console.WriteLine("the index would be:" + selectedIndex);
					//((Transform)(GameBehaviour.GameBehaviours[selectedIndex])).isSelected = true;
				}
					
					//hierarchyTree.Select();
					/*if (lastSelectedNode != null && lastSelectedNode == (Transform)hierarchyTree.SelectedNode.Tag)
					{
						Transform selected = (Transform)hierarchyTree.SelectedNode.Tag;
						lockCamToTransform(selected);
					}
					lastSelectedNode = (Transform)hierarchyTree.SelectedNode.Tag;*/
				//}
			}
			else
			{
				Transform.Selection = new List<Transform>();
			}

			//GL.PopName();
			GL.MatrixMode(MatrixMode.Projection);
			GL.PopMatrix();
			((GLControl)control).SwapBuffers();

			GL.Flush();
		}
		public GLControl getActiveWindow()
		{
			for (int i = 0; i < sceneViews.Count; i++)
			{
				if (isMouseOn(sceneViews[i]))
				{
					return ((GLControl)sceneViews[i].Controls[0]);
				}
			}
			return (null);
		}

		private int frame = 0;
		public void HandleInputs()
		{
			if (Input.GetMouseButton(Goliat.MouseButton.Middle))
			{
				Goliat.Vector2 diff = Input.Mouse.axis;
				//Console.WriteLine("axis:" + diff);
				//Console.WriteLine("position:" + Input.Mouse.position);
				float horizontal_speed = 5;
				float vertical_speed = 5;
				//Goliat.Vector2 pos = new Vector2(OpenTK.Input.Mouse.X, OpenTK.Input.Mouse.;
				GLControl current = getActiveWindow();
				if (current != null)
				{
					Transform cameraTransform = (Transform)current.Tag;
					Transform pivot_point = cameraTransform.GetComponent<Camera>().pivotPoint;
					cameraTransform.transform.RotateAround(pivot_point.transform.position, cameraTransform.right, -diff.y * vertical_speed * Time.deltaTime);
					cameraTransform.RotateAround(pivot_point.transform.position, Vector3.up, -diff.x * vertical_speed * Time.deltaTime);
				}
			}
			else if (Input.GetMouseButton(Goliat.MouseButton.Right))
			{
				Goliat.Vector2 diff = Input.Mouse.axis;

				float horizontal_speed = 5;
				float vertical_speed = 5;
				GLControl current = getActiveWindow();
				if (current != null)
				{
					Transform cameraTransform = (Transform)current.Tag;
					Transform pivot_point = cameraTransform.GetComponent<Camera>().pivotPoint;
					cameraTransform.RotateAround(cameraTransform.position, cameraTransform.right, -diff.y * vertical_speed * Time.deltaTime);
					cameraTransform.RotateAround(cameraTransform.position, Vector3.up, -diff.x * vertical_speed * Time.deltaTime);
					pivot_point.RotateAround(cameraTransform.position, cameraTransform.right, -diff.y * vertical_speed * Time.deltaTime);
					pivot_point.RotateAround(cameraTransform.position, Vector3.up, -diff.x * vertical_speed * Time.deltaTime);
				}
			}
			for (int i = 0; i < sceneViews.Count; i++)
			{
				/*if (sceneViews[i])
				{
					Console.WriteLine("got focus");
				}*/
				sceneViews[i].Controls[0].Invalidate();
				if (Input.GetMouseButtonDown(Goliat.MouseButton.Left))
				{
					if (getActiveWindow() != null)
					{
						System.Drawing.Point coordinates = getActiveWindow().PointToClient(System.Windows.Forms.Cursor.Position);
						GetSelection(coordinates.X, coordinates.Y, getActiveWindow());
					}
				}
				if (isMouseOn(sceneViews[i]))
				{
					Transform camtr = (Transform)sceneViews[i].Controls[0].Tag;
					Camera cam = camtr.GetComponent<Camera>();
					//Console.WriteLine("" + Time.deltaTime);
					camtr.position += (camtr.position - cam.pivotPoint.position).normalized * 15 * Time.deltaTime * Input.Mouse.WheelVariation;
					if (Input.GetKey(Goliat.Key.KeypadPlus))
					{
						camtr = (Transform)sceneViews[i].Controls[0].Tag;
						cam = camtr.GetComponent<Camera>();
						camtr.position += (camtr.position - cam.pivotPoint.position).normalized * 0.5f;
						Console.WriteLine("heyyyplus:" + camtr.position);
					}
					if (Input.GetKey(Goliat.Key.KeypadMinus))
					{
						camtr = (Transform)sceneViews[i].Controls[0].Tag;
						cam = camtr.GetComponent<Camera>();
						camtr.position -= (camtr.position - cam.pivotPoint.position).normalized * 0.5f;
						Console.WriteLine("heyyyminus:" + camtr.position);
					}
					//Console.WriteLine("got focus");
				}
				GLControl ctrl = (GLControl)sceneViews[i].Controls[0];
				GLControl_Paint(ctrl, null);
			}
			

		}
		public void UpdateRender(object sender, System.EventArgs e)
		{
				Update();
		}
		public void Update()
		{
			
				DependencyObject src = FileList;

				Input.Update();
				Time.update();
				if (Input.GetMouseButtonDown(Goliat.MouseButton.Left))
				{
				}
				if (!Input.GetMouseButton(Goliat.MouseButton.Left) && currentDragTarget != null)
				{
					currentDragTarget = null;
				}
				else if (Goliat.Input.GetMouseButton(Goliat.MouseButton.Left) && currentDragTarget != null)
				{
				}
				if (!SharedResource.mapped && SharedResource.isEnvironnementLoaded)
				{
					SharedResource.cleanMeta();
					SharedResource.MapRessources();
					SharedResource.mapped = true;
				}
				FileList_Update();
				HandleInputs();
				for (int i = 0; i < GameBehaviour.GameBehaviours.Count; i++)
				{
					GameBehaviour.GameBehaviours[i].Update();
				}
				//Physic.Update();
			/*if (cam != null)
			{
				// DRAW A ROTATION HANDLE.
				float lineWidth = 2f;
				DrawCircle(0.26f, 50, Vector3.zero, Quaternion.identity, Vector3.one * cam.transform.position.magnitude, Color.Blue, lineWidth);
				DrawCircle(0.26f, 50, Vector3.zero, new Quaternion(new Vector3(90, 0, 0)), Vector3.one * cam.transform.position.magnitude, Color.SeaGreen * 1.3f, lineWidth);
				DrawCircle(0.26f, 50, Vector3.zero, new Quaternion(new Vector3(0, 90, 0)), Vector3.one * cam.transform.position.magnitude, Color.Red, lineWidth);
				//DrawCircle(7, 50, Vector3.zero, Quaternion.identity, Vector3.one, Color.White);

				DrawCircle(0.26f, 50, Vector3.zero, cam.transform.rotation.inverse, Vector3.one * cam.transform.position.magnitude, Color.White, lineWidth);
				DrawCircle(0.30f, 50, Vector3.zero, cam.transform.rotation.inverse, Vector3.one * cam.transform.position.magnitude, Color.White, lineWidth);
				//DrawPlane(Vector2.one * 15, Vector3.zero, Quaternion.identity, Vector3.one, Color.Red);
			}*/
		}
		private void Inspector_Loaded(object sender, System.Windows.RoutedEventArgs e)
		{
			;
		}
		public bool isMouseOn(System.Windows.Forms.Control ctrl)
		{
			//Debug.Log("" +ctrl.PointToClient(Control.MousePosition));
			if (ctrl.ClientRectangle.Contains(ctrl.PointToClient(System.Windows.Forms.Control.MousePosition)))
				return (true);
			return (false);
		}
		public MainWindow()
		{
			System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
			customCulture.NumberFormat.NumberDecimalSeparator = ".";

			System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
			InitializeComponent();
			testWindow test = new testWindow();
			test.Show();
			Window1 win2 = new Window1();
			win2.Show();
			var options = new ToolkitOptions();
			options.Backend = PlatformBackend.PreferNative;
			Toolkit.Init(options);
			AllocConsole();
			CompositionTargetEx.Rendering += UpdateRender;
			//this.Dispatcher.BeginInvoke(new Action(() => Update()), DispatcherPriority.ApplicationIdle);
		}
		private void HierarchyTree_ItemChanged(object sender, System.Windows.RoutedPropertyChangedEventArgs<object> e)
		{
			// apres faudra foreach inspectors;
			;
			this.Inspector.LoadTransformInfo((Transform)(((TreeViewItem)((TreeView)sender).SelectedItem).Tag));
		}
		private void HierarchyTree_Loaded(object sender, System.Windows.RoutedEventArgs e)
		{
			((TreeView)HierarchyTree).SelectedItemChanged += HierarchyTree_ItemChanged;
			;
		}

		private void GetDirectories(DirectoryInfo[] subDirs,
			TreeViewItem nodeToAddTo)
		{
			TreeViewItem aNode;
			DirectoryInfo[] subSubDirs;
			foreach (DirectoryInfo subDir in subDirs)
			{
				
				StackPanel stack = new StackPanel();
				stack.Orientation = Orientation.Horizontal;
				BitmapImage image = (new BitmapImage(new Uri("pack://application:,,,/icons/folder.png", UriKind.Absolute)));
				Image img = new Image();
				img.Source = image;
				img.Width = 16;
				img.Height = 16;
				aNode = new TreeViewItem();
				aNode.Header = stack;
					//subDir.Name;
				aNode.Tag = subDir;
				Label lbl = new Label();
				lbl.Content = subDir.Name;
				stack.Children.Add(img);
				stack.Children.Add(lbl);
				//aNode = new TreeViewItem(subDir.Name, 0, 0);
				//aNode.Tag = subDir;
				//aNode.ImageKey = "folder";
				subSubDirs = subDir.GetDirectories();
				if (subSubDirs.Length != 0)
				{
					GetDirectories(subSubDirs, aNode);
				}
			
				nodeToAddTo.Items.Add(aNode);
			}
		}
		private void FolderTree_populate(object sender)
		{
			TreeViewItem rootNode;

			DirectoryInfo info = new DirectoryInfo(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Assets/"));
			if (info.Exists)
			{
				StackPanel stack = new StackPanel();
				stack.Orientation = Orientation.Horizontal;
				BitmapImage image = (new BitmapImage(new Uri("pack://application:,,,/icons/folder.png", UriKind.Absolute)));
				Image img = new Image();
				img.Source = image;
				img.Width = 16;
				img.Height = 16;
				rootNode = new TreeViewItem();
				rootNode.Header = stack;
				//subDir.Name;
				Label lbl = new Label();
				lbl.Content = "./";
				stack.Children.Add(img);
				stack.Children.Add(lbl);
				//aNode = new TreeViewItem(subDir.Name, 0, 0);
				//aNode.Tag = subDir;
				//aNode.ImageKey = "folder";
				rootNode.Tag = info;
				GetDirectories(info.GetDirectories(), rootNode);
				((TreeView)sender).Items.Add(rootNode);
			}
		}
		private void File_DragEnter(object sender, DragEventArgs e)
		{
			Console.WriteLine("DRagEnter");
		}

		private void File_Drop(object sender, DragEventArgs e)
		{
			Console.WriteLine("DropEnter");
		}
		private void File_DragOver(object sender, DragEventArgs e)
		{
			Console.WriteLine("DRagEnter");
		}
		private void FolderTree_Drop(object sender, DragEventArgs e)
		{
			Console.WriteLine("FolderTreeDrop");
		}
		private void HierarchyTree_Populate()
		{
			HierarchyTree.Items.Clear();
			for (int j = 0; j < Transform.roots.Count; j++)
			{
					Transform tr = Transform.roots[j];
					TreeViewItem sonitem = new TreeViewItem();
					sonitem.Header = tr.name;
					sonitem.Tag = tr;
					HierarchyTree.Items.Add(sonitem);
					addHierarchySonsRecursive(sonitem);
			}
		}
		private void addHierarchySonsRecursive(TreeViewItem item)
		{
			Transform tr = (Transform)item.Tag;
			if (tr.children == null)
			{
				Console.WriteLine("childs null");
				return;
			}
			for (int i = 0; i < tr.children.Count; i++)
			{
				TreeViewItem sonitem = new TreeViewItem();
				sonitem.Header = tr.children[i].name;
				sonitem.Tag = tr.children[i];
				item.Items.Add(sonitem);
				addHierarchySonsRecursive(sonitem);
			}
		}
		private void HierarchyTree_Drop(object sender, DragEventArgs e)
		{
			Console.WriteLine("HierarchyTreeDrop");
			object tag = e.Data.GetData(typeof(SharedResource.MappedRessourceInfo));
			Console.WriteLine("type2:" + e.Data.GetType());
			if (tag != null)
			{
				//if (tag.GetType() == typeof(SharedRessource.MappedRessourceInfo))
				//{
					Console.WriteLine("its an info?");
					SharedResource.MappedRessourceInfo info = (SharedResource.MappedRessourceInfo)tag;
					if (info.type == SharedResource.RessourceType.Scene)
					{
						Console.WriteLine("its a scene?");
						Scene scn = (Scene)info.Regenerate();
						Transform tr = scn.generateTransform();
						tr.AddToScene();
						TreeViewItem item = new TreeViewItem();
						item.Header = tr.name;
						item.Tag = tr;
						((TreeView)sender).Items.Add(item);
						addHierarchySonsRecursive(item);
						Console.WriteLine("hey!!!");
						Console.WriteLine("" + tr.localScale);
						Console.WriteLine("" + tr.children[0].localScale);
						
					}
				//}
			}
		}
		private bool FolderTree_IsLoaded = false;
		private void FolderTree_Loaded(object sender, System.Windows.RoutedEventArgs e)
		{
			if (FolderTree_IsLoaded)
				return;

			FolderTree_IsLoaded = true;
			FolderTree.DragEnter += File_DragEnter;
			FolderTree.Drop += FolderTree_Drop;
			HierarchyTree.Drop += HierarchyTree_Drop;
			FileList.DragEnter += File_DragEnter;
			FileList.Drop += File_Drop;
			FileList.DragOver += File_DragOver;
			((TreeView)sender).Resources[SystemColors.HighlightBrushKey] = Goliat.Color.CadetBlue.Window().Brush();
			FolderTree_populate(sender);
		}
		private object lastFile = null;
		private SharedResource.MappedRessourceInfo currentDragTarget = null;
		private void File_MouseDown(object sender, MouseEventArgs e)
		{
				if (lastFile != null)
				{
					DockPanel old = (DockPanel)lastFile;
					((Image)((Canvas)(old.Children[0])).Children[0]).Effect = null;
					((TextBlock)old.Children[1]).Background = null;
				}
				lastFile = sender;
				DockPanel dp = (DockPanel)sender;
				System.Windows.Media.Effects.DropShadowEffect effect = new System.Windows.Media.Effects.DropShadowEffect();
				System.Windows.Media.Color color = new System.Windows.Media.Color();
				effect.ShadowDepth = 0.01;
				effect.Color = Goliat.Color.CadetBlue.Window();
				((Image)((Canvas)(dp.Children[0])).Children[0]).Effect = effect;

				((TextBlock)dp.Children[1]).Background = Brushes.CadetBlue;
				currentDragTarget = (SharedResource.MappedRessourceInfo)((DockPanel)sender).Tag;
				DependencyObject src = dp.Parent;
				Object data = dp.Tag;
				DataObject data2 = new DataObject(dp.Tag);
				Console.WriteLine("type:" + dp.Tag.GetType());
				if (dp.Tag.GetType() == typeof(SharedResource.MappedRessourceInfo))
				{
					Console.WriteLine(((SharedResource.MappedRessourceInfo)dp.Tag).filePath);
				}
				//DragDropEffects effects = new DragDropEffects();//DragDropEffects.Move;
				System.Windows.DragDropEffects dragEffect = DragDropEffects.Move;
				//collection.directoryInfos.Add(((DirectoryInfo)(((TreeNode)(e.Item)).Tag)));
				DragDrop.DoDragDrop(src, data2, dragEffect);
				//Console.WriteLine("target:" + currentDragTarget);
		}

		
		private void Asset_Click(object sender, RoutedEventArgs e)
		{
			SharedResource.MappedRessourceInfo info = (SharedResource.MappedRessourceInfo)((Button)sender).Tag;
			Console.WriteLine("filepath:" + info.filePath);
			if (info.expanded)
			{
				info.expanded = false;
			}
			else
			{
				info.expanded = true;
			}
			FileList_Populate((DirectoryInfo)FileList.Tag);
		}
		private DockPanel generateInfoPanel(SharedResource.MappedRessourceInfo info)
		{
			DockPanel item = new DockPanel();
			Canvas canvas = new Canvas();
			
			item.Margin = new System.Windows.Thickness(6, 12, 6, 0);
			item.MouseDown += File_MouseDown;
			
			//string metaFile = Path.ChangeExtension(file.FullName, ".meta");
			TextBlock lbl = new TextBlock();
			lbl.TextWrapping = TextWrapping.NoWrap;
			lbl.Text = "";
			lbl.MaxWidth = 50;

			//BitmapImage image = (new BitmapImage(new Uri("pack://application:,,,/icons/folder.png", UriKind.Absolute)));
			Image img = new Image();
			//img.Source = image;

			img.Width = 50;
			img.Height = 50;
			canvas.Height = 50;
			canvas.Width = 50;
			item.Children.Add(canvas);
			DockPanel.SetDock(canvas, Dock.Top);
			item.Children.Add(lbl);
			DockPanel.SetDock(lbl, Dock.Bottom);
			//subDir.Name;
			//item.Tag = file;
			//aNode = new TreeViewItem(subDir.Name, 0, 0);
			//aNode.Tag = subDir;
			//aNode.ImageKey = "folder";
			//item.Tag = file;
			//FileList.Children.Add(item);
				info = SharedResource.MappedRessourceInfo.RessourceMap[info.guid];
				System.Drawing.Bitmap icon = info.icon;

				ImageSourceConverter c = new ImageSourceConverter();
				if (icon != null)
				{

					BitmapImage src = Convert(icon);
					ImageBrush brush = new ImageBrush();
					brush.ImageSource = src;
					Rectangle blueRectangle = new Rectangle();
					blueRectangle.Height = 50;
					blueRectangle.Width = 50;
					Button expand = new Button();
					expand.Click += Asset_Click;
					expand.Tag = info;
					expand.Width = 15;
					expand.Height = 15;
					BitmapImage imgbmp = new BitmapImage(new Uri("pack://application:,,,/icons/expand2.png", UriKind.Absolute));
					expand.Background = new ImageBrush(imgbmp);
					Canvas.SetLeft(expand, 45);
					Canvas.SetTop(expand, 15);
					Canvas.SetZIndex(expand, 1);
					Canvas.SetZIndex(img, 0);
					canvas.Children.Add(img);

					img.Source = src;
					img.Width = 50;
					img.Height = 50;

					img.Stretch = Stretch.Uniform;

					//canvas.Children.Add(img);
				}
			return (item);
		}
		private void appendInfoChildrenRecursively(SharedResource.MappedRessourceInfo info, WrapPanel panel)
		{
			if (info.meshs != null)
			{
				for (int i = 0; i < info.meshs.Count; i++)
				{
					panel.Children.Add(generateInfoPanel(info.meshs[i]));
				}
			}
			if (info.textures != null)
			{
				for (int i = 0; i < info.textures.Count; i++)
				{
					panel.Children.Add(generateInfoPanel(info.textures[i]));
				}
			}
			if (info.sons != null)
			{
				for (int i = 0; i < info.sons.Count; i++)
				{
					panel.Children.Add(generateInfoPanel(info.sons[i]));
					appendInfoChildrenRecursively(info.sons[i], panel);
				}
			}
		}
		private void FileList_Populate(DirectoryInfo nodeDirInfo)
		{
			
			FileList.Children.Clear();
			FileList.Tag = nodeDirInfo;
			DockPanel item = null;

			foreach (DirectoryInfo dir in nodeDirInfo.GetDirectories())
			{
				item = new DockPanel();
				item.MouseDown += File_MouseDown;
				//Label lbl = new Label();
				TextBlock lbl = new TextBlock();
				lbl.TextWrapping = TextWrapping.WrapWithOverflow;
				lbl.Text = dir.Name;
				lbl.MaxWidth = 50;

				BitmapImage image = (new BitmapImage(new Uri("pack://application:,,,/icons/folder.png", UriKind.Absolute)));
				Image img = new Image();
				img.Source = image;
				img.Width = 50;
				img.Height = 50;
				item.Children.Add(img);
				DockPanel.SetDock(img, Dock.Top);
				item.Children.Add(lbl);
				DockPanel.SetDock(lbl, Dock.Bottom);
				//subDir.Name;
				item.Tag = dir;
				//aNode = new TreeViewItem(subDir.Name, 0, 0);
				//aNode.Tag = subDir;
				//aNode.ImageKey = "folder";
				item.Tag = dir;
				FileList.Children.Add(item);

			}

			foreach (FileInfo file in nodeDirInfo.GetFiles())
			{

				//item = new ListViewItem(file.Name, 1);
				if (Path.GetExtension(file.Name).IsMeshExtension() || Path.GetExtension(file.Name) == ".cs" || Path.GetExtension(file.Name).IsTextureExtension())
				{
					Canvas canvas = new Canvas();
					
					item = new DockPanel();
					item.DragEnter += File_DragEnter;
					item.Margin = new System.Windows.Thickness(6, 12, 6, 0);
					item.MouseDown += File_MouseDown;
					string metaFile = file.FullName + ".meta";
					TextBlock lbl = new TextBlock();
					lbl.TextWrapping = TextWrapping.NoWrap;
					lbl.Text = file.Name;
					lbl.MaxWidth = 50;

					//BitmapImage image = (new BitmapImage(new Uri("pack://application:,,,/icons/folder.png", UriKind.Absolute)));
					Image img = new Image();
					//img.Source = image;

					img.Width = 50;
					img.Height = 50;
					canvas.Height = 50;
					canvas.Width = 50;
					item.Children.Add(canvas);
					DockPanel.SetDock(canvas, Dock.Top);
					item.Children.Add(lbl);
					DockPanel.SetDock(lbl, Dock.Bottom);
					//subDir.Name;
					//aNode = new TreeViewItem(subDir.Name, 0, 0);
					//aNode.Tag = subDir;
					//aNode.ImageKey = "folder";
					//item.Tag = file.FullName;
					item.Tag = null;
					FileList.Children.Add(item);
					if (File.Exists(metaFile) && !(Path.GetExtension(file.Name) == ".cs"))
					{

						SharedResource.MappedRessourceInfo info = (SharedResource.MappedRessourceInfo)Serializer.UnSerialize(metaFile);
						info = SharedResource.MappedRessourceInfo.RessourceMap[info.guid];
						System.Drawing.Bitmap icon = info.icon;
		
						ImageSourceConverter c = new ImageSourceConverter();
						if (icon != null)
						{

							BitmapImage src = Convert(icon);
							ImageBrush brush = new ImageBrush();
							brush.ImageSource = src;
							Rectangle blueRectangle = new Rectangle();
							blueRectangle.Height = 50;
							blueRectangle.Width = 50;
							Button expand = new Button();
							expand.Click += Asset_Click;
							expand.Tag = info;
							expand.Width = 15;
							expand.Height = 15;
							BitmapImage imgbmp = new BitmapImage(new Uri("pack://application:,,,/icons/expand2.png", UriKind.Absolute));
							expand.Background = new ImageBrush(imgbmp);
							if (info.expanded)
							{
								RotateTransform rotateTransform = new RotateTransform();
								rotateTransform.Angle = 90;
								var transformBitmap = new TransformedBitmap(imgbmp, rotateTransform);
								expand.Background = new ImageBrush(transformBitmap);
								appendInfoChildrenRecursively(info, FileList);
							}
							Canvas.SetLeft(expand, 45);
							Canvas.SetTop(expand, 15);
							Canvas.SetZIndex(expand, 1);
							Canvas.SetZIndex(img, 0);
							canvas.Children.Add(img);
							if (Path.GetExtension(file.Name).IsMeshExtension())
								canvas.Children.Add(expand);
							item.Tag = info;
							img.Source = src;
							img.Width = 50;
							img.Height = 50;

							img.Stretch = Stretch.Uniform;
							
							//canvas.Children.Add(img);
						}
					}
				}
			}
			Console.WriteLine("new height:" + FileList.ActualHeight);
			Console.WriteLine("new width:" + FileList.Width);
		}
		private void FolderTree_SelectedItemChanged(object sender, System.Windows.RoutedPropertyChangedEventArgs<object> e)
		{
			TreeViewItem newSelected = (TreeViewItem)((TreeView)sender).SelectedItem;
			//TreeNode newSelected = e.Node;
			

			DirectoryInfo nodeDirInfo = (DirectoryInfo)newSelected.Tag;
			FileList_Populate(nodeDirInfo);
			/*DoDragDrop(((DirectoryInfo)newSelected.Tag).Name, DragDropEffects.Move);
			fileList.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
			fileList.View = View.LargeIcon;*/
		}

		public BitmapImage Convert(System.Drawing.Bitmap value)
		{
			MemoryStream ms = new MemoryStream();
			((System.Drawing.Bitmap)value).Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
			BitmapImage image = new BitmapImage();
			image.BeginInit();
			ms.Seek(0, SeekOrigin.Begin);
			image.StreamSource = ms;
			image.EndInit();

			return image;
		}
		private void FileList_Loaded(object sender, System.Windows.RoutedEventArgs e)
		{
			;
		}
		private void GLControl_Load(object sender, EventArgs e)
		{
			loaded = true;
			SharedResource.isEnvironnementLoaded = true;
			//AddGLControl();
		}

		protected override void OnGotFocus(System.Windows.RoutedEventArgs e)
		{
			Console.WriteLine("oui");
			base.OnGotFocus(e);
		}
		bool test2 = false;
		bool once = false;
		private void GLControl_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			if (!loaded)
				return;
			GLControl ctrl = (GLControl)sender;
			ctrl.MakeCurrent();
			GL.Flush();
			if (!once)
			{
				Transform.TestRenderAllToBuffer();
				once = true;
			}
			Camera.current = ((Transform)ctrl.Tag).GetComponent<Camera>();
			//GL.MatrixMode(MatrixMode.Projection);
			//Console.WriteLine("here the viewport:" + ctrl.Size.Width + "\n" + ctrl.Size.Height);
			GL.Viewport(0, 0, ctrl.Size.Width, ctrl.Size.Height);
			GL.MatrixMode(MatrixMode.Modelview);
			GL.LoadIdentity();
			Matrix4 lookat2;
			Transform camTransform = (Transform)ctrl.Tag;
			//Matrix4 perspective = ((Transform)ctrl.Tag).getComponent<Camera>().perspectiveMatrix.gl();
			Camera cam = ((Transform)ctrl.Tag).GetComponent<Camera>();
			cam.width = ctrl.Size.Width;
			cam.height = ctrl.Size.Height;
			//Matrix4 perspective = Matrix4.CreatePerspectiveFieldOfView(cam.fov, cam.aspectRatio, cam.nearClipPlane, cam.farClipPlane);
			Matrix4 perspective = cam.perspectiveMatrix.gl();
			//Console.WriteLine("perspective?:" + perspective.ToString());
			lookat2 = camTransform.worldToLocalMatrix.gl();
			lookat2.Transpose();
			GL.LoadMatrix(ref lookat2);
			//Camera cam = scn.cameraTransform.getComponent<Camera>();// cam.aspectRatio = 

			//Console.WriteLine("octogon:" + perspective);

			GL.MatrixMode(MatrixMode.Projection);
			GL.LoadMatrix(ref perspective);
			if (sender == (object)getActiveWindow())
			{
				System.Drawing.Point coordinates = getActiveWindow().PointToClient(System.Windows.Forms.Cursor.Position);
				Goliat.Vector2 mousePos = new Goliat.Vector2(coordinates.X - 1, getActiveWindow().Height - coordinates.Y);
				//Console.WriteLine("MOUSE POS:" + mousePos);
				List<int> selection = Transform.GetSelection(new Goliat.Rect(mousePos.x, mousePos.y, mousePos.x + 1, mousePos.y + 1), cam);
				for (int i = 0; i < selection.Count; i++)
				{
					//-16776961
					
					if (Input.GetMouseButtonDown(Goliat.MouseButton.Left))
					{
						Console.WriteLine("SELECTED:" + selection[i]);
						GameBehaviour.GameBehaviours[selection[i]].transform.isSelected = true;
						;
					}
				}
				/*Console.WriteLine("MUST BE RED:" + Color.Red.rgba);
				Console.WriteLine("Red Hex: {0:X}", Color.Red.rgba);
				Console.WriteLine("MUST BE RED TOO:" + Color.FromRgba(Color.Red.rgba));
				Console.WriteLine("MUST BE BLUE:" + Color.Blue.rgba);
				Console.WriteLine("MUST BE BLUE TOO:" + Color.FromRgba(65535));*/
			
				//Color[,] block = Transform.GetSelectionBlock(new Goliat.Rect(0, 0, 1, 1), cam);

				//Transform.GetSelectionBuffer(cam);
				//Transform.TestFrameBuffer(cam);
			}
			else
			{
				Transform.RenderAll(cam);
			}
		//	Transform.RenderAll(cam);
			ctrl.SwapBuffers();
			//GL.Flush();
		}
		private void GL_DragEnter(object sender, System.Windows.Forms.ItemDragEventArgs e)
		{
			;
		}
		private bool loaded = false;
		private void AddGLControl(System.Windows.Forms.Panel host)
		{
			GLControl glcontrol = new GLControl();
			glcontrol.AllowDrop = true;
			//glcontrol.Dock = DockStyle.Fill;
			glcontrol.Load += GLControl_Load;
			Camera cam = new Camera((int)host.Width, (int)host.Height, MathHelper.PiOver4.toDeg(), 1, 1000);
			cam.pivotPoint = Transform.zero;
			Transform tr = new Transform(new Vector3(6, 0, 0), Quaternion.identity, Vector3.one);
			tr.LookAt(Vector3.zero, Vector3.up);
			cam.AddComponent(tr);
			glcontrol.Tag = tr;
			glcontrol.Dock = System.Windows.Forms.DockStyle.Fill;
			host.Controls.Add(glcontrol);
			glcontrol.Width = host.Width;
			glcontrol.Height = host.Height;
			glcontrol.Location = new System.Drawing.Point(0, 0);


			//Transform pivotPoint = Transform.zero;
			
		}
		private void test(object sender, QueryContinueDragEventArgs e)
		{
			Console.WriteLine("plz??");
			;
		}
		private void GL_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
		{
			Console.WriteLine("its ok?");
			;
		}
		private void GL_GotFocus(object sender, System.Windows.RoutedEventArgs e)
		{
			Console.WriteLine("got focus");
		}
		List<System.Windows.Forms.Panel> sceneViews = new List<System.Windows.Forms.Panel>();

		private void SceneView_Loaded(object sender, System.Windows.RoutedEventArgs e)
		{
			if (((WindowsFormsHost)sender).Child != null)
				return;
			((WindowsFormsHost)sender).MouseMove += GL_MouseMove;
			((WindowsFormsHost)sender).DragEnter += File_DragEnter;
			((WindowsFormsHost)sender).GotFocus += GL_GotFocus;
			
			System.Windows.Forms.Panel panel = new System.Windows.Forms.Panel();
			panel.Tag = true;
			panel.Dock = System.Windows.Forms.DockStyle.Fill;
			((WindowsFormsHost)sender).Child = panel;
			AddGLControl(panel);
			sceneViews.Add(panel);
		}

		private void DocumentPane_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			;
		}
	}
}
