﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using Goliat;
namespace Goliat.Engine.GUI
{

}

namespace Goliat.Controls
{
	/// <summary>
	/// Logique d'interaction pour AssetPicker.xaml
	/// </summary>

	public class ItemSelectedEventArgs : EventArgs
	{
		public object Selection = null;
		public Type type = null;
		public ItemSelectedEventArgs(Type type, object Selection)
		{
			this.type = type;
			this.Selection = Selection;
		}
	}
	public delegate void ItemSelectedEvent(object sender, ItemSelectedEventArgs e);
	public partial class AssetPicker : Window
	{
		public Type type = null;
		public object Selection = null;
		public event ItemSelectedEvent ItemSelected;

		public AssetPicker()
		{
			InitializeComponent();
		}
		public AssetPicker(Type type)
		{
			InitializeComponent();
			this.type = type;

		}
		private void Asset_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			ItemSelectedEventArgs args = new ItemSelectedEventArgs(this.type, ((DockPanel)sender).Tag);
			Selection = ((DockPanel)sender).Tag;
			if (ItemSelected != null)
				ItemSelected(this, args);
		}
		public void Populate()
		{
			if (this.type.IsSameOrSubclass(typeof(Goliat.GameBehaviour)))
			{
				foreach (Goliat.GameBehaviour behaviour in Goliat.GameBehaviour.GameBehaviours)
				{
					if (behaviour.GetType().IsSameOrSubclass(this.type))
					{
						DockPanel item = new DockPanel();
						this.SceneAssetsList.Children.Add(null);
						Canvas canvas = new Canvas();

						item = new DockPanel();
						//item.DragEnter += File_DragEnter;
						item.Margin = new System.Windows.Thickness(6, 12, 6, 0);
						//item.MouseDown += File_MouseDown;
						//string metaFile = Path.ChangeExtension(file.FullName, ".meta");
						TextBlock lbl = new TextBlock();
						lbl.TextWrapping = TextWrapping.NoWrap;
						lbl.Text = behaviour.id.ToString();
						lbl.MaxWidth = 50;

						//BitmapImage image = (new BitmapImage(new Uri("pack://application:,,,/icons/folder.png", UriKind.Absolute)));
						Image img = new Image();
						//img.Source = image;

						img.Width = 50;
						img.Height = 50;
						canvas.Height = 50;
						canvas.Width = 50;
						item.Children.Add(canvas);
						DockPanel.SetDock(canvas, Dock.Top);
						item.Children.Add(lbl);
						DockPanel.SetDock(lbl, Dock.Bottom);
						item.Tag = behaviour;
						item.MouseDown += Asset_Click;
						//subDir.Name;
						//aNode = new TreeViewItem(subDir.Name, 0, 0);
						//aNode.Tag = subDir;
						//aNode.ImageKey = "folder";
						//item.Tag = file.FullName;
						
						this.SceneAssetsList.Children.Add(item);
					}
				}
				;
			}
			else if (this.type.IsSameOrSubclass(typeof(Goliat.SharedResource)))
			{
				Console.WriteLine("HEYYY???");
				foreach (Goliat.SharedResource resource in Goliat.SharedResource.RessourceList)
				{

					if (resource.GetType().IsSameOrSubclass(this.type))
					{
						if (!Goliat.SharedResource.MappedRessourceInfo.RessourceMap.ContainsKey(resource.guid))
						{
							continue;
						}
						Canvas canvas = new Canvas();

						DockPanel item = new DockPanel();
						//item.DragEnter += File_DragEnter;
						item.Margin = new System.Windows.Thickness(6, 12, 6, 0);
						//item.MouseDown += File_MouseDown;
						TextBlock lbl = new TextBlock();
						lbl.TextWrapping = TextWrapping.NoWrap;

						Goliat.SharedResource.MappedRessourceInfo info = Goliat.SharedResource.MappedRessourceInfo.RessourceMap[resource.guid];
						lbl.Text = info.name;
						lbl.MaxWidth = 50;

						//BitmapImage image = (new BitmapImage(new Uri("pack://application:,,,/icons/folder.png", UriKind.Absolute)));
						Image img = new Image();
						//img.Source = image;

						img.Width = 50;
						img.Height = 50;
						canvas.Height = 50;
						canvas.Width = 50;
						item.Children.Add(canvas);
						DockPanel.SetDock(canvas, Dock.Top);
						item.Children.Add(lbl);
						DockPanel.SetDock(lbl, Dock.Bottom);
						item.Tag = resource;
						item.MouseDown += Asset_Click;
						//subDir.Name;
						//aNode = new TreeViewItem(subDir.Name, 0, 0);
						//aNode.Tag = subDir;
						//aNode.ImageKey = "folder";
						//item.Tag = file.FullName;
						item.Tag = null;
						System.Drawing.Bitmap icon = info.icon;

						ImageSourceConverter c = new ImageSourceConverter();
						if (icon != null)
						{

							BitmapImage src = Convert(icon);
							ImageBrush brush = new ImageBrush();
							brush.ImageSource = src;
							Rectangle blueRectangle = new Rectangle();
							blueRectangle.Height = 50;
							blueRectangle.Width = 50;
							Canvas.SetZIndex(img, 0);
							canvas.Children.Add(img);
							item.Tag = info;
							img.Source = src;
							img.Width = 50;
							img.Height = 50;

							img.Stretch = Stretch.Uniform;

							//canvas.Children.Add(img);
						}
						this.SceneAssetsList.Children.Add(item);
						;
					}
				}
				Goliat.SharedResource.RessourceType restype = Goliat.SharedResource.RessourceType.None;
				if (this.type.IsSameOrSubclass(typeof(Goliat.Mesh)))
				{
					restype = Goliat.SharedResource.RessourceType.Mesh;
					;
				}
				if (this.type.IsSameOrSubclass(typeof(Goliat.Scene)))
				{
					restype = Goliat.SharedResource.RessourceType.Scene;
					;
				}
				if (this.type.IsSameOrSubclass(typeof(Goliat.UserScript)))
				{
					restype = Goliat.SharedResource.RessourceType.Script;
					;
				}
				if (this.type.IsSameOrSubclass(typeof(Goliat.AudioClip)))
				{
					restype = Goliat.SharedResource.RessourceType.Sound;
					;
				}
				if (this.type.IsSameOrSubclass(typeof(Goliat.Texture)))
				{
					restype = Goliat.SharedResource.RessourceType.Texture;
					;
				}
				if (this.type.IsSameOrSubclass(typeof(Goliat.Material)))
				{
					restype = Goliat.SharedResource.RessourceType.Material;
					;
				}
				Console.WriteLine("RESTYPE:" + restype.ToString());
				Console.WriteLine("source type:" + this.type);
				foreach (Goliat.SharedResource.MappedRessourceInfo info in Goliat.SharedResource.MappedRessourceInfo.RessourceMap.Values)
				{

					if (info.type == restype)
					{

						Canvas canvas = new Canvas();

						DockPanel item = new DockPanel();
						//item.DragEnter += File_DragEnter;
						item.Margin = new System.Windows.Thickness(6, 12, 6, 0);
						//item.MouseDown += File_MouseDown;
						TextBlock lbl = new TextBlock();
						lbl.TextWrapping = TextWrapping.NoWrap;
						lbl.Text = info.name;
						lbl.MaxWidth = 50;

						//BitmapImage image = (new BitmapImage(new Uri("pack://application:,,,/icons/folder.png", UriKind.Absolute)));
						Image img = new Image();
						//img.Source = image;

						img.Width = 50;
						img.Height = 50;
						canvas.Height = 50;
						canvas.Width = 50;
						item.Children.Add(canvas);
						DockPanel.SetDock(canvas, Dock.Top);
						item.Children.Add(lbl);
						DockPanel.SetDock(lbl, Dock.Bottom);
						item.Tag = info;
						item.MouseDown += Asset_Click;
						//subDir.Name;
						//aNode = new TreeViewItem(subDir.Name, 0, 0);
						//aNode.Tag = subDir;
						//aNode.ImageKey = "folder";
						//item.Tag = file.FullName;
						item.Tag = null;
						System.Drawing.Bitmap icon = info.icon;

						ImageSourceConverter c = new ImageSourceConverter();
						if (icon != null)
						{

							BitmapImage src = Convert(icon);
							ImageBrush brush = new ImageBrush();
							brush.ImageSource = src;
							Rectangle blueRectangle = new Rectangle();
							blueRectangle.Height = 50;
							blueRectangle.Width = 50;
							Canvas.SetZIndex(img, 0);
							canvas.Children.Add(img);
							item.Tag = info;
							img.Source = src;
							img.Width = 50;
							img.Height = 50;

							img.Stretch = Stretch.Uniform;

							//canvas.Children.Add(img);
						}
						this.FolderAssetsList.Children.Add(item);
					}
				}
			}
		}

		public BitmapImage Convert(System.Drawing.Bitmap value)
		{
			MemoryStream ms = new MemoryStream();
			((System.Drawing.Bitmap)value).Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
			BitmapImage image = new BitmapImage();
			image.BeginInit();
			ms.Seek(0, SeekOrigin.Begin);
			image.StreamSource = ms;
			image.EndInit();

			return image;
		}
		/*protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
		{
			
			if (Selection == null)
			{
				// Prevent the user closing the window without pressing one of the buttons.
				e.Cancel = true;
			}
			else
			{

				;
			}
			base.OnClosing(e);
		}*/
	}
}
