﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Goliat.Controls
{
	/// <summary>
	/// Logique d'interaction pour PickBox.xaml
	/// </summary>
	/// 

	public enum ItemType
	{
		Behaviour,
		Resource
	}
	public partial class PickBox : UserControl
	{
		public event ItemSelectedEvent ItemSelected;
		private object _Asset = null;
		public object Asset
		{
			get
			{
				return (_Asset);
			}
			set
			{
				this._Asset = value;
				if (value != null)
					Console.WriteLine("" + ((SharedResource)value).name);
				if (value != null)
					this.TextBox.Text = (this.type.IsSameOrSubclass(typeof(SharedResource)) ? ((SharedResource)value).name : "" + this.type.Name + ((GameBehaviour)value).id);
				ItemSelected(this, new ItemSelectedEventArgs(this.type, this._Asset));
			}
		}
		private Type type;
		public PickBox()
		{
			InitializeComponent();
		}
		public PickBox(Type type)
		{
			this.type = type;
			InitializeComponent();
			;
		}
		private void Select_Item(object sender, ItemSelectedEventArgs e)
		{
			if (e.Selection.GetType() == typeof(SharedResource.MappedRessourceInfo))
			{
				this.Asset = ((SharedResource.MappedRessourceInfo)e.Selection).Regenerate();
			}
			else
				this.Asset = e.Selection;
			;
		}
		private void Close_Picker(object sender, EventArgs e)
		{
			Picker.IsChecked = false;
			;
		}
		private void Pick_Item(object sender, System.Windows.RoutedEventArgs e)
		{
			AssetPicker picker = new AssetPicker();
			picker.type = this.type;
			picker.ItemSelected += Select_Item;
			picker.Closed += Close_Picker;
			picker.Populate();
			picker.Show();

			// select item
			// (BehaviourField) this.Tag.value = le nouvel item
			// 
			;
		}
		private void Expand(object sender, System.Windows.RoutedEventArgs e)
		{
			;
		}
	}
}
