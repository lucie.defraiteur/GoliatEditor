﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using GL = OpenTK.Graphics.OpenGL.GL;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.Reflection;

namespace Goliat
{
	public class FrameBufferObject
	{
		public int width;
		public int height;
		public int id;
		public int gl_tex_id;
		public FrameBufferObject(int width, int height)
		{
			// Save dimensions
			this.width = width;
			this.height = height;
			// Generate a framebuffer ID
			CreateNullTexture(out this.gl_tex_id);
			CreateFBOandAssignTexture(out this.id, this.gl_tex_id);
			/*	this.id = GL.Ext.GenFramebuffer();
				// The texture we're going to render to
				this.gl_tex_id = GL.GenTexture();
				Renderer.Call(() => GL.BindTexture(TextureTarget.Texture2D, this.gl_tex_id));
				Renderer.Call(() => GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, width, height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, IntPtr.Zero));
				Renderer.Call(() => GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest));
				Renderer.Call(() => GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest));

				Renderer.Call(() => GL.BindTexture(TextureTarget.Texture2D, 0));

				Renderer.Call(() => GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, this.id));
				Renderer.Call(() => GL.Ext.FramebufferTexture2D(FramebufferTarget.FramebufferExt, FramebufferAttachment.ColorAttachment0Ext, TextureTarget.Texture2D, this.gl_tex_id, 0));

				//
				Renderer.Call(() => GL.DrawBuffer((DrawBufferMode)FramebufferAttachment.ColorAttachment0Ext));
				Renderer.Call(() => GL.ClearColor(Color.White.gl()));
				Renderer.Call(() => GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit));
				Renderer.Call(() => GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, 0));
				Renderer.Call(() => GL.DrawBuffer(DrawBufferMode.Back));
				//Renderer.Call(() => GL.DrawBuffers(FrameBufferAttachment.ColorAttachment0));
				//if (Renderer.Call(() => GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer) != FramebufferErrorCode.FramebufferComplete));
					//Debug.Log("Framebuffer incomplete: " + Renderer.Call(() => GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer)));
				//Renderer.Call(() => GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0));
				this.clear();*/
		}
		private Texture _texture;
		public Texture texture
		{
			get
			{
				if (this._texture == null)
				{
					this._texture = new Texture(this.gl_tex_id);
				}
				return (this._texture);
			}
			private set
			{
				this._texture = value;
			}
		}
		public int DepthRenderbuffer = -1;
		private void CreateFBOandAssignTexture(out int fbo, int texture)
		{
			//in
			GL.Ext.GenRenderbuffers(1, out DepthRenderbuffer);
			GL.Ext.BindRenderbuffer(RenderbufferTarget.RenderbufferExt, DepthRenderbuffer);
			GL.Ext.RenderbufferStorage(RenderbufferTarget.RenderbufferExt, (RenderbufferStorage)All.DepthComponent32, this.width, this.height);
			// create and bind an FBO
			GL.Ext.GenFramebuffers(1, out fbo);
			GL.Ext.BindFramebuffer(FramebufferTarget.DrawFramebuffer, fbo);

			// assign texture to FBO
			GL.Ext.FramebufferTexture2D(FramebufferTarget.DrawFramebuffer, FramebufferAttachment.ColorAttachment0Ext, TextureTarget.Texture2D, texture, 0);
			GL.Ext.FramebufferRenderbuffer(FramebufferTarget.FramebufferExt, FramebufferAttachment.DepthAttachmentExt, RenderbufferTarget.RenderbufferExt, DepthRenderbuffer);
			#region Test for Error

			string version = string.Empty;
			try
			{
				//GLControl control = new GLControl();
				version = GL.GetString(StringName.Version);
			}
			catch (Exception ex)
			{
			}

			switch (GL.Ext.CheckFramebufferStatus(FramebufferTarget.FramebufferExt))
			{
				case FramebufferErrorCode.FramebufferCompleteExt:
					{
						//Console.WriteLine("FBO: The framebuffer " + fbo + " is complete and valid for rendering.");
						break;
					}
				case FramebufferErrorCode.FramebufferIncompleteAttachmentExt:
					{
						Console.WriteLine("FBO: One or more attachment points are not framebuffer attachment complete. This could mean there’s no texture attached or the format isn’t renderable. For color textures this means the base format must be RGB or RGBA and for depth textures it must be a DEPTH_COMPONENT format. Other causes of this error are that the width or height is zero or the z-offset is out of range in case of render to volume.");
						break;
					}
				case FramebufferErrorCode.FramebufferIncompleteMissingAttachmentExt:
					{
						Console.WriteLine("FBO: There are no attachments.");
						break;
					}
				/* case  FramebufferErrorCode.GL_FRAMEBUFFER_INCOMPLETE_DUPLICATE_ATTACHMENT_EXT: 
					 {
						 Console.WriteLine("FBO: An object has been attached to more than one attachment point.");
						 break;
					 }*/
				case FramebufferErrorCode.FramebufferIncompleteDimensionsExt:
					{
						Console.WriteLine("FBO: Attachments are of different size. All attachments must have the same width and height.");
						break;
					}
				case FramebufferErrorCode.FramebufferIncompleteFormatsExt:
					{
						Console.WriteLine("FBO: The color attachments have different format. All color attachments must have the same format.");
						break;
					}
				case FramebufferErrorCode.FramebufferIncompleteDrawBufferExt:
					{
						Console.WriteLine("FBO: An attachment point referenced by GL.DrawBuffers() doesn’t have an attachment.");
						break;
					}
				case FramebufferErrorCode.FramebufferIncompleteReadBufferExt:
					{
						Console.WriteLine("FBO: The attachment point referenced by GL.ReadBuffers() doesn’t have an attachment.");
						break;
					}
				case FramebufferErrorCode.FramebufferUnsupportedExt:
					{
						Console.WriteLine("FBO: This particular FBO configuration is not supported by the implementation.");
						break;
					}
				default:
					{
						Console.WriteLine("FBO: Status unknown. (yes, this is really bad.)");
						break;
					}
			}

			#endregion Test for Error
			GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, 0); // disable rendering into the FBO
		}
		private void CreateNullTexture(out int texture)
		{
			// load texture 
			GL.GenTextures(1, out texture);

			// Still required else TexImage2D will be applyed on the last bound texture
			GL.BindTexture(TextureTarget.Texture2D, texture);

			// generate null texture
			GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, this.width, this.height, 0,
			OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, IntPtr.Zero);

			// set filtering to nearest so we get "atari 2600" look
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
		}



		/*private void GenerateLayer()
		{
			Renderer.Call(() => GL.BindTexture(TextureTarget.Texture2D, this._layerTexture));
			Renderer.Call(() => GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest));
			Renderer.Call(() => GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest));
			//Renderer.Call(() => GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)OpenTK.Graphics.OpenRenderer.Call(() => GL.TextureWrapMode.Clamp));
			//Renderer.Call(() => GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)OpenTK.Graphics.OpenRenderer.Call(() => GL.TextureWrapMode.Clamp));
			//Renderer.Call(() => GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba8, Width, Height, 0, OpenTK.Graphics.OpenRenderer.Call(() => GL.PixelFormat.Rgba, PixelType.UnsignedByte, IntPtr.Zero));
			Renderer.Call(() => GL.BindTexture(TextureTarget.Texture2D, 0));
			Renderer.Call(() => GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, FboHandle));
			Renderer.Call(() => GL.Ext.FramebufferTexture2D(FramebufferTarget.FramebufferExt, FramebufferAttachment.ColorAttachment0Ext, TextureTarget.Texture2D, this._layerTexture, 0));
			Renderer.Call(() => GL.DrawBuffer((DrawBufferMode)FramebufferAttachment.ColorAttachment0Ext));
			Renderer.Call(() => GL.ClearColor(Color.White));
			Renderer.Call(() => GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit));
			Renderer.Call(() => GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, 0));
			Renderer.Call(() => GL.DrawBuffer(DrawBufferMode.Back));
		}*/
		public void simplyBind()
		{
			int w = this.width;
			int h = this.height;
			GL.Ext.BindFramebuffer(FramebufferTarget.DrawFramebuffer, this.id);
			GL.BindTexture(TextureTarget.Texture2D, this.gl_tex_id);
		}
		public void bind()
		{
			GL.PushAttrib(AttribMask.ViewportBit);
			int w = this.width;
			int h = this.height;
			GL.MatrixMode(MatrixMode.Projection);
			GL.LoadIdentity();
			GL.Ortho(0, w, 0, h, -1, 1);
			GL.Viewport(0, 0, w, h);
			GL.Ext.BindFramebuffer(FramebufferTarget.DrawFramebuffer, this.id);
			GL.BindTexture(TextureTarget.Texture2D, this.gl_tex_id);
			//Renderer.Call(() => GL.BindTexture(TextureTarget.Texture2D, this.gl_tex_id));
			//Renderer.Call(() => GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, this.id));
			//Renderer.Call(() => GL.Ext.FramebufferTexture2D(FramebufferTarget.FramebufferExt, FramebufferAttachment.ColorAttachment0Ext, TextureTarget.Texture2D, this.gl_tex_id, 0));
			//Renderer.Call(() => GL.DrawBuffer((DrawBufferMode)FramebufferAttachment.ColorAttachment0Ext));
			//Renderer.Call(() => GL.PushAttrib(AttribMask.ViewportBit));
			//Renderer.Call(() => GL.Viewport(0, 0, this.width, this.height));
			//GL.PopAttrib();
		}
		public void unbind()
		{
			//Renderer.Call(() => GL.BindTexture(TextureTarget.Texture2D, 0));
			Renderer.Call(() => GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, 0));
			Renderer.Call(() => GL.DrawBuffer(DrawBufferMode.Back));
			GL.PopAttrib();
			//Renderer.Call(() => GL.PopAttrib());
			/*Renderer.Call(() => GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, 0);)*/

		}
		public void simplyUnbind()
		{
			Renderer.Call(() => GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, 0));
			Renderer.Call(() => GL.DrawBuffer(DrawBufferMode.Back));
		}
		public void save(string path)
		{
			//this.bind();
			int width = this.width;
			int height = this.height;
			System.Drawing.Imaging.PixelFormat format = System.Drawing.Imaging.PixelFormat.Format32bppArgb;
			Bitmap bmp = new Bitmap(width, height, format);
			System.Drawing.Imaging.BitmapData data = bmp.LockBits(new System.Drawing.Rectangle(0, 0, width, height),
			System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			Renderer.Call(() => GL.ReadBuffer(ReadBufferMode.ColorAttachment0));
			Renderer.Call(() => GL.ReadPixels(0, 0, width, height, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0));
			bmp.UnlockBits(data);
			bmp.RotateFlip(RotateFlipType.RotateNoneFlipY);
			try
			{
				bmp.Save(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), path));
			}
			catch (Exception e)
			{
				this.save(path);
			}
		}
		public void clear()
		{
			this.bind();
			Renderer.Call(() => GL.ClearColor(0f, 0f, 0f, 0f));
			Renderer.Call(() => GL.Clear(ClearBufferMask.ColorBufferBit));
			this.unbind();
		}

		internal void resize(int width, int height)
		{
			if (this.width != width || this.height != height)
			{
				this.width = width;
				this.height = height;
				GL.Ext.DeleteFramebuffer(this.id);
				GL.DeleteTexture(this.gl_tex_id);
				// Generate a framebuffer ID
				CreateNullTexture(out this.gl_tex_id);
				CreateFBOandAssignTexture(out this.id, this.gl_tex_id);
			}
			//throw new NotImplementedException();
		}
	}
}
