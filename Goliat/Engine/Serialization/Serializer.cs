﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FastMember;
using System.Reflection;
using System.IO;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using Fasterflect;
using System.Numerics;
using System.IO.Compression;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using Goliat;
using System.Threading;
using System.Diagnostics;
using Debug = Goliat.Debug;
using Newtonsoft.Json;
using SerializationInfo = Goliat.Serialization.Serializer.SerializationInfo;
using System.Runtime.InteropServices;
public static class Extension2
{
	public static Type GenerateType(this string str)
	{
		Type test = Type.GetType(str);
		if (test == null)
		{
			var types = (from assembly in AppDomain.CurrentDomain.GetAssemblies()
						 from type in assembly.GetTypes()
						 where type.FullName == str
						 select type).ToList();
			if (types.Count == 0)
			{
				;
			}
			return (types.Count > 0 ? types[0] : null);
		}
		return (test);
	}
	public static List<int> GetLengths(this object array)
	{
		List<int> res = new List<int>();
		dynamic obj = array;
		if (obj == null)
		{
			return (null);
		}
		for (int i = 0; i < obj.Rank; i++)
		{
			res.Add(obj.GetLength(i));
		}
		return (res);
	}
	public static bool isNumberType(this Type type)
	{
		return type == typeof(sbyte)
				|| type == typeof(byte)
				|| type == typeof(short)
				|| type == typeof(ushort)
				|| type == typeof(int)
				|| type == typeof(uint)
				|| type == typeof(long)
				|| type == typeof(ulong)
				|| type == typeof(float)
				|| type == typeof(double)
				|| type == typeof(decimal);
	}

}
namespace Goliat
{
	namespace Serialization
	{
		public enum ReferenceMode
		{
			XML,
			XML_Recursive,
			Standard,
			Binarize
		}
		public class SerializationStream
		{
			public int length = 0;
			public string open(string template)
			{
				return ("<" + template + ">");
			}
			public string close(string template)
			{
				return ("</" + template + ">");
			}
			public Dictionary<string, string> objects = new Dictionary<string, string>();
			public void AddValue(string name, string str)
			{

				StringBuilder sb = new StringBuilder();
				string template = "string";
				sb.Append(open(name));
				sb.Append(open(template));
				sb.Append(str);
				sb.Append(close(template));
				sb.Append(close(name));

				objects.AddOrReplace(name, sb.ToString());
			}
			public void AddValue(string name, Vector2 vector)
			{
				StringBuilder sb = new StringBuilder();
				string template = "Vector2";
				sb.Append(open(name));
				sb.Append(open(template));
				for (int i = 0; i < 2; i++ )
				{
					if (i != 0)
					{
						sb.Append(" ");
					}
					sb.Append("" + vector[i]);
					
				}
				sb.Append(close(template));
				sb.Append(close(name));
				objects.AddOrReplace(name, sb.ToString());
			}

			public void AddValue(string name, Vector3 vector)
			{
				StringBuilder sb = new StringBuilder();
				string template = "Vector3";
				sb.Append(open(name));
				sb.Append(open(template));
				for (int i = 0; i < 3; i++)
				{
					if (i != 0)
					{
						sb.Append(" ");
					}
					sb.Append("" + vector[i]);

				}
				sb.Append(close(template));
				sb.Append(close(name));
				objects.AddOrReplace(name, sb.ToString());
			}

			public void AddValue(string name, Color vector)
			{
				StringBuilder sb = new StringBuilder();
				string template = "Color";
				sb.Append(open(name));
				sb.Append(open(template));
				for (int i = 0; i < 4; i++)
				{
					if (i != 0)
					{
						sb.Append(" ");
					}
					sb.Append("" + vector[i]);

				}
				sb.Append(close(template));
				sb.Append(close(name));
				objects.AddOrReplace(name, sb.ToString());
			}


			public void AddValue(string name, Vector4 vector)
			{
				StringBuilder sb = new StringBuilder();
				string template = "vector4";
				sb.Append(open(name));
				sb.Append(open(template));
				for (int i = 0; i < 4; i++)
				{
					if (i != 0)
					{
						sb.Append(" ");
					}
					sb.Append("" + vector[i]);

				}
				sb.Append(close(template));
				sb.Append(close(name));
				objects.AddOrReplace(name, sb.ToString());
			}

			public void AddValue(string name, Quaternion vector)
			{
				StringBuilder sb = new StringBuilder();
				string template = "quaternion";
				sb.Append(open(name));
				sb.Append(open(template));
				for (int i = 0; i < 4; i++)
				{
					if (i != 0)
					{
						sb.Append(" ");
					}
					sb.Append("" + vector[i]);

				}
				sb.Append(close(template));
				sb.Append(close(name));
				objects.AddOrReplace(name, sb.ToString());
			}
			public void AddValue(string name, int value)
			{
				StringBuilder sb = new StringBuilder();
				string template = "int";
				sb.Append(open(name));
				sb.Append(open(template));
				sb.Append("" + value);
				sb.Append(close(template));
				sb.Append(close(name));
				objects.AddOrReplace(name, sb.ToString());
			}

			public void AddValue(string name, float value)
			{
				StringBuilder sb = new StringBuilder();
				string template = "int";
				sb.Append(open(name));
				sb.Append(open(template));
				sb.Append("" + value);
				sb.Append(close(template));
				sb.Append(close(name));
				objects.AddOrReplace(name, sb.ToString());
			}
			public void AddValue(string name, double value)
			{
				StringBuilder sb = new StringBuilder();
				string template = "int";
				sb.Append(open(name));
				sb.Append(open(template));
				sb.Append("" + value);
				sb.Append(close(template));
				sb.Append(close(name));
				objects.AddOrReplace(name, sb.ToString());
			}
			public void AddValue(string name, int[] value)
			{
				StringBuilder sb = new StringBuilder();
				string template = "int[]";
				sb.Append(open(name));
				sb.Append(open(template));
				for (int i = 0; i < value.Length; i++)
				{
					if (i != 0)
					{
						sb.Append(" ");
					}
					sb.Append("" + value[i]);

				}
				sb.Append(close(template));
				sb.Append(close(name));
				objects.AddOrReplace(name, sb.ToString());
			}
			public void AddValue(string name, float[] value)
			{
				StringBuilder sb = new StringBuilder();
				string template = "float[]";
				sb.Append(open(name));
				sb.Append(open(template));
				for (int i = 0; i < value.Length; i++)
				{
					if (i != 0)
					{
						sb.Append(" ");
					}
					sb.Append("" + value[i]);

				}
				sb.Append(close(template));
				sb.Append(close(name));
				objects.AddOrReplace(name, sb.ToString());
			}
			public void AddValue(string name, double[] value)
			{
				StringBuilder sb = new StringBuilder();
				string template = "double[]";
				sb.Append(open(name));
				sb.Append(open(template));
				for (int i = 0; i < value.Length; i++)
				{
					if (i != 0)
					{
						sb.Append(" ");
					}
					sb.Append("" + value[i]);

				}
				sb.Append(close(template));
				sb.Append(close(name));
				objects.AddOrReplace(name, sb.ToString());
			}
			public void AddValue(string name, string[] value)
			{
				StringBuilder sb = new StringBuilder();
				string template = "string[]";
				sb.Append(open(name));
				sb.Append(open(template));
				for (int i = 0; i < value.Length; i++)
				{
					if (i != 0)
					{
						sb.Append(" ");
					}
					sb.Append("<string>");
					sb.Append(value[i]);
					sb.Append("</string>");
				}
				sb.Append(close(template));
				sb.Append(close(name));
				objects.AddOrReplace(name, sb.ToString());
			}
			public void AddValue(string name, ObjectReference reference)
			{
				StringBuilder sb = new StringBuilder();
				string template = "ref";
				sb.Append(open(name));
				sb.Append(open(template));
				sb.Append(reference.guid);
				sb.Append(close(template));
				sb.Append(close(name));
				objects.AddOrReplace(name, sb.ToString());
			}
			public void AddValue(string name, StructureInfo info)
			{
				StringBuilder sb = new StringBuilder();
				string template = "" + info.type;
				sb.Append(open(name));
				sb.Append(open(template));
				sb.Append(info.ToString());
				sb.Append(close(template));
				sb.Append(close(name));
				;
			}
			public object GetValue(string name)
			{
				string value = objects[name];
				System.Xml.XmlDocument elem = new System.Xml.XmlDocument();
				elem.LoadXml(value);
				//elem.ChildNodes;

				for (int i = 0; i < elem.ChildNodes.Count; i++ )
				{
					Console.WriteLine("" + elem.ChildNodes[i].NodeType.ToString());
				}
				//<CustomLine><machin>fsdsdfsd</machin></CustomLine>

					return (null);
			}
			public override string ToString()
			{
				StringBuilder res = new StringBuilder();
				foreach (string key in this.objects.Keys)
				{
					res.Append(this.objects[key]);
				}
				return (res.ToString());
			}

		}
		public class StructureInfo
		{
			public Type type = null;
			public object value = null;
			public SerializationStream stream = new SerializationStream();
			public override string ToString()
			{
				//for (int i = 0; ) reprendre l'iteration membre de populate, et caster.

				//
				return base.ToString();
			}
			public StructureInfo(object value)
			{
				this.type = value.GetType();
				this.value = value;
			}
		}
		public interface IExportable
		{
			void OnExport(SerializationStream stream);
		}
	
		public class ObjectReference
		{
			private static BigInteger lastGuid = 0;
			public static string GenerateGuid()
			{
				BigInteger last = lastGuid;
				lastGuid = BigInteger.Add(lastGuid, 1);
				return (last.ToString("x"));
			}
			private string _guid = "";
			public string guid
			{
				get
				{
					if (_guid == "")
						_guid = GenerateGuid();
					return (_guid);
				}
				private set
				{
					this._guid = value;
				}
			}
			public object obj;
			public ObjectReference(object obj)
			{
				this.obj = obj;
				this._guid = GenerateGuid();
			}
			public static void ResetRefs()
			{
				lastGuid = 0;
			}
		}
		public class Average
		{
			public List <float> values = new List<float>();
			public float value
			{
				get
				{
					float res = 0;
					for (int i = 0; i < values.Count; i++)
					{
						res += values[i] / values.Count;
					}
					return (res);
				}
			}
			public void Add(float val)
			{
				this.values.Add(val);
			}

		}
		
		
		public class BinaryConverter
		{
			private static byte[] current_bytes = null; 
			public delegate void opcode_handler(BinaryObjectHeader header);
			#region opcode_handlers_delegates
			public static void generate_bool(BinaryObjectHeader header)
			{
				lock (mutexLock)
				{
					binaryValues[header.index] = BitConverter.ToBoolean(current_bytes, (int)header.index + (int)Marshal.SizeOf(typeof(BinaryObjectHeader)));
				}
			}
			public static void generate_boolArray(BinaryObjectHeader header)
			{

				bool[] res = new bool[header.size];
				for (int i = 0; i < header.size; i++ )
				{
					res[i] = BitConverter.ToBoolean(current_bytes, (int)header.index + (int)Marshal.SizeOf(typeof(BinaryObjectHeader)) + (sizeof(bool) * i)); 
				}
				lock (mutexLock)
				{
					binaryValues[header.index] = res;
				}
			}
			public static void generate_int(BinaryObjectHeader header)
			{
				lock (mutexLock)
				{
					binaryValues[header.index] = BitConverter.ToInt32(current_bytes, (int)header.index + (int)Marshal.SizeOf(typeof(BinaryObjectHeader)));
				}
			}
			public static void generate_intArray(BinaryObjectHeader header)
			{
				int[] res = new int[header.size];
				for (int i = 0; i < header.size; i++ )
				{
					res[i] = BitConverter.ToInt32(current_bytes, (int)header.index + (int)Marshal.SizeOf(typeof(BinaryObjectHeader)) + (sizeof(int) * i)); 
				}
				lock (mutexLock)
				{
					binaryValues[header.index] = res;
				}

			}
			public static void generate_uint(BinaryObjectHeader header)
			{

				lock (mutexLock)
				{
					binaryValues[header.index] = BitConverter.ToUInt32(current_bytes, (int)header.index + (int)Marshal.SizeOf(typeof(BinaryObjectHeader)));
				}
			}
			public static void generate_uintArray(BinaryObjectHeader header)
			{
				uint[] res = new uint[header.size];
				for (int i = 0; i < header.size; i++)
				{
					res[i] = BitConverter.ToUInt32(current_bytes, (int)header.index + (int)Marshal.SizeOf(typeof(BinaryObjectHeader)) + (sizeof(uint) * i));
				}
				lock (mutexLock)
				{
					binaryValues[header.index] = res;
				}

			}
			public static void generate_float(BinaryObjectHeader header)
			{
				lock (mutexLock)
				{
					binaryValues[header.index] = BitConverter.ToSingle(current_bytes, (int)header.index + (int)Marshal.SizeOf(typeof(BinaryObjectHeader)));
				}

			}
			public static void generate_floatArray(BinaryObjectHeader header)
			{
				float[] res = new float[header.size];
				for (int i = 0; i < header.size; i++)
				{
					res[i] = BitConverter.ToSingle(current_bytes, (int)header.index + (int)Marshal.SizeOf(typeof(BinaryObjectHeader)) + (sizeof(float) * i));
				}
				lock (mutexLock)
				{
					binaryValues[header.index] = res;
				}
			}
			public static void generate_double(BinaryObjectHeader header)
			{
				lock (mutexLock)
				{
					binaryValues[header.index] = BitConverter.ToDouble(current_bytes, (int)header.index + (int)Marshal.SizeOf(typeof(BinaryObjectHeader)));
				}
			}
			public static void generate_doubleArray(BinaryObjectHeader header)
			{
				double[] res = new double[header.size];
				for (int i = 0; i < header.size; i++)
				{
					res[i] = BitConverter.ToSingle(current_bytes, (int)header.index + (int)Marshal.SizeOf(typeof(BinaryObjectHeader)) + (sizeof(double) * i));
				}
				lock (mutexLock)
				{
					binaryValues[header.index] = res;
				}

			}
			public static void generate_customNumber(BinaryObjectHeader header)
			{
				//NON
				/*
				 * customnumber, type, valeur
				 */
				// en deux étapes ici: 
				/*
				 * un on 
				 * 
				 */

				/*
				 * 
				lock (mutexLock)
				{
					binaryValues[header.index] = BitConverter.To(current_bytes, (int)header.index + (int)Marshal.SizeOf(typeof(BinaryObjectHeader)));
				}*/
			}
			public static void generate_customNumberArray(BinaryObjectHeader header)
			{


			}
			public static void generate_string(BinaryObjectHeader header)
			{


			}
			public static void generate_stringArray(BinaryObjectHeader header)
			{


			}
			public static void generate_address(BinaryObjectHeader header)
			{


			}
			public static void generate_vector2(BinaryObjectHeader header)
			{


			}
			public static void generate_vector2Array(BinaryObjectHeader header)
			{


			}
			public static void generate_vector3(BinaryObjectHeader header)
			{


			}
			public static void generate_vector3Array(BinaryObjectHeader header)
			{


			}
			public static void generate_vector4(BinaryObjectHeader header)
			{


			}
			public static void generate_vector4Array(BinaryObjectHeader header)
			{


			}
			public static void generate_matrix4x4(BinaryObjectHeader header)
			{


			}
			public static void generate_matrix4x4Array(BinaryObjectHeader header)
			{


			}
			public static void generate_quaternion(BinaryObjectHeader header)
			{


			}
			public static void generate_quaternionArray(BinaryObjectHeader header)
			{


			}
			public static void generate_color(BinaryObjectHeader header)
			{


			}
			public static void generate_colorArray(BinaryObjectHeader header)
			{


			}
			public static void generate_transform(BinaryObjectHeader header)
			{


			}
			public static void generate_transformArray(BinaryObjectHeader header)
			{


			}
			public static void generate_transform_local_position(BinaryObjectHeader header)
			{


			}
			public static void generate_transform_local_rotation(BinaryObjectHeader header)
			{


			}
			public static void generate_transform_local_scale(BinaryObjectHeader header)
			{


			}
			public static void generate_transform_sons(BinaryObjectHeader header)
			{


			}
			public static void generate_transform_components_list(BinaryObjectHeader header)
			{


			}
			public static void generate_behaviour(BinaryObjectHeader header)
			{


			}
			public static void generate_behaviourArray(BinaryObjectHeader header)
			{


			}
			public static void generate_terrain(BinaryObjectHeader header)
			{


			}
			public static void generate_texture(BinaryObjectHeader header)
			{


			}
			public static void generate_bitmap(BinaryObjectHeader header)
			{


			}
			public static void generate_material(BinaryObjectHeader header)
			{


			}
			public static void generate_Array(BinaryObjectHeader header)
			{


			}
			public static void generate_Dictionary(BinaryObjectHeader header)
			{


			}
			public static void generate_List(BinaryObjectHeader header)
			{


			}
			public static void generate_transform_name(BinaryObjectHeader header)
			{


			}
			public static void generate_bone(BinaryObjectHeader header)
			{


			}
			public static void generate_bone_transform(BinaryObjectHeader header)
			{


			}
			public static void generate_mesh(BinaryObjectHeader header)
			{


			}
			public static void generate_customObject(BinaryObjectHeader header)
			{


			}
			public static void generate_endIndex(BinaryObjectHeader header)
			{


			}
			public static void generate_typeName(BinaryObjectHeader header)
			{


			}
			public static void generate_memberCount(BinaryObjectHeader header)
			{


			}
			public static void generate_memberIndex(BinaryObjectHeader header)
			{


			}
			public static void generate_byte(BinaryObjectHeader header)
			{


			}
			public static void generate_byteArray(BinaryObjectHeader header)
			{


			}
			public static void generate_char(BinaryObjectHeader header)
			{


			}
			public static void generate_charArray(BinaryObjectHeader header)
			{


			}
			public static void	generate_long(BinaryObjectHeader header)
			{


			}
			public static void	generate_longArray(BinaryObjectHeader header)
			{


			}
			public static void	generate_sbyte(BinaryObjectHeader header)
			{


			}
			public static void	generate_sbyteArray(BinaryObjectHeader header)
			{


			}
			public static void	generate_short(BinaryObjectHeader header)
			{


			}
			public static void	generate_shortArray(BinaryObjectHeader header)
			{


			}
			public static void	generate_ulong(BinaryObjectHeader header)
			{


			}
			public static void	generate_ulongArray(BinaryObjectHeader header)
			{


			}
			public static void	generate_ushort(BinaryObjectHeader header)
			{


			}
			public static void	generate_ushortArray(BinaryObjectHeader header)
			{


			}
			#endregion
			#region OpCodeHandlers
			private static Dictionary<OpCode, opcode_handler> OpCodeHandlers = new Dictionary<OpCode, opcode_handler>()
			{
				{OpCode.@bool, generate_bool}, 
				{OpCode.@boolArray, generate_boolArray}, 
				{OpCode.@int, generate_int}, 
				{OpCode.@intArray, generate_intArray}, 
				{OpCode.@uint, generate_uint}, 
				{OpCode.@uintArray, generate_uintArray}, 
				{OpCode.@float, generate_float}, 
				{OpCode.@floatArray, generate_floatArray}, 
				{OpCode.@double, generate_double}, 
				{OpCode.@doubleArray, generate_doubleArray}, 
				{OpCode.@customNumber, generate_customNumber},  // nombre dynamic
				{OpCode.@customNumberArray, generate_customNumberArray}, 
				{OpCode.@string, generate_string}, 
				{OpCode.@address, generate_address}, 
				{OpCode.@vector2, generate_vector2}, 
				{OpCode.@vector2Array, generate_vector2Array}, 
				{OpCode.@vector3, generate_vector3}, 
				{OpCode.@vector3Array, generate_vector3Array}, 
				{OpCode.@vector4, generate_vector4}, 
				{OpCode.@vector4Array, generate_vector4Array}, 
				{OpCode.@matrix4x4, generate_matrix4x4}, 
				{OpCode.@matrix4x4Array, generate_matrix4x4Array}, 
				{OpCode.@quaternion, generate_quaternion}, 
				{OpCode.@quaternionArray, generate_quaternionArray}, 
				{OpCode.@color, generate_color}, 
				{OpCode.@colorArray, generate_colorArray}, 
				{OpCode.@transform, generate_transform}, 
				{OpCode.@transformArray, generate_transformArray}, 
				{OpCode.@transform_local_position, generate_transform_local_position}, 
				{OpCode.@transform_local_rotation, generate_transform_local_rotation}, 
				{OpCode.@transform_local_scale, generate_transform_local_scale}, 
				{OpCode.@transform_sons, generate_transform_sons}, 
				{OpCode.@transform_components_list, generate_transform_components_list}, 
				{OpCode.@behaviour, generate_behaviour}, 
				{OpCode.@behaviourArray, generate_behaviourArray}, 
				{OpCode.@terrain, generate_terrain}, 
				{OpCode.@texture, generate_texture}, 
				{OpCode.@bitmap, generate_bitmap}, 
				{OpCode.@material, generate_material},
				//@textureArray,
				//@bitmapArray,
				{OpCode.@Array, generate_Array},
				{OpCode.@Dictionary, generate_Dictionary},
				{OpCode.@List, generate_List},
				{OpCode.@transform_name, generate_transform_name},
				{OpCode.@bone, generate_bone},
				{OpCode.@bone_transform, generate_bone_transform},
				{OpCode.@mesh, generate_mesh},
				{OpCode.@customObject, generate_customObject},
				{OpCode.@endIndex, generate_endIndex},
				{OpCode.@typeName, generate_typeName},
				{OpCode.@memberCount, generate_memberCount},
				{OpCode.@memberIndex, generate_memberIndex},
				{OpCode.@byte, generate_byte},
				{OpCode.@byteArray, generate_byteArray},
				{OpCode.@char, generate_char},
				{OpCode.@charArray, generate_charArray},
				{OpCode.@long, generate_long},
				{OpCode.@longArray, generate_longArray},
				{OpCode.@sbyte, generate_sbyte},
				{OpCode.@sbyteArray, generate_sbyteArray},
				{OpCode.@short, generate_short},
				{OpCode.@shortArray, generate_shortArray},
				{OpCode.@ulong, generate_ulong},
				{OpCode.@ulongArray, generate_ulongArray},
				{OpCode.@ushort, generate_ushort},
				{OpCode.@ushortArray, generate_ushortArray}	
			};
			#endregion
			#region OpCode
			public enum OpCode
			{
				@bool,
				@boolArray,
				@int,
				@intArray,
				@uint,
				@uintArray,
				@float,
				@floatArray,
				@double,
				@doubleArray,
				@customNumber, // nombre dynamic
				@customNumberArray,
				@string,
				@stringArray,
				@address,
				@vector2,
				@vector2Array,
				@vector3,
				@vector3Array,
				@vector4,
				@vector4Array,
				@matrix4x4,
				@matrix4x4Array,
				@quaternion,
				@quaternionArray,
				@color,
				@colorArray,
				@transform,
				@transformArray,
				@transform_local_position,
				@transform_local_rotation,
				@transform_local_scale,
				@transform_sons,
				@transform_components_list,
				@behaviour,
				@behaviourArray,
				@terrain,
				@texture,
				@bitmap,
				@material,
				//@textureArray,
				//@bitmapArray,
				@Array,
				@Dictionary,
				@List,
				@transform_name,
				@bone,
				@bone_transform,
				@mesh,
				@customObject,
				@endIndex,
				@typeName,
				@memberCount,
				@memberIndex,
				@byte,
				@byteArray,
				@char,
				@charArray,
				@long,
				@longArray,
				@sbyte,
				@sbyteArray,
				@short,
				@shortArray,
				@ulong,
				@ulongArray,
				@ushort,
				@ushortArray
			}
			#endregion
			private static object mutexLock = new Object();
			private static BinaryReader _currentStream;
			private static Dictionary<int, dynamic> binaryValues = new Dictionary<int, dynamic>();
			public static T ReadType<T>(BinaryReader reader)
			{
				byte[] bytes = reader.ReadBytes(Marshal.SizeOf(typeof(T)));

				GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
				T theStructure = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(T));
				handle.Free();
				OpCode test = OpCode.@bool;
				return theStructure;
			}

			public struct BinaryObjectHeader
			{
				public int index;
				public int opcode;
				private int _target;
				public int size;
				public int member_index;
				public int len;

				public static void Load(Stream currentStream)
				{
					_currentStream = new BinaryReader(currentStream);
				}
				public static BinaryObjectHeader GenerateFromAddress(int target)
				{
					if (target < 0)
					{
						return (BinaryObjectHeader.exception());

					}

					_currentStream.BaseStream.Seek(target, SeekOrigin.Current);

					byte[] resbytes = _currentStream.ReadBytes(System.Runtime.InteropServices.Marshal.SizeOf(typeof(BinaryObjectHeader)));
					GCHandle handle = GCHandle.Alloc(resbytes, GCHandleType.Pinned);
					BinaryObjectHeader theStructure = (BinaryObjectHeader)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(BinaryObjectHeader));
					handle.Free();
					return (theStructure);
				}
	
				public BinaryObjectHeader(int index = 0, int opcode = 0, int size = 0, int target = 0, int member_index = 0)
				{
					this.index = index;
					this.opcode = opcode;
					this.size = size;
					this._target = target;
					this.member_index = member_index;
					this.len = 0;
					/*
					 * a chaque objet, on écrit son header,
					 * on écrit sa valeur si int/float/etc...
					 * 
					 * on écrit endindex, typename, suivi de  et memberCount si customobject puis chaque member index, suivi de chaque description de membre. (genre la fonction qui prends en charge l'écriture d'endindex, n'écrit qu'après avoir construit la description.
					 * l'index membre correspond a une fiche immutable qu'on fait de l'objet d'une manière clean à chaque nouveau type à écrire, autant niveau sérialization que désérialization.
					 * 
					 * 
					 * si on thread:
					 * private object dataLock = new object();
					 * 
					 * lock(dataLock)
					 * {
					 *		on peut accéder a un truc partagé.
					 * }
					 * 
					 * // idées threading: on fait un nouveau thread pour chaque sousmembre non int/bool/float/uint/double d'un objet custom
					 * 
					 * BinaryReader n'etant pas safe, on ne peut plus avoir de "BinaryReader", il nous faut un array de bytes[] et gérer nous même la conversion.
					 * 
					 * on n'a pas a utiliser de lock ici: étant donné qu'on ne fait que lire les bytes.
					 * 
					 * BitConverter.ToInt32(bytes, index + sizeof(BinaryObjectHeader));
					 * 
					 * BitConverter.
					 *  // on lock quand on doit ajouter une référence à binaryValues, et peut etre aussi quand on doit read
					 *  genre:
					 *  
					 *  lock(mutexLock)
					 *  {
					 *		binaryValues.AddOrRetrieve(this.index, this.generate_value());
					 *  }
					 *  fin_ResolveValue(); // du coup le thread parent, une fois le thread join, n'a plus qu'a ajuster la valeur de son membre à binaryValues[index] et finalement, on place toutes les valeurs, dans le binaryValues
					 *  
					 * et au ResolveValue() bah c'est la qu'on se dit tien: je dois reconstuire ma valeur.
					 * 
					 * GCHandle handle = GCHandle.Alloc(resbytes, GCHandleType.Pinned);
						BinaryObjectHeader theStructure = (BinaryObjectHeader)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(BinaryObjectHeader));
						handle.Free();
					 * 
					 * 
					 * foreach (Thread thread in workerThreads) {
							  thread.Join();
						} //continuera l'execution quand tout les sous membres seront enregistrés.
					 *
					 * 
					 * 
					 * 
					 * 
					 */
					//BitConverter.ToInt32()
				}

				public object ResolveValue()
				{

					return (null);
					;
				}

				public static object GetValue(int index)
				{
					if (binaryValues.ContainsKey(index))
					{
						return (binaryValues[index]);
					}
					else
					{
						object res = (binaryValues.AddOrRetrieve(index, BinaryObjectHeader.GenerateFromAddress(index).ResolveValue()));
						_currentStream.BaseStream.Seek(index, SeekOrigin.Current);
						return (res);
					}
					;
				}
				public static BinaryObjectHeader exception()
				{
					return (new BinaryObjectHeader(-1, 0, (int)sizeof(int), (int)(0)));
				}

				BinaryObjectHeader target
				{
					get
					{
						if ((OpCode)this.opcode == OpCode.address)
						{
							return BinaryObjectHeader.GenerateFromAddress(this._target);
						}
						return (BinaryObjectHeader.exception());
					}
					set
					{

					}
				}

			}
		}
		public class CustomProperty
		{
			public static Dictionary<string, TypePerfInfo> typePerformances = new Dictionary<string, TypePerfInfo>();
			public string stringType = "";
			public static string logPath = "C:/tmp/testlog.log";
			public static CustomProperty currentParent = null;
			private static Dictionary<IRegenerable, SerializationInfo> toRegen = new Dictionary<IRegenerable, SerializationInfo>();
			private static List<IRestorable> toRestore = new List<IRestorable>();
			private static List<GameBehaviour> behaviours = new List<GameBehaviour>();
			public static string PerfLogPath = "C:/tmp/perflog.log";
			public static Dictionary<string, Average> timeAverages = new Dictionary<string, Average>();
			private object _value = null;
			private CustomProperty _parentObject = null;
			private string _name = "";
			public bool expanded = false;
			public List<CustomProperty> sons = new List<CustomProperty>();
			public bool isArray { get; private set; }
			private int[] _indexs = null;
			public bool isDictionary { get; private set; }
			public bool isEnumerable { get; private set; }
			public int enumerableIndex { get; private set; }
			private static List<Tuple<ObjectTree, CustomProperty>> missedReferences = new List<Tuple<ObjectTree, CustomProperty>>();
			private ObjectTree _description = null;
			public object key { get; private set; }
			public static void getArrayRecursively(CustomProperty parent, int currentRank, object array, int[] indexs)
			{
				dynamic obj = array;
				int maxRank = obj.Rank;
				//glurpFile += "[";
				int[] newIndexs;
				int len = 0;
				if (indexs != null)
				{
					newIndexs = new int[indexs.Length + 1];
					for (int i = 0; i < indexs.Length; i++)
					{
						newIndexs[i] = indexs[i];
					}
					len = indexs.Length;
				}
				else
				{
					newIndexs = new int[1];
					len = 0;
				}
				//current.sons = new ObjectTree[((dynamic)array).GetLength(currentRank)].ToList();
				for (int i = 0; i < ((dynamic)array).GetLength(currentRank); i++)
				{
					newIndexs[len] = i;
					if (currentRank == maxRank - 1)
					{
						CustomProperty son = new CustomProperty(parent, newIndexs, obj.GetValue(newIndexs));
					}
					else
					{
						getArrayRecursively(parent, currentRank + 1, array, newIndexs);
					}
				}
			}
			public static void getArray(object array, int recursionLvl, CustomProperty parent)
			{
				dynamic obj = array;
				int rank = obj.Rank;
				if (rank > 1)
				{
					try
					{
						int[] lengths = new int[rank];
						for (int i = 0; i < rank; i++)
						{
							lengths[i] = obj.GetLength(i);
						}
						int currentRank = 0;
						getArrayRecursively(parent, currentRank, array, null); //array, recursionLvl, currentRank, null, current);
					}
					catch (Exception e)
					{
						;
					}
					return;
				}
				for (int i = 0; i < obj.Length; i++)
				{
					try
					{
						CustomProperty son = new CustomProperty(parent, new int[] { i }, obj[i]);
					}
					catch (Exception e)
					{
						;
					}
				}
			}
			public static void getList(CustomProperty parent, object list)
			{
				dynamic obj = list;
				int i = 0;
				foreach (dynamic value in obj)
				{
					CustomProperty son = new CustomProperty(parent, (object)i, value);
					i++;
				}

			}
			public static void getDictionary(CustomProperty parent, object dictionary)
			{

				try
				{
					dynamic changedObj = dictionary;
					dynamic keys = changedObj.Keys;
					foreach (dynamic key in keys)
					{
						dynamic value = ((dynamic)dictionary)[key];
						new CustomProperty(parent, key, value);
					}
				}
				catch (Exception e)
				{
					;
				}
			}
			public void generateSons()
			{
				this.sons = new List<CustomProperty>();
				object obj = this.value;
				if (obj == null)
				{
					return;
				}
				if (obj.isNumber())
				{

					return;
				}
				else if (obj is bool)
				{
					return;
				}
				else if (obj is string)
				{
					return;
				}
				if (obj.isDictionary())
				{
					getDictionary(this, obj);
				}
				else if (obj.isList())
				{
					getList(this, obj);
				}
				else if (obj.isArray())
				{
					;
				}
				else if (obj == null)
				{
					return;
				}
				else
				{
					TypeAccessor accessor = TypeAccessor.Create(((dynamic)obj).GetType(), true);
					foreach (FieldInfo info in obj.GetType().Fields(Flags.Public | Flags.NonPublic | Flags.Instance))
					{
						if (info.IsDefined(typeof(System.NonSerializedAttribute)) || info.IsDefined(typeof(Goliat.NonSerialized)) || info.IsDefined(typeof(Goliat.HideInInspector)))
						{
							continue;
						}
						else
						{
							;
						}
						try
						{
							object value = obj.TryGetFieldValue(info.Name);
							Type type = info.Type();
							if (value == null)
							{
								;
								throw new Exception();

							}
							try
							{
								CustomProperty son = new CustomProperty(this, info.Name, value);
								//Console.WriteLine("adding:" + info.Name);
							}
							catch (Exception e)
							{
								continue;
							}
						}
						catch (Exception e)
						{

							try
							{
								object value = accessor[obj, info.Name];
								Type type = info.Type();
								try
								{
									CustomProperty son = new CustomProperty(this, info.Name, value);
									//Console.WriteLine("adding:" + info.Name);
								}
								catch (Exception e2)
								{
									continue;
								}
							}
							catch (Exception e2)
							{
								;
							}
						}
					}
					if (obj.GetType().IsValueType)
					{
						foreach (PropertyInfo info in obj.GetType().Properties(Flags.Public | Flags.NonPublic | Flags.Instance))
						{
							if (info.IsDefined(typeof(System.NonSerializedAttribute)) || info.IsDefined(typeof(Goliat.NonSerialized)) || info.IsDefined(typeof(Goliat.HideInInspector)))
							{
								continue;
							}
							try
							{
								object value = obj.TryGetPropertyValue(info.Name);
								if (value == null)
									throw new Exception();
								Type type = info.Type();
								CustomProperty son = new CustomProperty(this, info.Name, value);
								//Console.WriteLine("adding:" + info.Name);
							}
							catch (Exception e)
							{
								;
								try
								{
									object value = accessor[obj, info.Name];
									Type type = info.Type();
									CustomProperty son = new CustomProperty(this, info.Name, value);
									//Console.WriteLine("adding:" + info.Name);
								}
								catch (Exception e2)
								{
									;
								}
							}
						}
					}
				}
			}	
			public static void Clear()
			{
				CustomProperty.referenceDictionary = new Dictionary<string, object>();
				CustomProperty.missedReferences = new List<Tuple<ObjectTree, CustomProperty>>();
				for (int i = 0; i < toRestore.Count; i++)
				{
					toRestore[i].OnRestore();
				}
				foreach (IRegenerable key in toRegen.Keys)
				{
					key.OnRegen(toRegen[key]);
				}
				toRestore = new List<IRestorable>();
				toRegen = new Dictionary<IRegenerable, SerializationInfo>();
				
				for (int i = 0; i < behaviours.Count; i++)
				{
					behaviours[i].OnRestore();
				}
				behaviours = new List<GameBehaviour>();
			}
			public CustomProperty(CustomProperty parentObject, string name, object value)
			{
				//this._parentObject = parentObject;
				this.parentObject = parentObject;
				this._name = name;
				this._value = value;
			}
			public CustomProperty(CustomProperty parentObject, object key, object value)
			{
				//this._parentObject = parentObject;
				this.parentObject = parentObject;
				this.key = key;
				this._value = value;
			}
			public CustomProperty(CustomProperty parentObject, int[] indexs, object value)
			{
				//this.parentObject = parentObject;
				this._indexs = (int[])indexs.Clone();
				this.parentObject = parentObject;
				this._value = value;

			}
			public static CustomProperty FromType(Type type, string value)
			{
				object resval = null;
				TryConvertValue(value, out resval, type);
				CustomProperty res = new CustomProperty(null, "", resval);

				return (res);
			}
			public static Dictionary<string, object> referenceDictionary = new Dictionary<string, object>();
			public CustomProperty root
			{
				get
				{
					CustomProperty tmp = this;
					while (tmp.parentObject != null)
					{
						tmp = tmp.parentObject;
					}
					return (tmp);
				}
			}
			public static object GetValue(Type type, string value)
			{
				object resval = null;
				TryConvertValue(value, out resval, type);
				return (resval);

			}
			public static object GetDefault(Type type)
			{
				if (type.IsValueType)
				{
					return Activator.CreateInstance(type);
				}
				return null;
			}
			public static bool TryConvertValue(string stringValue, out object convertedValue, Type type)
			{
				var targetType = type;
				if (targetType == typeof(string))
				{
					convertedValue = Convert.ChangeType(stringValue, type);
					return true;
				}
				var nullableType = targetType.IsGenericType &&
							   targetType.GetGenericTypeDefinition() == typeof(Nullable<>);
				if (nullableType)
				{
					if (string.IsNullOrEmpty(stringValue))
					{
						convertedValue = GetDefault(targetType);
						return true;
					}
					targetType = new NullableConverter(targetType).UnderlyingType;
				}

				Type[] argTypes = { typeof(string), targetType.MakeByRefType() };
				var tryParseMethodInfo = targetType.GetMethod("TryParse", argTypes);
				if (tryParseMethodInfo == null)
				{
					convertedValue = GetDefault(targetType);
					return false;
				}

				object[] args = { stringValue, null };
				var successfulParse = (bool)tryParseMethodInfo.Invoke(null, args);
				if (!successfulParse)
				{
					convertedValue = GetDefault(targetType);
					return false;
				}

				convertedValue = args[1];
				return true;
			}
			private bool _updated = false;
			public bool updated
			{
				get
				{
					if (this._parentObject == null)
					{
						return (this._updated);
					}
					else
					{
						return (this._parentObject.updated);
					}
				}
				set
				{
					if (this._parentObject == null)
					{
						this._updated = value;
					}
					else
					{
						this._parentObject.updated = value;
					}
				}
			}
			public int[] indexs
			{
				get
				{
					return ((int[])_indexs.Clone());
				}
				private set
				{
					this._indexs = value;
				}
			}
			public void unwrapArrayRecursively(object array, ObjectTree desc, Type elementType, int currentRank, int[] indexs)
			{
				dynamic obj = array;
				int maxRank = obj.Rank;
				int[] newIndexs;
				int len = 0;
				if (indexs != null)
				{
					newIndexs = new int[indexs.Length + 1];
					for (int i = 0; i < indexs.Length; i++)
					{
						newIndexs[i] = indexs[i];
					}
					len = indexs.Length;
				}
				else
				{
					newIndexs = new int[1];
					len = 0;
				}
				for (int i = 0; i < ((dynamic)array).GetLength(currentRank); i++)
				{
					List<int> res = new List<int>();
					newIndexs[len] = i;
					if (currentRank == maxRank - 1)
					{
						ObjectTree itemDesc = (ObjectTree)((ObjectTree[])desc.array).GetValue(newIndexs);
						dynamic value;// = elementType.CreateDefault();
						// /!\ attetnion la aussi on devrait pas le faire comme ça.
						Type type = itemDesc.type.GenerateType();
						try
						{
							value = type.CreateDefault();
						}
						catch (Exception e)
						{
							try
							{
								value = FormatterServices.GetUninitializedObject(type);
							}
							catch (Exception e2)
							{
								value = null;
							}

						}
						CustomProperty son = new CustomProperty(this, newIndexs, value);
						son.populate(itemDesc);
						son.SetPropValue(son.value);
					}
					else
					{
						unwrapArrayRecursively(array, desc, elementType, currentRank + 1, newIndexs);
					}
				}
			}
			public void unwrapArray(object array, ObjectTree desc)
			{
				dynamic obj = array;
				this.isArray = true;
				int rank = obj.Rank;
				if (rank >= 0)
				{
					try
					{
						int currentRank = 0;
						unwrapArrayRecursively(array, desc, obj.GetType().GetElementType(), currentRank, null);
					}
					catch (Exception e)
					{
						;
					}
					return;
				}
			}
			static string StringFromArray(Array array)
			{
				int rank = array.Rank;
				int[] lengths = new int[rank];
				StringBuilder sb = new StringBuilder();
				for (int dimension = 0; dimension < rank; dimension++)
				{
					if (array.GetLowerBound(dimension) != 0)
						throw new NotSupportedException("Only zero-indexed arrays are supported.");
					int length = array.GetLength(dimension);
					lengths[dimension] = length;
				}

				int[] indices = new int[rank];
				bool notDone = true;
			NextRank:
				while (notDone)
				{
					notDone = false;

					string valueString = (array.GetValue(indices) ?? String.Empty).ToString();
					sb.Append(':');
					sb.Append(valueString);

					for (int j = rank - 1; j > -1; j--)
					{
						if (indices[j] < (lengths[j] - 1))
						{
							indices[j]++;
							if (j < (rank - 1))
							{
								for (int m = j + 1; m < rank; m++)
									indices[m] = 0;
							}
							notDone = true;
							goto NextRank;
						}
					}

				}
				return sb.ToString();
			}
			public ObjectTree description
			{
				get
				{
					return (this._description);
				}
				set
				{
					this._description = value;
				}
			}
			public static void CheckMissedReferences()
			{
				List<Tuple<ObjectTree, CustomProperty>> newOne = new List<Tuple<ObjectTree, CustomProperty>>();
				for (int i = 0; i < missedReferences.Count; i++)
				{
					if (referenceDictionary.ContainsKey(missedReferences[i].Item1.reference))
					{
						dynamic value = referenceDictionary[missedReferences[i].Item1.reference]; //erreur ici.
						CustomProperty son = missedReferences[i].Item2;//new CustomProperty(this, desc.name, value);
						son.SetPropValue(value);
						newOne.Add(missedReferences[i]);
					}
					else
					{
						CustomProperty son = missedReferences[i].Item2;
					}
				}
				missedReferences = newOne;
				return;
			}

			
			public class TypePerfInfo
			{
				public Dictionary<string, Average> memberSerialzationTimeAverages = new Dictionary<string, Average>();
				public Average totalAverage = new Average();
			}
			public void populate(ObjectTree description)
			{
				this.stringType = description.type;
				Stopwatch watch = new Stopwatch();
				currentParent = this;
				typePerformances.AddOrRetrieve("" + description.type, new TypePerfInfo());
				watch.Start();
				
				this.populate_(description);
				watch.Stop();
				typePerformances["" + description.type].totalAverage.Add(watch.ElapsedMilliseconds);
				if (parentObject != null)
				{
					typePerformances["" + parentObject.stringType].memberSerialzationTimeAverages.AddOrRetrieve(this.name, new Average());
					typePerformances["" + parentObject.stringType].memberSerialzationTimeAverages[this.name].Add(watch.ElapsedMilliseconds);
				}
				timeAverages.AddOrRetrieve("" + description.type, new Average());
				timeAverages["" + description.type].Add(watch.ElapsedMilliseconds);
				
		
				//
				//this.name
				//this.parentObject
				;
			}
			public void populate_(ObjectTree description)
			{
				this._description = description;
				//Console.WriteLine("populating:" + description.type);
				Type thisType = description.type.GenerateType();
				#region check_specific_patterns
				#region check_serializable_objects
				if (description.resource_guid != "")
				{
					SharedResource tmp = SharedResource.MappedRessourceInfo.RessourceMap[description.resource_guid].Regenerate();
					this.SetPropValue((object)tmp);
					if (description.guid != "")
					{
						if (referenceDictionary.ContainsKey(description.guid))
						{
							referenceDictionary[description.guid] = this.value;
						}
						else
							referenceDictionary.Add(description.guid, this.value);
					}
					return;
				}
				if (description.bytes != null)
				{
					try
					{
						Stopwatch timeTest = Stopwatch.StartNew();

						MemoryStream memStream = new MemoryStream();
						BinaryFormatter binForm = new BinaryFormatter();
						memStream.Write(description.bytes, 0, description.bytes.Length);
						memStream.Seek(0, SeekOrigin.Begin);
						Object obj = (Object)binForm.Deserialize(memStream);
						this.SetPropValue(obj);
						timeTest.Stop();
					}
					catch (Exception e)
					{
						;
					}
					if (description.guid != "")
					{
						if (referenceDictionary.ContainsKey(description.guid))
						{
							referenceDictionary[description.guid] = this.value;
						}
						else
							referenceDictionary.Add(description.guid, this.value);
					}
					return;
				}
				else if (description.info != null && thisType.GetInterfaces().Contains(typeof(ISerializable)))
				{
					try
					{
						this._value = thisType.CreateDefault();
					}
					catch (Exception e)
					{
						this._value = FormatterServices.GetUninitializedObject(thisType);
					}
					if (this._value == null)
					{
						;
					}
					//Console.WriteLine("deserialize");
					((Goliat.ISerializable)this._value).OnDeserialize(description.info);
					if (this._value == null)
					{
						Console.WriteLine("value null");
					}
					this.SetPropValue(this._value);
					if (description.guid != "")
					{
						if (referenceDictionary.ContainsKey(description.guid))
						{
							referenceDictionary[description.guid] = this.value;
						}
						else
							referenceDictionary.Add(description.guid, this.value);
					}
					return;
				}
				#endregion
				#region check_null
				
				this.name = description.name;

				if (description.value == "NULL")
				{
					return;
				}
				if (description.value == "" && description.sons.Count == 0 && description.array == null && description.reference == "" && description.info == null && description.keys.Count == 0)
				{
					return;
				}
				if (thisType == null)
				{
					return;
				}
				#endregion
				#region check_existing_object
				if (description.reference != "")
				{
					//Console.WriteLine("found reference");
					if (referenceDictionary.ContainsKey(description.reference))
					{
						this.value = referenceDictionary[description.reference];
						this.SetPropValue(value);
					}
					else
					{
						if (missedReferences == null || missedReferences.Count == 0)
						{
							missedReferences = new List<Tuple<ObjectTree, CustomProperty>>();
						}
						Console.WriteLine("found missed ref:" + description.type);
						missedReferences.Add(Tuple.Create(description, this));
					}
					return;
				}
				#endregion
				#region check_number_or_string
				if (thisType.isNumberType() || thisType == typeof(string) || thisType == typeof(bool))
				{
					this._value = thisType.CreateDefault();
					if (thisType.isNumberType() || thisType == typeof(string) || thisType == typeof(bool))
					{
						if (thisType == typeof(string))
						{
							this._value = description.value;
							this.SetPropValue(value);
							return;
						}
						else
						{
							dynamic tmp;
							TryConvertValue(description.value, out tmp, thisType);
							_value = tmp;
							if (value == null)
							{
								return;
							}
							this.SetPropValue(this._value);

							return;
						}
					}
					if (description.guid != "")
					{
						if (referenceDictionary.ContainsKey(description.guid))
						{
							referenceDictionary[description.guid] = this.value;
						}
						else
							referenceDictionary.Add(description.guid, this.value);
					}
				}
				#endregion
				#region check_dictionary
				else if (thisType.IsGenericType && thisType.GetGenericTypeDefinition() == typeof(Dictionary<,>))
				{
					//Console.WriteLine("DICTIONARY FOUND");
					List<ObjectTree> keys = description.keys;
					List<ObjectTree> values = description.values;
					Type[] arguments = thisType.GetGenericArguments();
					Type keyType = arguments[0];
					Type valueType = arguments[1];
					this._value = thisType.CreateDefault();
					this.SetPropValue(this._value);

					for (int i = 0; i < keys.Count; i++)
					{
						//Console.WriteLine("DECODING KEY:" + keys[i]);
						if (((keys[i].value == "" && keys[i].sons.Count == 0 && keys[i].array == null && keys[i].bytes == null && keys[i].json == "" && keys[i].keys.Count == 0 && keys[i].resource_guid == "") || keys[i].value == "NULL") && keys[i].reference == "" && keys[i].info == null)
						{
							continue;
						}
						dynamic key = keyType.CreateDefault();
						dynamic value = valueType.CreateDefault();
						CustomProperty son = new CustomProperty(null, "", key);
						son.populate(keys[i]);
						son = new CustomProperty(this, son.value, value);
						son.populate(values[i]);
						//son.SetPropValue(son.value); //!\\
					}

					if (description.guid != "")
					{
						if (referenceDictionary.ContainsKey(description.guid))
						{
							referenceDictionary[description.guid] = this.value;
						}
						else
							referenceDictionary.Add(description.guid, this.value);
					}
				}
				#endregion
				#region check_list

					
				else if (!thisType.IsArray && thisType.IsGenericType && thisType.GetInterfaces().Contains(typeof(IList)))
				{
					this.isEnumerable = true;
					this._value = thisType.CreateDefault();
					List<ObjectTree> subObjects = description.sons;
					if (description.name.Contains("components"))
					{
						Console.WriteLine("found component list for:" + (description.parent == null ? "Transform" : description.parent.type));
					}
					Type elemType = thisType.GetGenericArguments()[0];
					for (int i = 0; i < subObjects.Count; i++)
					{
						
						//Console.WriteLine("found subobject:" + subObjects[i].type);
						if (((subObjects[i].value == "" && subObjects[i].sons.Count == 0 && subObjects[i].array == null && subObjects[i].bytes == null && subObjects[i].keys.Count == 0 && subObjects[i].json == "" && subObjects[i].resource_guid == "") || subObjects[i].value == "NULL") && subObjects[i].reference == "" && subObjects[i].info == null)
						{

							//Console.WriteLine("its null somehow");
							//Console.WriteLine("name:" + subObjects[i].name);
							//Console.WriteLine("info null?" + (subObjects[i].info == null));
							continue;
					
							System.IO.BinaryWriter writer = new System.IO.BinaryWriter(new MemoryStream());
							writer.Write(25); 
							/*
							 * 
							 * 
							 * 
							 * bo bool
							 * 
							 * in int
							 * 
							 * fl float
							 * 
							 * do double
							 * 
							 * st string
							 *  
							 * di dictionary
							 * 
							 * 
							 * puis
							 * 
							 * uint size
							 * 
							 * pour une chaine c'est le nombre de caractères,
							 * 
							 * pour un nombre c'est la taille du nombre
							 * 
							 * pour un dictionaire le nombre de clés
							 * 
							 * pour 
							 * 
							 * 
							 * 
							 * 
							 */

						}

						
						dynamic value;
						try
						{
							value = elemType.CreateDefault();
						}
						catch (Exception e)
						{
							value = FormatterServices.GetUninitializedObject(elemType);
						}
						if (description.name.Contains("components"))
						{
							Console.WriteLine("found comp:" + subObjects[i].type);
						}
						
						//Console.WriteLine("its not null");
						CustomProperty son = new CustomProperty(this, (object)i, value);
						son.populate(subObjects[i]);
						//son.SetPropValue(son.value); //!\\
					}
					if (description.name.Contains("components"))
					{
						Console.WriteLine("end");
					}
					this.SetPropValue(this._value);
					if (description.guid != "")
					{
						if (referenceDictionary.ContainsKey(description.guid))
						{
							referenceDictionary[description.guid] = this.value;
						}
						else
							referenceDictionary.Add(description.guid, this.value);
					}
				}
				#endregion
				#region check_array
				else if (thisType.IsArray)
				{
					List<int> lengths = description.array.GetLengths();
					this.isArray = true;
					Type elementType = ((dynamic)thisType).GetElementType();
					this._value = Array.CreateInstance(elementType, lengths.ToArray());

					unwrapArray(this._value, description);

					this.SetPropValue(this._value);
					if (description.guid != "")
					{
						if (referenceDictionary.ContainsKey(description.guid))
						{
							referenceDictionary[description.guid] = this.value;
						}
						else
							referenceDictionary.Add(description.guid, this.value);
					}

				}
				#endregion
				#endregion
				#region handle_custom_objects
				else
				{
					try
					{
						this._value = thisType.CreateDefault();
					}
					catch (Exception e)
					{
						this._value = FormatterServices.GetUninitializedObject(thisType);
					}
					try
					{
						this.SetPropValue(this._value);
					}
					catch (Exception e)
					{
						;
					}
					if (description.guid != "")
					{
						if (referenceDictionary.ContainsKey(description.guid))
						{
							referenceDictionary[description.guid] = this.value;
						}
						else
							referenceDictionary.Add(description.guid, this.value);
					}
					for (int i = 0; i < description.sons.Count; i++)
					{
						bool debug6 = false;
						if (((description.sons[i] != null) && description.sons[i].value != "") || description.sons[i].sons.Count > 0 || description.sons[i].keys.Count > 0 || description.sons[i].array != null || description.sons[i].bytes != null || description.sons[i].info != null || description.json != "" || description.sons[i].reference != "" || description.sons[i].resource_guid != "")
						{
							ObjectTree desc = description.sons[i];
							if (desc.resource_guid != "")
							{
								CustomProperty son = new CustomProperty(this, desc.name, null);
								son.populate(desc);
								continue;
							}
							if (desc.bytes != null || desc.json != "" || desc.info != null)
							{
								CustomProperty son = new CustomProperty(this, desc.name, null);

								son.populate(desc);
								continue;
							}
							Type type = desc.type.GenerateType();
							dynamic value = null;
							if (desc.reference != "")
							{
								if (debug6 == true)
								{
									;
								}
								if (referenceDictionary.ContainsKey(desc.reference))
								{
									value = referenceDictionary[desc.reference];
									CustomProperty son = new CustomProperty(this, desc.name, value);
									son.SetPropValue(son.value);

								}
								else
								{
									if (missedReferences == null || missedReferences.Count == 0)
									{
										missedReferences = new List<Tuple<ObjectTree, CustomProperty>>();
									}
									CustomProperty son = new CustomProperty(this, desc.name, null);
									missedReferences.Add(Tuple.Create(desc, son));
								}
								continue;

							}

							if (desc.value == "NULL")
							{
								continue;
							}
							if (type == null)
							{
								continue;
							}
							if (type.IsArray)
							{
								List<int> lengths = desc.array.GetLengths();
								value = Array.CreateInstance((type), lengths.ToArray());
								CustomProperty son = new CustomProperty(this, desc.name, value);
								son.isArray = true;
								son.populate(desc);
								//son.SetPropValue(son.value); //!\\
								continue;
							}
							else if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Dictionary<,>))
							{
								value = type.CreateDefault();
								CustomProperty son = new CustomProperty(this, desc.name, value);
								son.isDictionary = true;
								son.populate(desc);
								//son.SetPropValue(son.value); //!\\
								continue;
							}
							else if (!type.IsArray && type.IsGenericType && type.GetGenericTypeDefinition() == typeof(IList<>))
							{
								value = type.CreateDefault();
								CustomProperty son = new CustomProperty(this, desc.name, value);
								son.isEnumerable = true;
								son.populate(desc);
								//son.SetPropValue(son.value); //!\\
								continue;
							}
							else if (type.IsValueType || type == typeof(string))
							{
								value = type.CreateDefault();
								CustomProperty son = new CustomProperty(this, desc.name, value);
								if (((object)value).isNumber() || type == typeof(string))
								{
									if (type == typeof(string))
									{
										byte[] bytes = Convert.FromBase64String(value);
										value = Encoding.Default.GetString(bytes);
										son.populate(desc);
										//son.SetPropValue(son.value); //!\\
										continue;
									}
									else
									{
										string trueValue = desc.value;
										value = Convert.ChangeType(trueValue, type);
										son.populate(desc);
										//son.SetPropValue(son.value); //!\\
										continue;
									}
								}
								son.populate(desc);
								//son.SetPropValue(son.value); //!\\
							}
							else
							{

								try
								{
									value = type.CreateDefault();
								}
								catch (Exception e)
								{
									try
									{
										value = FormatterServices.GetUninitializedObject(type);
									}
									catch (Exception e3)
									{
										Console.WriteLine("type:" + type); 
									}
								}
								CustomProperty son = new CustomProperty(this, desc.name, value);

								son.populate(desc);
								//son.SetPropValue(son.value); //!\\

							}
						}
					}
					this.SetPropValue(this._value);
				}
				#endregion
				if (description.guid != "")
				{
					if (referenceDictionary.ContainsKey(description.guid))
					{
						referenceDictionary[description.guid] = this.value;
					}
					else
						referenceDictionary.Add(description.guid, this.value);
				}
			}
			public CustomProperty parentObject
			{
				get
				{
					return (this._parentObject);
				}
				set
				{
					if (value == null)
					{
						this._parentObject = null;
						return;
					}
					if (this._parentObject != null && this._parentObject == value)
						return;
					if (value.sons == null)
						value.sons = new List<CustomProperty>();
					value.sons.Add(this);
					this._parentObject = value;
				}
			}
			public string name
			{
				get
				{
					return (this._name);
				}
				private set
				{
					this._name = value;
				}
			}
			public object value
			{
				get
				{
					return (this._value);
				}
				set
				{
					if (this.parentObject != null)
						this.SetPropValue((object)value);
					else
						this._value = value;
				}
			}
			public void SetValue(object value)
			{
				this.value = value;
			}
			public void UpdateValue()
			{
				this.SetPropValue(this._value);
			}
			public void SetPropValue(object value)
			{
				bool debug5 = false;
				this._value = (dynamic)value;
				if (this._value != null && this._value is IRestorable)
				{
					if (!toRestore.Contains(this._value))
					{
						toRestore.Add((IRestorable)this._value);
					}

					/*dynamic tmpval = this._value.base;
					if (tmpval is IRestorable && !toRestore.Contains(tmpval))
					{
						toRestore.Add((IRestorable)tmpval);
					}*/
				}
				if (this._value != null && this._value is IRegenerable)
				{
					//Console.WriteLine("hey.0");
					if (!toRegen.ContainsKey((IRegenerable)this._value))
					{
						//Console.WriteLine("hey.0.1");
						if (description == null)
						{
							;//Console.WriteLine("desc null");
						}
						if (this.description.info == null)
						{
							toRegen.Add((IRegenerable)this._value, this.description.info);
							;
						}
						//Console.WriteLine("hey.0.2");
						
					}
//					Console.WriteLine("hey.1");

					/*dynamic tmpval = this._value.base;
					if (tmpval is IRestorable && !toRestore.Contains(tmpval))
					{
						toRestore.Add((IRestorable)tmpval);
					}*/
				}
				if (this._value != null && this._value is GameBehaviour)
				{
					behaviours.Add((GameBehaviour)this._value);
				}

				CustomProperty tmp = this.parentObject;
				if (this.parentObject != null)
				{
					if (this.parentObject.isArray)
					{
						//Console.WriteLine("HEYYYYYYY");
						((dynamic)this.parentObject.value).SetValue(value, this.indexs);
						this.parentObject.SetPropValue(this.parentObject.value);
						return;
					}
					if (this.parentObject.isEnumerable)
					{
						List<object> tmplist = new List<object>();
						for (int i = 0; i < ((dynamic)this.parentObject.value).Count; i++)
						{
							tmplist.Add(((dynamic)this.parentObject.value)[i]);
						}
						while (tmplist.Count <= (int)this.key)
						{
							tmplist.Add(this._value);
						}
						tmplist[(int)this.key] = this._value;
						this.parentObject.value = this.parentObject.value.GetType().CreateDefault();
						for (int i = 0; i < tmplist.Count; i++)
						{
							((dynamic)this.parentObject.value).Add((dynamic)tmplist[i]);
						}
						this.parentObject.SetPropValue(this.parentObject.value);
						return;
					}
					if (this.parentObject.isDictionary)
					{
						// a l'air faux.
						dynamic test = this.value.GetType().CreateDefault();
						Convert.ChangeType(test, this.value.GetType());
						if ((((dynamic)this.parentObject.value)).ContainsKey((dynamic)this.key))
						{
							((dynamic)this.parentObject.value)[(dynamic)this.key] = (dynamic)this._value;
						}
						else
							((dynamic)this.parentObject.value).Add(((dynamic)this.key), (dynamic)this._value);
						this.parentObject.SetPropValue(this.parentObject.value);
						return;
					}
					if (this.parentObject != null)
					{

						if (!this.parentObject.value.GetType().IsValueType)
						{
							if (this.parentObject.value.TryGetValue(this.name) == this._value)
							{
								return;
							}
						}
						this.parentObject.value.SetPropValue(this.name, value);
						while (tmp != null && tmp.parentObject != null)
						{
							if (tmp.parentObject != null)
							{
								;
							}
							tmp.parentObject.value.SetPropValue(tmp.name, tmp.value);
							tmp = tmp.parentObject;
						}
						if (tmp != null && tmp.parentObject != null)
						{
							tmp.parentObject.value.SetPropValue(tmp.name, tmp.value);
						}
						this.parentObject.UpdateValue();
					}
				}
			}
		}
		[System.Serializable]
		public class ObjectTree
		{
			public byte[] bytes = null;
			public SerializationInfo info = null;
			public string json = "";
			public string ToString(int recursion)
			{
				string tabs = "";
				for (int i = 0; i < recursion; i++)
				{
					tabs += "\t";
				}
				string res = tabs + "-----------------------------------------------------\n";
				res += tabs;
				res += "Object Tree:\n";
				res += "\n";
				res += tabs;
				res += "name:" + name;
				res += "\n";
				res += tabs;
				res += "type:" + type;
				res += "\n";
				res += tabs;
				res += "sons: " + "count:" + sons.Count + "\n";
				res += tabs;
				for (int i = 0; i < sons.Count; i++)
				{
					res += sons[i].ToString(recursion + 1);
					res += "\n";
					res += tabs;
				}
				if (array != null)
				{
					res += "array";
				}
				if (array != null && ((dynamic)array).Rank == 1)
				{
					for (int i = 0; i < ((dynamic)array).Length; i++)
					{
						res += "Oui!!";
						res += (((dynamic)array)[i]).ToString(recursion + 1);
						res += "\n";
					}
				}
				res += "*******************************************************************\n";
				return (res);
			}
			public override string ToString()
			{
				string res = "-----------------------------------------------------\n";
				res += "Object Tree:\n";
				res += "\n";
				res += "name:" + name;
				res += "\n";
				res += "type:" + type;
				res += "\n";
				res += "sons: " + "count:" + sons.Count + "\n";
				for (int i = 0; i < sons.Count; i++)
				{
					res += sons[i].ToString(1);
					res += "\n";
				}
				if (array != null)
				{
					res += "array";
				}
				if (array != null && ((dynamic)array).Rank == 1)
				{
					for (int i = 0; i < ((dynamic)array).Length; i++)
					{
						res += "Oui!!";
						res += (((dynamic)array)[i]).ToString(1);
						res += "\n";
					}
				}
				res += "*******************************************************************\n";


				return (res);
			}
			public List<ObjectTree> sons = new List<ObjectTree>();
			public string reference = "";
			public string name = "";
			public string value = "";
			public string type = "";
			public string guid = "";
			public string resource_guid = "";
			public string elementType = "";
			public string keyType = "";
			public string valueType = "";


			public ObjectTree parent = null;
			public List<int> indexs = null;
			public int index = 0;
			public List<ObjectTree> keys = new List<ObjectTree>();
			public List<ObjectTree> values = new List<ObjectTree>();


			private object _array = null;
			public object array
			{
				get
				{
					return (this._array);
				}
				set
				{
					if (value.isArray())
					{
						this._array = value;
					}
				}
			}
			public ObjectTree()
			{
				;
			}
		}
		public class Shtouille
		{
			public int maurice = 12;
			public Shtouille()
			{
				;
			}
		}
		public class StackTrace
		{
			public string Name = "";
			public Type type = null;
			public StackTrace(Type type, string name)
			{
				this.Name = name;
				this.type = type;
			}
		}
		public static class Serializer
		{

			public static void encodeArrayRecursively(object array, int recursionLvl, int currentRank, int[] indexs, ObjectTree current)
			{
				dynamic obj = array;
				int maxRank = obj.Rank;
				//glurpFile += "[";
				int[] newIndexs;
				int len = 0;
				if (indexs != null)
				{
					newIndexs = new int[indexs.Length + 1];
					for (int i = 0; i < indexs.Length; i++)
					{
						newIndexs[i] = indexs[i];
					}
					len = indexs.Length;
				}
				else
				{
					newIndexs = new int[1];
					len = 0;
				}
				//current.sons = new ObjectTree[((dynamic)array).GetLength(currentRank)].ToList();
				for (int i = 0; i < ((dynamic)array).GetLength(currentRank); i++)
				{
					newIndexs[len] = i;
					if (currentRank == maxRank - 1)
					{
						if ((((dynamic)current.array).GetValue(newIndexs)) == null)
						{
							((dynamic)current.array).SetValue(new ObjectTree(), newIndexs);
						}
						serializeObject(obj.GetValue(newIndexs), recursionLvl, ((dynamic)current.array).GetValue(newIndexs));
						((ObjectTree)(((dynamic)current.array).GetValue(newIndexs))).parent = current;
						((ObjectTree)(((dynamic)current.array).GetValue(newIndexs))).type = "" + obj.GetValue(newIndexs).GetType();
						current.elementType = "" + obj.GetType().GetElementType();
					}
					else
					{
						encodeArrayRecursively(array, recursionLvl, currentRank + 1, newIndexs, current);
					}
				}
			}
			public static void encodeArray(object array, int recursionLvl, ObjectTree current) // on met dans un tableau au nombre exact de dimensions de ObjectTree , puis on s'en ressert juste tranquil après.
			{
				dynamic obj = array;
				int rank = obj.Rank;
				if (rank > 1)
				{
					try
					{
						int[] lengths = new int[rank];
						for (int i = 0; i < rank; i++)
						{
							lengths[i] = obj.GetLength(i);
						}
						int currentRank = 0;
						current.array = Array.CreateInstance(typeof(ObjectTree), lengths);
						encodeArrayRecursively(array, recursionLvl, currentRank, null, current);
					}
					catch (Exception e)
					{
						;
					}
					return;
				}
				//glurpFile += "[ ";
				current.array = new ObjectTree[obj.Length];
				current.elementType = "" + obj.GetType().GetElementType();
				//current.sons = new ObjectTree[obj.Length].ToList();
				for (int i = 0; i < obj.Length; i++)
				{
					try
					{
						((ObjectTree[])current.array)[i] = new ObjectTree();
						((ObjectTree[])current.array)[i].parent = current;
						((ObjectTree[])current.array)[i].type = "" + obj[i].GetType();
						serializeObject(obj[i], recursionLvl, ((ObjectTree[])current.array)[i]);


					}
					catch (Exception e)
					{
						Debug.Log(CustomProperty.logPath, e.ToString());
					}
				}
			}
			public static void encodeList(object list, int recursionLvl, ObjectTree current)
			{
				dynamic obj = list;
				bool debug = false;
				//Debug.Log("ENCODING LIST!!!" + list.GetType());
				//Debug.Log(CustomProperty.logPath, "ENCODING LIST!!!");
				current.elementType = "" + obj.GetType().GetElementType();
				current.type = "" + obj.GetType();
				if (current.name == "faces")
				{
					debug = true;
					Debug.Log(CustomProperty.logPath, "FACES:");
				}

				foreach (dynamic value in obj)
				{
					ObjectTree son = new ObjectTree();
					//Debug.Log(CustomProperty.logPath, "you serious?");
					//if (value == null)
					//Debug.Log(CustomProperty.logPath, "value null");
					if (debug)
						Debug.Log(CustomProperty.logPath, "son:" + son.ToString());
					try
					{
						son.type = "" + value.GetType();


						//Debug.Log(CustomProperty.logPath, "WHAT?");
						son.parent = current;
						current.sons.Add(son);
						serializeObject(value, recursionLvl, son);
					}
					catch (Exception e)
					{
						Debug.Log(CustomProperty.logPath, e.ToString());
					}
				}
				if (debug)
					Debug.Log(CustomProperty.logPath, "END!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
				//Debug.Log(CustomProperty.logPath, current.ToString() + "END ENCODING");
				//Debug.Log("END ENCODING");

			}
			public static void encodeDictionary(object dictionary, int recursionLvl, ObjectTree current)
			{

				try
				{
					dynamic changedObj = dictionary;
					dynamic keys = changedObj.Keys;
					Type[] arguments = ((dynamic)dictionary).GetType().GetGenericArguments();
					Type keyType = arguments[0];
					Type valueType = arguments[1];

					current.keyType = "" + arguments[0];
					current.valueType = "" + arguments[1];
					foreach (dynamic key in keys)
					{
						Console.WriteLine("ENCODING KEY:" + key);
						dynamic value = ((dynamic)dictionary)[key];
						ObjectTree keyTree = new ObjectTree();
						keyTree.type = "" + key.GetType();
						keyTree.parent = current;
						current.keys.Add(keyTree);
						ObjectTree valueTree = new ObjectTree();
						valueTree.parent = current;
						valueTree.type = "" + value.GetType();
						current.values.Add(valueTree);
						//c
						//keyTree.type = "" + current.type.GetElementType();

						serializeObject(key, recursionLvl, keyTree);
						serializeObject(value, recursionLvl, valueTree);
					}
					//glurpFile += " $}";
					//clean filePath);
				}
				catch (Exception e)
				{
					Debug.Log(CustomProperty.logPath, e.ToString());
				}

			}
			public static Dictionary<Type, List<string>> ignoredAttributes = new Dictionary<Type, List<string>>();
			public static Dictionary<Type, List<ObjectReference>> refDictionary = new Dictionary<Type, List<ObjectReference>>();
			public static List<StackTrace> stackTrace = new List<StackTrace>();
			public static bool checkStackTrace()
			{
				for (int i = 0; i < stackTrace.Count; i++)
				{
					for (int j = i + 1; j < stackTrace.Count; j++)
					{
						if (stackTrace[i].Name == stackTrace[j].Name && stackTrace[i].type == stackTrace[j].type)
						{
							for (int k = j; k < stackTrace.Count; k++)
							{
								for (int f = k + 1; f < stackTrace.Count; f++)
								{
									if (stackTrace[k].Name == stackTrace[f].Name && stackTrace[k].type == stackTrace[f].type)
									{
										//Debug.Log(CustomProperty.logPath, "problem:" + stackTrace[k].Name);
										return (false);
									}
								}
							}
						}
					}
				}
				return (true);
			}
			[System.Serializable]
			public class SerializationInfo
			{
				public Dictionary<string, ObjectTree> storage = new Dictionary<string, ObjectTree>();
				//public virtual T addComponent<T>() where T : new()
				public void AddValue(String name, object value)
				{
					ObjectTree current = new ObjectTree();
					current.name = name;
					current.value = "";
					if (value == null)
					{
						current.value = "NULL";
						return;
					}
					current.type = "" + value.GetType();
					serializeObject((object)value, 0, current);
					if (storage.ContainsKey(name))
					{
						storage[name] = current;
					}
					else
					{
						storage.Add(name, current);
						Debug.Log(CustomProperty.logPath, "here is the string:" + current.value);
					}
					return;
				}
				public object GetValue(String name)
				{
					CustomProperty prop = new CustomProperty(null, "", null);
					if (storage.ContainsKey(name))
					{
						prop.populate(storage[name]);
						return (prop.value);
					}
					return (null);
				}
			}
			public static void serializeObject(object obj, int recursionLvl, ObjectTree current)
			{
				//Console.WriteLine("serializing object:" + obj);
				string logPath = CustomProperty.logPath;
				current.json = "";
				current.resource_guid = "";
				if (obj == null)
				{
					current.value = "NULL";
					return;
				}
				if (obj.isNumber())
				{

					current.value = obj.ToString();
					return;
				}
				else if (obj is bool)
				{
					current.value = obj.ToString();
					return;
				}
				else if (obj is string)
				{
					current.value = (string)obj;
					return;
				}
				List<ObjectReference> referencedValues;
				if (refDictionary.TryGetValue(obj.GetType(), out referencedValues))
				{
					for (int i = 0; i < referencedValues.Count; i++)
					{
						if (referencedValues[i].obj == obj)
						{
							current.value = "";
							current.reference = referencedValues[i].guid;
							current.guid = "";
							return;
						}
					}
					ObjectReference reference = new ObjectReference(obj);
					refDictionary[obj.GetType()].Add(reference);
					current.guid = reference.guid;
				}
				else
				{
					ObjectReference reference = new ObjectReference(obj);
					refDictionary.Add(obj.GetType(), new List<ObjectReference>());
					refDictionary[obj.GetType()].Add(reference);
					current.guid = reference.guid;
				}
				if (obj is Goliat.SharedResource && obj.GetType() != typeof(Material) && obj.GetType() != typeof(Texture))
				{
				//	Console.WriteLine("shared resource found");
					current.type = "" + obj.GetType();
					current.resource_guid = ((SharedResource)obj).guid;
					return;
				}
				if (obj is Goliat.IRegenerable)
				{
					SerializationInfo info = new SerializationInfo();
					((Goliat.IRegenerable)obj).OnClose(info);
					current.type = "" + obj.GetType();
					current.info = info;
				}
				if (obj is Goliat.ISerializable)
				{
					SerializationInfo info = new SerializationInfo();
					((Goliat.ISerializable)obj).OnSerialize(info);
					current.type = "" + obj.GetType();
					current.info = info;
					
				//	Console.WriteLine("serializable:" + obj.GetType());
					/*if (obj.GetType() == typeof(Transform))
					{
				//		Console.WriteLine("name:" + ((Transform)obj).name);
					}*/
					//Console.WriteLine("info null?" + (info == null));
					return;
				}
				

				if (obj.IsSerializable() && !obj.IsNumber() && !(obj is bool) && !(obj is string))
				{
					try
					{
						if (obj == null)
							return;
						BinaryFormatter bf = new BinaryFormatter();
						MemoryStream ms = new MemoryStream();
						bf.Serialize(ms, obj);
						current.bytes = ms.ToArray();
						return;
					}
					catch (Exception e)
					{
						current.bytes = null;
						if (obj.GetType() == typeof(Mesh))
						{
							Debug.Log(e.ToString());
						}
					}
				}
				if (!obj.GetType().IsValueType)
					recursionLvl = 0;
				
			
				if (obj.isDictionary())
				{
					encodeDictionary(obj, recursionLvl, current);
				}
				else if (obj.isList())
				{
					encodeList(obj, recursionLvl, current);
				}
				else if (obj.isArray())
				{
					encodeArray(obj, recursionLvl, current);
				}
				else if (obj == null)
				{
					current.value = "NULL";
					return;
				}
				else
				{
					TypeAccessor accessor = TypeAccessor.Create(((dynamic)obj).GetType(), true);
					foreach (FieldInfo info in obj.GetType().Fields(Flags.Public | Flags.NonPublic | Flags.Instance))
					{
						if (info.IsDefined(typeof(System.NonSerializedAttribute)) || info.IsDefined(typeof(Goliat.NonSerialized)))
						{
							continue;
						}
						else
						{
							;
						}
						try
						{
							object value = obj.TryGetFieldValue(info.Name);
							Type type = info.Type();
							if (value == null)
							{
								;
								throw new Exception();

							}
							try
							{
								serializeField(info.Name, type, value, recursionLvl, current);
							}
							catch (Exception e)
							{
								continue;
							}
						}
						catch (Exception e)
						{
							try
							{
								object value = accessor[obj, info.Name];
								Type type = info.Type();
								try
								{
									serializeField(info.Name, type, value, recursionLvl, current);
								}
								catch (Exception e2)
								{
									continue;
								}
							}
							catch (Exception e2)
							{
								;
							}
						}
					}
					if (obj.GetType().IsValueType)
					{
						foreach (PropertyInfo info in obj.GetType().Properties(Flags.Public | Flags.NonPublic | Flags.Instance))
						{
							if (info.IsDefined(typeof(System.NonSerializedAttribute)) || info.IsDefined(typeof(Goliat.NonSerialized)))
							{
								continue;
							}
							try
							{
								object value = obj.TryGetPropertyValue(info.Name);
								if (value == null)
									throw new Exception();
								Type type = info.Type();
								serializeField(info.Name, type, value, recursionLvl, current);
							}
							catch (Exception e)
							{
								;
								try
								{
									object value = accessor[obj, info.Name];
									Type type = info.Type();
									if (recursionLvl > 2)
									{

										if (!checkStackTrace())
										{
											continue;
										}
										else
										{
											stackTrace.Add(new StackTrace(type, info.Name));
										}
									}
									serializeField(info.Name, type, value, recursionLvl, current);
								}
								catch (Exception e2)
								{
									;
								}
							}
						}
					}
				}

			}
			public static void serializeField(string fieldName, Type type, object obj, int recursionLvl, ObjectTree parent)
			{
				ObjectTree tree = new ObjectTree();
				tree.parent = parent;
				parent.sons.Add(tree);
				if (obj == null)
				{
					tree.name = fieldName;
					tree.type = "" + type;
					tree.value = "NULL";
					return;
				}
				tree.value = "";
				tree.reference = "";
				tree.name = fieldName;
				tree.type = "" + type.FullName;
				serializeObject(obj, recursionLvl + 1, tree);
			}
			public static void Serialize(string filePath, object value)
			{
				refDictionary = new Dictionary<Type, List<ObjectReference>>();
				ObjectTree tree = new ObjectTree();
				tree.type = value.GetType().FullName;
				serializeObject(value, 0, tree);
				FileStream stream = File.Create(filePath);
				var formatter = new BinaryFormatter();
				formatter.Serialize(stream, tree);
				stream.Close();
			}
			public static object UnSerialize(string filePath)
			{
				File.WriteAllText(CustomProperty.PerfLogPath, "");
				CustomProperty.timeAverages = new Dictionary<string, Average>();
				CustomProperty.Clear();
				refDictionary = new Dictionary<Type, List<ObjectReference>>();
				Stopwatch watch = Stopwatch.StartNew();
				var formatter = new BinaryFormatter();
				Stream stream = File.OpenRead(filePath);
				ObjectTree v = (ObjectTree)(formatter.Deserialize(stream));

				stream.Close();
				File.AppendAllText(CustomProperty.PerfLogPath, "time for debinarization:" + (watch.ElapsedMilliseconds / 1000) + " seconds" + ";\n");
				CustomProperty root = new CustomProperty(null, "", null);
				Console.WriteLine("tree type:" + v.type);
				root.populate((ObjectTree)v);
				if (root.sons.Count == 0)
				{
					Console.WriteLine("hein?");
				}
				for (int i = 0; i < root.sons.Count; i++)
				{
					if (root.sons[i] != null && root.sons[i].value != null && root.sons[i].description != null)
					{
						CustomProperty son = root.sons[i];
						ObjectTree desc = son.description;
						Console.WriteLine("root son: name :" + desc.name + ";\n type:" + desc.type + ";\n guid:" + desc.guid + ";\n value:" + son.value.ToString() + " ;" + "value type:" + son.value.GetType().ToString() + "; value ref:" + desc.reference + "\n");
					}
				}
				Console.WriteLine("Tree sons:");
				for (int i = 0; i < v.sons.Count; i++)
				{
					ObjectTree desc = (ObjectTree)v.sons[i];
					Console.WriteLine("name :" + desc.name + ";\n type:" + desc.type + "; guid:" + desc.guid + ";\n ");
				}
				CustomProperty.CheckMissedReferences();
				CustomProperty.Clear();
				List<string> sorted = new List<string>();
				Dictionary<string, float> averages = new Dictionary<string, float>();
				foreach (string key in CustomProperty.timeAverages.Keys)
				{
					averages.Add(key, CustomProperty.timeAverages[key].value);
				}
				foreach (string key in CustomProperty.timeAverages.Keys)
				{
					if (sorted.Count == 0)
					{
						sorted.Add(key);
						continue;
					}
					for (int i = 0; i < sorted.Count; i++ )
					{
						if (averages[key] <= averages[sorted[i]] && i == 0)
						{
							sorted.Insert(0, key);
							break;
						}
						if (averages[key] >= averages[sorted[i]] && ((sorted.Count <= i + 1) || averages[key] <= averages[sorted[i + 1]]))
						{
							sorted.Insert(i + 1, key);
							break;
						}
					}
					
				}
				File.AppendAllText(CustomProperty.PerfLogPath, "Total time in seconds:" + (watch.ElapsedMilliseconds) + ";\n");
				for (int i = 0; i < sorted.Count; i++ )
				{
					string text = buildPerformanceInfo(sorted[sorted.Count - i - 1]);
					File.AppendAllText(CustomProperty.PerfLogPath, text + ";\n");
				}
				return (root.value);
			}
			public static string buildPerformanceInfo(string typeName)
			{
				
				string res = "";
				string tabs = "\t\t\t";
				res += "[" + CustomProperty.typePerformances[typeName].totalAverage.value + "]\n";
				res += "TYPE PERFORMANCES(-> " + typeName + " <-) :\n";
				res += "{\n";
				CustomProperty.TypePerfInfo info = CustomProperty.typePerformances[typeName];
				List<string> sorted = new List<string>();
				Dictionary<string, float> averages = new Dictionary<string, float>();
				foreach (string key in info.memberSerialzationTimeAverages.Keys)
				{
					averages.Add(key, info.memberSerialzationTimeAverages[key].value);
				}
				foreach (string key in info.memberSerialzationTimeAverages.Keys)
				{
					if (sorted.Count == 0)
					{
						sorted.Add(key);
						continue;
					}
					for (int i = 0; i < sorted.Count; i++)
					{
						if (averages[key] <= averages[sorted[i]] && i == 0)
						{
							sorted.Insert(0, key);
							break;
						}
						if (averages[key] >= averages[sorted[i]] && ((sorted.Count <= i + 1) || averages[key] <= averages[sorted[i + 1]]))
						{
							sorted.Insert(i + 1, key);
							break;
						}
					}
				}
				for (int i = 0; i < sorted.Count; i++)
				{
					res += tabs + "MEMBER NAME:" + sorted[i] + "\n";
					res += tabs + "\tSERIALIZATION TIME:" + averages[sorted[i]] + "\n" + tabs + "\n";
				}
				res += "}\n";
				return (res);
			}
		}
	}
	public class StringParser
	{
		public static string GetNextLine(string toparse, ref int index)
		{
			int i = index;
			List<char> tmpres = new List<char>();
			while (i < toparse.Length)
			{
				if (toparse[i] == '\n')
				{
					index = i + 1;
					return (new String(tmpres.ToArray()));
				}
				tmpres.Add(toparse[i]);

				i++;
			}
			index = i;
			return (new String(tmpres.ToArray()));
		}
		public static string GetNextLine(string toparse, int index)
		{
			int i = index;
			List<char> tmpres = new List<char>();
			while (i < toparse.Length)
			{
				if (toparse[i] == '\n')
				{
					index = i + 1;
					return (new String(tmpres.ToArray()));
				}
				tmpres.Add(toparse[i]);

				i++;
			}
			return (new String(tmpres.ToArray()));
		}
		public static string TrimExtremity(string toparse, string beginingTrim, string endTrim)
		{
			int i = 0;
			if (CheckOccurence(toparse, beginingTrim, ref i))
			{
				;
			}
			int k = toparse.Length - endTrim.Length;
			if (!CheckOccurence(toparse, endTrim, ref k))
			{
				k = toparse.Length;
				return (toparse.Substring(i, toparse.Length - i));
			}
			return (toparse.Substring(i, toparse.Length - i).Substring(0, k - 1 - i));
		}


		public static string GetBetween(string toparse, string s1, string s2, string trim)
		{
			int i = 0;
			string res = "";
			List<char> tmpres = new List<char>();
			bool entered = false;
			while (i < toparse.Length)
			{
				if (!CheckOccurence(toparse, trim, ref i))
				{
					if (!entered && CheckOccurence(toparse, s1, ref i))
					{
						entered = true;
					}
					else if (entered && CheckOccurence(toparse, s2, ref i))
					{
						return (new String(tmpres.ToArray()));
					}
					if (CheckOccurence(toparse, trim, ref i))
					{
						continue;
					}
					if (entered)
					{
						tmpres.Add(toparse[i]);
					}
					i++;
				}
			}
			res = new String(tmpres.ToArray());
			if (res == "")
				return (toparse);
			return (res);
		}
		public static string RemoveFirst(string toparse, string s1)
		{
			int i = 0;
			string res = "";
			List<char> tmpres = new List<char>();
			bool check = false;
			while (i < toparse.Length)
			{
				if (!check && CheckOccurence(toparse, s1, ref i))
				{
					check = true;
				}
				tmpres.Add(toparse[i]);
				//res += toparse[i];
				i++;
			}

			return (new String(tmpres.ToArray()));
		}
		public static bool FindBetween(string toparse, string s1, string s2, List<string> levelUp, List<string> levelDown, int lvlmax)
		{
			int i = 0;
			string res = "";
			//List<char> res = new List<char>();
			bool entered = false;
			int[] level = new int[levelUp.Count];
			int tmp = 0;
			int j = 0;
			bool ignore = false;
			while (i < toparse.Length)
			{
				j = i;
				if (entered && CheckAllInferior(level, lvlmax) && CheckOccurence(toparse, s2, ref i))
				{
					return (true);
				}
				if (!entered && !ignore && CheckOccurence(toparse, s1, ref i))
				{
					entered = true;
					j = i;
				}
				for (int k = 0; k < levelUp.Count; k++)
				{
					if (CheckOccurence(toparse, levelUp[k], ref i))
					{
						i = j;
						level[k]++;
						j = i;
					}
				}
				if (!CheckAllInferior(level, lvlmax))
				{
					ignore = true;
				}


				bool check = false;
				//					if (entered && ignore && CheckOccurence(toparse, ""))
				for (int k = 0; k < levelDown.Count; k++)
				{
					if ((CheckOccurence(toparse, levelDown[k], ref i)))
					{
						i = j;
						level[k]--;
						check = true;
					}
				}
				if (entered && CheckAllInferior(level, lvlmax))
				{
					ignore = false;
				}
				if (check)
				{
					;
				}
				i++;
			}
			return (false);
		}
		public static string GetBetween(string toparse, string s1, string s2, string trim, bool leveling, string levelUp, string levelDown, int lvlmax)
		{
			int i = 0;
			string res = "";
			bool entered = false;
			int level = 0;
			int tmp = 0;
			int j = 0;
			List<char> tmpres = new List<char>();
			while (i < toparse.Length)
			{
				if (!CheckOccurence(toparse, trim, ref i))
				{
					j = i;
					if (!entered && CheckOccurence(toparse, s1, ref i))
					{
						entered = true;
						j = i;
					}
					if (entered && leveling && CheckOccurence(toparse, levelUp, ref i))
					{
						i = j;
						level++;
						j = i;
					}
					if (entered && ((!leveling && CheckOccurence(toparse, s2, ref i)) || (leveling && CheckOccurence(toparse, levelDown, ref i))))
					{
						if (leveling && level > lvlmax)
						{
							i = j;
							level--;
						}
					}
					else if (entered && (leveling && level <= lvlmax && CheckOccurence(toparse, s2, ref i)))
					{
						return (new String(tmpres.ToArray()));
					}
					else if (CheckOccurence(toparse, trim, ref i))
					{
						continue;
					}
					if (entered)
					{
						tmpres.Add(toparse[i]);
					}
					i++;

				}
			}
			res = new String(tmpres.ToArray());
			if (res == "")
				return (toparse);
			return (res);
		}
		private static bool CheckAllInferior(int[] lvls, int lvlMax)
		{
			for (int i = 0; i < lvls.Length; i++)
			{
				if (lvls[i] > lvlMax)
				{
					return (false);
				}
			}
			return (true);
		}




		public static string GetBetween(string toparse, string s1, string s2, string trim, bool leveling, string levelUp, string levelDown)
		{
			int i = 0;
			string res = "";
			bool entered = false;
			int level = 0;
			List<char> tmpres = new List<char>();
			while (i < toparse.Length)
			{
				if (!CheckOccurence(toparse, trim, ref i))
				{
					if (!entered && CheckOccurence(toparse, s1, ref i))
					{
						entered = true;
					}
					if (entered && leveling && CheckOccurence(toparse, levelUp, ref i))
					{
						level++;
					}
					if (entered && ((!leveling && CheckOccurence(toparse, s2, ref i)) || (leveling && CheckOccurence(toparse, levelDown, ref i))))
					{
						if (leveling && level > 0)
						{
							level--;
						}
						else
						{
							return (new String(tmpres.ToArray()));
						}
					}
					else if (CheckOccurence(toparse, trim, ref i))
					{
						continue;
					}
					else if (entered && (!leveling || (leveling && level == 0)))
					{
						tmpres.Add(toparse[i]);
						//res += toparse[i];
					}
					i++;

				}
			}
			res = new String(tmpres.ToArray());
			if (res == "")
				return (toparse);
			return (res);
		}
		public static string GetBetween(string toparse, string s1, string s2, string trim, bool leveling)
		{
			int i = 0;
			string res = "";
			bool entered = false;
			int level = 0;
			List<char> tmpres = new List<char>();
			while (i < toparse.Length)
			{
				if (!CheckOccurence(toparse, trim, ref i))
				{
					if (!entered && CheckOccurence(toparse, s1, ref i))
					{
						entered = true;
					}
					if (entered && leveling && CheckOccurence(toparse, s1, ref i))
					{
						level++;
					}
					if (entered && CheckOccurence(toparse, s2, ref i))
					{
						if (leveling && level > 0)
						{
							level--;
						}
						else
						{
							return (new String(tmpres.ToArray()));
						}
					}
					else if (CheckOccurence(toparse, trim, ref i))
					{
						continue;
					}
					else if (entered && (!leveling || (leveling && level == 0)))
					{
						tmpres.Add(toparse[i]);
						//res += toparse[i];

					}
					i++;

				}
			}
			res = new String(tmpres.ToArray());
			if (res == "")
				return (toparse);
			return (res);
		}

		public static string GetBetween(string toparse, string separator)
		{
			int i = 0;
			string res = "";
			List<char> tmpres = new List<char>();
			bool entered = false;
			while (i < toparse.Length)
			{
				if (!entered && CheckOccurence(toparse, separator, ref i))
				{
					entered = true;
				}
				if (entered && CheckOccurence(toparse, separator, ref i))
				{
					return (new String(tmpres.ToArray()));
				}
				else if (entered)
				{
					tmpres.Add(toparse[i]);
					//					res += toparse[i];
				}
				i++;
			}
			res = new String(tmpres.ToArray());
			if (res == "")
				return (toparse);
			return (res);
		}

		public static List<string> Split(string tosplit, string separator)
		{
			int i = 0;
			int j = 0;
			int lastIndex = 0;
			List<string> res = new List<string>();
			while (i < tosplit.Length)
			{
				if (tosplit[i] == separator[0])
				{
					j = i;
					if (CheckOccurence(tosplit, separator, ref i))
					{
						string substring = tosplit.Substring(lastIndex, j - lastIndex);
						res.Add(substring);
						lastIndex = i;
					}
				}
				i++;
			}
			if (i != lastIndex)
			{
				res.Add(tosplit.Substring(lastIndex, tosplit.Length - lastIndex));
			}
			return (res);
		}
		public static List<string> Split(string tosplit, string separator, List<string> lvlUp, List<string> lvlDown, int lvlMax)
		{
			int i = 0;
			int[] lvl = new int[lvlUp.Count];
			int lastIndex = 0;
			List<string> res = new List<string>();
			while (i < tosplit.Length)
			{
				int k = i;
				if (tosplit[i] == separator[0] && CheckAllInferior(lvl, lvlMax))
				{
					int j = i;
					if (CheckOccurence(tosplit, separator, ref i))
					{
						//Debug.Log(logPath,"???");
						string substring = tosplit.Substring(lastIndex, j - lastIndex);
						res.Add(substring);
						lastIndex = i;
						continue;
					}

				}
				k = i;
				for (int l = 0; l < lvlUp.Count; l++)
				{
					if (CheckOccurence(tosplit, lvlUp[l], ref i))
					{
						i = k;
						lvl[l]++;
					}
				}
				for (int l = 0; l < lvlDown.Count; l++)
				{
					if (CheckOccurence(tosplit, lvlDown[l], ref i))
					{
						i = k;
						lvl[l]--;
					}
				}
				i++;
			}
			if (lastIndex < tosplit.Length)
			{
				res.Add(tosplit.Substring(lastIndex, tosplit.Length - lastIndex));
			}
			if (res.Count == 0)
			{
				res.Add(tosplit);
			}
			return (res);
		}
		public static List<string> Split(string tosplit, string separator, string lvlUp, string lvlDown, int lvlMax)
		{
			int i = 0;
			int lvl = 0;
			int lastIndex = 0;
			List<string> res = new List<string>();
			while (i < tosplit.Length)
			{
				int k = i;
				if (tosplit[i] == separator[0])
				{
					int j = i;
					if (lvl <= lvlMax && CheckOccurence(tosplit, separator, ref i))
					{
						//Debug.Log(logPath,"???");
						string substring = tosplit.Substring(lastIndex, j - lastIndex);
						res.Add(substring);
						lastIndex = i;
						continue;
					}

				}
				k = i;
				if (CheckOccurence(tosplit, lvlUp, ref i))
				{
					i = k;
					lvl++;
				}
				if (CheckOccurence(tosplit, lvlDown, ref i))
				{
					i = k;
					lvl--;
				}
				i++;
			}
			if (res.Count == 0)
			{
				res.Add(tosplit);
			}
			return (res);
		}
		public static bool CheckOccurence(string tosplit, string separator, ref int index)
		{
			//Debug.Log(logPath,"tosplit:" + tosplit.Substring(index, tosplit.Length - index) + ", separator:" + separator);
			int lastIndex = index;
			for (int i = lastIndex; i < tosplit.Length; i++)
			{
				//Debug.Log(logPath,"???");
				if (i - lastIndex >= separator.Length)
				{
					index = i;
					return (true);
				}
				if (tosplit[i] != separator[i - lastIndex])
					return (false);
			}
			if (index < tosplit.Length && (tosplit.Length - index) == separator.Length)
			{
				index = tosplit.Length;
				return (true);
			}
			return (false);
		}

		public static string Replace(string toparse, List<string> keys, List<string> values)
		{
			int i = 0;
			string res = "";
			bool entered = false;
			int len = toparse.Length;
			List<char> tmpres = new List<char>();
			string current = toparse;
			int k = 0;
			while (i < len)
			{
				bool check = true;
				for (int l = 0; l < keys.Count; l++)
				{
					k = i;
					if (current[i] != keys[l][0] || !CheckOccurence(current, keys[l], ref i))
					{
						;
					}
					else
					{
						check = false;
						char[] tmpbuf = (values[l]).ToCharArray();
						for (int j = 0; j < tmpbuf.Length; j++)
						{
							tmpres.Add(tmpbuf[j]);
						}
						current = current.Substring(0, k) + current.Substring(i, current.Length - i);
						len = current.Length;
						i = k;
					}
					if (i >= len)
						break;
				}
				if (check && i < len)
				{
					tmpres.Add(current[i]);
					i++;
				}
			}
			res = new String(tmpres.ToArray());
			if (res == "")
			{
				return (toparse);
			}
			return (res);
		}
		public static string Replace(string toparse, string key, string value)
		{
			int i = 0;
			string res = "";
			bool entered = false;
			int len = toparse.Length;
			List<char> tmpres = new List<char>();
			string current = toparse;
			int k = 0;
			while (i < len)
			{
				k = i;
				if (current[i] != key[0] || !CheckOccurence(current, key, ref i))
				{
					tmpres.Add(current[i]);
					i++;
				}
				else
				{
					char[] tmpbuf = (value).ToCharArray();
					for (int j = 0; j < tmpbuf.Length; j++)
					{
						tmpres.Add(tmpbuf[j]);
					}
					current = current.Substring(0, k) + current.Substring(i, current.Length - i);
					len = current.Length;
					i = k;
				}
			}
			res = new String(tmpres.ToArray());
			if (res == "")
			{
				return (toparse);
			}
			return (res);
		}
		public static string GetBetween(string toparse, string s1, string s2, string trim, bool leveling, List<string> levelUp, List<string> levelDown, int lvlmax)
		{
			//Debug.Log(logPath,"TOPARSE:" + toparse);
			int i = 0;
			string res = "";
			bool entered = false;
			int[] level = new int[levelUp.Count];
			int tmp = 0;
			int j = 0;
			List<char> tmpres = new List<char>();
			bool ignore = false;
			while (i < toparse.Length)
			{
				if (!CheckOccurence(toparse, trim, ref i))
				{
					j = i;
					if (!entered && CheckOccurence(toparse, s1, ref i))
					{
						entered = true;
						j = i;
					}
					if (entered && (leveling && CheckAllInferior(level, lvlmax) && CheckOccurence(toparse, s2, ref i)))
					{
						return (new String(tmpres.ToArray()));
					}
					for (int k = 0; k < levelUp.Count; k++)
					{
						if (entered && leveling && CheckOccurence(toparse, levelUp[k], ref i))
						{
							i = j;
							level[k]++;
							j = i;
						}
					}
					bool check = false;
					//					if (entered && ignore && CheckOccurence(toparse, ""))
					if (!ignore)
					{
						for (int k = 0; k < levelDown.Count; k++)
						{
							if (entered && ((!leveling && CheckOccurence(toparse, s2, ref i)) || (leveling && CheckOccurence(toparse, levelDown[k], ref i))))
							{
								if (leveling)
								{
									if (level[k] > lvlmax)
										i = j;
									level[k]--;
								}
								check = true;
							}
						}
					}
					if (check)
					{
						;
					}
					else if (CheckOccurence(toparse, trim, ref i))
					{
						continue;
					}
					if (entered && i < toparse.Length)
					{
						tmpres.Add(toparse[i]);
						//Debug.Log(logPath,toparse[i]);
						//res += toparse[i];
					}
					i++;
				}
			}
			res = new String(tmpres.ToArray());
			if (res == "")
				return (toparse);
			return (res);
		}
		public static string GetBetween(string toparse, string s1, string s2)
		{
			int i = 0;
			string res = "";
			List<char> tmpres = new List<char>();
			bool entered = false;
			while (i < toparse.Length)
			{
				if (!entered && CheckOccurence(toparse, s1, ref i))
				{
					//Debug.Log(logPath,"Entered:" + toparse.Substring(i, toparse.Length - i));
					entered = true;
				}
				if (entered)
				{
					;//Debug.Log(logPath,"WHAT???");
				}
				if (entered && CheckOccurence(toparse, s2, ref i))
				{
					//Debug.Log(logPath,"MaisALors???");
					return (new String(tmpres.ToArray()));
				}
				else
				{
					if (entered)
					{
						tmpres.Add(toparse[i]);
						//Debug.Log(logPath,"HEYOOHHHH");
						//res += toparse[i];
						//Debug.Log(logPath,"res:" + res);
					}
				}
				i++;

			}
			res = new String(tmpres.ToArray());
			if (res == "")
				return (toparse);
			return (res);
		}
		public static string Encapsulate(string toparse, string s1, string s2)
		{
			return (s1 + toparse + s2);
		}
	}
}

