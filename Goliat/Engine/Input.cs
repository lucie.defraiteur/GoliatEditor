﻿using OpenTK.Input;
using Goliat;
using FastMember;
using System;
using System.Collections.Generic;
using System.Collections;
using Keyboard = OpenTK.Input.Keyboard;
using Key = OpenTK.Input.Key;

/*

Ce projet appartient au groupe Goliat

Introduction:

	Non pas seulement sous la loi, mais sous ma demande de respect envers travail accompli, (j ai fait toute cette librairie seul, et uniquement seul.)

Je ne demande pas qu on evite a tout prix d utiliser mes sources pour des utilisations commerciales, mais un minimum de participation active à votre apprentissage personnel,
et votre avancée dans votre propre vie, si vous modifiez mes sources, assez bien parceque vous avez pu comprendre, pas de souci, debrouillez vous comme vous pouvez.
Ne tentez surtout pas le diable, evitez de chercher seuls a me depasser, me voler mon travail.

PS:
enjoy good work.
be happy.

You can:
Share it, copy it, distribute it and communicate about this product by all the ways and under any formats.
Adapt it, remix it, transform it and create things with it.
the bidder cant remove the rights given by the license whenever you follow the terms of this license.

Under thoose conditions:

No Commercial Use - You can't sell it, part of it, or all of it, part or all of it. But make good training for you.

Vous etes authorisés à:
Partager — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
Adapter — remixer, transformer et créer à partir du matériel
L'Offrant ne peut retirer les autorisations concédées par la licence tant que vous appliquez les termes de cette licence.
Selon les conditions suivantes :

Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.

Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.
No additional restrictions — Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence.

Notes:

Vous n'êtes pas dans l'obligation de respecter la licence pour les éléments ou matériel appartenant au domaine public ou dans le cas où l'utilisation que vous souhaitez faire est couverte par une exception.
Aucune garantie n'est donnée. Il se peut que la licence ne vous donne pas toutes les permissions nécessaires pour votre utilisation. Par exemple, certains droits comme les droits moraux, le droit des données personnelles et le droit à l'image sont susceptibles de limiter votre utilisation.

Si vraiment vous en etes capable, faire votre propre version ne devrait pas etre trop difficile xP, amusez vous bien.
Peace.

For all the doubts, there is only one hope: win, win and not loose.

*/

namespace Goliat
{

	public enum LogType
	{
		Activated,
		Down,
		Up,
		Off
	}

	public class LogInfo
	{
		public LogType type;
		public static implicit operator bool(LogInfo instance)
		{

			return (instance.type != LogType.Off);
		}
		public static LogInfo Off = new LogInfo(LogType.Off);
		public LogInfo(LogType type)
		{
			this.type = type;
		}
		public static implicit operator LogType(LogInfo instance)
		{
			return (instance.type);
		}
		public LogInfo()
		{
			;
		}
	}

	public class MouseLog
	{
		public Dictionary<int, LogInfo> keyValue = new Dictionary<int, LogInfo>();
		public dynamic this[int mouseIndex]
		{
			get
			{
				var mouse = Mouse.GetState();
				LogInfo log;
				if (keyValue.TryGetValue(mouseIndex, out log) && mouse[(OpenTK.Input.MouseButton)mouseIndex])
				{
					return (this.keyValue[mouseIndex].type);
				}
				else if (mouse[(OpenTK.Input.MouseButton)mouseIndex])
				{
					this.keyValue[mouseIndex] = new LogInfo();
					this.keyValue[mouseIndex].type = LogType.Down;
					return (this.keyValue[mouseIndex].type);
				}
				else if (keyValue.TryGetValue(mouseIndex, out log) && !mouse[(OpenTK.Input.MouseButton)mouseIndex])
				{
					return (LogType.Up);
				}
				return (LogInfo.Off);
			}
		}
	}


	public class KeyLog
	{
		public Dictionary<int, LogInfo> keyValue = new Dictionary<int, LogInfo>();
		public dynamic this[int keyIndex]
		{
			get
			{
				var state = OpenTK.Input.Keyboard.GetState();
				LogInfo log;
				if (keyValue.TryGetValue(keyIndex, out log) && state[(short)keyIndex])
				{
					//this.keyValue[keyIndex].type = LogType.Activated;
					return (this.keyValue[keyIndex].type);
				}
				else if (state[(short)keyIndex])
				{
					this.keyValue[keyIndex] = new LogInfo();
					this.keyValue[keyIndex].type = LogType.Down;
					return (this.keyValue[keyIndex].type);
				}
				else if (keyValue.TryGetValue(keyIndex, out log) && !state[(short)keyIndex])
				{
					//this.keyValue[keyIndex] = new LogInfo();
					//this.keyValue[keyIndex].type = LogType.Up;
					//LogInfo tmp = this.keyValue[keyIndex];
					//keyValue.Remove(keyIndex);
					return (LogType.Up);
				}
				return (LogInfo.Off);
			}
		}
	}

	public class KeyReg
	{
		public KeyLog Log = new KeyLog();
		public bool ButtonDown(int mouseIndex)
		{
			//var mouse = Keyboard.GetState();
			if (mouseIndex >= 0)
			{
				return (Log[mouseIndex] == LogType.Down);
			}
			return (false);
		}
		private KeyLog activatedsMouseButtons;
		public bool Button(int mouseIndex)
		{
			//var mouse = Keyboard.GetState();
			if (mouseIndex >= 0)
			{
				return (this.Log[mouseIndex] != LogType.Off);
			}
			return (false);
		}
		public bool ButtonUp(int mouseIndex)
		{
			if (mouseIndex >= 0)
			{
				return (this.Log[mouseIndex] == LogType.Up);
			}
			return (false);
		}
		public dynamic GetState()
		{
			return (OpenTK.Input.Keyboard.GetState());
		}

		/*{
			get
			{
				//var mouse = Mouse.GetState();
				return (this._position);
			}
		}*/

		public bool ButtonDown(Key mouseIndex)
		{
			//var mouse = Keyboard.GetState();
			if ((short)mouseIndex >= 0)
			{
				return (Log[(short)mouseIndex] == LogType.Down);
			}
			return (false);
		}
		public bool Button(Key mouseIndex)
		{
			//var mouse = Keyboard.GetState();
			if ((short)mouseIndex >= 0)
			{
				return (this.Log[(short)mouseIndex] != LogType.Off);
			}
			return (false);
		}
		public bool ButtonUp(Key mouseIndex)
		{
			if ((short)mouseIndex >= 0)
			{
				return (this.Log[(short)mouseIndex] == LogType.Up);
			}
			return (false);
		}


		public bool this[int index]
		{
			get
			{
				if (index >= 0)
					return (this.Log[index] != LogType.Off);
				return (false);
			}
		}
	}

	public class MouseReg
	{
		private Vector2 lastPos = Vector2.zero;
		private Vector2 _axis = Vector2.zero;
		public Vector2 axis
		{
			get
			{
				return (this._axis);
			}
		}
		public void UpdateAxis()
		{
			_axis = this.position - lastPos;
			lastPos = this.position;
		}
		public void Update()
		{
			UpdateWheel();
			UpdateAxis();
			this.position = new Vector2(System.Windows.Forms.Cursor.Position.X, System.Windows.Forms.Cursor.Position.Y);
		}
		public void UpdateWheel()
		{
			_wheelVar = lastWheel - Wheel;
			lastWheel = Wheel;
		}
		private float lastWheel = Mouse.GetState().WheelPrecise;
		public float Wheel
		{
			get
			{
				var mouse = Mouse.GetState();
				return (mouse.WheelPrecise);
			}
		}
		private float _wheelVar = 0;
		public float WheelVariation
		{
			get
			{
				return _wheelVar;
			}
		}
		public Vector2 position = Vector2.zero;
		public MouseLog Log = new MouseLog();
		public bool ButtonDown(int mouseIndex)
		{
			//var mouse = Mouse.GetState();
			if (mouseIndex >= 0)
			{
				return (this.Log[mouseIndex] == LogType.Down);
			}
			return (false);
		}
		public bool Button(int mouseIndex)
		{
			//var mouse = Mouse.GetState();
			if (mouseIndex >= 0)
			{
				return (this.Log[mouseIndex] != LogType.Off);
			}
			return (false);
		}
		public bool ButtonUp(int mouseIndex)
		{
			//var mouse = Mouse.GetState();
			if (mouseIndex >= 0)
			{
				return (this.Log[mouseIndex] == LogType.Activated);
			}
			return (false);
		}

		public bool ButtonDown(MouseButton mouseIndex)
		{
			//var mouse = Mouse.GetState();
			if ((short)mouseIndex >= 0)
			{
				return (this.Log[(short)mouseIndex] == LogType.Down);
			}
			return (false);
		}
		private KeyLog activatedsMouseButtons;
		public bool Button(MouseButton mouseIndex)
		{
			//var mouse = Mouse.GetState();
			if ((short)mouseIndex >= 0)
			{
				return (this.Log[(short)mouseIndex] != LogType.Off);
			}
			return (false);
		}
		public bool ButtonUp(MouseButton mouseIndex)
		{
			//var mouse = Mouse.GetState();
			if ((short)mouseIndex >= 0)
			{
				return (this.Log[(short)mouseIndex] == LogType.Activated);
			}
			return (false);
		}

		public dynamic GetState()
		{
			return (OpenTK.Input.Mouse.GetState());
		}
		public Ray toRay(Camera camera)
		{
			return (camera.ScreenPointToRay(this.position));
		}


		public bool this[int index]
		{
			get
			{
				if (index >= 0)
					return (this.Log[index] != LogType.Off);
				return (false);
			}

		}



		public Ray ray
		{
			get
			{
				if (Camera.main != null)
				{
					return (Camera.main.ScreenPointToRay(this.position));
				}
				else
				{
					return (new Ray(Vector3.zero, Vector3.zero));
				}
			}
		}
	}

	public class Input
	{
		public static KeyReg Keyboard = new KeyReg();
		public static MouseReg Mouse = new MouseReg();
		// a update a la fin d'un tour de boucle principale uniquement.
		//
		public static void Update()
		{
			var arrayOfAllKeys = ((Dictionary<int, LogInfo>)Keyboard.Log.keyValue).Keys;
			var arrayOfAllValues = Keyboard.Log.keyValue.Values;
			var state = OpenTK.Input.Keyboard.GetState();
			List<int> toRemove = new List<int>();
			Mouse.Update();
			foreach (int keyIndex in arrayOfAllKeys)
			{
				LogInfo log;
				if (Keyboard.Log.keyValue.TryGetValue(keyIndex, out log) && state[(short)keyIndex])
				{
					Keyboard.Log.keyValue[keyIndex].type = LogType.Activated;
				}
				else if (state[(short)keyIndex])
				{
					Keyboard.Log.keyValue[keyIndex] = new LogInfo();
					Keyboard.Log.keyValue[keyIndex].type = LogType.Down;
				}
				else if (Keyboard.Log.keyValue.TryGetValue(keyIndex, out log) && !state[(short)keyIndex])
				{
					//Keyboard.Log.keyValue[keyIndex] = new LogInfo();
					//Keyboard.Log.keyValue[keyIndex].type = LogType.Up;
					//LogInfo tmp = Keyboard.Log.keyValue[keyIndex];
					//Keyboard.Log.keyValue.Remove(keyIndex);
					toRemove.Add(keyIndex);
				}
			}
			for (int i = 0; i < toRemove.Count; i++)
			{
				Keyboard.Log.keyValue.Remove(toRemove[i]);
			}

			arrayOfAllKeys = ((Dictionary<int, LogInfo>)Mouse.Log.keyValue).Keys;
			arrayOfAllValues = Mouse.Log.keyValue.Values;
			var mouse = Mouse.GetState();
			toRemove = new List<int>();
			foreach (int keyIndex in arrayOfAllKeys)
			{
				//var mouse = OpenTK.Input.Mouse.GetState();
				LogInfo log;
				if (Mouse.Log.keyValue.TryGetValue(keyIndex, out log) && mouse[(OpenTK.Input.MouseButton)keyIndex])
				{
					Mouse.Log.keyValue[keyIndex].type = LogType.Activated;
				}
				else if (mouse[(OpenTK.Input.MouseButton)keyIndex])
				{
					Mouse.Log.keyValue[keyIndex] = new LogInfo();
					Mouse.Log.keyValue[keyIndex].type = LogType.Down;
				}
				else if (Mouse.Log.keyValue.TryGetValue(keyIndex, out log) && !mouse[(OpenTK.Input.MouseButton)keyIndex])
				{
					//Mouse.Log.keyValue[keyIndex] = new LogInfo();
					//Mouse.Log.keyValue[keyIndex].type = LogType.Up;
					//LogInfo tmp = Mouse.Log.keyValue[keyIndex];

					toRemove.Add(keyIndex);
				}
			}
			for (int i = 0; i < toRemove.Count; i++)
			{
				Mouse.Log.keyValue.Remove(toRemove[i]);
			}
		}

		//public static Mouse mouse; 
		public static bool GetMouseButtonDown(int mouseIndex)
		{
			//var mouse = OpenTK.Input.Mouse.GetState();
			if (mouseIndex >= 0)
			{
				return (Input.Mouse.Log[mouseIndex] == LogType.Down);
			}
			return (false);
		}

		public static bool GetMouseButton(int mouseIndex)
		{
			//var mouse = OpenTK.Input.Mouse.GetState();
			if (mouseIndex >= 0)
			{
				return (Input.Mouse.Log[mouseIndex] != LogType.Off);
			}
			return (false);
		}
		public static bool GetMouseButtonUp(int mouseIndex)
		{
			//var mouse = OpenTK.Input.Mouse.GetState();
			if (mouseIndex >= 0)
			{
				return (Input.Mouse.Log[mouseIndex] == LogType.Up);
			}
			return (false);
		}
		public static bool GetKey(int keyindex)
		{
			//var keyboard = OpenTK.Input.Keyboard.GetState();
			if (keyindex >= 0)
			{
				return (Input.Keyboard.Log[keyindex] != LogType.Off);
			}
			return (false);
		}
		public static bool GetKeyDown(int keyindex)
		{
			//var keyboard = OpenTK.Input.Keyboard.GetState();
			//Console.WriteLine("OHHHHHHHHH");
			//Debug.Log("dafuk");
			if (keyindex >= 0)
			{
				//Debug.Log("HEY");
				return (Input.Keyboard.Log[keyindex] == LogType.Down);
			}
			return (false);
		}
		public static bool GetKeyUp(int keyindex)
		{
			//var keyboard = OpenTK.Input.Keyboard.GetState();
			if (keyindex >= 0)
			{
				return (Input.Keyboard.Log[keyindex] == LogType.Up);
			}
			return (false);
		}



		public static bool GetMouseButtonDown(MouseButton mouseIndex)
		{
			//var mouse = OpenTK.Input.Mouse.GetState();
			if (mouseIndex >= 0)
			{
				return (Input.Mouse.Log[(short)mouseIndex] == LogType.Down);
			}
			return (false);
		}

		public static bool GetMouseButton(MouseButton mouseIndex)
		{
			//var mouse = OpenTK.Input.Mouse.GetState();
			if (mouseIndex >= 0)
			{
				return (Input.Mouse.Log[(short)mouseIndex] != LogType.Off);
			}
			return (false);
		}
		public static bool GetMouseButtonUp(MouseButton mouseIndex)
		{
			//var mouse = OpenTK.Input.Mouse.GetState();
			if (mouseIndex >= 0)
			{
				return (Input.Mouse.Log[(short)mouseIndex] == LogType.Up);
			}
			return (false);
		}
		public static bool GetKey(Key keyindex)
		{
			//var keyboard = OpenTK.Input.Keyboard.GetState();
			if (keyindex >= 0)
			{
				return (Input.Keyboard.Log[(short)keyindex] != LogType.Off);
			}
			return (false);
		}
		public static bool GetKeyDown(Key keyindex)
		{
			//var keyboard = OpenTK.Input.Keyboard.GetState();
			if (keyindex >= 0)
			{
				return (Input.Keyboard.Log[(short)keyindex] == LogType.Down);
			}
			return (false);
		}
		public static bool GetKeyUp(Key keyindex)
		{
			//var keyboard = OpenTK.Input.Keyboard.GetState();
			if (keyindex >= 0)
			{
				return (Input.Keyboard.Log[(short)keyindex] == LogType.Up);
			}
			return (false);
		}
	}
}