﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goliat.Engine.GUI;
namespace Goliat
{
	public enum AnimationBlendMode
	{
		Blend,
		Additive,
		Mixed
	}
	
	public enum AnimationCullingType
	{
		AlwaysAnimate,
		BasedOnRenderers,
		BasedOnClipBounds
	}
	public enum WrapMode
	{
		Once,
		Loop,
		PingPong,
		Default,
		ClampForever
	}
	public enum PlayMode
	{
		StopSameLayer,
		StopAll,
		Continue,
		Resume
	}
	public enum QueueMode
	{
		CompleteOthers,
		PlayNow
	}
	[Goliat.Serializable]
	[System.Serializable]
	public class AnimationState
	{
		public AnimationBlendMode blendMode = AnimationBlendMode.Blend;
		public readonly AnimationClip clip = null;
		private bool _way = true;
		[Goliat.NonSerialized]
		public bool way
		{
			get
			{
				return (_way);
			}
			set
			{
				if (_way != value)
				{
					this._startTime = (Time.time / 1000) - ((this.length - this.time ));
				}
				_way = value;
			}
		}

		private bool _enabled = false;
		[Goliat.NonSerialized]
		public bool enabled
		{
			get
			{
				return (this._enabled);
			}
			set
			{
				this._startTime = (Time.time / 1000);
				this._enabled = value;
			}
		}
		[Goliat.NonSerialized]
		public float length
		{
			get
			{
				return (this.clip.length * speed);
			}
		}
		public string name = "";
		public float speed = 1;
		private float _startTime = 0.0f;
		[Goliat.NonSerialized]
		public float time
		{
			get
			{
				float res = ((Time.time / 1000) - _startTime);
				if (res > length)
				{
					this._startTime = Time.time / 1000;
//					this._startTime = this.getStartTime(0);
					Console.WriteLine("new - start:" + ((Time.time / 1000) - _startTime));
					Console.WriteLine("new Start time!" + this._startTime);
					Console.WriteLine("wrap mode:" + this.wrapMode);
				}
				return (res);
			}
			set
			{
				this._startTime = Time.time - value;
			}
		}
		public float weight = 0;
		public WrapMode wrapMode = WrapMode.Default;
		public AnimationState(AnimationClip clip)
		{
			this.clip = clip;
			this.name = clip.name;
			this.speed = 1;
			this.time = 0;
			this.weight = 0;
			this.wrapMode = clip.wrapMode;
		}
		private float getStartTime( int recursion)
		{
			float currentTime = ((Time.time / 1000) - _startTime);
			AnimationState state = this;
			float res = 0;
			WrapMode wrapMode = WrapMode.Default;
			switch (recursion)
			{
				case 0:
					wrapMode = this.wrapMode;
					break;
				case 1:
					wrapMode = this.wrapMode;
					break;
				case 2:
					wrapMode = this.clip.wrapMode;
					break;
				case 3:
					return ((Time.time / 1000) - ((currentTime) - this.length));
			}

			switch (state.wrapMode)
			{
				case WrapMode.ClampForever:
					return ((Time.time / 1000) - this.length);
				case WrapMode.Default:
					return (getStartTime(recursion + 1));
				case WrapMode.Loop:
					Console.WriteLine("i remove:" + ((currentTime) - this.length) + "from time");
					return ((Time.time / 1000) - ((currentTime) - this.length));
				case WrapMode.Once:
					this._enabled = false;
					//this._time = 0;
					return (0);
				case WrapMode.PingPong:
					this._way = (this._way ? false : true);
					return ((Time.time / 1000) - (this.length - (((currentTime) - this.length))));
			}
			return (res);
		}
	}
	[System.Serializable]
	[Goliat.Serializable]
	class Animation : GameBehaviour, IEditable
	{
		public void OnInspectorGUI()
		{
			this._current.speed = InspectorGUI.FloatField("SPEED", this._current.speed);
		}
		//private Dictionary<>
		public void UpdateState(AnimationState state)
		{
			if (state.enabled && state.clip != null)
			{
				if (state.time > state.length)
				{
					;
					//float time = getTime(state, 0);
					
				}
				//float realTime = state.way ? state.time : state.length - state.time;
				//state.ticks = 
				//float realTime = state.way ? state.time : state.length - state.time;

			}
		}
		// attention timeinTicks pour chaque animation pas en secondes, donc faut convertir avant usage, et pour les clés aussi, en temps en secondes, comme ça fuck cette histoire de tick.
		public override void Update()
		{
			base.Update();
			bool check = false;
			Dictionary<string, List<AnimationState>> affectedStates = new Dictionary<string, List<AnimationState>>();
			//Console.WriteLine("0.0");
			if (this.states == null)
			{
				Console.WriteLine("this states null");
			}
			foreach (AnimationState state in this.states.Values)
			{
				//Console.WriteLine("here is a state:" + state.name);
				if (state.enabled)
				{
					//Console.WriteLine("state:" + state.name + " is enabled");
					//UpdateState(state);
					for (int i = 0; i < state.clip.affectedBones.Count; i++)
					{
						affectedStates.AddOrRetrieve((string)state.clip.affectedBones[i], (List<AnimationState>)new List<AnimationState>());
						affectedStates[state.clip.affectedBones[i]].Add(state);
					}
					check = true;
				}
				
			} //erreur ici quand désérialization.
			//Console.WriteLine("0.1");
			//Console.WriteLine("sqfnhfklnsd");
			/*if (this.clip == null)
			{
				//Console.WriteLine("this clip null");
			}*/
			//Console.WriteLine("0.1.01");
			if (!check && this.clip != null)
			{
				//Console.WriteLine("0.1.0");
				/*if (this.clip.name == null)
				{
					Console.WriteLine("oh yea!");
				}
				if (this.states == null)
				{
					Console.WriteLine("states null");
				}
				else if (!this.states.ContainsKey(this.clip.name))
				{
					Console.WriteLine("does not contains key");
				}
				Console.WriteLine("0.1.1");
				*/
				this.states[this.clip.name].enabled = true;
				this.states[this.clip.name].weight = 1;
				this.states[this.clip.name].wrapMode = WrapMode.Loop;
				//Console.WriteLine("hey");
				//Console.WriteLine("0.2");
				for (int i = 0; i < this.states[this.clip.name].clip.affectedBones.Count; i++)
				{
					affectedStates.AddOrRetrieve(this.states[this.clip.name].clip.affectedBones[i], (List<AnimationState>)new List<AnimationState>());
					affectedStates[this.states[this.clip.name].clip.affectedBones[i]].Add(this.states[this.clip.name]);
				}
				//Console.WriteLine("0.3");
				//Console.WriteLine("oh");
				
			}
			//Console.WriteLine("merde");
			//Console.WriteLine("1.0");

			foreach (string boneName in affectedStates.Keys)
			{
				// euh pb yen a plus qu'un quand je fais ça gros truc louche ici alors.
				//Console.WriteLine("only bone :" + boneName);
				//Console.WriteLine("this.bone[bonename].name =" + this.bones[boneName].name);
				float totalWeight = 0;
				for (int i = 0; i < affectedStates[boneName].Count; i++)
				{
					totalWeight += affectedStates[boneName][i].weight / affectedStates[boneName].Count;
				}
				Dictionary<AnimationBlendMode, List<AnimationStep>> steps = new Dictionary<AnimationBlendMode, List<AnimationStep>>();
				for (int i = 0; i < affectedStates[boneName].Count; i++)
				{
					AnimationState state = affectedStates[boneName][i];
					float realtime = (state.time * state.speed) / state.clip.frameRate;
					//Console.WriteLine("0.0");
					Vector3 indexs = state.clip.animationDictionary[boneName].indexs(realtime);
					//Console.WriteLine("0.1");
					steps.AddOrRetrieve(state.blendMode, new List<AnimationStep>());
					//Console.WriteLine("blendMode:" + state.blendMode);
					steps[state.blendMode].Add(new AnimationStep(realtime, state.clip.animationDictionary[boneName], state.blendMode, state.weight / totalWeight));
				}
				steps.AddOrRetrieve(AnimationBlendMode.Blend, new List<AnimationStep>());
				steps.AddOrRetrieve(AnimationBlendMode.Additive, new List<AnimationStep>());
				steps.AddOrRetrieve(AnimationBlendMode.Mixed, new List<AnimationStep>());
				computeBoneTransformation(this.bones[boneName], steps);
			}
		}
		public void computeBoneTransformation(Bone bone, Dictionary<AnimationBlendMode, List<AnimationStep>> stepDictionary)
		{
			Quaternion resRotation = Quaternion.identity;
			Vector3 resPosition = Vector3.zero;
			Vector3 resScale = Vector3.one;
			bool check = false;
			float currentWeight = 0;
			float totalweight = 0;

			List<AnimationStep> steps = stepDictionary[AnimationBlendMode.Mixed];
			for (int i = 0; i < steps.Count; i++)
			{
				if (!check && i == 0)
				{
					resRotation = steps[0].rotation;
					resPosition = steps[0].position;
					resScale = steps[0].scale;
					currentWeight = steps[0].weight;
					totalweight = currentWeight;
					check = true;
					continue;
				}
				totalweight += steps[i].weight;
				if (currentWeight > steps[i].weight)
				{
					//Console.WriteLine("here is a weight:" + currentWeight / totalweight);
					resRotation = Quaternion.Slerp(resRotation, steps[i].rotation, 1 - (currentWeight / totalweight));
					resPosition = Vector3.Slerp(resPosition, steps[i].position, 1 - (currentWeight / totalweight)); // ou lerp si trop long a computer
					resScale = Vector3.Slerp(resScale, steps[i].scale, 1 - (currentWeight / totalweight)); // idem
				}
				else
				{
					resRotation = Quaternion.Slerp(resRotation, steps[i].rotation, steps[i].weight / totalweight);
					resPosition = Vector3.Slerp(resPosition, steps[i].position, steps[i].weight / totalweight); // idem
					resScale = Vector3.Slerp(resScale, steps[i].scale, steps[i].weight / totalweight); // idem
				}
				currentWeight += steps[i].weight;
			}

			steps = stepDictionary[AnimationBlendMode.Blend];
			for (int i = 0; i < steps.Count; i++ )
			{
				if (!check && i == 0)
				{
					resRotation = steps[0].rotation;
					resPosition = steps[0].position;
					resScale = steps[0].scale;
					bone.localScale = resScale;
					
					bone.localRotation = resRotation;
					bone.localPosition = resPosition;
					currentWeight = steps[0].weight;
					totalweight = currentWeight;
					check = true;
					continue;
				}
				totalweight += steps[i].weight;
				resRotation = Quaternion.Nlerp(resRotation, steps[i].rotation, (currentWeight / totalweight));
				resPosition = Vector3.Nlerp(resPosition, steps[i].position, (currentWeight / totalweight));
				resScale = Vector3.Nlerp(resScale, steps[i].scale, (currentWeight / totalweight));
				currentWeight += steps[i].weight;
			}
			
			steps = stepDictionary[AnimationBlendMode.Additive];
			for (int i = 0; i < steps.Count; i++)
			{
				if (!check && i == 0)
				{
					resRotation = steps[0].rotation * steps[0].weight;
					resPosition = steps[0].position * steps[0].weight;
					resScale = steps[0].scale * steps[0].weight;
					currentWeight = steps[0].weight;
					totalweight = currentWeight;
					check = true;
					continue;
				}
				totalweight += steps[i].weight;
				resRotation *= (steps[i].rotation * steps[i].weight);
				resPosition += (steps[i].position * steps[i].weight);
				resScale += (steps[i].scale * steps[i].weight);
				currentWeight += steps[i].weight;
			}
			bone.localScale = resScale;
			bone.localRotation = resRotation;
			bone.localPosition = resPosition;
		}
			
		private void addBonesRecursively(Transform root)
		{
			for (int i = 0; i < root.children.Count; i++)
			{
				if (root.children[i].GetType().IsSameOrSubclass(typeof(Bone)))
				{
					this._bones.Add(root.children[i].name, (Bone)root.children[i]);
				}
				this.addBonesRecursively(root.children[i]);
			}
		}
		[Goliat.NonSerialized]
		protected Dictionary<string, Bone> _bones = null;
		
		[Goliat.NonSerialized]
		public Dictionary<string, Bone> bones
		{
			get
			{
				if (_bones == null)
				{
					this._bones = new Dictionary<string, Bone>();
					// /!\ pas cool, faut que ce soit fait seulement pour ceux concernés par l'animaiton sinon c'est chiant pas optimal.
					Transform root = this.GetComponent<Transform>();
				
					for (int i = 0; i < root.children.Count; i++)
					{
						if (root.children[i].GetType().IsSameOrSubclass(typeof(Bone)))
						{
							this._bones.Add(root.children[i].name, (Bone)root.children[i]);
						}
						this.addBonesRecursively(root.children[i]);
					}
				}
				return (this._bones);
				;
			}
			protected set
			{
				;
			}
		}
		
		

		//protected Dictionary<string, SkinnedMeshFilter> _filters = new Dictionary<string, SkinnedMeshFilter>();

		public Animation()
		{
			;
		}
		public Animation(AnimationInfo info)
		{
			if (info != null && info.clips != null && info.clips.Count > 0)
			{
				for (int i = 0; i < info.clips.Count; i++)
				{
					this.states.Add(info.clips[i].name, new AnimationState(info.clips[i]));
				}
				this.clip = info.clips[0];
			}
		}
		[Goliat.NonSerialized]
		public AnimationState this[string animationName]
		{
			get
			{
				return (this.states.ContainsKey(animationName) ? this.states[animationName] : null);
			}
			private set
			{
				this.states.AddOrReplace(animationName, value);
			}
		}
		[Goliat.NonSerialized]
		public List<AnimationClip> clips
		{
			get
			{
				List<AnimationClip> clips = new List<AnimationClip>();
				foreach (string key in this.states.Keys)
				{
					clips.Add(this.states[key].clip);
				}
				return (clips);
			}
		}
	
		private Dictionary<string, AnimationState> states = new Dictionary<string, AnimationState>();
		//private List<AnimationClip> clips = new List<AnimationClip>();
		//[Goliat.NonSerialized]
		public bool animatePhysics
		{
			get;
			set;
		}
	
		private AnimationState _current = null;
		public AnimationClip clip // le current? ouai.
		{
			get
			{
				if (this._current == null)
				{
					return (null);
				}
				return (this._current.clip);
			}
			set
			{
				this._current = (value != null && states.ContainsKey(value.name)) ? states[value.name] : this._current;
			}
		}

		public AnimationCullingType cullingType = AnimationCullingType.BasedOnRenderers;
		private bool _isPlaying = false;
		public bool isPlaying
		{
			get
			{
				return(this._isPlaying);
			}
			set
			{
				this._isPlaying = value;
			}
		}
		[Goliat.NonSerialized]
		public Bounds localBounds
		{
			get
			{
				return (new Bounds(Vector3.zero, Vector3.one));
			}
		}
		private bool _playAutomatically = false;
		public bool playAutomatically
		{
			get
			{
				return (this._playAutomatically);
			}
			set
			{
				this._playAutomatically = value;
			}
		}
		public WrapMode wrapMode = WrapMode.Default;
		public void Blend(string animation, float targetWeight = 1.0f, float fadeLength = 0.3f)
		{
			;
		}
		public void CrossFade(string animation, float fadeLength = 0.3f, PlayMode mode = PlayMode.StopSameLayer)
		{
			;
		}
		public void CrossFadeQueued(string animation, float fadeLength = 0.3f, PlayMode mode = PlayMode.StopSameLayer, QueueMode queue = QueueMode.CompleteOthers)
		{
			;
		}
		public bool IsPlaying(string animation) //  d ou la classe animation state, qui dit, par rapport a des clips qui eux sont simplement des resources, des stats. a savoir ou en est on de ce clip, est il playing, etc...
		{
			if (this.clip.name == animation)
				return (true);
			return (false);
		}
		public void Play(string animation, PlayMode mode = PlayMode.StopSameLayer)
		{
			if (mode == PlayMode.StopSameLayer || mode == PlayMode.StopAll)
			{
				this.Stop();
			}
			else if (mode == PlayMode.Continue)
			{
				if (this.states.ContainsKey(animation))
				{
					this.states[animation].time = 0;
					this.states[animation].enabled = true;
				}
			}
			else if (mode == PlayMode.Resume)
			{
				if (this.states.ContainsKey(animation))
				{
					this.states[animation].enabled = true;
				}
			}
			this._isPlaying = true; // si le wrapMode == default, bah on joue suivant le wrap mode du clip en cours. sinon on joue selon notre wrapmode.
		}
		public void Play(PlayMode mode = PlayMode.StopSameLayer)
		{
			this._isPlaying = true;
		}
		public void AddClip(AnimationClip clip)
		{
			if (!this.states.ContainsKey(clip.name))
			{
				this.states.Add(clip.name, new AnimationState(clip));
			}
		}
		public void RemoveClip(AnimationClip clip)
		{
			if (this.states.ContainsKey(clip.name))
			{
				this.states.Remove(clip.name);
			}
			;
		}
		public void Stop()
		{
			this.pausedStates = null;
			foreach (string key in this.states.Keys)
			{
				this.states[key].time = 0;
				this.states[key].enabled = false;
			}
		}
		public void Pause()
		{
			if (this.pausedStates != null)
			{
				int count = pausedStates.Count;
				foreach (string key in this.states.Keys)
				{
					if (this.states[key].enabled && !this.pausedStates.Contains(key))
					{
						this.pausedStates = new List<string>();
						break;
					}
					else if (this.states[key].enabled)
					{
						count--;
					}
					this.states[key].enabled = false;
				}
				if (count != 0)
				{
					this.pausedStates = new List<string>();
				}
				else
				{
					return;
				}
			}
			this.pausedStates = new List<string>();
			this._isPlaying = false;
			foreach (string key in this.states.Keys)
			{
				if (this.states[key].enabled)
				{
					this.pausedStates.Add(key);
				}
				this.states[key].enabled = false;
			}
		}
		public void Resume()
		{
			for (int i = 0; i < this.pausedStates.Count; i++)
			{
				this.states[this.pausedStates[i]].enabled = true;
			}
			this._isPlaying = true;
		}
		private List<string> pausedStates = null;
	}
}
