﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Dynamic;
using Goliat.Serialization;
namespace Goliat
{
	[System.Serializable]
	public class GameBehaviour : IRestorable
	{
		private static int _index;
		private int _id;
		private List<GameBehaviour> _components;
		public List<GameBehaviour> components
		{
			get
			{
				return ((List<GameBehaviour>)this._components.Clone());
			}
			set
			{
				this._components = value;
			}
		}
		private List<UserScript> userScripts = new List<UserScript>();
		private UserScript script = null;
		public bool hasScript(String scriptName)
		{
			for (int i = 0; i < userScripts.Count; i++)
			{
				if (userScripts[i].className == scriptName)
				{
					return (true);
				}
			}
			return (false);
		}
		public virtual void OnEnable()
		{
			//Console.WriteLine("BASE ON ENABLE");
			return;
		}
		public static readonly List<GameBehaviour> GameBehaviours = new List<GameBehaviour>();
		public virtual int id
		{
			get
			{
				return (this._id);
			}
			set
			{
				;
			}
		}
		public void OnSave()
		{
			;
		}
		public void OnRestore()
		{
			this._id = GameBehaviour._index;
			_index += 1;
			GameBehaviour.GameBehaviours.Add(this);
		}

		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}

			// If parameter cannot be cast to Point return false.
			GameBehaviour p = obj as GameBehaviour;
			if ((System.Object)p == null)
			{
				return false;
			}

			// Return true if the fields match:
			return (p.GetHashCode() == this.GetHashCode());
		}
		public bool Equals(GameBehaviour other)
		{
			if ((object)other == null)
			{
				return false;
			}

			// Return true if the fields match:
			return (other.GetHashCode() == this.GetHashCode());
		}
		public override int GetHashCode()
		{
			return (this._id);
		}

		public override string ToString()
		{
			return ("GameBehaviour :\n nb : " + this.GetType() + ";");
		}
		//public static GameBehaviour							(componentTable[])(string value);
		//public static int										nbAvaillable;
		public Transform transform
		{
			get
			{
				return (this.GetComponent<Transform>());
			}
		}
		public GameBehaviour gameObject;
		public GameBehaviour meshFilter
		{
			get
			{
				return (this.GetComponent<MeshFilter>());
			}
		}

		public static bool operator ==(GameBehaviour self, GameBehaviour other)
		{
			if ((object)other == null && (object)self == null)
				return (true);
			if ((object)other == null)
				return (false);
			if ((object)self == null)
			{
				return (false);
			}
			if (self.GetHashCode() == other.GetHashCode())
			{
				return (true);
			}
			return (false);
		}
		public static bool operator !=(GameBehaviour self, GameBehaviour other)
		{
			return (!(self == other));
		}
		public virtual void copyStandards(GameBehaviour other)
		{
			this.gameObject = other.gameObject;
			//this.mesh = other.mesh;
		}
		/// ca va pas faut l enlever de tt les autres aussi.


		public virtual void unloadScripts()
		{
			for (int i = 0; i < this.userScripts.Count; i++)
			{
				UserScript script = this.userScripts[i];
				this.removeComponents(script.type, true);
			}
		}

		public virtual void unloadScripts(UserScript script)
		{
			for (int i = 0; i < this.userScripts.Count; i++)
			{
				if (script == this.userScripts[i])
				{
					//UserScript script = this.userScripts[i];
					this.removeComponents(script.type, true);
				}
			}
		}

		private CustomProperty _customProperty = null;
		public virtual CustomProperty customProperty
		{
			get
			{
				if (this._customProperty == null)
				{
					//Debug.Log("NULL");
					if (this.transform == null)
					{
						Debug.Log("Transform NULL");
					}
					else
						this._customProperty = new CustomProperty(null, this.transform.name, this);
				}
				return (this._customProperty);
			}
			set
			{
				if (this._customProperty == null)
				{
					//Debug.Log("NULL");
					this._customProperty = new CustomProperty(null, this.transform.name, this);
				}
				this._customProperty = value;
			}
		}

		public virtual dynamic AddComponent(Type type)
		{
			UserScript script = null;
			if (UserScript.Contains(type.Name))
			{
				Debug.Log("script found");
				script = UserScript.Find(type.Name);
				this.userScripts.Add(script);
				for (int i = 0; i < _components.Count; i++)
				{
					_components[i].userScripts.Add(script);
				}
			}
			GameBehaviour self = this;

			if ((type).IsSubclassOf(typeof(GameBehaviour)))
			{
				dynamic tmp = Activator.CreateInstance(type);
				if (script != null)
				{
					Debug.Log("setted script to script");
					((GameBehaviour)tmp).script = script;
				}
				((GameBehaviour)tmp).copyStandards(this);
				if (this._components == null)
				{
					this._components = new List<GameBehaviour>();
				}
				this._components.Add((GameBehaviour)tmp);
				//((GameBehaviour)tmp)._components = ((GameBehaviour)this)._components;
				for (int i = 0; i < _components.Count; i++)
				{
					_components[i]._components = this._components;
				}
				return (tmp);
			}
			else
			{
				Debug.Log("wrong type T.T");
			}
			return (null);
		}

		public virtual dynamic AddComponent(GameBehaviour behaviour)
		{
			Type type = behaviour.GetType();
			UserScript script = null;
			if (UserScript.Contains(type.Name))
			{
				Debug.Log("script found");
				script = UserScript.Find(type.Name);
				this.userScripts.Add(script);
				for (int i = 0; i < _components.Count; i++)
				{
					_components[i].userScripts.Add(script);
				}
			}
			GameBehaviour self = this;

			if ((type).IsSubclassOf(typeof(GameBehaviour)))
			{
				dynamic tmp = behaviour;//Activator.CreateInstance(type);
				if (script != null)
				{
					Debug.Log("setted script to script");
					((GameBehaviour)tmp).script = script;
				}
				((GameBehaviour)tmp).copyStandards(this);
				if (this._components == null)
				{
					this._components = new List<GameBehaviour>();
				}
				this._components.Add((GameBehaviour)tmp);
				//((GameBehaviour)tmp)._components = ((GameBehaviour)this)._components;
				for (int i = 0; i < _components.Count; i++)
				{
					_components[i]._components = this._components;
				}
				return (tmp);
			}
			else
			{
				Debug.Log("wrong type T.T");
			}
			return (null);
		}
		public virtual void reloadScripts()
		{
			this.unloadScripts();
			for (int i = 0; i < this.userScripts.Count; i++)
			{
				UserScript script = this.userScripts[i];
				if (script.Exist())
				{
					if (script.type.IsSubclassOf(typeof(GameBehaviour)))
					{
						AddComponent(script.type);
					}
				}
			}
		}

		public virtual void reloadScripts(UserScript script)
		{

			this.unloadScripts(script);
			Debug.Log("i have:" + this.userScripts.Count + " scripts");
			for (int i = 0; i < this.userScripts.Count; i++)
			{
				if (this.userScripts[i].className == script.className)
				{
					if (script.Exist())
					{
						Console.WriteLine("j'existe");
						if (script.type.IsSubclassOf(typeof(GameBehaviour)))
						{
							AddComponent(script.type);
						}
					}
					Console.WriteLine("trouvé");
				}
			}
			Console.WriteLine("non");
		}

		public virtual T AddComponent<T>() where T : GameBehaviour, new()
		{
			bool check = false;
			UserScript script = null;
			if (UserScript.Contains(typeof(T).Name))
			{
				script = UserScript.Find(typeof(T).Name);
				this.userScripts.Add(script);
				for (int i = 0; i < _components.Count; i++)
				{
					_components[i].userScripts.Add(script);
				}
				check = true;
			}
			GameBehaviour self = this;
			if ((typeof(T)).IsSubclassOf(typeof(GameBehaviour)))
			{
				object tmp = new T();
				if (check)
					((GameBehaviour)tmp).script = script;
				((GameBehaviour)tmp).copyStandards(this);
				if (this._components == null)
				{
					this._components = new List<GameBehaviour>();
				}
				this._components.Add((GameBehaviour)tmp);
				((GameBehaviour)tmp)._components = ((GameBehaviour)this)._components;
				for (int i = 0; i < _components.Count; i++)
				{
					_components[i]._components = this._components;
				}
				//throw new System.Exception("OYYYYY");
				((T)tmp).OnEnable();
				return ((T)tmp);
			}
			return (default(T));
		}

		public virtual T GetComponent<T>() where T : new()
		{
			if ((typeof(T)).IsSubclassOf(typeof(GameBehaviour)))
			{
				for (int i = 0; i < this._components.Count; i++)
				{
					if (this._components[i].GetType().IsSameOrSubclass(typeof(T)))
					{
						object tmp = this._components[i];
						return ((T)tmp);
					}
				}
			}
			return (default(T));
		}

		public virtual List<T> getComponents<T>() where T : new()
		{
			List<T> res = new List<T>();
			if ((typeof(T)).IsSubclassOf(typeof(GameBehaviour)))
			{
				for (int i = 0; i < this._components.Count; i++)
				{
					if (this._components[i].GetType().IsSameOrSubclass(typeof(T)))
					{
						object tmp = this._components[i];
						res.Add((T)tmp);
					}
				}
			}
			return (res);
		}

		public GameBehaviour()
		{
			//this.mesh = null;
			//this.transform = null;
			//Debug.Log("HEy oh!");
			this.gameObject = null;
			this._components = new List<GameBehaviour>();

			this._id = GameBehaviour._index;
			//Console.WriteLine("HAS SET ID OF BEHAVIOR:" + this._id);
			//System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace();
			//Console.WriteLine("TRACE:" + trace.ToString());
			GameBehaviour._index += 1;
			Type t = this.GetType();
			if (UserScript.Contains(t.Name))
			{
				this.script = UserScript.Find(t.Name);
			}
			this._components.Add(this);
			GameBehaviour.GameBehaviours.Add(this);
			//GameBehaviour tmp = this.addComponent< >();
			//tmp = this;
		}


		public virtual void removeComponent<T>() where T : new()
		{
			if ((typeof(T)).IsSubclassOf(typeof(GameBehaviour)))
			{
				List<GameBehaviour> newComponents = new List<GameBehaviour>();
				List<UserScript> newUserScripts = new List<UserScript>();
				for (int i = 0; i < this._components.Count; i++)
				{
					if ((this._components[i].GetType().IsSameOrSubclass(typeof(T))))
					{
						;
					}
					else
					{
						if (this._components[i].script != null)
						{
							newUserScripts.Add(this._components[i].script);
						}
						newComponents.Add(((this._components)[i]));
					}
				}
				List<GameBehaviour> tmp = _components;
				_components = newComponents;
				userScripts = newUserScripts;
				for (int i = 0; i < this._components.Count; i++)
				{
					((this._components)[i])._components = this._components;
					this._components[i].userScripts = this.userScripts;
				}
			}
		}
		public virtual List<GameBehaviour> getAllComponents()
		{
			//List<GameBehaviour> res = new List<GameBehaviour>();
			return ((List<GameBehaviour>)this._components.Clone());

		}
		public virtual void removeComponents(Type type, bool keepScript)
		{
			List<GameBehaviour> newComponents = new List<GameBehaviour>();
			List<UserScript> newUserScripts = new List<UserScript>();
			for (int i = 0; i < this._components.Count; i++)
			{
				if ((this._components[i].GetType().IsSameOrSubclass(type)))
				{
					if (keepScript && this._components[i].script != null)
					{
						Debug.Log("keeped script");
						newUserScripts.Add(this._components[i].script);
						Debug.Log("newUserScripts:" + newUserScripts.Count);
					}
					GameBehaviour.GameBehaviours[this._components[i].GetHashCode()] = null;
				}
				else
				{
					if (this._components[i].script != null)
					{
						newUserScripts.Add(this._components[i].script);
					}
					newComponents.Add(((this._components)[i]));
				}
			}
			Debug.Log("MAIS???:" + newUserScripts.Count);
			List<GameBehaviour> tmp = _components;
			_components = newComponents;
			userScripts = newUserScripts;
			for (int i = 0; i < this._components.Count; i++)
			{
				((this._components)[i])._components = this._components;
				this._components[i].userScripts = this.userScripts;
			}
			Debug.Log("i have:" + userScripts.Count + " scripts");
		}

		public virtual void removeComponents(Type type)
		{
			List<GameBehaviour> newComponents = new List<GameBehaviour>();
			List<UserScript> newUserScripts = new List<UserScript>();
			for (int i = 0; i < this._components.Count; i++)
			{
				if ((this._components[i].GetType().IsSameOrSubclass(type)))
				{
					GameBehaviour.GameBehaviours[this._components[i].GetHashCode()] = null;
				}
				else
				{
					if (this._components[i].script != null)
					{
						newUserScripts.Add(this._components[i].script);
					}
					newComponents.Add(((this._components)[i]));
				}
			}
			List<GameBehaviour> tmp = _components;
			_components = newComponents;
			userScripts = newUserScripts;
			for (int i = 0; i < this._components.Count; i++)
			{
				((this._components)[i])._components = this._components;
				this._components[i].userScripts = this.userScripts;
			}
		}
		public virtual void OnCollisionEnter(Collision collision)
		{
			;
		}
		public virtual void OnCollisionStay(Collision collision)
		{
			;
		}
		public virtual void OnCollisionExit(Collision collision)
		{
			;
		}
		public virtual void OnTriggerEnter(Collision collision)
		{
			;
		}
		public virtual void OnTriggerStay(Collision collision)
		{
			;
		}
		public virtual void OnTriggerExit(Collision collision)
		{
			;
		}
		public virtual void Update()
		{
			;
		}
		public virtual void Start()
		{
			;
		}
	};



}
