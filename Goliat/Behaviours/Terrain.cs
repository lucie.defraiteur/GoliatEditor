﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GL = OpenTK.Graphics.OpenGL.GL;
using System.IO;
using System.Data;
using System.Reflection;
namespace Goliat
{
	public enum TerrainPaintMode
	{
		raiseHeight, paintHeight, smoothHeight, paintTexture, paintDetails, parameters
	}
	public struct Brush
	{
		public Texture texture;
	}
	public struct BrushParameters
	{
		public int brushSize;
		public int opacity;
		public int targetStrength;
	}
	public class Terrain : GameBehaviour
	{
		private Mesh mesh;
		public TerrainPaintMode paintMode;

		public List<Brush> brushes = null;
		public List<Mesh> detailMeshs = null;
		public List<Texture> textures = null;
		public BrushParameters brushParameters;
		private Texture _heightMap = null;
		public Texture heightMap
		{
			get
			{
				if (this._heightMap == null || this._heightMap.width != this.heightMapResolution.x || this._heightMap.height != this.heightMapResolution.y)
				{
					//Console.WriteLine("AAAAAHHHH");
					//Console.WriteLine("resolution:" + this.heightMapResolution);

					this._heightMap = new Texture((int)this.heightMapResolution.x, (int)this.heightMapResolution.y, Color.Black);
					//Console.WriteLine("resolution2:" + this._heightMap.width + "," + this._heightMap.height + ";");
					//Console.WriteLine("???");
					//GL.BindTexture(0);
				}
				GL.BindTexture(OpenTK.Graphics.OpenGL.TextureTarget.Texture2D, 0);
				return (this._heightMap);
			}
			private set
			{
				this._heightMap = value;
			}
		}
		private float lastTime = Time.time;
		private bool pushBrush()
		{
			int radius = (int)((float)(5f / this.width * this.heightMapResolution.x));
			if (Time.time - lastTime > 700)
			{
				lastTime = Time.time;
				GC.WaitForPendingFinalizers();
				GC.Collect();
			}
			//this.heightMap.setPixelRect(Vector2.zero, new Vector2(this.heightMap.width, this.heightMap.height), Color.White);
			//this.heightMap.save(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "testterrain/tmp.bmp"));
			if (mouseImpact.x < this.width && mouseImpact.x >= 0 && mouseImpact.z < this.height && mouseImpact.z >= 0)
			{
				Color[,] pixelRect = this.heightMap.getPixelRect(getUvAtPoint(mouseImpact) - new Vector2(radius, radius), new Vector2(radius * 2, radius * 2));
				for (int i = 0; i < radius * 2; i++)
				{
					for (int j = 0; j < radius * 2; j++)
					{
						if (Mathf.Sqrt(Mathf.Square(i - radius) + Mathf.Square(j - radius)) < radius)
						{
							pixelRect[i, j] = pixelRect[i, j] + (Color.White * Time.deltaTime * 0.6f);
							pixelRect[i, j] = pixelRect[i, j].standardized;
						}
						//Console.WriteLine("" + pixelRect[i, j]);
						//pixelRect[i, j] = Color.White;
						//Console.WriteLine(pixelRect[i, j]);
					}
				}
				this.heightMap.setPixelRect(getUvAtPoint(mouseImpact) - new Vector2(radius, radius), pixelRect);
				return (true);
				//this.heightMap.save(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "testterrain/tmp.bmp"));
			}
			return (false);

		}
		public static bool isDrawing = false;
		public bool draw;

		private int _width = 10;
		public int width
		{
			get
			{
				return (this._width);
			}
			set
			{
				this._width = value;
				this.recalculate();
			}
		}

		private int _height = 10;
		public int height
		{
			get
			{
				return (this._height);
			}
			set
			{
				this._height = value;
				this.recalculate();
			}
		}

		public Terrain()
		{
			this._width = 10;
			this._height = 10;
			this._heightMapResolution = new Vector2(512, 512);
			this.recalculate();
		}

		private Vector2 _heightMapResolution = new Vector2(512, 512);
		public Vector2 heightMapResolution
		{
			get
			{
				return (this._heightMapResolution);
			}
			set
			{
				this._heightMapResolution = value;
				this.recalculate();
			}
		}

		public Terrain(int width, int height, int heightMapWidth, int heightMapHeight)
		{
			this._width = width;
			this._height = height;
			this._heightMapResolution = new Vector2(heightMapWidth, heightMapHeight);
			this.recalculate();
		}

		public Vector2 getUvAtPoint(Vector3 worldPosition)
		{
			Vector3 localPosition = this.transform.inverseTransformPoint(worldPosition);
			if (localPosition.x < 0 || localPosition.z < 0 || localPosition.x >= this.width || localPosition.z >= this.height)
			{
				return (Vector2.zero);
			}
			else
			{
				return (new Vector2(localPosition.z / this._width * this.heightMapResolution.x, (this._height - localPosition.x) / this._height * this.heightMapResolution.y));
			}
		}
		public Vector3 mouseImpact = Vector3.zero;
		public override void Update()
		{

			if (this.GetComponent<Transform>() != null)
			{
				if (this.GetComponent<Transform>().isSelected)
				{

					float start = Time.time;
					Vector2 position = Input.Mouse.position;
					//Debug.Log("Temps0.0:" + (Time.time - start));
					Ray test = Camera.current.ScreenPointToRay(position);
					//Debug.Log("TEMPS0:" + (Time.time - start));
					//start = Time.time;
					mouseImpact = Mathf.intersectPlane(Vector3.up, Vector3.zero, test);
					//Debug.Log("TEMPS1:" + (Time.time - start));
					//start = Time.time;
					if (Input.Mouse[0])
					{
						//Debug.Log("???");
						Terrain.isDrawing = this.pushBrush();
					}
					if (Input.Keyboard[(int)Key.S])
					{
						Debug.Log("saving heightmap;");
						this.heightMap.save(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "testterrain/tmp.bmp"));
						//this.heightMap.save();
					}
					//Debug.Log("TEMPS2:" + (Time.time - start));

				}
			}
		}


		public void recalculate() //Terrain(int width, int height, int heightMapWidth, int heightMapHeight)
		{
			Vector2[] uvs = new Vector2[(width + 1) * (height + 1)];
			Vector3[] normals = new Vector3[(width + 1) * (height + 1)];
			Face[] faces = new Face[(width * height)];
			Vector3[] vertices = new Vector3[(width + 1) * (height + 1)];
			float stepx = this.heightMapResolution.x / (width);
			float stepy = this.heightMapResolution.y / (height);
			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++)
				{
					normals[(i * (width + 1)) + j] = Vector3.up;
					uvs[i * (width + 1) + j] = new Vector2((stepx * (float)j) / this.heightMapResolution.x, stepy * (float)(height - 1 - i) / this.heightMapResolution.y);
					vertices[(i * (width + 1)) + j] = new Vector3((float)i, 0, (float)j);

					normals[(i * (width + 1)) + j + 1] = Vector3.up;
					uvs[(i * (width + 1)) + j + 1] = new Vector2(stepx * (float)(j + 1) / this.heightMapResolution.x, stepy * (float)(height - 1 - i) / this.heightMapResolution.y);
					vertices[(i * (width + 1)) + j + 1] = new Vector3((float)i, 0, (float)j + 1);

					normals[((i + 1) * (width + 1)) + j] = Vector3.up;
					uvs[((i + 1) * (width + 1)) + j] = new Vector2(stepx * (float)j / this.heightMapResolution.x, stepy * (float)(height - 1 - (i + 1)) / this.heightMapResolution.y);
					vertices[((i + 1) * (width + 1)) + j] = new Vector3((float)i + 1, 0, (float)j);
				}
			}

			normals[((height) * (width + 1)) + width] = Vector3.up;
			uvs[((height) * (width + 1)) + width] = new Vector2(stepx * (float)(width) / this.heightMapResolution.x, stepy * (float)(0) / this.heightMapResolution.y);
			vertices[((height) * (width + 1)) + width] = new Vector3((float)height, 0, (float)width);
			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++)
				{
					faces[(i * width) + j] = new Face();
					faces[(i * width) + j].indices = new int[4];
					faces[(i * width) + j].indices[0] = ((i * (width + 1)) + j);
					faces[(i * width) + j].indices[1] = ((i * (width + 1)) + j + 1);
					faces[(i * width) + j].indices[2] = (((i + 1) * (width + 1)) + j + 1);
					faces[(i * width) + j].indices[3] = (((i + 1) * (width + 1)) + j);
				}
			}
			if (this.GetComponent<MeshFilter>() == null)
			{
				MeshFilter tmp = this.AddComponent<MeshFilter>();
				if (tmp.sharedMesh == null)
				{
					tmp.sharedMesh = new Mesh();
				}
			}
			Mesh myMesh = this.GetComponent<MeshFilter>().sharedMesh;
			myMesh.faces = faces;
			myMesh.vertices = vertices;
			myMesh.uvs = uvs;
			myMesh.normals = normals;
		}
	}
}
