﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;

namespace Goliat
{
	//string hexValue = decValue.ToString("X");

	class RessourceMap
	{
		public static List<SharedResource> ressources;
		public static bool isSoundFile(string filepath)
		{
			string s = filepath;
			return (s.EndsWith(".mp3") || s.EndsWith("aiff") || s.EndsWith("mp3") || s.EndsWith("wma") || s.EndsWith("ogg") || s.EndsWith("flac"));
		}

		public static bool isImageFile(string filepath)
		{
			string s = filepath;
			return (s.EndsWith(".jpg") || s.EndsWith(".png") || s.EndsWith(".bmp") || s.EndsWith("gif"));
		}

		public static bool isMeshFile(string filepath)
		{
			string s = filepath;
			return (s.EndsWith(".obj") || s.EndsWith(".dae") || s.EndsWith(".octo") || s.EndsWith(".cs") || s.EndsWith(".3ds") || s.EndsWith(".ply") || s.EndsWith(".x") || s.EndsWith(".3d") || s.EndsWith(".smd")
				|| s.EndsWith(".b3d") || s.EndsWith(".vta") || s.EndsWith(".blend") || s.EndsWith(".X"));
		}
		public static void Update()
		{

			string[] ressourceFiles = Directory.GetFiles(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "*.*", SearchOption.AllDirectories).Where(
				s => s.EndsWith(".mp3") || s.EndsWith("aiff") || s.EndsWith("mp3") || s.EndsWith("wma") || s.EndsWith("ogg") || s.EndsWith("flac") //music formats
				|| s.EndsWith(".jpg") || s.EndsWith(".png") || s.EndsWith(".bmp") || s.EndsWith("gif") // images formats
				|| s.EndsWith(".obj") || s.EndsWith(".dae") || s.EndsWith(".octo") || s.EndsWith(".cs") || s.EndsWith(".3ds") || s.EndsWith(".ply") || s.EndsWith(".x") || s.EndsWith(".3d") || s.EndsWith(".smd")
				|| s.EndsWith(".b3d") || s.EndsWith(".vta") || s.EndsWith(".blend")).ToArray(); // mesh formats

			for (int i = 0; i < ressourceFiles.Length; i++)
			{
				string file = ressourceFiles[i];
				if (isSoundFile(file))
				{

				}
				else if (isImageFile(file))
				{

				}
				else if (isMeshFile(file))
				{

				}
			}
		}
	}
}
