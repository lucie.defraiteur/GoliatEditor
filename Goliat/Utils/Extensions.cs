﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Goliat;
using Microsoft.CodeAnalysis;
using Microsoft.CSharp;
using System.CodeDom.Compiler;
using System.Data;
using System.IO;
using Fasterflect;
using FastMember;
using System.Runtime.Serialization;
using System.ComponentModel;
/*
using Roslyn.Compilers;
using Roslyn.Compilers.CSharp;
using Roslyn.Scripting;
using Roslyn.Scripting.CSharp;
*/
namespace Goliat
{
	public class StandardExtensions
	{
		
		public static OpenTK.Vector3 FromVector(Assimp.Vector3D vec)
		{
			OpenTK.Vector3 v;
			v.X = vec.X;
			v.Y = vec.Y;
			v.Z = vec.Z;
			return v;
		}

		public static OpenTK.Graphics.Color4 FromColor(Assimp.Color4D color)
		{
			OpenTK.Graphics.Color4 c;
			c.R = color.R;
			c.G = color.G;
			c.B = color.B;
			c.A = color.A;
			return c;
		}

		public static OpenTK.Graphics.Color4 FromColor(Color color)
		{
			OpenTK.Graphics.Color4 c;
			c.R = color.R;
			c.G = color.G;
			c.B = color.B;
			c.A = color.A;
			return c;
		}

		public static OpenTK.Graphics.Color4 FromIntColor(uint rgba)
		{
			OpenTK.Graphics.Color4 C;
			C.R = rgba & 0xFF000000;
			C.G = rgba & 0x00FF0000;
			C.B = rgba & 0x0000FF00;
			C.A = rgba & 0x000000FF;
			return C;
		}
		public static uint ToIntColor(Byte4 C)
		{
			uint res = 0;
			res = res | C.A;
			res = res | (uint)(C.B << 8);
			res = res | (uint)(C.G << 16);
			res = res | (uint)(C.R << 24);
			return (res);
		}
		public static bool IsNumber(object value)
		{
			return value is sbyte
					|| value is byte
					|| value is short
					|| value is ushort
					|| value is int
					|| value is uint
					|| value is long
					|| value is ulong
					|| value is float
					|| value is double
					|| value is decimal;
		}
		public static bool IsGenericList(object value)
		{
			bool isGenericList = false;

			var type = value.GetType();

			if (type.IsGenericType && (type.GetGenericTypeDefinition() == typeof(List<>)))
				isGenericList = true;

			return isGenericList;
		}
	}

	public static class Extensions
	{
		public static string ToDAEString(this float[] array)
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < array.Length; i++)
			{
				sb.Append(array[i].ToString());
				if (i + 1 < array.Length)
				{
					sb.Append(" ");
				}
			}
			return (sb.ToString());
		}

		public static string ToDAEString(this int[] array)
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < array.Length; i++)
			{
				sb.Append(array[i].ToString());
				if (i + 1 < array.Length)
				{
					sb.Append(" ");
				}
			}
			return (sb.ToString());
		}
		public static object GetDefault(Type type)
		{
			if (type.IsValueType)
			{
				return Activator.CreateInstance(type);
			}
			return null;
		}
		public static bool TryConvertValue(string stringValue, out object convertedValue, Type type)
		{
			var targetType = type;
			if (targetType == typeof(string))
			{
				convertedValue = Convert.ChangeType(stringValue, type);
				return true;
			}
			var nullableType = targetType.IsGenericType &&
						   targetType.GetGenericTypeDefinition() == typeof(Nullable<>);
			if (nullableType)
			{
				if (string.IsNullOrEmpty(stringValue))
				{
					convertedValue = GetDefault(targetType);
					return true;
				}
				targetType = new NullableConverter(targetType).UnderlyingType;
			}

			Type[] argTypes = { typeof(string), targetType.MakeByRefType() };
			var tryParseMethodInfo = targetType.GetMethod("TryParse", argTypes);
			if (tryParseMethodInfo == null)
			{
				convertedValue = GetDefault(targetType);
				return false;
			}

			object[] args = { stringValue, null };
			var successfulParse = (bool)tryParseMethodInfo.Invoke(null, args);
			if (!successfulParse)
			{
				convertedValue = GetDefault(targetType);
				return false;
			}

			convertedValue = args[1];
			return true;
		}
		public static T ConvertString<T>(this string textValue)
		{
			object res;
			TryConvertValue(textValue, out res, (typeof(T)));
			return ((T)res);
		}
		public static void AddOrReplace<T1, T2>(this Dictionary<T1, T2> dictionary, T1 key, T2 value)
		{
			if (dictionary.ContainsKey(key))
			{
				dictionary[key] = value;
			}
			else
			{
				dictionary.Add(key, value);
			}
			return;
		}
		public static T2 AddOrRetrieve<T1, T2>(this Dictionary<T1, T2> dictionary, T1 key, T2 value)
		{
			if (dictionary.ContainsKey(key))
			{
				return (dictionary[key]);
			}
			else
			{
				dictionary.Add(key, value);
			}
			return (dictionary[key]);
		}
		public static IEnumerable Append(this IEnumerable first, params object[] second)
		{
			return first.OfType<object>().Concat(second);
		}
		public static object CreateDefault(this Type type)
		{
			if (type == typeof(string))
			{

				return (Activator.CreateInstance(type, new object[] { "".ToCharArray() }));
			}
			object value = null;
			try
			{
				value = Activator.CreateInstance(type);
			}
			catch (Exception e)
			{
				value = FormatterServices.GetUninitializedObject(type);
			}
			return (value);
		}
		public static bool isNumber(this object value)
		{
			return value is sbyte
					|| value is byte
					|| value is short
					|| value is ushort
					|| value is int
					|| value is uint
					|| value is long
					|| value is ulong
					|| value is float
					|| value is double
					|| value is decimal;
		}
		public static bool isNumberType(this Type type)
		{
			return type == typeof(sbyte)
					|| type == typeof(byte)
					|| type == typeof(short)
					|| type == typeof(ushort)
					|| type == typeof(int)
					|| type == typeof(uint)
					|| type == typeof(long)
					|| type == typeof(ulong)
					|| type == typeof(float)
					|| type == typeof(double)
					|| type == typeof(decimal);
		}
		public static bool isNullable(this object value)
		{
			Type type = value.GetType();
			return (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>));
		}
		public static bool isList(this object value)
		{//!thisType.IsArray && thisType.IsGenericType && thisType.GetInterfaces().Contains(typeof(IList)))
			if (!value.GetType().IsArray && value.GetType().IsGenericType && value.GetType().GetInterfaces().Contains(typeof(IList)))
			{
				return (true);
			}
			return (false);
		}
		public static bool isDictionary(this object value)
		{
			if (value is IDictionary && value.GetType().IsGenericType)
			{
				return (true);
			}
			return (false);
		}
		public static bool isArray(this object value)
		{
			if (value.GetType().IsArray)
			{
				return (true);
			}
			return (false);
		}
		public static Type elementType(this object value)
		{
			Type type = value.GetType().GetProperty("Item").PropertyType;
			return (type);
		}
		public static Type elementType(this object dictionaryCandidate, bool key)
		{
			Type[] arguments = dictionaryCandidate.GetType().GetGenericArguments();
			Type keyType = arguments[0];
			Type valueType = arguments[1];
			if (key)
				return (keyType);
			return (valueType);
		}
		public static void SetPropValue(this object obj, string name, object value, string debugPath)
		{
			TypeAccessor accessor = TypeAccessor.Create(((dynamic)obj).GetType(), true);
			if (obj.GetType().IsArray)
			{
				;
			}
			try
			{

				obj.SetFieldValue(name, value);
			}
			catch (Exception e)
			{
				try
				{
					obj.SetPropertyValue(name, value);
				}
				catch (Exception e2)
				{
					Debug.Log(debugPath, e2.ToString());
					try
					{
						accessor[obj, name] = value;
					}
					catch (Exception e3)
					{
						;
					}
				}
			}
		}
		public static void SetPropValue(this object obj, string name, object value)
		{
			TypeAccessor accessor = TypeAccessor.Create(((dynamic)obj).GetType(), true);
			if (obj.GetType().IsArray)
			{
				//Type elementType = obj.
			}
			try
			{

				obj.SetFieldValue(name, value);
			}
			catch (Exception e)
			{
				try
				{
					obj.SetPropertyValue(name, value);
				}
				catch (Exception e2)
				{
					try
					{
						accessor[obj, name] = value;
					}
					catch (Exception e3)
					{
						;
					}
				}
			}
		}

		public static float square(this float nb)
		{
			float res = nb * nb;
			return (res);
		}
		public static bool HasMethod(this object objectToCheck, string methodName)
		{
			var type = objectToCheck.GetType();
			return type.GetMethod(methodName) != null;
		}
		public static bool IsNumber(this object value)
		{
			return value is sbyte
					|| value is byte
					|| value is short
					|| value is ushort
					|| value is int
					|| value is uint
					|| value is long
					|| value is ulong
					|| value is float
					|| value is double
					|| value is decimal;
		}
		public static Vector3 toMinValues(this Vector3 vector, Vector3 other)
		{
			float x = vector.x < other.x ? vector.x : other.x;
			float y = vector.y < other.y ? vector.y : other.y;
			float z = vector.z < other.z ? vector.z : other.z;
			return (new Vector3(x, y, z));
		}
		public static Vector3 toMaxValues(this Vector3 vector, Vector3 other)
		{
			float x = vector.x > other.x ? vector.x : other.x;
			float y = vector.y > other.y ? vector.y : other.y;
			float z = vector.z > other.z ? vector.z : other.z;
			return (new Vector3(x, y, z));
		}
		public static float max(this Vector3 vector)
		{
			float max = vector.x;
			for (int i = 0; i < 3; i++)
			{
				if (max < vector[i])
				{
					max = vector[i];
				}
			}
			return (max);
		}
		public static Bounds updateToBigger(this Bounds bounds, Bounds other)
		{
			float maxx = bounds.max.x > other.max.x ? bounds.max.x : other.max.x;
			float maxy = bounds.max.y > other.max.x ? bounds.max.x : other.max.x;
			float maxz = bounds.max.z > other.max.x ? bounds.max.x : other.max.x;
			float minx = bounds.min.x < other.min.x ? bounds.min.x : other.min.x;
			float miny = bounds.min.y < other.min.x ? bounds.min.x : other.min.x;
			float minz = bounds.min.z < other.min.x ? bounds.min.x : other.min.x;
			Vector3 max = new Vector3(maxx, maxy, maxz);
			Vector3 min = new Vector3(minx, miny, minz);
			bounds.max = max;
			bounds.min = min;
			return (bounds);

			//if (.max)
		}

		public static bool IsSameOrSubclass(this Type potentialDescendant, Type potentialBase)
		{
			return potentialDescendant.IsSubclassOf(potentialBase)
				   || potentialDescendant == potentialBase;
		}

		public static System.Windows.Media.Color Window(this Goliat.Color color)
		{
			System.Windows.Media.Color res = new System.Windows.Media.Color();
			res.A = (byte)(color.a * 255);
			res.R = (byte)(color.r * 255);
			res.G = (byte)(color.g * 255);
			res.B = (byte)(color.b * 255);
			return (res);
		}
		public static System.Windows.Media.Brush Brush(this System.Windows.Media.Color color)
		{
			return (new System.Windows.Media.SolidColorBrush(color));
		}


		public static bool IsGenericList(this object obj)
		{
			bool isGenericList = false;

			var type = obj.GetType();

			if (type.IsGenericType && (type.GetGenericTypeDefinition() == typeof(List<>)))
				isGenericList = true;

			return isGenericList;
		}
		public static int mod(this int nb, int max)
		{
			int res = nb;
			while (res < 0)
				res = res + max;
			return (res % max);
		}
		public static List<ObjectGroup> getObjectProperties(this object obj)
		{
			return (UserScript.GetProperties(obj));
		}
		public static bool IsValueType<T>(this T obj)
		{
			return default(T) != null;
		}
		public static float GetDist(this List<float> v1, List<float> v2)
		{
			float squared = 0;
			for (int i = 0; i < v1.Count; i++)
			{
				if (i >= v2.Count)
				{
					break;
				}
				squared += (v1[i] + v2[i]).square();
			}
			return (squared.sqrt());
		}
		public static Vector3 goliat(this Jitter.LinearMath.JVector vector)
		{
			return (new Vector3(vector.X, vector.Y, vector.Z));
		}

		public static Jitter.LinearMath.JVector jitter(this Vector3 vector)
		{
			return (new Jitter.LinearMath.JVector(vector.x, vector.y, vector.z));
		}

		public static Quaternion goliat(this Jitter.LinearMath.JQuaternion vector)
		{
			return (new Quaternion(vector.X, vector.Y, vector.Z, vector.W));
		}

		public static Jitter.LinearMath.JQuaternion jitter(this Quaternion vector)
		{
			return (new Jitter.LinearMath.JQuaternion(vector.x, vector.y, vector.z, vector.w));
		}

		public static Matrix goliat(this Jitter.LinearMath.JMatrix matrix)
		{
			//Jitter.LinearMath.JQuaternion.CreateFromMatrix()
			Matrix res = new Matrix(matrix);
			return (res);

		}

		public static float pow(this float nb, uint power)
		{
			float res = 1;
			while (power > 0)
			{
				res = res * nb;
				power--;
			}
			return (res);
		}
		public static List<ObjectGroup> serialize(this object obj)
		{
			return (UserScript.GetProperties(obj));
		}

		public static Vector3[] octogon(this Assimp.Vector3D[] array)
		{
			Vector3[] res = new Vector3[array.Length];
			for (int i = 0; i < array.Length; i++)
			{
				res[i] = array[i].octogon();
			}
			return (res);
		}

		public static Face[] octogon(this Assimp.Face[] array)
		{
			Face[] res = new Face[array.Length];
			for (int i = 0; i < array.Length; i++)
			{
				res[i] = new Face(array[i]);
			}
			return (res);
		}

		public static Face octogon(this Assimp.Face face)
		{

			Face res = new Face(face);
			return (res);
		}

		public static Face octogon(this Assimp.Face face, int offset)
		{

			Face res = new Face(face, offset);
			return (res);
		}

		public static OpenTK.Vector3 gl(this Goliat.Vector3 vector)
		{
			return (new OpenTK.Vector3(vector.x, vector.y, vector.z));
		}
		public static OpenTK.Vector2 gl(this Goliat.Vector2 vector)
		{
			return (new OpenTK.Vector2(vector.x, vector.y));
		}
		public static Vector4 octogon(this OpenTK.Vector4 vector)
		{
			return (new Goliat.Vector4(vector.X, vector.Y, vector.Z, vector.W));
		}
		public static Matrix octogon(this OpenTK.Matrix4 matrix)
		{
			Matrix res = Matrix.Identity;
			res.setRow(0, matrix.Row0.octogon());
			res.setRow(1, matrix.Row1.octogon());
			res.setRow(2, matrix.Row2.octogon());
			res.setRow(3, matrix.Row3.octogon());
			return (res);
		}
		public static void Eval(this string sCSCode)
		{
			try
			{
				Console.WriteLine("Input string:" + sCSCode);
				CSharpCodeProvider c = new CSharpCodeProvider();
				CompilerParameters cp = new CompilerParameters();

				cp.ReferencedAssemblies.Add("system.dll");
				cp.ReferencedAssemblies.Add("system.xml.dll");
				cp.ReferencedAssemblies.Add("system.data.dll");
				cp.ReferencedAssemblies.Add("system.windows.forms.dll");
				cp.ReferencedAssemblies.Add("system.drawing.dll");
				cp.ReferencedAssemblies.Add(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Goliat.dll"));
				cp.ReferencedAssemblies.Add(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "AssimpNet.dll"));

				cp.CompilerOptions = "/t:library";
				cp.GenerateInMemory = true;

				StringBuilder sb = new StringBuilder("");
				sb.Append("using System;\n");
				sb.Append("using System.Xml;\n");
				sb.Append("using System.Data;\n");
				sb.Append("using System.Data.SqlClient;\n");
				sb.Append("using System.Windows.Forms;\n");
				sb.Append("using System.Drawing;\n");
				sb.Append("using Goliat;\n");
				sb.Append("using System.IO;\n");
				sb.Append("using System.Reflection;\n");
				sb.Append("using System.Collections;\n");
				sb.Append("using System.Collections.Generic;\n");
				sb.Append("using Assimp;\n");


				sb.Append("namespace CSCodeEvaler{ \n");
				sb.Append("public class CSCodeEvaler{ \n");
				sb.Append("public void EvalCode(){\n");
				sb.Append(sCSCode + " \n");
				sb.Append("} \n");
				sb.Append("} \n");
				sb.Append("}\n");

				CompilerResults cr = c.CompileAssemblyFromSource(cp, sb.ToString());
				if (cr.Errors.Count > 0)
				{
					Console.WriteLine("ERROR: " + cr.Errors[0].ErrorText);
					return;
				}

				System.Reflection.Assembly a = cr.CompiledAssembly;
				object o = a.CreateInstance("CSCodeEvaler.CSCodeEvaler");


				Type t = o.GetType();
				MethodInfo mi = t.GetMethod("EvalCode");

				mi.Invoke(o, null);
				//return s;
			}
			catch (Exception e)
			{
				Debug.Log(e.ToString());
			}

		}
		public static bool IsSerializable(this object obj)
		{
			if (obj == null)
				return (false);
			else
			{
				return (obj.GetType().IsSerializable);
			}
		}
		public static bool HasAttribute(this object obj, Type attributeType)
		{
			if (obj == null)
				return (false);
			return (Attribute.IsDefined(obj.GetType(), attributeType));
		}
		public static bool HasAttribute(this Type type, Type attributeType)
		{
			return (Attribute.IsDefined(type, attributeType));
		}

		public static Vector3 octogon(this Assimp.Vector3D vector)
		{
			return (new Vector3(vector.X, vector.Y, vector.Z));
		}

		public static object GetPropValue(this object src, string propName)
		{
			return (UserScript.GetPropValue(src, propName));
			//			return src.GetType().GetProperty(propName).GetValue(src, null);
		}


		public static IList<T> Clone<T>(this IList<T> listToClone)
		{
			return listToClone.Select(item => (T)item).ToList();
		}
		public static bool TryCast<T>(this object obj)
		{
			if (obj is T)
			{
				return true;
			}
			return false;
		}
		public static bool TryCast<T>(this object obj, out T result)
		{
			if (obj is T)
			{
				result = (T)obj;
				return true;
			}
			result = default(T);
			return false;
		}
		public static float sqrt(this float nb)
		{
			return ((float)Mathf.Sqrt(nb));
		}
		public static object CloneObject(this object o)
		{
			Type t = o.GetType();
			PropertyInfo[] properties = t.GetProperties();


			Object p = t.InvokeMember("", System.Reflection.
				BindingFlags.CreateInstance, null, o, null);


			foreach (PropertyInfo pi in properties)
			{
				if (pi.CanWrite)
				{
					pi.SetValue(p, pi.GetValue(o, null), null);
				}
			}
			return p;
		}
		public static float toDeg(this float angl)
		{
			return ((float)((angl / (2 * Math.PI)) * 360));
		}
		public static float toRad(this float angl)
		{
			return ((float)((angl / 360) * (2 * Math.PI)));
		}
		public static double toDeg(this double angl)
		{
			return ((double)((angl / (2 * Math.PI)) * 360));
		}
		public static double toRad(this double angl)
		{
			return ((float)((angl / 360) * (2 * Math.PI)));
		}
		public static bool SoundExtension(string filepath)
		{
			string s = filepath;
			return (s == ".mp3" || s == "aiff" || s == "mp3" || s == "wma" || s == "ogg" || s == "flac");
		}

		public static bool ImageExtension(string filepath)
		{
			string s = filepath;
			return (s == ".jpg" || s == ".png" || s == ".bmp" || s == "gif");
		}

		public static bool MeshExtension(string filepath)
		{
			string s = filepath;
			return (s == ".obj" || s == ".dae" || s == ".3ds" || s == ".ply" || s == ".x" || s == ".X" || s == ".3d" || s == ".smd"
				|| s == ".b3d" || s == ".vta" || s == ".blend");
		}
		public static bool ScriptExtension(string filepath)
		{
			string s = filepath;
			return (s == ".cs");
		}
		public static bool IsMeshExtension(this string ext)
		{
			return (MeshExtension(ext));
		}
		public static bool IsAudioExtension(this string ext)
		{
			return (SoundExtension(ext));
		}
		public static bool IsTextureExtension(this string ext)
		{
			return (ImageExtension(ext));
		}
		public static Quaternion octogon(this Assimp.Quaternion quat)
		{
			return (new Quaternion(quat));
		}
		public static T[] Resized<T>(this T[] array, int length)
		{
			T[] res = new T[length];
			for (int i = 0; i < length; i++)
			{
				if (i >= array.Length)
					return (res);
				res[i] = array[i];
			}
			return (res);
		}
		public static bool TrySetValue<T, T2>(this Dictionary<T, T2> dictionary, T key, T2 value)
		{
			if (dictionary.ContainsKey(key))
			{
				dictionary[key] = value;
				return (true);
			}
			return (false);
			
		}
		public static T2 TryGetValueOrNull<T, T2>(this Dictionary<T, T2> dictionary, T key) where T2 : class
		{
			T2 res;
			if (!dictionary.TryGetValue(key, out res))
			{
				return (null);
			}
			return (res);
		}
		public static bool IsScriptExtension(this string ext)
		{
			return (ScriptExtension(ext));
		}
		public static Assimp.NodeAnimationChannel FindNodeAnim(this Assimp.Animation anim, Assimp.Node node)
		{
			Assimp.NodeAnimationChannel res = null;
			for (int i = 0; i < anim.NodeAnimationChannelCount; i++ )
			{
				if (anim.NodeAnimationChannels[i].NodeName == node.Name)
				{
					return (anim.NodeAnimationChannels[i]);
				}
			}
			return (res);
		}
		public static float toFloat(this bool bl)
		{
			if (bl == false)
			{
				return (-1);
			}
			return (1);
		}
		public static bool sign(this float nb)
		{
			if (nb >= 0)
				return (true);
			else
				return (false);
		}
	}
}
