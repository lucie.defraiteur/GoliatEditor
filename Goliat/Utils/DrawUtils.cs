﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goliat;
using Vector3D = Assimp.Vector3D;
using System.IO;
using System.Runtime.InteropServices;
using Assimp;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Platform;
using Config = OpenTK.Configuration;
using Utilities = OpenTK.Platform.Utilities;
using Vector3 = Goliat.Vector3;
using Quaternion = Goliat.Quaternion;
using Matrix = Goliat.Matrix;
using Color = Goliat.Color;
using Goliat.Serialization;
using Goliat.Engine.GUI;
using InspectorCard = Goliat.Engine.GUI.InspectorGUI.InspectorCard;

namespace Goliat.Utils
{
	public static class DrawUtils
	{
		public static void DrawBox(Vector3 extents, Vector3 worldPosition, Quaternion worldRotation, Vector3 worldScale, Color color, bool faces)
		{

			Vector3[] g_vertex_buffer_data = new Vector3[]{
			new Vector3(-1.0f,-1.0f,-1.0f), //0 //1
			new Vector3(-1.0f,-1.0f, 1.0f), //1 //2
			new Vector3(-1.0f, 1.0f, 1.0f), //2 //3

			new Vector3(-1.0f,-1.0f,-1.0f), //0 //1
			new Vector3(-1.0f, 1.0f, 1.0f), //2 //3
			new Vector3(-1.0f, 1.0f,-1.0f), //4 //4

			//ok //ok blender
			new Vector3(1.0f,-1.0f, 1.0f), //5 //1
			new Vector3(-1.0f,-1.0f, 1.0f), //1 //2
			new Vector3(-1.0f,-1.0f,-1.0f), //0 //3

			new Vector3(1.0f,-1.0f, 1.0f), //5 //1
			new Vector3(-1.0f,-1.0f,-1.0f), //0 //3
			new Vector3(1.0f,-1.0f,-1.0f), //6 //4

			//ok blender
			new Vector3(1.0f, 1.0f,-1.0f), //3 //1 
			new Vector3(1.0f,-1.0f,-1.0f), //6 //2
			new Vector3(-1.0f,-1.0f,-1.0f), //0 //3

			new Vector3(1.0f, 1.0f,-1.0f), //3 //1
			new Vector3(-1.0f,-1.0f,-1.0f), //0 //3
			new Vector3(-1.0f, 1.0f,-1.0f), //4 ///4
			
			//ok blender
			new Vector3(-1.0f, 1.0f, 1.0f), //2 //1  //dessus.
			new Vector3(-1.0f,-1.0f, 1.0f), //1 //2
			new Vector3(1.0f,-1.0f, 1.0f), //5 //3

			
			new Vector3(-1.0f, 1.0f, 1.0f), //2 //1 //dessus
			new Vector3(1.0f,-1.0f, 1.0f), //5 //3
			new Vector3(1.0f, 1.0f, 1.0f), //7 //4 

			//ok blender

			new Vector3(1.0f, 1.0f, 1.0f), //7 //1
			new Vector3(1.0f, 1.0f,-1.0f), //3 //2
			new Vector3(-1.0f, 1.0f,-1.0f), //4 //3

			new Vector3(1.0f, 1.0f, 1.0f), //7 //1
			new Vector3(-1.0f, 1.0f,-1.0f), //4 //3
			new Vector3(-1.0f, 1.0f, 1.0f), //2 //4

			//ok blender
			new Vector3(1.0f, 1.0f, 1.0f), //7 //1
			new Vector3(1.0f,-1.0f, 1.0f), //5 //2
			new Vector3(1.0f,-1.0f,-1.0f), //6 //3

			new Vector3(1.0f, 1.0f, 1.0f), //7 //1
			new Vector3(1.0f,-1.0f,-1.0f), //6 //3
			new Vector3(1.0f, 1.0f,-1.0f) //3 //4
			};
			if (!faces)
				GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
			else
			{
				GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
			}
			//GL.Disable(EnableCap.Lighting);
			GL.PushMatrix();
			Matrix4 transformation = Matrix.Transformation(worldPosition, worldRotation, worldScale).gl();
			transformation.Transpose();
			GL.MultMatrix(ref transformation);
			for (int i = 0; i < g_vertex_buffer_data.Length / 3; i++)
			{
				GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
				GL.BindTexture(TextureTarget.Texture2D, 0);
				GL.CullFace(CullFaceMode.FrontAndBack);
				GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Diffuse, color.gl());
				//GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Diffuse, Color.Blue.gl());
				//GL.Color4(1, 0, 0, 1);
				GL.Begin(OpenTK.Graphics.OpenGL.PrimitiveType.Polygon); //_LINE_LOOP);
				int k = i * 3;

				Vector3 normal = Vector3.GenerateNormal(new Vector3[] { g_vertex_buffer_data[k] * extents + extents * 2, g_vertex_buffer_data[k + 1] * extents, g_vertex_buffer_data[k + 2] * extents });
				GL.Normal3(normal.gl());
				//GL.Vertex3(g_vertex_buffer_data[i * 3] * extents / 2, g_vertex_buffer_data[(i * 3) + 1] * extents / 2, g_vertex_buffer_data[(i * 3) + 2] * extents / 2);
				for (int j = i * 3; j < ((i * 3) + 3); j++)
				{

					Vector3 tmp = (g_vertex_buffer_data[j] * extents / 2);
					GL.Color4(1, 0, 0, 1);
					if (j == i * 3)
					{
						GL.TexCoord2(new Vector2(0, 0).gl());
					}
					else if (j == (i * 3) + 1)
					{
						GL.TexCoord2(new Vector2(0, 1).gl());
					}
					else if (j == (i * 3) + 2)
					{
						GL.TexCoord2(new Vector2(1, 0).gl());
					}
					//GL.TexCoord2(new Vector2(1, 1).gl());
					//Console.WriteLine("this color:" + color.ToString());
					GL.Vertex3(tmp.gl());
					GL.Normal3(normal.gl());
				}
				GL.End();
				;
			}
			GL.Flush();
			
			GL.PopMatrix();
			//GL.Enable(EnableCap.Lighting);
		}
		public static void DrawPlane(Vector2 dimensions, Vector3 worldPosition, Quaternion worldRotation, Vector3 worldScale, Color color)
		{
			GL.PushMatrix();
			GL.Disable(EnableCap.Lighting);
			Matrix4 transformation = Matrix.Transformation(worldPosition, worldRotation, worldScale).gl();
			GL.MultMatrix(ref transformation);
			Vector3 offset = new Vector3(-(dimensions.x / 2), -(dimensions.y / 2), 0);
			Vector2 tile = new Vector2(dimensions.x / 10, dimensions.y / 10);
			for (int i = 0; i < 10; i++)
			{
				for (int j = 0; j < 10; j++)
				{
					Vector3 lu = new Vector3(tile.x * i, tile.y * j, 0) + offset;
					Vector3 ru = new Vector3(tile.x * (i + 1), tile.y * j, 0) + offset;
					Vector3 rd = new Vector3(tile.x * (i + 1), tile.y * (j + 1), 0) + offset;
					Vector3 ld = new Vector3(tile.x * i, tile.y * (j + 1), 0) + offset;
					GL.Begin(OpenTK.Graphics.OpenGL.PrimitiveType.LineLoop); //_LINE_LOOP); 
					GL.Color4(color.gl());
					GL.Vertex3(lu.x, 0, lu.y);

					GL.Color4(color.gl());
					GL.Vertex3(ru.x, 0, ru.y);

					GL.Color4(color.gl());
					GL.Vertex3(rd.x, 0, rd.y);

					GL.Color4(color.gl());
					GL.Vertex3(ld.x, 0, ld.y);

					GL.Color4(color.gl());
					GL.Vertex3(lu.x, 0, lu.y);

					GL.Color4(color.gl());
					GL.Vertex3(rd.x, 0, rd.y);
					GL.End();
				}
				;
			}

			GL.Enable(EnableCap.Lighting);
			GL.PopMatrix();
		}
		public static void DrawCircle(float radius, int num_segments, Vector3 worldPosition, Quaternion worldRotation, Vector3 worldScale, Color color, float line_width = -1f)
		{
			GL.PushMatrix();
			GL.Disable(EnableCap.Lighting);
			float lineWidth = GL.GetFloat(GetPName.LineWidth);
			if (line_width >= 0f)
			{
				GL.LineWidth(line_width);
			}
			Matrix4 transformation = Matrix.Transformation(worldPosition, worldRotation, worldScale).gl();
			GL.MultMatrix(ref transformation);
			float theta = 2 * 3.1415926f / ((float)(num_segments));
			float tangetial_factor = (float)System.Math.Tan(theta);
			float radial_factor = (float)System.Math.Cos(theta);
			float r = radius;
			float x = r;
			float y = 0;
			GL.Begin(OpenTK.Graphics.OpenGL.PrimitiveType.LineLoop);
			for (int ii = 0; ii < num_segments; ii++)
			{
				GL.Color4(color.gl());
				GL.Vertex3(x, y, 0);
				float tx = -y;
				float ty = x;

				x += tx * tangetial_factor;
				y += ty * tangetial_factor;

				x *= radial_factor;
				y *= radial_factor;
			}
			GL.End();
			GL.Enable(EnableCap.Lighting);
			GL.PopMatrix();
			GL.LineWidth(lineWidth);
		}
		/*public static void DrawLines(List<Vector3> lines, Vector3 worldPosition, Quaternion worldRotation, Vector3 worldScale, Color color, float line_width = -1f)
		{
			GL.PushMatrix();
			GL.Disable(EnableCap.Lighting);
			float lineWidth = GL.GetFloat(GetPName.LineWidth);
			if (line_width >= 0f)
			{
				GL.LineWidth(line_width);
			}
			Matrix4 transformation = Matrix.Transformation(worldPosition, worldRotation, worldScale).gl();
			GL.MultMatrix(ref transformation);
			float theta = 2 * 3.1415926f / ((float)(num_segments));
			float tangetial_factor = (float)System.Math.Tan(theta);
			float radial_factor = (float)System.Math.Cos(theta);
			float r = radius;
			float x = r;
			float y = 0;
			GL.Begin(OpenTK.Graphics.OpenGL.PrimitiveType.LineLoop);
			for (int ii = 0; ii < num_segments; ii++)
			{
				GL.Color4(color.gl());
				GL.Vertex3(x, y, 0);
				float tx = -y;
				float ty = x;

				x += tx * tangetial_factor;
				y += ty * tangetial_factor;

				x *= radial_factor;
				y *= radial_factor;
			}
			GL.End();
			GL.Enable(EnableCap.Lighting);
			GL.PopMatrix();
			GL.LineWidth(lineWidth);
			;
		}*/
		public static void DrawSphere(float radius, int num_segments, int num_rings, Vector3 worldPosition, Quaternion worldRotation, Vector3 worldScale, Color color, bool faces = false, bool debug = false)
		{
			//GL.Disable(EnableCap.Lighting);
			if (num_rings < 3)
				num_rings = 3;
			if (num_segments < 3)
				num_segments = 3;
			List<List<Vector3>> segments = new List<List<Vector3>>();
			List<Vector3> oneSegment = new List<Vector3>();
			float angleStep = 180  / num_rings;
			float startAngle = -90;
			float endAngle = 90;
			//Quaternion rotation = new Quaternion(new Vector3(0, 0, 0));
			
			for (float angle = startAngle; angle <= endAngle; angle += angleStep)
			{
				Vector3 point = new Vector3((float)Math.Cos(angle * Mathf.DegToRad), (float)Math.Sin(angle * Mathf.DegToRad), 0f);
				//point = point * rotation;
				oneSegment.Add(point);
			}
			angleStep = 360 / num_segments;
			Quaternion rotationStep = new Quaternion(new Vector3(0, angleStep, 0));
			startAngle = angleStep;
			endAngle = 360;
			List<Vector3> activeSegment = oneSegment;
			for (float angle = startAngle; angle <= endAngle; angle += angleStep)
			{
				List<Vector3> newSegment = new List<Vector3>();
				for (int i = 0; i < activeSegment.Count; i++)
				{
					newSegment.Add(activeSegment[i] * rotationStep);
				}
				activeSegment = newSegment;
				segments.Add(activeSegment);
			}
			List<Vector3> g_vertex_buffer_data = new List<Vector3>();
			for (int i = 0; i < segments.Count; i++)
			{
				List<Vector3> segment = segments[i];
				List<Vector3> nextSegment = segments[(i + 1) % segments.Count];

				for (int j = 0; j < segment.Count - 1; j++)
				{
					if (j == 0)
					{
						g_vertex_buffer_data.Add(segment[0] * radius);
						g_vertex_buffer_data.Add(segment[1] * radius);
						g_vertex_buffer_data.Add(nextSegment[1] * radius);
					}
					else if (j + 1 >= segments.Count)
					{
						g_vertex_buffer_data.Add(segment[segment.Count - 2] * radius);
						g_vertex_buffer_data.Add(nextSegment[segment.Count - 1] * radius);
						g_vertex_buffer_data.Add(nextSegment[segment.Count - 2] * radius);
					}
					else
					{
						g_vertex_buffer_data.Add(segment[j] * radius);
						g_vertex_buffer_data.Add(segment[j + 1] * radius);
						g_vertex_buffer_data.Add(nextSegment[j + 1] * radius);

						g_vertex_buffer_data.Add(nextSegment[j + 1] * radius);
						g_vertex_buffer_data.Add(nextSegment[j] * radius);
						g_vertex_buffer_data.Add(segment[j] * radius);

					}
				}
			}
			if (!faces)
				GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
			else
			{
				GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
			}
			radius = 1;
			GL.PushMatrix();
			Matrix4 transformation = Matrix.Transformation(worldPosition, worldRotation, worldScale).gl();
			transformation.Transpose();
			GL.MultMatrix(ref transformation);
			GL.BindTexture(TextureTarget.Texture2D, 0);
			GL.CullFace(CullFaceMode.FrontAndBack);
			GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Diffuse, color.gl());
			g_vertex_buffer_data.Reverse();
			for (int i = 0; i < g_vertex_buffer_data.Count / 3; i++)
			{
				GL.Begin(OpenTK.Graphics.OpenGL.PrimitiveType.Polygon);
				int k = i * 3;

				Vector3 normal = Vector3.GenerateNormal(new Vector3[] { g_vertex_buffer_data[k], g_vertex_buffer_data[k + 1], g_vertex_buffer_data[k + 2] });
				//if (debug)
					//Console.WriteLine("FACE:" + g_vertex_buffer_data[k] + "; " + g_vertex_buffer_data[k + 1] + "; " + g_vertex_buffer_data[k + 2] + ";");
				GL.Normal3(normal.gl());

				for (int j = i * 3; j < ((i * 3) + 3); j++)
				{

					Vector3 tmp = (g_vertex_buffer_data[j]);
					GL.Color4(1, 0, 0, 1);
					if (j == i * 3)
					{
						GL.TexCoord2(new Vector2(0, 0).gl());
					}
					else if (j == (i * 3) + 1)
					{
						GL.TexCoord2(new Vector2(0, 1).gl());
					}
					else if (j == (i * 3) + 2)
					{
						GL.TexCoord2(new Vector2(1, 0).gl());
					}
					GL.Vertex3(tmp.gl());
					GL.Normal3(normal.gl());
				}
				GL.End();
				;
			}
			GL.Flush();
			GL.PopMatrix();
		}
	}
}
