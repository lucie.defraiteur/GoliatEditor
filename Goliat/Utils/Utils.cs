﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Goliat
{
	public struct Byte4
	{
		public byte R, G, B, A;

		public Byte4(byte[] input)
		{
			R = input[0];
			G = input[1];
			B = input[2];
			A = input[3];
		}

		public uint ToUInt32()
		{
			byte[] temp = new byte[] { this.R, this.G, this.B, this.A };
			return BitConverter.ToUInt32(temp, 0);
		}

		public override string ToString()
		{
			return this.R + ", " + this.G + ", " + this.B + ", " + this.A;
		}
	}


	public class Interpolation
	{
		private float _value = 0;
		public float value
		{
			get
			{
				return (this._value);
			}
			set
			{
				this._value = value;
				this.update();
			}
		}
		private void update()
		{
			if (this._value < 0)
				this._value = 0;
			if (this._value > 1)
				this._value = 1;
		}
		public static implicit operator float(Interpolation self)
		{
			return (self.value);
		}
	}

	public class Range
	{
		private Interpolation _interpolation = new Interpolation();
		public float interpolation
		{
			get
			{
				return ((float)this._interpolation);
			}
			set
			{

				this._interpolation.value = value;
				this._value = (this._interpolation.value * (this.max - this.min)) + this.min;
				this.update();
			}
		}
		private float _min = 0;
		public float min
		{
			get
			{
				return (this._min);
			}
			set
			{
				this._min = value;
				this.update();
			}
		}

		private float _value = 0;
		public float value
		{
			get
			{
				return (this._value);
			}
			set
			{
				this._value = value;
				this.update();
			}
		}

		private float _max = 0;
		public float max
		{
			get
			{
				return (this._min);
			}
			set
			{
				this._max = value;
				this.update();
			}
		}

		private void update()
		{
			this._interpolation.value = (this.max - this.min) / (this.value - this.min);
			if (this._value < this.min)
				this._value = this.min;
			if (this._value >= this.max)
				this._value = this.max;
		}

		public static implicit operator float(Range self)
		{
			//self.update();
			return ((float)self.value);
		}
		public static implicit operator int(Range self)
		{
			//self.update();
			return ((int)self.value);
		}
		public Range(float value, float min, float max)
		{
			this._value = value;
			this._min = min;
			this._max = max;
			this.update();
		}
	}
}
