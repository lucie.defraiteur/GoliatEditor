﻿/*

Ce projet appartient au groupe Goliat

Introduction:

	Non pas seulement sous la loi, mais sous ma demande de respect envers travail accompli, (j ai fait toute cette librairie seul, et uniquement seul.)

Je ne demande pas qu on evite a tout prix d utiliser mes sources pour des utilisations commerciales, mais un minimum de participation active à votre apprentissage personnel,
et votre avancée dans votre propre vie, si vous modifiez mes sources, assez bien parceque vous avez pu comprendre, pas de souci, debrouillez vous comme vous pouvez.
Ne tentez surtout pas le diable, evitez de chercher seuls a me depasser, me voler mon travail.

PS:
enjoy good work.
be happy.

You can:
Share it, copy it, distribute it and communicate about this product by all the ways and under any formats.
Adapt it, remix it, transform it and create things with it.
the bidder cant remove the rights given by the license whenever you follow the terms of this license.

Under thoose conditions:

No Commercial Use - You can't sell it, part of it, or all of it, part or all of it. But make good training for you.

Vous etes authorisés à:
Partager — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
Adapter — remixer, transformer et créer à partir du matériel
L'Offrant ne peut retirer les autorisations concédées par la licence tant que vous appliquez les termes de cette licence.
Selon les conditions suivantes :

Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.

Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
Pas d’Utilisation Commerciale — Vous n'êtes pas autoriser à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.
No additional restrictions — Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence.

Notes:

Vous n'êtes pas dans l'obligation de respecter la licence pour les éléments ou matériel appartenant au domaine public ou dans le cas où l'utilisation que vous souhaitez faire est couverte par une exception.
Aucune garantie n'est donnée. Il se peut que la licence ne vous donne pas toutes les permissions nécessaires pour votre utilisation. Par exemple, certains droits comme les droits moraux, le droit des données personnelles et le droit à l'image sont susceptibles de limiter votre utilisation.

Si vraiment vous en etes capable, faire votre propre version ne devrait pas etre trop difficile xP, amusez vous bien.
Peace.

For all the doubts, there is only one hope: win, win and not loose.

*/

using System;
using Goliat;

namespace Goliat
{
	[System.Serializable]
	public struct RaycastHit
	{
		public Vector3 barycentricCoordinate;
		public Collider collider;
		public float distance;
		public Vector2 lightmapCoord;
		public Vector3 normal;
		public Vector3 point;
		public RigidBody rigidbody;
		public Vector2 textureCoord;
		public Vector2 textureCoord2;
		public Transform transform;
		public int triangleIndex;
	}
	[System.Serializable]
	public struct Plane
	{
		public Vector3 origin;
		public Vector3 normal;
		public Plane(Vector3 normal, Vector3 origin)
		{
			this.origin = origin;
			this.normal = normal;
		}
		public static Plane zero
		{
			get
			{
				return (new Plane(Vector3.up, Vector3.zero));
			}
		}
	}
	public struct Ray
	{
		public Vector3 origin;
		public Vector3 end;
		public Vector3 direction;
		public Ray(Vector3 origin, Vector3 direction)
		{
			this.origin = origin;
			this.end = direction + origin;
			this.direction = direction.normalized;

		}

		public static Ray toEnd(Vector3 origin, Vector3 end)
		{
			return (new Ray(origin, end - origin));
		}

		public Vector3 GetPoint(float distance)
		{
			return (this.origin + (this.direction * distance));
		}
		public override string ToString()
		{
			string res = "Ray(origin:" + this.origin + ", direction:" + this.direction + ", end:" + this.end + "): \n";
			return res;
		}
		/*public List<Collider> cast()
		{
			for (int i = 0; i < Collider.Collider)
		}*/
		public Vector3 cast(Plane plane)
		{
			return (Mathf.intersectPlane(plane, this));
		}
		public Vector3 cast(Face face)
		{
			//(Vector3 planeNormal, Vector3 planeOrigin, Vector3 lineOrigin, Vector3 lineDirection)
			return (Mathf.intersectPlane(face.plane, this));
		}

		/*public bool cast(Collider collider, out float maxDistance)
		{
			return (collider.cast(this, maxDistance));
		}*/
		/*public bool cast(RaycastHit hitInfo, out float maxDistance)
		{
			maxDistance = 0;
			float dist = -1;
			int index = -1;
			for (int i = 0; i < Collider.Colliders.Count; i++)
			{
				if (ray.cast(Collider.Colliders[i], out distance))
				{
					if (dist == -1 || distance < dist)
					{
						dist = distance;
						maxDistance = dist;
						index = i;
					}
				}
			}
			if (index == -1)
				return (false);
			return (true);
		}*/
	}
}