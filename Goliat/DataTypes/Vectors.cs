﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Runtime.InteropServices;
using Goliat.Serialization;
using Goliat.Engine.GUI;
//using BulletSharp.Math;

namespace Goliat
{
	[Goliat.Serializable]
	public struct Vector2 : Goliat.ISerializable, IEditable
	{
		public static implicit operator Vector3(Vector2 self)
		{
			return (new Vector3(self.x, self.y, 0));
		}

		public void OnSerialize(Serializer.SerializationInfo info)
		{
			info.AddValue("array", this._array);
		}
		public override bool Equals(object obj)
		{
			return (obj != null && obj.GetType() == this.GetType() && (Vector2)obj != this);
		}
		public override int GetHashCode()
		{
			int res = (int)this._array[0];
			for (int i = 0; i < this._array.Length; i++ )
			{
				res += (int)((this._array[i] * 3) + 4);
			}
			return res;
		}
		public void OnInspectorGUI()
		{
			Vector2 tmp = InspectorGUI.Vector2Field("", this);
			this.x = tmp.x;
			this.y = tmp.y;
		}
		public void OnDeserialize(Serializer.SerializationInfo info)
		{
			float[] array = (float[])info.GetValue("array");
			for (int i = 0; i < 2; i++ )
			{
				this._array[i] = array[i];
			}
		}
		
		private readonly float[] _array;
		public float x
		{
			get
			{
				return (this._array[0]);
			}
			set
			{
				this._array[0] = value;
			}
		}
		public float y
		{
			get
			{
				return (this._array[1]);
			}
			set
			{
				this._array[1] = value;
			}
		}

		public float X
		{
			get
			{
				return (this._array[0]);
			}
			set
			{
				this._array[0] = value;
			}
		}
		public float Y
		{
			get
			{
				return (this._array[1]);
			}
			set
			{
				this._array[1] = value;
			}
		}

		public float this[int index]
		{
			get
			{
				return (this._array[index]);
			}
			set
			{
				this._array[index] = value;
			}
		}
		public float magnitude
		{
			get
			{
				return ((this.x.square() + this.y.square()).sqrt());
			}
		}
		public Vector2(float x, float y)
		{
			this._array = new float[2];
			this._array[0] = x;
			this._array[1] = y;
		}

		public static Vector2 operator *(Vector2 self, Vector2 other)
		{
			return (new Vector2(self.x * other.x, self.y * other.y));
		}

		public static Vector2 operator *(float other, Vector2 self)
		{
			return (new Vector2(self.x * other, self.y * other));
		}

		public float dot(Vector2 v2)
		{
			return ((this.x * v2.x) + (this.y * v2.y));
		}
		public Vector2 cross(Vector2 v2)
		{
			Vector2 res = new Vector2((this.x * v2.y) - (this.y * v2.x), (this.y * v2.x) - (this.x * v2.y));
			return (res);
		}

		public static float Dot(Vector2 v1, Vector2 v2)
		{
			return ((v1.x * v2.x) + (v1.y * v2.y));
		}
		public static Vector2 Cross(Vector2 v1, Vector2 v2)
		{
			Vector2 res = new Vector2((v1.x * v2.y) - (v1.y * v2.x), (v1.y * v2.x) - (v1.x * v2.y));
			return (res);
		}

		Vector2(Vector2 copy)
		{
			this._array = new float[2];
			this._array[0] = copy.x;
			this._array[1] = copy.y;
		}
		public static Vector2 operator -(Vector2 vector)
		{
			return (new Vector2(-vector.x, -vector.y));
		}
		public static Vector2 operator -(Vector2 vector, Vector2 other)
		{
			return (new Vector2(vector.x - other.x, vector.y - other.y));
		}

		public static Vector2 operator +(Vector2 vector)
		{
			return (new Vector2(vector.x, vector.y));
		}
		public static Vector2 operator +(Vector2 vector, Vector2 other)
		{
			return (new Vector2(vector.x + other.x, vector.y + other.y));
		}
		public override string ToString()
		{
			return ("Vector2(" + this.x + ", " + this.y + ");\n");
		}
		public static Vector2 zero
		{
			get
			{
				return (new Vector2(0, 0));
			}
		}
		public static Vector2 one
		{
			get
			{
				return (new Vector2(1, 1));
			}
		}
		public static Vector2 up
		{
			get
			{
				return (new Vector2(0, 1));
			}
		}
		public static Vector2 right
		{
			get
			{
				return (new Vector2(1, 0));
			}
		}
		public Vector2 normalized
		{
			get
			{
				if (this.magnitude == 0)
					return (Vector2.zero);
				Vector2 res = new Vector2(this.x / this.magnitude, this.y / this.magnitude);
				return (this == res ? this : res);
			}
		}
		public static bool operator ==(Vector2 self, Vector2 other)
		{
			return (self.x == other.x && self.y == other.y);
		}
		public static bool operator !=(Vector2 self, Vector2 other)
		{
			return (!(self == other));
		}
		public static Vector2 operator *(Vector2 self, float other)
		{
			return (new Vector2(self.x * other, self.y * other));
		}
		public static Vector2 operator /(Vector2 self, float other)
		{
			return (new Vector2(self.x / other, self.y / other));
		}
		public static Vector2 operator /(Vector2 self, Vector2 other)
		{
			Vector2 rectified = other;

			if (other.x == 0)
			{
				rectified.x = 0.0001f;
			}
			if (other.y == 0)
			{
				rectified.y = 0.0001f;
			}
			return (new Vector2(self.x / other.x, self.y / other.y));
		}
	}

	[Goliat.Serializable]
	public struct Vector3 : Goliat.ISerializable, IEditable
	{
		public override bool Equals(object obj)
		{
			return (obj != null && obj.GetType() == this.GetType() && (Vector3)obj != this);
		}
		public override int GetHashCode()
		{
			int res = (int)this._array[0];
			for (int i = 0; i < this._array.Length; i++)
			{
				res += (int)((this._array[i] * 3) + 4);
			}
			return res;
		}
		public static Vector3 GenerateNormal(Vector3[] face)
		{
			return ((face[1] - face[0]).Cross(face[2] - face[1]));
			//this._normal = Vector3.Cross(this.vertices[1] - this.vertices[0], this.vertices[2] - this.vertices[1]);
		}
		public void OnInspectorGUI()
		{
			if (this._array == null)
			{
				this._array = new float[4];
			}
			Vector3 tmp = InspectorGUI.Vector3Field("", this);
			this.x = tmp.x;
			this.y = tmp.y;
			this.z = tmp.z;
		}
		public void OnSerialize(Serializer.SerializationInfo info)
		{
			info.AddValue("array", this._array);
		}
		public void OnDeserialize(Serializer.SerializationInfo info)
		{
			this._array = (float[])info.GetValue("array");
		}
		public float GetDist(Vector3 other)
		{
			return ((other - this).Length());
		}
		public Vector3(Assimp.Vector3D other)
		{
			this._array = new float[4];
			this._array[0] = other.X;
			this._array[1] = other.Y;
			this._array[2] = other.Z;
			this._array[3] = 1;
		}
		public static Vector3 Nlerp(Vector3 v1, Vector3 v2, float factor)
		{
			float t = factor;
			return (((1 - t) * v1) + (t * v2)).normalized;
		}
		public static Vector3 Slerp(Vector3 v1, Vector3 v2, float factor)
		{
			float cosAlpha, alpha, sinAlpha;
			float t1, t2;

			cosAlpha = Vector3.Dot(v1, v2);

			alpha = (float)Math.Acos(cosAlpha);
			sinAlpha = (float)Math.Sin(alpha);

			if(sinAlpha == 0)
				return ((v1) + ((v2 - (v1)) * factor));

			t1 = (float)Math.Sin((1 - factor) * alpha) / sinAlpha;
			t2 = (float)Math.Sin(factor * alpha) / sinAlpha;

			return ((v1 * t1) + (v2 * t2));
		}
		public static Vector3 Lerp(Vector3 v1, Vector3 v2, float factor)
		{
			return (v1 + ((v2 - v1) * factor));
		}
		public Vector3 normalized
		{
			get
			{
				if (this.magnitude == 0)
					return (Vector3.zero);
				Vector3 res = new Vector3(this.x / this.magnitude, this.y / this.magnitude, this.z / this.magnitude);
				return (this == res ? this : res);
			}
		}
		public override string ToString()
		{
			if (this._array == null)
			{
				this._array = new float[4];
				this._array[0] = 0;
				this._array[1] = 0;
				this._array[2] = 0;
				this._array[3] = 1;
			}
			return ("Vector3(" + this.x + ", " + this.y + ", " + this.z + ");\n");
		}
		public float magnitude
		{
			get
			{
				return ((this.x.square() + this.y.square() + this.z.square()).sqrt());
			}
		}
		public float x
		{
			get
			{
				return (this._array[0]);
			}
			set
			{
				this._array[0] = value;
			}
		}
		public static bool operator ==(Vector3 self, Vector3 other)
		{
			if (self._array == null && other._array == null)
			{
				return (true);
			}
			else if (self._array == null || other._array == null)
			{
				return (false);
			}
			if (self.x == other.x && self.y == other.y && self.z == other.z)
			{
				return (true);
			}
			return (false);
		}
		public static bool operator !=(Vector3 self, Vector3 other)
		{
			return (!(self == other));
		}
		public float y
		{
			get
			{
				return (this._array[1]);
			}
			set
			{
				this._array[1] = value;
			}
		}
		public float z
		{
			get
			{
				return (this._array[2]);
			}
			set
			{
				this._array[2] = value;
			}
		}
		public float w
		{
			get
			{
				return (this._array[3]);
			}
			set
			{
				this._array[3] = value;
			}
		}

		public float X
		{
			get
			{
				return (this._array[0]);
			}
			set
			{
				this._array[0] = value;
			}
		}
		public float Y
		{
			get
			{
				return (this._array[1]);
			}
			set
			{
				this._array[1] = value;
			}
		}
		public float Z
		{
			get
			{
				return (this._array[2]);
			}
			set
			{
				this._array[2] = value;
			}
		}
		public float W
		{
			get
			{
				return (this._array[3]);
			}
			set
			{
				this._array[3] = value;
			}
		}

		/*internal BulletSharp.Math.Vector3 bullet()
		{
			return new BulletSharp.Math.Vector3(this.x, this.y, this.z);
		}*/

		public float this[int index]
		{
			get
			{
				return (this._array[index]);
			}
			set
			{
				this._array[index] = value;
			}
		}
		public Vector3(float x, float y, float z)
		{
			this._array = new float[4];
			for (int i = 0; i < 4; i++)
			{
				this._array[i] = 0.0f;
			}
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = 1;
		}
		public Vector3(float x, float y, float z, float w)
		{
			this._array = new float[4];
			for (int i = 0; i < 4; i++)
			{
				this._array[i] = 0.0f;
			}
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = 1;
		}
		public float[] array
		{
			get
			{
				return ((float[])this._array.Clone());
			}
			set
			{
				this._array = value;
			}
		}
		private float[] _array;
		public static Vector3 up
		{
			get
			{
				return (new Vector3(0, 1, 0));
			}
		}
		public static Vector3 right
		{
			get
			{
				return (new Vector3(1, 0, 0));
			}
		}
		public static Vector3 forward
		{
			get
			{
				return (new Vector3(0, 0, 1));
			}
		}
		public static Vector3 one
		{
			get
			{
				return (new Vector3(1, 1, 1));
			}
		}
		public static Vector3 zero
		{
			get
			{
				return (new Vector3(0, 0, 0));
			}
		}
		public Vector3(Vector3 copy)
		{
			this._array = new float[4];
			for (int i = 0; i < 4; i++)
			{
				this._array[i] = 0.0f;
			}
			this.x = copy.x;
			this.y = copy.y;
			this.z = copy.z;
			this.w = copy.w;
		}
		public Vector3 Cross(Vector3 other)
		{
			return (Vector3.Cross(this, other));
		}
		public static Vector3 Cross(Vector3 v1, Vector3 v2)
		{
			Vector3 tmp = new Vector3((v1.y * v2.z) - (v2.y * v1.z), (v1.z * v2.x) - (v2.z * v1.x), (v1.x * v2.y) - (v2.x * v1.y));
			return ((Vector3)(tmp));
		}
		public float Length()
		{
			return ((float)Mathf.Sqrt(Mathf.Square(this.x) + Mathf.Square(this.y) + Mathf.Square(this.z)));
		}
		public static Vector3 operator -(Vector3 self)
		{
			return (Vector3.zero - self);
		}
		public static Vector3 operator -(Vector3 self, Vector3 other)
		{
			return (new Vector3(self.x - other.x, self.y - other.y, self.z - other.z));
		}
		public static Vector3 Normalize(Vector3 vector)
		{
			return (vector / vector.Length());
		}
		public static Vector3 operator *(Vector3 self, float other)
		{
			return (new Vector3(self.x * other, self.y * other, self.z * other));
		}
		public static Vector3 operator /(Vector3 self, float other)
		{
			return (new Vector3(self.x / other, self.y / other, self.z / other));
		}
		public static Vector3 operator *(float other, Vector3 self)
		{
			return (new Vector3(self.x * other, self.y * other, self.z * other));
		}
		public static Vector3 operator *(Vector3 self, Vector3 other)
		{
			return (new Vector3(self.x * other.x, self.y * other.y, self.z * other.z));
		}
		public static Vector3 operator /(Vector3 self, Vector3 other)
		{
			Vector3 rectified = other;

			if (other.x == 0)
			{
				rectified.x = 0.0001f;
			}
			if (other.y == 0)
			{
				rectified.y = 0.0001f;
			}
			if (other.z == 0)
			{
				rectified.z = 0.0001f;
			}
			return (new Vector3(self.x / other.x, self.y / other.y, self.z / other.z));
		}
		public static Vector3 operator *(Vector3 self, Matrix other)
		{
			return (other * self);
		}
		public static Vector3 operator *(Vector3 self, Quaternion other)
		{
			return (other * self);
		}
		public static Vector3 Random(Vector3 minimums, Vector3 maximums)
		{
			System.Random rand = new System.Random();
			Vector3 diff = maximums - minimums;
			Vector3 tmp = new Vector3((float)rand.NextDouble(), (float)rand.NextDouble(), (float)rand.NextDouble());
			tmp = tmp * diff;
			tmp += minimums;
			return (tmp);

		}
		public float Dot(Vector3 v2)
		{
			return ((this.x * v2.x) + (this.y * v2.y) + (this.z * v2.z));
		}
		public static float Dot(Vector3 v1, Vector3 v2)
		{
			return ((v1.x * v2.x) + (v1.y * v2.y) + (v1.z * v2.z));
		}
		public static Vector3 operator +(Vector3 self, Vector3 other)
		{
			return (new Vector3(self.x + other.x, self.y + other.y, self.z + other.z));
		}
		public Size size()
		{
			return (new Size(this.X, this.Y, this.Z, this.W));
		}
		public Vector3 rounded(int decimals)
		{
			return (new Vector3((float)Math.Round(this.x, 3), (float)Math.Round(this.y, 3), (float)Math.Round(this.z, 3)));
		}
	}
	[Goliat.Serializable]
	public struct Vector4 : Goliat.ISerializable
	{
		public override bool Equals(object obj)
		{
			return (obj != null && obj.GetType() == this.GetType() && (Vector4)obj != this);
		}
		public override int GetHashCode()
		{
			int res = (int)this._array[0];
			for (int i = 0; i < this._array.Length; i++)
			{
				res += (int)((this._array[i] * 3) + 4);
			}
			return res;
		}
		public void OnInspectorGUI()
		{
			if (this._array == null)
			{
				this._array = new float[4];
			}
			Vector4 tmp = InspectorGUI.Vector4Field("", this);
			this.x = tmp.x;
			this.y = tmp.y;
			this.z = tmp.z;
			this.w = tmp.w;
		}
		public void OnSerialize(Serializer.SerializationInfo info)
		{
			info.AddValue("array", this._array);
		}
		public void OnDeserialize(Serializer.SerializationInfo info)
		{
			this._array = (float[])info.GetValue("array");
		}
		public float GetDist(Vector4 other)
		{
			return ((other - this).Length());
		}
		public Vector4(Assimp.Vector3D other)
		{
			this._array = new float[4];
			this._array[0] = other.X;
			this._array[1] = other.Y;
			this._array[2] = other.Z;
			this._array[3] = 1;
		}
		public Vector4 normalized
		{
			get
			{

				if (this.magnitude == 0)
					return (Vector4.zero);
				Vector4 res = new Vector4(this.x / this.magnitude, this.y / this.magnitude, this.z / this.magnitude, this.w / this.magnitude);
				return (this == res ? this : res);
			}
		}
		public override string ToString()
		{
			if (this._array == null)
			{
				this._array = new float[4];
				this._array[0] = 0;
				this._array[1] = 0;
				this._array[2] = 0;
				this._array[3] = 1;
			}
			return ("Vector4(" + this.x + ", " + this.y + ", " + this.z + "," + this.w + ");\n");
		}
		public float magnitude
		{
			get
			{
				return ((this.x.square() + this.y.square() + this.z.square() + this.w.square()).sqrt());
			}
		}
		public float x
		{
			get
			{
				return (this._array[0]);
			}
			set
			{
				this._array[0] = value;
			}
		}
		public static bool operator ==(Vector4 self, Vector4 other)
		{
			if (self.x == other.x && self.y == other.y && self.z == other.z && self.w == other.w)
			{
				return (true);
			}
			return (false);
		}
		public static bool operator !=(Vector4 self, Vector4 other)
		{
			return (!(self == other));
		}
		public float y
		{
			get
			{
				return (this._array[1]);
			}
			set
			{
				this._array[1] = value;
			}
		}
		public float z
		{
			get
			{
				return (this._array[2]);
			}
			set
			{
				this._array[2] = value;
			}
		}
		public float w
		{
			get
			{
				return (this._array[3]);
			}
			set
			{
				this._array[3] = value;
			}
		}

		public float X
		{
			get
			{
				return (this._array[0]);
			}
			set
			{
				this._array[0] = value;
			}
		}
		public float Y
		{
			get
			{
				return (this._array[1]);
			}
			set
			{
				this._array[1] = value;
			}
		}
		public float Z
		{
			get
			{
				return (this._array[2]);
			}
			set
			{
				this._array[2] = value;
			}
		}
		public float W
		{
			get
			{
				return (this._array[3]);
			}
			set
			{
				this._array[3] = value;
			}
		}


		public float this[int index]
		{
			get
			{
				return (this._array[index]);
			}
			set
			{
				this._array[index] = value;
			}
		}
		public Vector4(float x, float y, float z)
		{
			this._array = new float[4];
			for (int i = 0; i < 4; i++)
			{
				this._array[i] = 0.0f;
			}
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = 1;
		}
		public Vector4(float x, float y, float z, float w)
		{
			this._array = new float[4];
			for (int i = 0; i < 4; i++)
			{
				this._array[i] = 0.0f;
			}
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}
		public float[] array
		{
			get
			{
				return ((float[])this._array.Clone());
			}
			set
			{
				this._array = value;
			}
		}
		private float[] _array;
		public static Vector4 one
		{
			get
			{
				return (new Vector4(1, 1, 1, 1));
			}
		}
		public static Vector4 zero
		{
			get
			{
				return (new Vector4(0, 0, 0, 0));
			}
		}
		public Vector4(Vector4 copy)
		{
			this._array = new float[4];
			for (int i = 0; i < 4; i++)
			{
				this._array[i] = 0.0f;
			}
			this.x = copy.x;
			this.y = copy.y;
			this.z = copy.z;
			this.w = copy.w;
		}
		public Vector4 Cross(Vector4 other)
		{
			return (Vector4.Cross(this, other));
		}
		public static Vector4 Cross(Vector4 v1, Vector4 v2)
		{
			Vector4 tmp = new Vector4((v1.y * v2.z) - (v2.y * v1.z), (v1.z * v2.w) - (v2.z * v1.w), (v1.w * v2.x) - (v1.x * v2.w), (v1.x * v2.y) - (v2.x * v1.y));
			return ((Vector4)(tmp));
		}
		public float Length()
		{
			return ((float)Mathf.Sqrt(Mathf.Square(this.x) + Mathf.Square(this.y) + Mathf.Square(this.z) + Mathf.Square(this.w)));
		}
		public static Vector4 operator -(Vector4 self)
		{
			return (Vector4.zero - self);
		}
		public static Vector4 operator -(Vector4 self, Vector4 other)
		{
			return (new Vector4(self.x - other.x, self.y - other.y, self.z - other.z, self.w - other.w));
		}
		public static Vector4 Normalize(Vector4 vector)
		{
			return (vector / vector.Length());
		}
		public static Vector4 operator *(Vector4 self, float other)
		{
			return (new Vector4(self.x * other, self.y * other, self.z * other, self.w * other));
		}
		public static Vector4 operator /(Vector4 self, float other)
		{
			return (new Vector4(self.x / other, self.y / other, self.z / other, self.w / other));
		}
		public static Vector4 operator *(float other, Vector4 self)
		{
			return (new Vector4(self.x * other, self.y * other, self.z * other, self.w * other));
		}
		public static Vector4 operator *(Vector4 self, Vector4 other)
		{
			return (new Vector4(self.x * other.x, self.y * other.y, self.z * other.z, self.w * other.w));
		}
		public static Vector4 operator /(Vector4 self, Vector4 other)
		{
			Vector4 rectified = other;

			if (other.x == 0)
			{
				rectified.x = 0.0001f;
			}
			if (other.y == 0)
			{
				rectified.y = 0.0001f;
			}
			if (other.z == 0)
			{
				rectified.z = 0.0001f;
			}
			if (other.w == 0)
				rectified.w = 0.0001f;
			return (new Vector4(self.x / other.x, self.y / other.y, self.z / other.z, self.w / other.w));
		}
		public static Vector4 operator *(Vector4 self, Matrix other)
		{
			return (other * self);
		}
		public static Vector4 operator *(Vector4 self, Quaternion other)
		{
			return (other * self);
		}

		public float Dot(Vector4 v2)
		{
			return ((this.x * v2.x) + (this.y * v2.y) + (this.z * v2.z) + (this.w * v2.w));
		}
		public static float Dot(Vector4 v1, Vector4 v2)
		{
			return ((v1.x * v2.x) + (v1.y * v2.y) + (v1.z * v2.z) + (v1.w * v2.w));
		}
		public static Vector4 operator +(Vector4 self, Vector4 other)
		{
			return (new Vector4(self.x + other.x, self.y + other.y, self.z + other.z, self.w + other.w));
		}
		public Size size()
		{
			return (new Size(this.X, this.Y, this.Z, this.W));
		}
	}
	public class Size : Goliat.ISerializable
	{
		public void OnSerialize(Serializer.SerializationInfo info)
		{
			info.AddValue("array", this._array);
		}
		public void OnDeserialize(Serializer.SerializationInfo info)
		{
			this._array = (float[])info.GetValue("array");
		}
		public float GetDist(Size other)
		{
			return ((other - this).Length());
		}
		public Size(Assimp.Vector3D other)
		{
			this._array = new float[4];
			this._array[0] = other.X;
			this._array[1] = other.Y;
			this._array[2] = other.Z;
			this._array[3] = 1;
		}
		public Size normalized
		{
			get
			{
				if (this.magnitude == 0)
					return (Size.zero);
				Size res = new Size(this.x / this.magnitude, this.y / this.magnitude, this.z / this.magnitude);
				return (this == res ? this : res);
				//return (new Size(this.x / this.magnitude, this.y / this.magnitude, this.z / this.magnitude));
			}
		}
		public override string ToString()
		{
			return ("Size(" + this.x + ", " + this.y + ", " + this.z + ");\n");
		}
		public float magnitude
		{
			get
			{
				return ((this.x.square() + this.y.square() + this.z.square()).sqrt());
			}
		}
		public float x
		{
			get
			{
				return (this._array[0]);
			}
			set
			{
				this._array[0] = value;
			}
		}
		public static bool operator ==(Size self, Size other)
		{
			if (self.x == other.x && self.y == other.y && self.z == other.z)
			{
				return (true);
			}
			return (false);
		}
		public static bool operator !=(Size self, Size other)
		{
			return (!(self == other));
		}
		public float y
		{
			get
			{
				return (this._array[1]);
			}
			set
			{
				this._array[1] = value;
			}
		}
		public float z
		{
			get
			{
				return (this._array[2]);
			}
			set
			{
				this._array[2] = value;
			}
		}
		public float w
		{
			get
			{
				return (this._array[3]);
			}
			set
			{
				this._array[3] = value;
			}
		}

		public float X
		{
			get
			{
				return (this._array[0]);
			}
			set
			{
				this._array[0] = value;
			}
		}
		public float Y
		{
			get
			{
				return (this._array[1]);
			}
			set
			{
				this._array[1] = value;
			}
		}
		public float Z
		{
			get
			{
				return (this._array[2]);
			}
			set
			{
				this._array[2] = value;
			}
		}
		public float W
		{
			get
			{
				return (this._array[3]);
			}
			set
			{
				this._array[3] = value;
			}
		}


		public float this[int index]
		{
			get
			{
				return (this._array[index]);
			}
			set
			{
				this._array[index] = value;
			}
		}
		public Size(float x, float y, float z)
		{
			this._array = new float[4];
			for (int i = 0; i < 4; i++)
			{
				this._array[i] = 0.0f;
			}
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = 1;
		}
		public Size(float x, float y, float z, float w)
		{
			this._array = new float[4];
			for (int i = 0; i < 4; i++)
			{
				this._array[i] = 0.0f;
			}
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = 1;
		}
		public float[] array
		{
			get
			{
				return ((float[])this._array.Clone());
			}
			set
			{
				this._array = value;
			}
		}
		private float[] _array;
		public static Size up
		{
			get
			{
				return (new Size(0, 1, 0));
			}
		}
		public static Size right
		{
			get
			{
				return (new Size(1, 0, 0));
			}
		}
		public static Size forward
		{
			get
			{
				return (new Size(0, 0, 1));
			}
		}
		public static Size one
		{
			get
			{
				return (new Size(1, 1, 1));
			}
		}
		public static Size zero
		{
			get
			{
				return (new Size(0, 0, 0));
			}
		}
		public Size(Size copy)
		{
			this._array = new float[4];
			for (int i = 0; i < 4; i++)
			{
				this._array[i] = 0.0f;
			}
			this.x = copy.x;
			this.y = copy.y;
			this.z = copy.z;
			this.w = copy.w;
		}
		public Size Cross(Size other)
		{
			return (Size.Cross(this, other));
		}
		public static Size Cross(Size v1, Size v2)
		{
			Size tmp = new Size((v1.y * v2.z) - (v2.y * v1.z), (v1.z * v2.x) - (v2.z * v1.x), (v1.x * v2.y) - (v2.x * v1.y));
			return ((Size)(tmp));
		}
		public float Length()
		{
			return ((float)Mathf.Sqrt(Mathf.Square(this.x) + Mathf.Square(this.y) + Mathf.Square(this.z)));
		}
		public static Size operator -(Size self, Size other)
		{
			return (new Size(self.x - other.x, self.y - other.y, self.z - other.z));
		}
		public static Size Normalize(Size vector)
		{
			return (vector / vector.Length());
		}
		public static Size operator *(Size self, float other)
		{
			return (new Size(self.x * other, self.y * other, self.z * other));
		}
		public static Size operator /(Size self, float other)
		{
			return (new Size(self.x / other, self.y / other, self.z / other));
		}
		public static Size operator *(float other, Size self)
		{
			return (new Size(self.x * other, self.y * other, self.z * other));
		}
		public static Size operator *(Size self, Size other)
		{
			return (new Size(self.x * other.x, self.y * other.y, self.z * other.z));
		}
		public static Size operator /(Size self, Size other)
		{
			Size rectified = other;

			if (other.x == 0)
			{
				rectified.x = 0.0001f;
			}
			if (other.y == 0)
			{
				rectified.y = 0.0001f;
			}
			if (other.z == 0)
			{
				rectified.z = 0.0001f;
			}
			return (new Size(self.x / other.x, self.y / other.y, self.z / other.z));
		}
		public Vector3 vector()
		{
			return (new Vector3(this.X, this.Y, this.Z, this.W));
		}
		public static Size operator *(Size self, Matrix other)
		{
			return ((other * self.vector()).size());
		}
		public static Size operator *(Size self, Quaternion other)
		{
			return ((other * self.vector()).size());
		}

		public float Dot(Size v2)
		{
			return ((this.x * v2.x) + (this.y * v2.y) + (this.z * v2.z));
		}
		public static float Dot(Size v1, Size v2)
		{
			return ((v1.x * v2.x) + (v1.y * v2.y) + (v1.z * v2.z));
		}
		public static Size operator +(Size self, Size other)
		{
			return (new Size(self.x + other.x, self.y + other.y, self.z + other.z));
		}
	}
};
