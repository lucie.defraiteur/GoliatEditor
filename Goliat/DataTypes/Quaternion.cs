﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goliat;
using Goliat.Engine.GUI;

namespace Goliat
{
	[Goliat.Serializable]
	public struct Quaternion : IEditable
	{
		public override bool Equals(object obj)
		{
			return (obj != null && obj.GetType() == this.GetType() && (Quaternion)obj != this);
		}
		public override int GetHashCode()
		{
			int res = (int)this._array[0];
			for (int i = 0; i < this._array.Length; i++)
			{
				res += (int)((this._array[i] * 3) + 14);
				
			}
			return res;
		}
		public static Quaternion Slerp(Quaternion start, Quaternion end, float factor)
		{
			//Calc cosine theta
			float cosom = (start.X * end.X) + (start.Y * end.Y) + (start.Z * end.Z) + (start.W * end.W);

			//Reverse signs if needed
			if (cosom < 0.0f)
			{
				cosom = -cosom;
				end.X = -end.X;
				end.Y = -end.Y;
				end.Z = -end.Z;
				end.W = -end.W;
			}

			//calculate coefficients
			float sclp, sclq;
			//0.0001 -> some episilon
			if ((1.0f - cosom) > 0.0001f)
			{
				//Do a slerp
				float omega, sinom;
				omega = (float)Math.Acos(cosom); //extract theta from the product's cos theta
				sinom = (float)Math.Sin(omega);
				sclp = (float)Math.Sin((1.0f - factor) * omega) / sinom;
				sclq = (float)Math.Sin(factor * omega) / sinom;
			}
			else
			{
				//Very close, do a lerp instead since its faster
				sclp = 1.0f - factor;
				sclq = factor;
			}

			Quaternion q = Quaternion.identity;
			q.X = (sclp * start.X) + (sclq * end.X);
			q.Y = (sclp * start.Y) + (sclq * end.Y);
			q.Z = (sclp * start.Z) + (sclq * end.Z);
			q.W = (sclp * start.W) + (sclq * end.W);
			return q;
		}
		public void OnInspectorGUI()
		{
			if (this._array == null)
			{
				this._array = new float[4];
			}
			Quaternion tmp = InspectorGUI.QuaternionField("", this);
			this.x = tmp.x;
			this.y = tmp.y;
			this.z = tmp.z;
			this.w = tmp.w;
		}
		public static bool operator ==(Quaternion Q1, Quaternion vector)
		{
			if (Q1._array == null && vector._array == null)
			{
				return (true);
			}
			else if (Q1._array == null || vector._array == null)
			{
				return (false);
			}
			return (Q1.x == vector.x && Q1.y == vector.y && Q1.z == vector.z &&  Q1.w == vector.w);
			//return (new Quaternion(x, y, z, w));
		}
		public static bool operator !=(Quaternion Q1, Quaternion Q2)
		{
			return (!(Q1 == Q2));
		}
		public float this[int index]
		{
			get
			{
				return (this._array[index]);
			}
			set
			{
				this._array[index] = value;
			}
		}
		public float x
		{
			get
			{
				return (this[0]);
			}
			set
			{
				this[0] = value;
			}
		}
		public float y
		{
			get
			{
				return (this[1]);
			}
			set
			{
				this[1] = value;
			}
		}
		public float z
		{
			get
			{
				return (this[2]);
			}
			set
			{
				this[2] = value;
			}
		}
		public float w
		{
			get
			{
				return (this[3]);
			}
			set
			{
				this[3] = value;
			}
		}
		public float X
		{
			get
			{
				return (this[0]);
			}
			set
			{
				this[0] = value;
			}
		}
		public float Y
		{
			get
			{
				return (this[1]);
			}
			set
			{
				this[1] = value;
			}
		}
		public float Z
		{
			get
			{
				return (this[2]);
			}
			set
			{
				this[2] = value;
			}
		}
		public float W
		{
			get
			{
				return (this[3]);
			}
			set
			{
				this[3] = value;
			}
		}
		public static Quaternion identity
		{
			get
			{
				return (new Quaternion(0, 0, 0, 1));
			}
		}

		public Quaternion(Quaternion copy)
		{
			this._array = (float[])copy.array.Clone();
		}
		public Quaternion(Vector3 eulerRepresentation)
		{
			this._array = identity.array;
			this.eulerAngles = new Vector3(eulerRepresentation.x, eulerRepresentation.y, eulerRepresentation.z);// eulerRepresentation;
		}
		public Quaternion(float x, float y, float z, float w)
		{
			this._array = new float[] { x, y, z, w };
		}
		public Quaternion(Vector3 xyz, float w)
		{
			this._array = new float[] { xyz.x, xyz.y, xyz.z, w };
		}
		/*public Quaternion(Vector4 vector)
		{
			this._array = vector.array;
		}*/
		public float magnitude
		{
			get
			{
				return ((this.x.square() + this.y.square() + this.z.square() + this.w.square()).sqrt());
			}
		}
		public Quaternion normalized
		{
			get
			{
				float mag = this.magnitude;
				float x = this.x / mag;
				float y = this.y / mag;
				float z = this.z / mag;
				float w = this.w / mag;
				return (new Quaternion(x, y, z, w));
			}
		}
		public static Quaternion Nlerp(Quaternion Q1, Quaternion Q2, float factor)
		{
			float t = factor;
			return (((1 - t) * Q1) + (t * Q2)).normalized;
		}
		private float[] _array;
		public float[] array
		{
			get
			{
				return ((float[])_array.Clone());
			}
			set
			{
				this._array = value;
			}
		}
		public Vector3 eulerAngles
		{
			get
			{
				double yaw = Math.Atan2((2 * ((this[3] * this[0]) + (this[1] * this[2]))), 1 - (2 * (Mathf.Square(this[0]) + Mathf.Square(this[1]))));
				double pitch = Math.Asin(2 * ((this[3] * this[1]) - (this[2] * this[0])));
				double roll = Math.Atan2(2 * ((this[3] * this[2]) + (this[0] * this[1])), 1 - (2 * (Mathf.Square(this[1]) + Mathf.Square(this[2]))));
				return (new Vector3((float)yaw.toDeg(), (float)pitch.toDeg(), (float)roll.toDeg()));
			}
			set
			{

				double w = (Math.Cos(value.x.toRad() / 2) * Math.Cos(value.y.toRad() / 2) * Math.Cos(value.z.toRad() / 2)) + (Math.Sin(value.x.toRad() / 2) * Math.Sin(value.y.toRad() / 2) * Math.Sin(value.z.toRad() / 2));
				double x = (Math.Sin(value.x.toRad() / 2) * Math.Cos(value.y.toRad() / 2) * Math.Cos(value.z.toRad() / 2)) - (Math.Cos(value.x.toRad() / 2) * Math.Sin(value.y.toRad() / 2) * Math.Sin(value.z.toRad() / 2));
				double y = (Math.Cos(value.x.toRad() / 2) * Math.Sin(value.y.toRad() / 2) * Math.Cos(value.z.toRad() / 2)) + (Math.Sin(value.x.toRad() / 2) * Math.Cos(value.y.toRad() / 2) * Math.Sin(value.z.toRad() / 2));
				double z = (Math.Cos(value.x.toRad() / 2) * Math.Cos(value.y.toRad() / 2) * Math.Sin(value.z.toRad() / 2)) - (Math.Sin(value.x.toRad() / 2) * Math.Sin(value.y.toRad() / 2) * Math.Cos(value.z.toRad() / 2));
				this.x = (float)x;
				this.y = (float)y;
				this.z = (float)z;
				this.w = (float)w;
			}
		}
		public Matrix orthogonalMatrix
		{
			get
			{
				Matrix res = new Matrix(4, 4);
				res.setRow(0, new double[] { Mathf.Square(this.w) + Mathf.Square(this.x) - Mathf.Square(this.y) - Mathf.Square(this.z), (2 * this.x * this.y) - (2 * this.w * this.z), 2 * (this.w * this.y) + (2 * (this.x * this.z)), 0 });
				res.setRow(1, new double[] { 2 * this.w * this.z + 2 * this.x * this.y, Mathf.Square(this.w) - Mathf.Square(this.x) + Mathf.Square(this.y) - Mathf.Square(this.z), 2 * this.y * this.z - 2 * this.w * this.x, 0 });
				res.setRow(2, new double[] { 2 * this.x * this.z - 2 * this.w * this.y, 2 * this.w * this.x + 2 * this.y * this.z, Mathf.Square(this.w) - Mathf.Square(this.x) - Mathf.Square(this.y) + Mathf.Square(this.z), 0 });
				res.setRow(3, new double[] { 0, 0, 0, 1 });
				return (res);
			}
		}
		public Vector3 vector
		{
			get
			{
				return (new Vector3(this.x, this.y, this.z));
			}
			set
			{
				this.x = value.x;
				this.y = value.y;
				this.z = value.z;
			}
		}
		public static Quaternion scalar(float nb)
		{
			Quaternion tmp = Quaternion.identity;
			return (new Quaternion(0, 0, 0, nb));
		}
		public static Quaternion i
		{
			get
			{
				return (new Quaternion(1, 0, 0, 0));
			}
		}
		public static Quaternion j
		{
			get
			{
				return (new Quaternion(0, 1, 0, 0));
			}
		}
		public static Quaternion k
		{
			get
			{
				return (new Quaternion(0, 0, 1, 0));
			}
		}

		public float modulus
		{
			get
			{
				Quaternion conjugated = this * this.conjugate();
				return ((float)Mathf.Sqrt(conjugated.x.square() + conjugated.y.square() + conjugated.z.square() + conjugated.w.square()));
			}
		}
		public static Quaternion operator *(Quaternion Q1, Quaternion Q2)
		{

			float w = (Q1.w * Q2.w) - (Q1.x * Q2.x) - (Q1.y * Q2.y) - (Q1.z * Q2.z);
			float x = (Q1.w * Q2.x) + (Q1.x * Q2.w) + (Q1.y * Q2.z) - (Q1.z * Q2.y);
			float y = (Q1.w * Q2.y) - (Q1.x * Q2.z) + (Q1.y * Q2.w) + (Q1.z * Q2.x);
			float z = (Q1.w * Q2.z) + (Q1.x * Q2.y) - (Q1.y * Q2.x) + (Q1.z * Q2.w);
			return (new Quaternion(x, y, z, w));
		}

		public static Quaternion operator *(float nb, Quaternion Q2)
		{
			float w = Q2.w * nb;
			float x = (Q2.x * nb);
			float y = (Q2.y * nb);
			float z = (Q2.z * nb);
			return (new Quaternion(x, y, z, w));
		}

		public static Quaternion operator *(Quaternion Q1, float nb)
		{

			float w = Q1.w * nb;
			float x = (Q1.x * nb);
			float y = (Q1.y * nb);
			float z = (Q1.z * nb);
			return (new Quaternion(x, y, z, w));
		}
		public static Vector3 operator *(Quaternion Q1, Vector3 vector)
		{
			return (Q1.orthogonalMatrix * vector);
			//return (new Quaternion(x, y, z, w));
		}

		public static Vector4 operator *(Quaternion Q1, Vector4 vector)
		{
			return (Q1.orthogonalMatrix * vector);
			//return (new Quaternion(x, y, z, w));
		}
		public static Vector3 operator /(Quaternion Q1, Vector3 vector)
		{
			return (Q1.inverse * vector);
		}
		public static Quaternion operator +(Quaternion Q1, Quaternion Q2)
		{

			return (new Quaternion(Q1.x + Q2.x, Q1.y + Q2.y, Q1.z + Q2.z, Q1.w + Q2.w));
		}
		public static Quaternion operator -(Quaternion Q1, Quaternion Q2)
		{

			return (new Quaternion(Q1.x - Q2.x, Q1.y - Q2.y, Q1.z - Q2.z, Q1.w - Q2.w));
		}
		public static Quaternion operator /(Quaternion Q1, Quaternion Q2)
		{
			return (Q1.inverse * Q2);
		}
		public static Quaternion operator /(Quaternion self, float nb)
		{
			return (new Quaternion(self.x / nb, self.y / nb, self.z / nb, self.w / nb));
		}
		public double norm()
		{
			return Mathf.Sqrt(this[0] * this[0] + this[1] * this[1] +
									  this[2] * this[2] + this[3] * this[3]);
		}
		public Quaternion conjugate()
		{
			return (new Quaternion(-this.x, -this.y, -this.z, this.w));  // * ((this + i) * (this) * (i + j) * (this) * (j + k) * (this * k)));
		}
		public Quaternion inverseQuaternion()
		{
			return (this.conjugate() / (float)this.norm());
		}
		public Quaternion inverse
		{
			get
			{
				return (this.inverseQuaternion());
			}
			set
			{

				Quaternion tmp = value.inverseQuaternion();
				this._array = tmp.array;
			}
		}
		public static Quaternion LookAtQuaternion(Vector3 sourcePoint, Vector3 destPoint, Vector3 up)
		{
			Vector3 forwardVector = Vector3.Normalize(destPoint - sourcePoint);

			float dot = Vector3.Dot(up, forwardVector);

			if (Math.Abs(dot - (-1.0f)) < 0.000001f)
			{
				return new Quaternion(Vector3.up.x, Vector3.up.y, Vector3.up.z, 3.1415926535897932f);
			}
			if (Math.Abs(dot - (1.0f)) < 0.000001f)
			{
				return Quaternion.identity;
			}

			float rotAngle = (float)Math.Acos(dot);
			Vector3 rotAxis = Vector3.Cross(Vector3.forward, up);
			rotAxis = Vector3.Normalize(rotAxis);
			return (CreateFromAxisAngle(rotAxis, rotAngle));
		}
		public static Quaternion LookRotation(Vector3 lookAt, Vector3 up) {
	/*Vector forward = lookAt.Normalized();
	Vector right = Vector::Cross(up.Normalized(), forward);
	Vector up = Vector::Cross(forward, right);*/

	Vector3 forward = lookAt.normalized;
	//Vector::OrthoNormalize(&up, &forward); // Keeps up the same, make forward orthogonal to up
	Vector3 right = Vector3.Cross(up, forward);

	Quaternion ret = Quaternion.identity;
	ret.w = (float)Math.Sqrt(1.0f + right.x + up.y + forward.z) * 0.5f;
	float w4_recip = 1.0f / (4.0f * ret.w);
	ret.x = (forward.y - up.z) * w4_recip;
	ret.y = (right.z - forward.x) * w4_recip;
	ret.z = (up.x - right.y) * w4_recip;

	return ret;
}

		public static Quaternion ForwardUpRight(Vector3 forward, Vector3 up, Vector3 right)
		{
			/*Vector forward = lookAt.Normalized();
			Vector right = Vector::Cross(up.Normalized(), forward);
			Vector up = Vector::Cross(forward, right);*/

			forward = forward.normalized;
			//Vector::OrthoNormalize(&up, &forward); // Keeps up the same, make forward orthogonal to up
			right = right.normalized;

			Quaternion ret = Quaternion.identity;
			ret.w = (float)Math.Sqrt(1.0f + right.x + up.y + forward.z) * 0.5f;
			float w4_recip = 1.0f / (4.0f * ret.w);
			ret.x = (forward.y - up.z) * w4_recip;
			ret.y = (right.z - forward.x) * w4_recip;
			ret.z = (up.x - right.y) * w4_recip;

			return ret;
		}
		public static Quaternion FromDirection(Vector3 upVector, Vector3 direction)
		{
			Vector3 side = direction.Cross(upVector);
			Vector3 U = direction.Cross(side);
			Matrix res = Matrix.Identity;
			res.setRow(0, direction);
			res.setRow(1, U);
			res.setRow(2, side);
			return (res.toQuaternion());
		}

		// just in case you need that function also
		public static Quaternion CreateFromAxisAngle(Vector3 axis, float angle)
		{
			float halfAngle = angle * .5f;
			float s = (float)System.Math.Sin(halfAngle);
			Quaternion q = Quaternion.identity;
			q.x = axis.x * s;
			q.y = axis.y * s;
			q.z = axis.z * s;
			q.w = (float)System.Math.Cos(halfAngle);
			return q;
		}
		public Quaternion(Matrix mat)
		{
			Quaternion tmp = mat.quaternion;
			this._array = tmp.array;
		}
		public Quaternion(Assimp.Quaternion other)
		{
			Quaternion tmp = Quaternion.identity;
			this._array = tmp._array;
			this.x = other.X;
			this.y = other.Y;
			this.z = other.Z;
			this.w = other.W;
		}


		public void LookAt(Vector3 sourcePoint, Vector3 destPoint)
		{
			Quaternion tmp = LookAtQuaternion(sourcePoint, destPoint, Vector3.up);
			this._array = tmp.array;
		}

		public override string ToString()
		{
			if (this._array == null)
			{
				this._array = new float[4];
				this._array[0] = 0;
				this._array[1] = 0;
				this._array[2] = 0;
				this._array[3] = 1;
			}
			string res = "Quaternion( w: " + this.w + ", " + this.x + ", " + this.y + ", " + this.z + "); \n";
			return res;
		}
	}
}
