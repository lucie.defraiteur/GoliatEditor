﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using System.Security.Policy;
using System.Security;
using System.Security.Permissions;
using FastMember;
using Microsoft.CSharp;
using System.CodeDom.Compiler;
using Goliat;

namespace Goliat
{
	public class ObjectGroup
	{
		public string name;
		public dynamic value;
		public ObjectGroup(string name, dynamic value)
		{
			this.name = name;
			this.value = value;
		}

	}

	[AttributeUsage(AttributeTargets.Assembly)]
	public class FileName : Attribute
	{
		string someText;
		public FileName() : this(string.Empty) { }
		public FileName(string txt) { someText = txt; }
	}


	public class Proxy : MarshalByRefObject
	{
		public AppDomain check;
		public Assembly GetAssembly(string assemblyPath)
		{
			try
			{
				return Assembly.ReflectionOnlyLoadFrom(assemblyPath);
			}
			catch (Exception)
			{
				return null;
			}
		}
	}
	public class UserScript : SharedResource
	{
		public UserScript(MappedRessourceInfo info) : base(info)
		{
			;
		}
		public AppDomain domain;
		public Assembly assembly;
		public Type type;
		//private String		_dllPath;
		public String dllPath;
		public String scriptPath;
		public String className;
		public String file;

		public static Dictionary<Assembly, UserScript> scriptDictionary = new Dictionary<Assembly, UserScript>();
		public static List<UserScript> scripts = new List<UserScript>();

		public static bool Contains(String className)
		{
			for (int i = 0; i < UserScript.scripts.Count; i++)
			{
				if (UserScript.scripts[i].className == className)
				{
					return (true);
				}
			}
			return (false);
		}

		public static UserScript Find(String className)
		{
			for (int i = 0; i < UserScript.scripts.Count; i++)
			{
				if (UserScript.scripts[i].className == className)
				{
					return (UserScript.scripts[i]);
				}
			}
			return (null);
		}
		private static object _tmp;
		public static object tmp
		{
			get
			{
				return (_tmp);
			}
			private set
			{
				_tmp = value;
			}
		}
		public static ObjectGroup GetProperty(object obj, string name)
		{
			Debug.Log("CHECKING:" + name);
			tmp = obj;
			dynamic callingObj = obj;
			dynamic accessor = TypeAccessor.Create(((dynamic)callingObj).GetType());
			string propName = name;

			return (new ObjectGroup(name, accessor[(dynamic)callingObj, propName]));
		}

		public static bool SetProperty(object callingObj, string name, object value)
		{
			try
			{
				Debug.Log("callingObject:" + callingObj.GetType().Name);
				Debug.Log("ok:" + value + ";" + "name:" + name);
				tmp = callingObj;
				TypeAccessor accessor = TypeAccessor.Create(((dynamic)callingObj).GetType());
				string propName = name;




				accessor[(dynamic)callingObj, propName] = value;
				return (true);
			}
			catch (Exception e)
			{
				return (false);
			}
		}



		public static List<ObjectGroup> GetProperties(object obj)
		{
			return (Serialize(obj));
		}
		public static List<ObjectGroup> Serialize(object obj)
		{
			//	Console.WriteLine("what?");
			if (obj != null)
			{
				List<ObjectGroup> res = new List<ObjectGroup>();
				TypeAccessor accessor = TypeAccessor.Create(((dynamic)obj).GetType());
				var values = accessor.GetMembers();//.Select(member => accessor[ynamic)obj, member.Name]).ToList();
				for (int i = 0; i < values.Count; i++)
				{
					try
					{
						object value = accessor[obj, values[i].Name];
						//Debug.Log("ADDED:" + values[i].Name + ";");
						res.Add(new ObjectGroup(values[i].Name, value));
					}
					catch (Exception e)
					{
						continue;
					}
				}
				return (res);
			}
			return (null);
		}
		public static object GetPropValue(object src, string propName, bool dynamic)
		{
			if (!dynamic)
			{

				return src.GetType().GetProperty(propName).GetValue(src, null);
			}
			else
			{
				UserScript test = null;
				if (UserScript.scriptDictionary.TryGetValue(src.GetType().Assembly, out test))
				{
					return GetProperty(src, propName).value;
				}
				else
				{
					return src.GetType().GetProperty(propName).GetValue(src, null);
				}
			}
		}
		public static bool SetPropValue(object src, string propName, object value)
		{
			return (SetProperty(src, propName, value));
		}
		public static bool SetPropValue(object src, string propName, object value, bool dynamic)
		{
			return (SetProperty(src, propName, value));
		}
		public static object GetPropValue(object src, string propName)
		{
			if (src.GetType().Assembly == Assembly.GetExecutingAssembly())
				return src.GetType().GetProperty(propName).GetValue(src, null);
			else
			{
				UserScript test = UserScript.Find(src.GetType().Name);
				if (test != null)
				{
					return GetProperty(src, propName).value;
				}
				else
				{
					return src.GetType().GetProperty(propName).GetValue(src, null);
				}
			}
		}
		public bool Exist()
		{
			try
			{
				System.Console.WriteLine("Child domain: " + domain.FriendlyName);
			}
			catch (System.AppDomainUnloadedException e)
			{
				return (false);
			}
			if (!File.Exists(this.scriptPath))
			{
				this.Unload();
				return (false);
			}
			return (true);
		}

		public List<Transform> Unload()
		{
			List<Transform> affecteds = new List<Transform>();
			if (this.type != null)
			{
				for (int i = 0; i < Transform.Transforms.Count; i++)
				{
					Transform mb = Transform.Transforms[i];

					if (mb != null && mb.hasScript(this.className))
					{
						((GameBehaviour)mb).removeComponents(this.type);
						affecteds.Add(mb);
					}
				}
			}
			GC.Collect();
			GC.WaitForPendingFinalizers();
			GC.Collect();
			this.type = null;
			scripts.Remove(this);
			GC.Collect();
			GC.WaitForPendingFinalizers();
			GC.Collect();
			return (affecteds);

		}
		public override void Dispose()
		{
			/*base.Dispose();
			List<Transform> affecteds = new List<Transform>();
			if (this.type != null)
			{
				for (int i = 0; i < Transform.Transforms.Count; i++)
				{
					Transform mb = Transform.Transforms[i];

					if (mb != null && mb.hasScript(this.className))
					{
						((GameBehaviour)mb).removeComponents(this.type);
						affecteds.Add(mb);
					}
				}
			}
			if (affecteds.Count > 0)
			{
				throw new Exception("Trying to dispose a script currently attached to a transform.");
				return;
			}
			GC.Collect();
			GC.WaitForPendingFinalizers();
			GC.Collect();
			this.type = null;
			scripts.Remove(this);
			GC.Collect();
			GC.WaitForPendingFinalizers();
			GC.Collect();*/
			//return (affecteds);

		}
		public uint nameExtension = 0;
		public static void CheckUpdates()
		{
			for (int i = 0; i < UserScript.scripts.Count; i++)
			{
				UserScript.scripts[i].checkUpdate();
			}
		}
		public void checkUpdate()
		{
			if (File.Exists(this.scriptPath))
			{
				try
				{
					//if (checkPermissions(this.scriptPath))
					//{
					String tmp = File.ReadAllText(this.scriptPath);
					if (tmp != file)
					{
						this.ReLoad();
					}
					//}
				}
				catch (IOException ioEx)
				{
					;
				}
			}
		}
		public void ReLoad()
		{

			List<Transform> affecteds = this.Unload();
			scripts.Add(this);
			this.Load();
			for (int i = 0; i < affecteds.Count; i++)
			{
				//Debug.Log("oheyyy");
				((GameBehaviour)affecteds[i]).AddComponent(this.type);
			}
		}
		private bool Compile(String path)
		{
			Dictionary<string, string> providerOptions = new Dictionary<string, string>
                {
                    {"CompilerVersion", "v3.5"}
                };
			CSharpCodeProvider provider = new CSharpCodeProvider(providerOptions);

			CompilerParameters compilerParams = new CompilerParameters
			{
				GenerateInMemory = true,
				GenerateExecutable = false,
				IncludeDebugInformation = true
			};
			compilerParams.ReferencedAssemblies.Add(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Goliat.dll"));
			compilerParams.ReferencedAssemblies.Add(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "AssimpNet.dll"));
			compilerParams.ReferencedAssemblies.Add("System.dll");
			compilerParams.ReferencedAssemblies.Add("System.Core.dll");
			//compilerParams.ReferencedAssemblies.Add("AssimpNet.dll");
			String text = File.ReadAllText(path);
			CompilerResults results = provider.CompileAssemblyFromSource(compilerParams, text);
			if (results.Errors.Count != 0)
			{
				foreach (var error in results.Errors)
				{
					Debug.Log(error.ToString());
				}

				Debug.Log("failed");
			}
			Debug.Log("oheyyy");
			//                throw new Exception("Mission failed!");
			className = Path.GetFileNameWithoutExtension(path);
			try
			{
				this.assembly = results.CompiledAssembly;
				type = results.CompiledAssembly.GetType(className);
				UserScript.scriptDictionary[results.CompiledAssembly] = this;
				Debug.Log("TYPE:" + type.Name);
			}
			catch (Exception e)
			{
				return (false);
			}

			//  type = o.GetType();
			//mi.Invoke(o, null);

			/*
			String dir;
			ProcessStartInfo start = new ProcessStartInfo();
			dir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			var permissionSet = new PermissionSet(PermissionState.None);
			dir = Path.Combine(dir, Path.GetFileNameWithoutExtension(path) + ".dll");
			if (File.Exists(dir))
			{
				File.Delete(dir);
			}
			this.dllPath = dir;
			dir = this.dllPath;
			start.FileName = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Mono\\lib\\mono\\4.5\\mcs.exe");
			start.UseShellExecute = false;
			start.RedirectStandardError = true;
			start.RedirectStandardOutput = true;
			start.Arguments = "\"" + path + "\" " + "/target:library" + " " + "/out:" + "\"" + dir + "\"" + " " + "/reference:Goliat.dll" + " /reference:AssimpNet.dll";
			using (Process process = Process.Start(start))
			{
				using (StreamReader reader = process.StandardError)
				{
					string result = reader.ReadToEnd();
					if (result.Length > 0)
						Debug.Log(result);
				}
				using (StreamReader reader = process.StandardOutput)
				{
					string result = reader.ReadToEnd();
					if (result.Length > 0)
						Debug.Log(result);
				}
			}
			//Debug.Log("compilation ok");*/
			return (true);
		}

		private void Compile2(String scriptPath)
		{

		}

		public void Load()
		{
			//Debug.Log("Load");
			Debug.Log("HEYYYYWTF");
			if (Compile(scriptPath))
			{
				String dir = Path.GetDirectoryName(this.dllPath);
				//File.Delete(dir);
				//dir = Path.Combine(dir, Path.GetFileNameWithoutExtension(this.dllPath) + ".dll");
				//className = Path.GetFileNameWithoutExtension(this.dllPath);
				/*byte[] raw = File.ReadAllBytes(this.dllPath);
				Assembly assembly = AppDomain.CurrentDomain.Load(raw);
				type = assembly.GetType(className);
			*/
				//Debug.Log("TYPENAME:" + type.Name);
				scripts.Add(this);
				if (File.Exists(scriptPath))
					this.file = File.ReadAllText(scriptPath);
			}
		}
		public static List<String> Trash = new List<String>();

		public UserScript(String scriptPath)
		{

			//base.register();
			if (File.Exists(scriptPath))
				this.file = File.ReadAllText(scriptPath);

			//this.file
			//Debug.Log("yoyo");
			this.scriptPath = scriptPath;
			this.Load();


		}

	}
}
