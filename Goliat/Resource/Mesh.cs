﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goliat;
using Assimp;
using System.Diagnostics;

//http://thomasdiewald.com/blog/?p=1888 pour mesh colliders, perfecto.

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Platform;
using Config = OpenTK.Configuration;
using Utilities = OpenTK.Platform.Utilities;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Vector3 = Goliat.Vector3;
using Quaternion = Goliat.Quaternion;
using Matrix = Goliat.Matrix;
using Color = Goliat.Color;
using System.IO;
using Goliat.Serialization;
using Goliat.Engine.GUI;
using InspectorCard = Goliat.Engine.GUI.InspectorGUI.InspectorCard;

namespace Goliat
{
	[System.Serializable]
	public class Face
	{
		public int[] indices;
		public int materialIndex = 0;
		public override string ToString()
		{

			string res = base.ToString() + "(\nvertices:\n";
			if (this.vertices != null)
			{
				for (int i = 0; i < this.vertices.Count; i++)
				{
					res += this.vertices[i].ToString();
				}
				res += "end\n";
			}
			else
			{
				res += "face vertices null\n";
			}
			if (indices != null)
			{
				res += "indices\n";
				for (int i = 0; i < this.indices.Length; i++)
				{
					res += this.indices[i].ToString();
				}
				res += "end\n";
			}
			else
			{
				res += "face indices null\n";
			}
			res += "\n);\n";
			return (res);

		}
		public Mesh mesh = null;
		private List<Vector3> _vertices = null;
		public List<Vector3> vertices
		{
			get
			{
				if (this._vertices != null)
				{
					return (this._vertices);
				}
				else if (this.mesh != null && this.indices != null)
				{
					List<Vector3> res = new List<Vector3>();
					for (int i = 0; i < this.indices.Length; i++)
					{
						res.Add(this.mesh.vertices[this.indices[i]]);
					}
					return (res);
				}
				else
				{
					return (null);
				}
			}
			set
			{
				if (this._vertices != null)
				{
					this._vertices = value;
				}
				return;
			}
		}
		private Vector3 _normal = Vector3.zero;

		public Vector3 median
		{
			get
			{
				Vector3 res = Vector3.zero;
				for (int i = 0; i < this.vertices.Count; i++)
				{
					res += this.vertices[i] / this.vertices.Count;
				}
				return (res);
			}
			set
			{
				;
			}
		}/*
		public List<Vector2> planeProjected(Vector3 upVector)
		{
			Quaternion tmp = Quaternion.FromDirection(this._normal, upVector);
			List<Vector2> res = new List<Vector2>();
			for (int i = 0; i < vertices.Count; i++)
			{
				Vector3 rotated = tmp * (this.vertices[i] - this.median);
				Vector2 res2 = new Vector2(rotated.x, rotated.z);
				res.Add(res2);
			}
			return (res);
		}
		*/
		public Vector3 normal
		{
			get
			{
				if (this._normal == Vector3.zero)
				{
					this.recalculateNormal();
					return (this._normal);
				}
				return (this._normal);
			}
			private set
			{
				this._normal = value;
			}
		}
		public void recalculateNormal()
		{
			if (this.vertices != null && this.vertices.Count > 2)
			{
				this._normal = Vector3.Cross(this.vertices[1] - this.vertices[0], this.vertices[2] - this.vertices[1]);
			}
			else
				this._normal = Vector3.zero;
		}
		public Face()
		{
			indices = null;
		}
		public Face(Assimp.Face face)
		{
			indices = new int[face.Indices.Count];
			for (int i = 0; i < face.Indices.Count; i++)
			{
				indices[i] = face.Indices[i];
			}
		}

		public Face(Assimp.Face face, int offset)
		{
			indices = new int[face.Indices.Count];
			for (int i = 0; i < face.Indices.Count; i++)
			{
				indices[i] = face.Indices[i] + offset;
			}
		}

		public Face(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
		{
			indices = null;
			vertices = new List<Vector3>();
			vertices.Add(p0);
			vertices.Add(p1);
			vertices.Add(p2);
			vertices.Add(p3);
		}
		public Plane plane
		{
			get
			{
				if (this.normal != Vector3.zero)
				{
					Plane res = new Plane(this.normal, this.vertices[0]);
					return (res);
				}
				else
				{
					return (Plane.zero);
				}
			}
		}
	}
	[System.Serializable]
	public class Primitive : Mesh
	{
		public static Primitive Sphere
		{
			get
			{
				return (Goliat.Sphere.standard);
			}
		}
		public static Primitive Cube
		{
			get
			{
				return (Goliat.Cube.standard);
			}
		}
	}
	[System.Serializable]
	public class Cube : Primitive
	{
		private static Transform basePrimitive = Transform.ImportFromFile("cubePrimitive.dae");
		public static Cube standard
		{
			get
			{
				return (new Cube(1f));
			}
		}
		public Cube(float radius)
		{
			this._radius = radius;
			this.recalculate();
		}
		private float _radius;
		public float radius
		{
			get
			{
				return (this._radius);
			}
			set
			{
				this._radius = value;
				this.recalculate();
			}
		}
		public void recalculate()
		{

			//this.mesh
			/*for (int i = 0; i < cube.Length; i++)
			{
				;	//this.
			}*/
			//this.recalculateNormals();

		}
	}
	[System.Serializable]
	public class Sphere : Primitive
	{
		public static Sphere standard
		{
			get
			{
				return (new Sphere(1, 1, 32, 16));
			}
		}
		private float _radius;
		public float radius
		{
			get
			{
				return (this._radius);
			}
			set
			{
				this._radius = value;
				this.recalculate();
			}
		}

		private float _height;
		public float height
		{
			get
			{
				return (this._height);
			}
			set
			{
				this._height = value;
				this.recalculate();
			}
		}
		public struct Vertex
		{ // mimic InterleavedArrayFormat.T2fN3fV3f
			public Vector2 TexCoord;
			public Vector3 Normal;
			public Vector3 Position;
		}

		public void calculateVertices()
		{
			var data = new Vertex[(int)segments * (int)rings];

			int i = 0;

			for (double y = 0; y < rings; y++)
			{
				double phi = (y / (rings - 1)) * Math.PI; //was /2 
				for (double x = 0; x < segments; x++)
				{
					double theta = (x / (segments - 1)) * 2 * Math.PI;

					Vector3 v = new Vector3()
					{
						X = (float)(radius * Math.Sin(phi) * Math.Cos(theta)),
						Y = (float)(height * Math.Cos(phi)),
						Z = (float)(radius * Math.Sin(phi) * Math.Sin(theta)),
					};
					Vector3 n = Vector3.Normalize(v);
					Vector2 uv = new Vector2()
					{
						X = (float)(x / (segments - 1)),
						Y = (float)(y / (rings - 1))
					};
					// Using data[i++] causes i to be incremented multiple times in Mono 2.2 (bug #479506).
					data[i] = new Vertex() { Position = v, Normal = n, TexCoord = uv };
					i++;
				}
			}
			this.uvs = new Vector2[data.Length];
			this.vertices = new Vector3[data.Length];
			this.normals = new Vector3[data.Length];
			for (int j = 0; j < data.Length; j++)
			{
				this.uvs[j] = data[j].TexCoord;
				this.normals[j] = data[j].Normal;
				this.vertices[j] = data[j].Position;
			}
		}

		public void calculateElements()
		{
			var num_vertices = segments * rings;
			var data = new int[(int)num_vertices * 6];

			ushort i = 0;

			for (byte y = 0; y < rings - 1; y++)
			{
				for (byte x = 0; x < segments - 1; x++)
				{
					data[i++] = (int)((y + 0) * segments + x);
					data[i++] = (int)((y + 1) * segments + x);
					data[i++] = (int)((y + 1) * segments + x + 1);

					data[i++] = (int)((y + 1) * segments + x + 1);
					data[i++] = (int)((y + 0) * segments + x + 1);
					data[i++] = (int)((y + 0) * segments + x);
				}
			}

			// Verify that we don't access any vertices out of bounds:
			foreach (int index in data)
				if (index >= segments * rings)
					throw new IndexOutOfRangeException();
			int[] tmp = new int[3];
			this.faces = new Face[data.Length / 3];

			for (int j = 0; j < data.Length; j++)
			{
				if (j % 3 == 0 && j != 0)
					this.faces[(int)(j / 3) - 1].indices = (int[])tmp.Clone();
				if (j % 3 == 0)
					tmp = new int[3];
				tmp[j % 3] = data[j];
			}
		}
		public void recalculate()
		{
			this.calculateVertices();
			this.calculateElements();
		}

		private float _segments;
		public float segments
		{
			get
			{
				return (this._segments);
			}
			set
			{
				this._segments = value;
				this.recalculate();
			}
		}

		private float _rings;
		public float rings
		{
			get
			{
				return (this._rings);
			}
			set
			{
				this._segments = value;
				this.recalculate();
			}
		}

		public Sphere(float radius, float height, float segments, float rings)
		{
			this._radius = radius;
			this._height = height;
			this._segments = segments;
			this._rings = rings;
			this.recalculate();

		}
	}
	[System.Serializable]
	public class MeshFilter : GameBehaviour, IEditable
	{
		public void OnInspectorGUI()
		{
			this._sharedMesh = InspectorGUI.RessourceField<Mesh>("Mesh:", this._sharedMesh);
		}
		public Mesh mesh
		{
			get
			{
				return (new Mesh(this._sharedMesh));
			}
			set
			{
				this._sharedMesh = value;
			}
		}

		private Mesh _sharedMesh = new Mesh();
		public Mesh sharedMesh
		{
			get
			{
				return (this._sharedMesh);
			}
			set
			{
				this._sharedMesh = value;
			}
		}
	}
	
	//[System.Serializable]
	public class MeshRenderer : GameBehaviour, IEditable
	{
		public bool castShadows;
		public bool isVisible;
		public List<Goliat.Material> materials = new List<Goliat.Material>();
		public string name;
		public Bounds bounds;
		public bool enabled;
		public bool receiveShadows;
		public void OnInspectorGUI()
		{
			//int count = InspectorGUI.IntField("")
			;
		}
	}
	public enum MeshExtension
	{
		collada, ply, stl, obj
	}
	[Goliat.Serializable]
	public class Mesh : SharedResource, ISerializable
	{
		public int getSubMeshOffset(int subMeshNumber)
		{
			if (subMeshNumber >= subMeshs.Count)
				return (-1);
			int nb = 0;
			for (int i = 0; i < subMeshs.Count; i++)
			{
				if (subMeshNumber == i)
				{
					return (nb);
				}
				else
				{
					nb += subMeshs[i].Length;
				}
			}
			return (nb);
		}

		private List<int[]> subMeshs = new List<int[]>();
		public Mesh(MappedRessourceInfo info)
			: base(info)
		{
			if (info.parent == null)
			{
				; // ici on se gere comme avant en deserialization
			}
			else
			{
				/*Scene scn = (Scene)SharedResource.getRessource(info.root.guid);
				if (scn == null)
				{
					Scene scn = info.root.Regenerate();
				}
				scn.getFromSibling();*/
			}
		}
		[System.NonSerialized]
		public Vector3[] normals = null;
		[System.NonSerialized]
		public Assimp.Mesh importVersion;
		[System.NonSerialized]
		public Assimp.Scene importScene = null;
		[System.NonSerialized]
		public Vector3[] vertices = null;
		[System.NonSerialized]
		public Vector2[] uvs = null;
		public string name;
		[System.NonSerialized]
		public Goliat.Face[] faces = null;
		public List<Vector3[]> debugLines = new List<Vector3[]>();

		public bool hasNormals
		{
			get
			{
				if (normals == null || vertices == null || vertices.Length != normals.Length)
					return (false);
				return (true);
			}
		}
		public void export(string filePath)//, MeshExtension ext)
		{
			//AssimpContext exporter = new AssimpContext();
			//Scene exportScene = new Scene();
			ObjExporter.ExportObj(this, filePath);
			/*exportScene.Meshes = new List<Assimp.Mesh>();
			exportScene.Meshes[0] = new Assimp.Mesh;
			exportScene.Meshes[0].Faces = new List<Assimp.Face>();
			exportScene
			exporter.ExportFile(null, filePath, ext.ToString());*/
		}
		//[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		/*public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
				throw new ArgumentNullException("info");
			if (this.filePath == "")
			{
				//this.guid;
				this.filePath = SharedResource.RessourceFolderPath + "/" + this.guid + ".obj";
				this.export(filePath);
				//AssimpContext exporter = new AssimpContext();

			}
			info.AddValue("filePath", this.filePath, typeof(string));
			//Debug.Log(Serializer.CustomProperty.logPath, "obj here:" + File.ReadAllText(this.filePath));
			//info.AddValue("Text", _Text);
		}*/
		public void OnSerialize(Serializer.SerializationInfo info) //en fait, le faire comme ça que quand il n'est pas présent en tant que méta, sinon, utiliser son guid, pareil pour les animationsClips.
		{
			Console.WriteLine("serilizing mesh");
			if (info == null)
				throw new ArgumentNullException("info");
			if (this.filePath == "")
			{
				//this.guid;
				this.filePath = SharedResource.RessourceFolderPath + "/" + this.guid + ".obj";
				this.export(filePath);
				//AssimpContext exporter = new AssimpContext();

			}
			info.AddValue("guid", this.guid);
			info.AddValue("filePath", (object)this.filePath);
		}

		public void OnDeserialize(Serializer.SerializationInfo info)
		{
			if (info == null)
			{
				Debug.Log(CustomProperty.logPath, "info null");
				throw new ArgumentNullException("info");
			}
			this.guid = (string)info.GetValue("guid");
			if (SharedResource.getRessource(guid) != null)
			{
				Mesh mesh = (Mesh)SharedResource.getRessource(guid);
				this.vertices = mesh.vertices;
				this.uvs = mesh.uvs;
				this.normals = mesh.normals;
				this.faces = mesh.faces;
				return;
			}
			else
			{
				SharedResource.addRessource(this);
			}
			//Console.WriteLine("heyyyyysalut");
			/*if (info.GetValue(filePath) == null)
			{
				Console.WriteLine("value null ;ssss");
			}*/
			this.filePath = (string)info.GetValue("filePath");
			//Console.WriteLine("ohohoh");
			AssimpContext importer = new AssimpContext();
			try
			{
				Stopwatch sw = Stopwatch.StartNew();
				Mesh mesh = ObjExporter.StringToMesh(File.ReadAllText(this.filePath));
				this.faces = mesh.faces;
				this.vertices = mesh.vertices;
				this.uvs = mesh.uvs;
				this.normals = mesh.normals;
				sw.Stop();
				Debug.Log(CustomProperty.logPath, "elapsed for convert to goliat mesh:" + sw.ElapsedMilliseconds);
			}
			catch (Exception e)
			{
				;

			}

		}

		public override string ToString()
		{
			string res = "Mesh:\n";
			if (normals != null)
			{
				res += "normals:\n";
				for (int i = 0; i < normals.Length; i++)
				{
					res += normals[i].ToString();
				}
				res += "end\n";
			}
			else
			{
				res += "normals null\n";
			}
			if (vertices != null)
			{
				res += "vertices:\n";
				for (int i = 0; i < vertices.Length; i++)
				{
					res += vertices[i].ToString();
				}
				res += "end\n";
			}
			else
			{
				res += "vertices null\n";
			}
			if (uvs != null)
			{
				res += "uvs:\n";
				for (int i = 0; i < uvs.Length; i++)
				{
					res += uvs[i].ToString();
				}
				res += "end\n";
			}
			else
			{
				res += "uvs null\n";
			}
			if (faces != null)
			{
				res += "faces:\n";
				for (int i = 0; i < faces.Length; i++)
				{
					if (faces[i] == null)
					{
						res += "null face\n";
					}
					else
						res += faces[i].ToString();
				}
				res += "end\n";
			}
			else
			{
				res += "faces null\n";
			}
			return (res);
		}
		public bool hasTextureCoords
		{
			get
			{
				if (uvs == null || vertices == null || vertices.Length != uvs.Length)
					return (false);
				return (true);
			}
		}
		public Mesh(Mesh copy)
		{
			this.normals = (Vector3[])copy.normals.Clone();
			this.importVersion = copy.importVersion;
			this.vertices = (Vector3[])copy.vertices.Clone();
			this.uvs = (Vector2[])copy.uvs.Clone();
			this.faces = (Face[])copy.faces.Clone();
		}



		private static Mesh checkNodeRecursive(Assimp.Node node, Assimp.Scene m_model, List<int> hierarchyPath, int siblingIndex)
		{
			if ((hierarchyPath == null || hierarchyPath.Count == 0) && siblingIndex < node.ChildCount)
			{
				return (getMesh(node.Children[siblingIndex], m_model));
			}
			if (node.Children != null && node.ChildCount > hierarchyPath[0])
			{
				int index = hierarchyPath[0];
				hierarchyPath.RemoveAt(0);
				return (checkNodeRecursive(node.Children[index], m_model, hierarchyPath, siblingIndex));
			}
			return (null);
		}

		private static Mesh getMesh(Assimp.Node node, Assimp.Scene m_model)
		{
			Mesh sharedMesh = null;
			sharedMesh = new Mesh();
			for (int i = 0; i < node.MeshIndices.Count; i++)
			{
				
				List<Material> materials = new List<Goliat.Material>();

				Assimp.Mesh mesh = m_model.Meshes[node.MeshIndices[i]];
				Assimp.Material mat = m_model.Materials[mesh.MaterialIndex];
				for (int j = 0; j < mat.GetMaterialTextures(TextureType.Diffuse).Length; j++)
				{
					materials.Add(new Goliat.Material(mat, j));
				}
				if (mat.GetMaterialTextures(TextureType.Diffuse).Length == 0)
				{
					materials.Add(new Goliat.Material(mat, 0));
				}
				if (sharedMesh == null)
					sharedMesh = new Mesh();
				sharedMesh.Assimp_Import(m_model.Meshes[node.MeshIndices[i]], m_model, true);
			}
			return (sharedMesh);
		}

		private static Assimp.AssimpContext importer = new Assimp.AssimpContext();
		public static Mesh ImportFromInfo(RessourceInfo info, List<int> hierarchyPath, int siblingIndex)
		{
			Assimp.Scene m_model = importer.ImportFile(info.filePath, Assimp.PostProcessPreset.TargetRealTimeMaximumQuality);
			Assimp.Node current = m_model.RootNode;
			return (checkNodeRecursive(current, m_model, (List<int>)hierarchyPath.Clone(), siblingIndex));
		}
		public Mesh()
		{
			;
		}

		private Matrix4 FromMatrix(Matrix4x4 mat)
		{
			Matrix4 m = new Matrix4();
			m.M11 = mat.A1;
			m.M12 = mat.A2;
			m.M13 = mat.A3;
			m.M14 = mat.A4;
			m.M21 = mat.B1;
			m.M22 = mat.B2;
			m.M23 = mat.B3;
			m.M24 = mat.B4;
			m.M31 = mat.C1;
			m.M32 = mat.C2;
			m.M33 = mat.C3;
			m.M34 = mat.C4;
			m.M41 = mat.D1;
			m.M42 = mat.D2;
			m.M43 = mat.D3;
			m.M44 = mat.D4;
			return m;
		}
		public void recalculateBounds()
		{
			if (this.faces == null || this.faces.Count() < 1)
			{
				this._bounds = new Bounds(Vector3.zero, Vector3.zero);
			}
			bool check = false;
			Vector3 min = Vector3.zero;
			Vector3 max = Vector3.zero;
			Vector3 center = Vector3.zero;
			this._bounds = new Bounds(Vector3.zero, Vector3.one);
			for (int i = 0; i < this.faces.Count(); i++)
			{
				if (this.faces[i].indices != null && this.faces[i].indices.Length > 0 && !check)
				{
					min = this.vertices[this.faces[i].indices[0]];
					max = this.vertices[this.faces[i].indices[0]];
					check = true;
				}
				else if (this.faces[i].indices != null && this.faces[i].indices.Length > 0 && check)
				{
					foreach (int indice in this.faces[i].indices)
					{
						min = min.toMinValues(this.vertices[indice]);
						max = max.toMaxValues(this.vertices[indice]);
						//center += (this.vertices[indice] / this.vertices.Length);
					}
				}
			}
			for (int i = 0; i < this.vertices.Length; i++)
			{
				center += this.vertices[i] / this.vertices.Length;
			}
			this._bounds.max = max;
			this._bounds.min = min;
			this._bounds.center = center;
		}
		[System.NonSerialized]
		private Bounds _bounds = null;
		public Bounds bounds
		{
			get
			{
				if (this._bounds == null && this.vertices != null)
				{
					this.recalculateBounds();
				}
				return (this._bounds);
			}
			private set
			{
				this._bounds = value;
			}
		}

		public override void generateIcon()
		{
			FrameBufferObject tmp = new FrameBufferObject(90, 90);
			tmp.bind();

			GL.MatrixMode(MatrixMode.Modelview);
			GL.LoadIdentity();
			Matrix4 lookat2;
			//Transform camTransform = (Transform)ctrl.Tag;
			//Matrix4 perspective = ((Transform)ctrl.Tag).getComponent<Camera>().perspectiveMatrix.gl();
			//Camera cam = ((Transform)ctrl.Tag).getComponent<Camera>();
			//cam.width = ctrl.Size.Width;
			//cam.height = ctrl.Size.Height;

			Camera cam = new Camera();

			Transform pivotPoint = Transform.zero;
			cam.pivotPoint = pivotPoint;
			float aspectRatio = tmp.width / (float)tmp.height;
			cam.aspectRatio = aspectRatio;
			cam.width = tmp.width;
			cam.height = tmp.height;
			//Console.WriteLine(cam.width);
			cam.nearClipPlane = 1;
			cam.farClipPlane = 1000;
			cam.fov = MathHelper.PiOver4.toDeg();

			//Matrix4 perspective = Matrix4.CreatePerspectiveFieldOfView(cam.fov, cam.aspectRatio, cam.nearClipPlane, cam.farClipPlane);
			Matrix4 perspective = cam.perspectiveMatrix.gl();
			/*lookat2 = camTransform.worldToLocalMatrix.gl();
			lookat2.Transpose();
			GL.LoadMatrix(ref lookat2);*/
			//Camera cam = scn.cameraTransform.getComponent<Camera>();// cam.aspectRatio = 

			//Console.WriteLine("octogon:" + perspective);

			GL.MatrixMode(MatrixMode.Projection);
			GL.Viewport(0, 0, tmp.width, tmp.height);
			GL.LoadMatrix(ref perspective);
			this.render(cam);

			Debug.Log("Saved texture plzz!!");
			GL.Flush();
			tmp.unbind();

			//System.Threading.Thread.Sleep(1000);
			tmp.texture.save("C:/tmp/testalien2.png", System.Drawing.RotateFlipType.Rotate90FlipNone);
			this.icon = tmp.texture;
			return;
			/*FrameBufferObject fbo = new FrameBufferObject(250, 250);
			fbo.bind();
			this.render();
			fbo.texture.save("C:/tmp/testtext.png");
			fbo.unbind();
			this.icon = fbo.texture;*/
		}
		public void render(Camera cam)
		{

			OpenTK.Matrix4 m = Transform.zero.transformation.gl();
			GL.FrontFace(FrontFaceDirection.Ccw);

			GL.MatrixMode(MatrixMode.Modelview);
			GL.Enable(EnableCap.Texture2D);
			GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);
			GL.Enable(EnableCap.Lighting);
			GL.Enable(EnableCap.Light0);
			GL.Enable(EnableCap.DepthTest);
			GL.Enable(EnableCap.Normalize);
			float[] mat_specular = { 1.0f, 1.0f, 1.0f, 1.0f };
			float[] mat_shininess = { 50.0f };
			float[] light_position = { 1.0f, 1.0f, 1.0f, 0.0f };
			float[] light_ambient = { 0.5f, 0.5f, 0.5f, 1.0f };
			GL.Light(LightName.Light0, LightParameter.Diffuse, mat_specular);
			GL.Light(LightName.Light0, LightParameter.Specular, mat_specular);
			GL.LoadIdentity();
			Matrix4 lookat2;
			//Console.WriteLine("center:" + this.bounds.center);
			Vector3 center = this.bounds.center;
			Vector3 farer = Vector3.zero;
			for (int i = 0; i < this.vertices.Length; i++)
			{
				if (farer.magnitude < (this.vertices[i] - this.bounds.center).magnitude)
				{
					farer = this.vertices[i] - this.bounds.center;
				}
			}

			Vector3 farer2 = Vector3.zero;
			farer = farer + this.bounds.center;
			for (int i = 0; i < this.vertices.Length; i++)
			{
				if (farer2.magnitude < (this.vertices[i] - farer).magnitude)
				{
					farer2 = this.vertices[i] - farer;
				}
			}
			farer2 = farer2 + farer;
			center = (farer + farer2) / 2;
			float dist = (farer - center).magnitude / (float)Math.Tan(cam.fov.toRad() / 2);

			Vector3 cross = Vector3.Cross(farer - center, Vector3.one).normalized;
			//Console.WriteLine("farer mag:" + dist);
			Transform cameraTransform = new Transform(new Vector3(center.x + cross.x * dist, center.y + cross.y * dist, center.z + cross.z * dist), Quaternion.identity, Vector3.one);
			cam.AddComponent(cameraTransform);
			cameraTransform.LookAt(center, Vector3.up);
			lookat2 = cameraTransform.worldToLocalMatrix.gl();
			lookat2.Transpose();
			GL.LoadMatrix(ref lookat2);
			//GL.Clear(ClearBufferMask.ColorBufferBit);
			GL.ClearColor(Color.LightBlue.gl());
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			GL.FrontFace(FrontFaceDirection.Ccw);

			GL.LoadIdentity();
			GL.LoadMatrix(ref lookat2);

			m.Transpose();
			GL.Disable(EnableCap.LineStipple);
			GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
			Matrix4 m2 = Transform.zero.transformation.gl();
			m2.Transpose();
			GL.PushMatrix();
			GL.MultMatrix(ref m);
			Material.standard.Bind();
			Goliat.Mesh myMesh = this;
			Assimp.Mesh mesh = myMesh.importVersion;
			if (myMesh.hasNormals)
			{
				GL.Enable(EnableCap.Lighting);
			}
			else
			{
				GL.Disable(EnableCap.Lighting);
			}
			foreach (Face face in myMesh.faces)
			{
				BeginMode faceMode;
				switch (face.indices.Length)
				{
					case 1:
						faceMode = BeginMode.Points;
						break;
					case 2:
						faceMode = BeginMode.Lines;
						break;
					case 3:
						faceMode = BeginMode.Triangles;
						break;
					default:
						faceMode = BeginMode.Polygon;
						break;
				}


				GL.Begin(OpenTK.Graphics.OpenGL.PrimitiveType.Polygon);

				for (int i = 0; i < face.indices.Length; i++)
				{
					int indice = face.indices[i];
					GL.Color4(Color.LightGray.gl());
					/*if (myMesh.hasColors)
					{
						Color4 vertColor = FromIntColor(0);
						if (hasColors)
							vertColor = FromColor(mesh.VertexColorChannels[0][indice]);

						GL.Color4(vertColor);
					}*/
					if (myMesh.hasNormals)
					{
						//OpenTK.Vector3 normal = FromVector(mesh.Normals[indice]);
						if (myMesh.normals != null)
						{
							OpenTK.Vector3 normal = myMesh.normals[indice].gl();
							GL.Normal3(normal);
						}
					}
					OpenTK.Vector3 pos = myMesh.vertices[indice].gl();
					GL.Vertex3(pos);
				}

				GL.End();
			}

			GL.PopMatrix();
		}
		public void recalculateNormals()
		{
			//Console.WriteLine("heyyy0");
			if (this.normals == null)
			{
				Console.WriteLine("normals null bordel");
			}
			//	Console.WriteLine("heyyy1");
			for (int i = 0; i < this.normals.Length; i++)
			{
				this.normals[i] = Vector3.zero;
			}
			//		Console.WriteLine("heyyy2");
			if (this.faces == null)
			{
				Console.WriteLine("faces null bordel");
			}
			//			Console.WriteLine("heyyy3");
			for (int i = 0; i < this.faces.Length; i++)
			{
				if (this.faces[i] == null)
				{
					Console.WriteLine("got a null face");
				}
				if (this.faces[i].indices != null)
				{
					;
				}
				for (int j = 0; j < this.faces[i].indices.Length; j++)
				{
					this.normals[this.faces[i].indices[j]] += faces[i].normal;
				}
			}
			for (int i = 0; i < this.normals.Length; i++)
			{
				this.normals[i] = this.normals[i].normalized;
			}
		}
		public void Assimp_Import(Assimp.Mesh mesh, Assimp.Scene scene)
		{
			//Console.WriteLine(scene.RootNode.Name);
			importVersion = mesh;
			normals = new Vector3[mesh.Normals.Count];
			vertices = new Vector3[mesh.Vertices.Count];
			faces = new Face[mesh.Faces.Count];
			int compcount = mesh.UVComponentCount[0];
			//Console.WriteLine("UV COMPONENT COUNT:" + compcount);
			Console.WriteLine("UV CHANNEL COUNT:" + mesh.TextureCoordinateChannelCount);
			int[] materialIndex = new int[vertices.Length];
			subMeshs = new List<int[]>();
			int[] indices = new int[vertices.Length];
			int verticesCount = mesh.Vertices.Count;

			for (int i = 0; i < verticesCount; i++)
			{

				vertices[i] = mesh.Vertices[i].octogon();
				indices[i] = i;
			}
			subMeshs.Add(indices);
			int normalCount = mesh.Normals.Count;
			if (normalCount != verticesCount)
			{
				normalCount = verticesCount;
				normals = new Vector3[normalCount];
				for (int i = 0; i < normalCount; i++)
				{
					normals[i] = Vector3.zero;
				}
			}
			else
			{
				//Console.WriteLine("normals pre count:" + mesh.Normals.Count);
				for (int i = 0; i < normalCount; i++)
				{
					normals[i] = mesh.Normals[i].octogon();
				}
			}
			Vector3[] tmp_uv = mesh.TextureCoordinateChannels[0].ToArray().octogon();

			uvs = new Vector2[tmp_uv.Length];
			//Console.WriteLine("heyyy0.0");
			for (int i = 0; i < tmp_uv.Length; i++)
			{
				uvs[i] = new Vector2(tmp_uv[i].x, 1 - tmp_uv[i].y);
				materialIndex[i] = (int)tmp_uv[i].z;
				if (materialIndex[i] != 0)
				{
					Console.WriteLine("HEYEYEYEYEYEYEYEYEYEYYEEYEYEYEYEY");
				}

			}

			int facesCount = mesh.Faces.Count;
			for (int i = 0; i < facesCount; i++)
			{
				faces[i] = mesh.Faces[i].octogon();
				faces[i].mesh = this;
				faces[i].materialIndex = 0;
			}
			
			//Console.WriteLine("heyyy0.1");
			this.recalculateNormals();

		}

		public void Assimp_Import(Assimp.Mesh mesh, Assimp.Scene scene, bool expand)
		{
			if (!expand)
			{
				this.Assimp_Import(mesh, scene);
			}
			//Console.WriteLine(scene.RootNode.Name);
			importVersion = mesh;
			int startIndex = 0;
			int materialIndex = 0;
			Console.WriteLine("heyyy");
			
			if (subMeshs == null)
				subMeshs = new List<int[]>();
			if (subMeshs.Count > 0)
			{
				Console.WriteLine("submeshs have count");
				if (subMeshs[subMeshs.Count - 1] != null)
				{
					for (int i = 0; i < subMeshs.Count; i++ )
						startIndex += subMeshs[i].Length; // faux.
				}
				else
				{
					Console.WriteLine("MERRDDEEEEUHHHh");
				}
				materialIndex = subMeshs.Count;
			}
			else
			{
				;
			}
			Console.WriteLine("WHAT?");
			int[] indices = new int[mesh.Vertices.Count];
			//subMeshs.Add(new int[mesh.Vertices.Count]);
			Console.WriteLine("hey kedal");

			Vector3[] normals = new Vector3[mesh.Vertices.Count + startIndex];
			Vector3[] vertices = new Vector3[mesh.Vertices.Count + startIndex];
			int faceCount = this.faces != null ? this.faces.Length : 0;
			Face[] faces = new Face[mesh.Faces.Count + faceCount];
			Vector3[] tmp_uv = mesh.TextureCoordinateChannels[0].ToArray().octogon();
			Vector2[] uvs = new Vector2[mesh.Vertices.Count + startIndex];

			Console.WriteLine("heyy0");
			if (this.vertices != null)
			{
				for (int i = 0; i < this.vertices.Length; i++)
				{
					normals[i] = this.normals[i];
					vertices[i] = this.vertices[i];
					uvs[i] = this.uvs[i];
				}
				for (int i = 0; i < faceCount; i++)
				{
					faces[i] = this.faces[i];
				}
			}

			int compcount = mesh.UVComponentCount[0];
			//Console.WriteLine("UV COMPONENT COUNT:" + compcount);
			Console.WriteLine("UV CHANNEL COUNT:" + mesh.TextureCoordinateChannelCount);
			//int[] materialIndex = new int[vertices.Length];
			Console.WriteLine("heyy0.1");
			int verticesCount = mesh.Vertices.Count;

			//Console.WriteLine("nb vertices:" + (verticesCount + startIndex));
			
			for (int i = 0; i < verticesCount; i++)
			{
				vertices[i + startIndex] = mesh.Vertices[i].octogon();
				indices[i] = i + startIndex;
			}
			subMeshs.Add(indices);
			int normalCount = mesh.Normals.Count;
			if (normalCount != verticesCount)
			{
				normalCount = verticesCount;
				//normals = new Vector3[normalCount];
				for (int i = 0; i < normalCount; i++)
				{
					normals[i + startIndex] = Vector3.zero;
				}
			}
			else
			{
				//Console.WriteLine("normals pre count:" + mesh.Normals.Count);
				for (int i = 0; i < normalCount; i++)
				{
					normals[i + startIndex] = mesh.Normals[i].octogon();
				}
			}
			Console.WriteLine("hey 0.2");
			
			//Console.WriteLine("heyyy0.0");
			for (int i = 0; i < mesh.Vertices.Count; i++)
			{
				if (tmp_uv.Length > i)
					uvs[i + startIndex] = new Vector2(tmp_uv[i].x, 1 - tmp_uv[i].y);
				else
				{
					uvs[i + startIndex] = Vector2.zero;
				}
				//materialIndex[i] = (int)tmp_uv[i].z;

			}

			int facesCount = mesh.Faces.Count;
			for (int i = 0; i < facesCount; i++)
			{
				faces[i + faceCount] = mesh.Faces[i].octogon(startIndex);
				faces[i + faceCount].mesh = this;
				faces[i + faceCount].materialIndex = materialIndex;
			}
			this.vertices = vertices;
			this.uvs = uvs;
			this.normals = normals;
			this.faces = faces;
			Console.WriteLine("hey 0.3");
			Console.WriteLine(this.guid + ";i got now:" + this.vertices.Length + "vertices");

			//Console.WriteLine("heyyy0.1");
			this.recalculateNormals();

		}
		public List<float> computeBarycentric(Vector2 p, List<Vector2> polygon, int n)
		{
			List<float> weights = new List<float>();
			int j = 0;
			float weightSum = 0;
			foreach (Vector2 qj in polygon)
			{
				Vector2 prev = polygon[(j - 1).mod(n)];
				Vector2 next = polygon[(j + 1).mod(n)];
				float wj = (cotangent(p, qj, prev) + cotangent(p, qj, next)) / (p - qj).magnitude.square();
				weightSum += wj;
				j++;
			}
			j = 0;
			foreach (float wj in weights)
			{
				weights[j] = wj / n;
				j++;
			}
			return (weights);
		}
		public float cotangent(Vector2 a, Vector2 b, Vector2 c)
		{
			Vector2 ba = a - b;
			Vector2 bc = c - b;
			return (bc.dot(ba) / (bc * ba).magnitude);
		}
	}



	[System.Serializable]
	public struct CoordinateLimiter
	{
		public List<bool> _limits;
		public List<bool> limits
		{
			get
			{
				return ((List<bool>)this._limits.Clone());
			}
			private set
			{
				this._limits = value;
			}
		}
		public CoordinateLimiter(bool x, bool y, bool z)
		{
			this._limits = new List<bool>();
			this.limits.Add(x);
			this.limits.Add(y);
			this.limits.Add(z);
		}
		public CoordinateLimiter(List<bool> limits)
		{
			this._limits = (List<bool>)limits.Clone();
		}
	}

}
