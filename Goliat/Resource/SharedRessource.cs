﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Platform;
using System.Reflection;
using System.Drawing;
using System.Drawing.Imaging;
using TextureType = Assimp.TextureType;
using TextureSlot = Assimp.TextureSlot;
using PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web.Script.Serialization;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Color = Goliat.Color;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Reflection;
using System.IO;
using Goliat.Serialization;
using StackTrace = System.Diagnostics.StackTrace;
namespace Goliat
{
	public class Sound : SharedResource
	{
		public Sound(string filePath)
		{
			;
		}
	}


	

	

	public class AudioClip : SharedResource, IDisposable
	{
		private NAudio.Wave.WaveOut waveOut = null;
		//public string filePath = "";
		private NAudio.Wave.Mp3FileReader rdr = null;
		public AudioClip(string filePath)
		{
			this.filePath = filePath;
			rdr = new NAudio.Wave.Mp3FileReader(filePath);
			this.waveOut = new NAudio.Wave.WaveOut(NAudio.Wave.WaveCallbackInfo.FunctionCallback());
			this.waveOut.Init(rdr);
		}

		public AudioClip(MappedRessourceInfo info)
			: base(info)
		{
			;
		}
		public void Play()
		{
			if (this.waveOut.PlaybackState == NAudio.Wave.PlaybackState.Playing)
			{
				this.waveOut.Stop();
			}
			this.waveOut.Play();
		}
		public void Pause()
		{
			if (this.waveOut.PlaybackState == NAudio.Wave.PlaybackState.Playing)
			{
				this.waveOut.Pause();
			}
		}
		public void Resume()
		{
			if (this.waveOut.PlaybackState == NAudio.Wave.PlaybackState.Playing)
			{
				this.waveOut.Resume();
			}
		}
		public void Stop()
		{
			if (this.waveOut.PlaybackState == NAudio.Wave.PlaybackState.Playing)
			{
				this.waveOut.Stop();
			}
		}
		public void Dispose()
		{
			rdr.Dispose();
			waveOut.Dispose();
		}
	}

	[Goliat.Serializable]
	[System.Serializable]
	public class SharedResource : IDisposable
	{
		public static bool mapped = false;
		public static string RessourceFolderPath
		{
			get
			{
				string res = "../../SharedRessources";
				if (!Directory.Exists(res))
				{
					Directory.CreateDirectory(res);
				}
				return (res);
			}
		}


		[System.Serializable]
		public enum RessourceType
		{
			None,
			Texture,
			Sound,
			Scene,
			Script,
			Material,
			Mesh,
			Animation
		}

		public static void cleanMetaRecursively(string directoryPath)
		{
			try
			{
				string[] filePaths = Directory.GetFiles(directoryPath);
				for (int i = 0; i < filePaths.Length; i++)
				{
					string meta = Path.ChangeExtension(filePaths[i], ".meta");
					if (File.Exists(meta))
					{
						File.Delete(meta);
					}
				}
				foreach (string directory in Directory.GetDirectories(directoryPath))
				{
					cleanMetaRecursively(directory);
				}
			}
			catch (Exception e)
			{
				;
			}
		}

		public static void cleanMeta()
		{
			string directoryPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Assets");
			cleanMetaRecursively(directoryPath);
		}
		/*public static SharedResource SharedResource[string key, bool loadedOnly]
		{
			get
			{
				if (RessourceDictionary.ContainsKey(key))
				{
					return (RessourceDictionary[key]);
				}
				else if (!loadedOnly && MappedRessourceInfo.RessourceMap.ContainsKey(key))
				{
					return (MappedRessourceInfo.RessourceMap[key].Regenerate());
				}
				return (null);
			}
			private set
			{
				if (!RessourceDictionary.ContainsKey(key))
				{
					RessourceDictionary.Add(key, value);
				}
				else
				{
					RessourceDictionary[key] = value;
				}
			}
		}

		public static SharedResource this[string key]
		{
			get
			{
				if (RessourceDictionary.ContainsKey(key))
				{
					return (RessourceDictionary[key]);
				}
				else if (MappedRessourceInfo.RessourceMap.ContainsKey(key))
				{
					return (MappedRessourceInfo.RessourceMap[key].Regenerate());
				}
				return (null);
			}
			private set
			{
				if (!RessourceDictionary.ContainsKey(key))
				{
					RessourceDictionary.Add(key, value);
				}
				else
				{
					RessourceDictionary[key] = value;
				}
			}
		}*/
		public static List<SharedResource> RessourceList
		{
			get
			{
				List<SharedResource> res = new List<SharedResource>();
				foreach (SharedResource ressource in RessourceDictionary.Values)
				{
					res.Add(ressource);
				}
				return (res);
			}
		}

		[System.Serializable]
		public class MappedRessourceInfo
		{
			private bool _expanded = false;
			public bool expanded
			{
				get
				{
					this.CheckPresence();
					if (MappedRessourceInfo.RessourceMap[this.guid] != this)
					{
						return (MappedRessourceInfo.RessourceMap[this.guid].expanded);
					}
					else
					{
						return (this._expanded);
					}
				}
				set
				{
					this.CheckPresence();
					if (MappedRessourceInfo.RessourceMap[this.guid] != this)
					{
						MappedRessourceInfo.RessourceMap[this.guid].expanded = value;
					}
					else
					{
						this._expanded = value;
					}
				}
			}
			public string guid = "";
			public string filePath = "";
			public RessourceType type = RessourceType.None;
			public string ext = "";
			public List<MappedRessourceInfo> sons = new List<MappedRessourceInfo>();
			public List<int> sibling = null;
			public MappedRessourceInfo parent = null;
			public int LastWriteSeconds = 0;
			public List<MappedRessourceInfo> meshs = new List<MappedRessourceInfo>();
			public List<MappedRessourceInfo> materials = new List<MappedRessourceInfo>();
			public List<MappedRessourceInfo> textures = new List<MappedRessourceInfo>();
			public List<MappedRessourceInfo> animationsClips = new List<MappedRessourceInfo>();
			public string name = "";
			public MappedRessourceInfo root
			{
				get
				{
					if (this.parent != null)
					{
						return (this.parent.root);
					}
					else
					{
						return (this);
					}
				}
			}
			public static Dictionary<string, MappedRessourceInfo> RessourceMap = new Dictionary<string, MappedRessourceInfo>();
			public void Register()
			{
				if (!RessourceMap.ContainsKey(this.guid))
				{
					RessourceMap.Add(this.guid, this);
				}
				else
				{
					RessourceMap[this.guid] = this;
				}
			}
			public MappedRessourceInfo(MappedRessourceInfo parent, Scene node, List<int> sibling)
			{
				this.parent = parent;
				this.guid = node.guid;
				this.sibling = sibling;
				node.generateIcon();
				this.name = node.name;
				if (!Directory.Exists("Meta"))
				{
					Directory.CreateDirectory("Meta");
				}
				string folderpath = "Meta/" + node.guid;
				if (node.guid.Length >= 2)
				{
					folderpath = "Meta/" + node.guid.Substring(0, 2);
				}
				if (!Directory.Exists(folderpath))
				{
					Directory.CreateDirectory(folderpath);
				}
				folderpath = folderpath + "/" + node.guid;
				if (!Directory.Exists(folderpath))
				{
					Directory.CreateDirectory(folderpath);
				}
				folderpath += "/icon";
				if (!Directory.Exists(folderpath))
				{
					Directory.CreateDirectory(folderpath);
				}
				node.icon.save(folderpath + "/" + node.guid + ".png", System.Drawing.RotateFlipType.Rotate270FlipNone);
				node.icon.Dispose();
				for (int i = 0; i < node.children.Count; i++)
				{
					List<int> newSibling = (List<int>)sibling.Clone();
					newSibling.Add(i);
					MappedRessourceInfo son = new MappedRessourceInfo(this, node.children[i], newSibling);
					this.sons.Add(son);
				}
				for (int i = 0; i < node.meshs.Count; i++)
				{
					MeshInfo info = node.meshs[i];
					List<int> newSibling = (List<int>)sibling.Clone();
					newSibling.Add(i);
					MappedRessourceInfo son = new MappedRessourceInfo(this, info, newSibling);
					this.meshs.Add(son);
				}

				this.Register();
			}
			public MappedRessourceInfo(MappedRessourceInfo parent, AnimationClip clip, List<int> sibling)
			{
				this.name = parent.name;
				this.type = RessourceType.Animation;
				this.guid = clip.guid;
				this.parent = parent;
				this.sibling = sibling;
				this.Register();
			}
			public MappedRessourceInfo(MappedRessourceInfo parent, Material material, List<int> sibling)
			{
				this.name = parent.name;
				this.type = RessourceType.Material;
				this.guid = material.guid;
				this.parent = parent;
				this.sibling = sibling;
				for (int i = 0; i < material.textures.Length; i++)
				{
					if (material.textures[i] != null)
					{
						List<int> newSibling = (List<int>)sibling.Clone();

						MappedRessourceInfo son = new MappedRessourceInfo(this, material.textures[i], newSibling);
						this.textures.Add(son);
					}
				}
				this.Register();
			}

			public MappedRessourceInfo(MappedRessourceInfo parent, Texture texture, List<int> sibling)
			{
				this.type = RessourceType.Texture;
				this.guid = texture.guid;
				this.parent = parent;
				this.sibling = sibling;
				this.Register();
				this.name = parent.name;
			}
			public MappedRessourceInfo(MappedRessourceInfo parent, MeshInfo info, List<int> sibling)
			{
				this.name = info.name;
				this.parent = parent;
				this.guid = info.mesh.guid;
				this.sibling = sibling;
				this.type = RessourceType.Mesh;
				for (int i = 0; i < info.materials.Count; i++)
				{
					List<int> newSibling = (List<int>)sibling.Clone();
					newSibling.Add(i);
					MappedRessourceInfo son = new MappedRessourceInfo(this, info.materials[i], newSibling);
					this.materials.Add(son);
				}
				info.mesh.generateIcon();
				if (!Directory.Exists("Meta"))
				{
					Directory.CreateDirectory("Meta");
				}
				string folderpath = "Meta/" + info.mesh.guid;
				if (info.mesh.guid.Length > 2)
					folderpath = "Meta/" + info.mesh.guid.Substring(0, 2);
				if (!Directory.Exists(folderpath))
				{
					Directory.CreateDirectory(folderpath);
				}
				folderpath = folderpath + "/" + info.mesh.guid;
				if (!Directory.Exists(folderpath))
				{
					Directory.CreateDirectory(folderpath);
				}
				folderpath += "/icon";
				if (!Directory.Exists(folderpath))
				{
					Directory.CreateDirectory(folderpath);
				}
				info.mesh.icon.save(folderpath + "/" + info.mesh.guid + ".png", System.Drawing.RotateFlipType.Rotate270FlipNone);
				info.mesh.icon.Dispose();
				this.Register();
			}
			private Bitmap _icon = null;
			public Bitmap icon
			{
				get
				{
					return (this.getIcon());
				}
				set
				{
					this._icon = value;
				}
			}
			public void CheckPresence()
			{
				if (!MappedRessourceInfo.RessourceMap.ContainsKey(this.guid))
				{
					MappedRessourceInfo.RessourceMap.Add(this.guid, this);
				}
			}
			public Bitmap getIcon()
			{

				if (this._icon != null)
				{
					return (this._icon);
				}
				if (!MappedRessourceInfo.RessourceMap.ContainsKey(this.guid))
				{
					MappedRessourceInfo.RessourceMap.Add(this.guid, this);
				}
				else if (MappedRessourceInfo.RessourceMap[this.guid] != this)
				{
					return (MappedRessourceInfo.RessourceMap[this.guid].icon);
				}
				if (!Directory.Exists("Meta"))
				{
					Directory.CreateDirectory("Meta");
				}
				string folderpath = "Meta/" + this.guid;
				if (this.guid.Length >= 2)
				{
					folderpath = "Meta/" + this.guid.Substring(0, 2);
				}


				if (!Directory.Exists(folderpath))
				{
					Directory.CreateDirectory(folderpath);
				}
				folderpath = folderpath + "/" + this.guid;
				if (!Directory.Exists(folderpath))
				{
					Directory.CreateDirectory(folderpath);
				}
				folderpath += "/icon";
				/*				if (!Directory.Exists(folderpath))
								{
									Directory.CreateDirectory(folderpath);
								}*/
				folderpath += "/" + this.guid + ".png";

				if (!File.Exists(folderpath))
				{
					return (null);
				}
				this._icon = new Bitmap(folderpath);
				return (this._icon);
			}
			public MappedRessourceInfo(string filePath)
			{
				bool debug = false;
				this.LastWriteSeconds = DateTime.Now.Second;
				if (Path.GetFileName(filePath) == "alien2.dae")
				{
					Console.WriteLine("alien found");
					debug = true;
				}
				string ext = Path.GetExtension(filePath);
				//this.guid = info.mguid;
				//Bitmap bitmap = new Bitmap();
				this.filePath = filePath;
				//	bool check = false;
				if (ext.IsMeshExtension())
				{
					this.type = RessourceType.Scene;
					Scene scn = new Scene(filePath);
					this.name = scn.name;
					this.guid = scn.guid;
					for (int i = 0; i < scn.children.Count; i++)
					{
						List<int> sibling = new List<int>();
						sibling.Add(i);
						MappedRessourceInfo son = new MappedRessourceInfo(this, scn.children[i], sibling);
						this.sons.Add(son);
					}
					for (int i = 0; i < scn.meshs.Count; i++)
					{
						MeshInfo info = scn.meshs[i];
						List<int> newSibling = new List<int>();
						newSibling.Add(i);
						MappedRessourceInfo son = new MappedRessourceInfo(this, info, newSibling);
						this.meshs.Add(son);
					}
					if (scn.animation != null)
					{
						for (int i = 0; i < scn.animation.clips.Count; i++)
						{
							List<int> newSibling = new List<int>();
							newSibling.Add(i);
							MappedRessourceInfo son = new MappedRessourceInfo(this, scn.animation.clips[i], newSibling);
							this.animationsClips.Add(son);
						}
					}
					scn.generateIcon();
					if (!Directory.Exists("Meta"))
					{
						Directory.CreateDirectory("Meta");
					}
					string folderpath = "Meta/" + scn.guid;
					if (scn.guid.Length >= 2)
					{
						folderpath = "Meta/" + scn.guid.Substring(0, 2);
					}


					if (!Directory.Exists(folderpath))
					{
						Directory.CreateDirectory(folderpath);
					}
					folderpath = folderpath + "/" + scn.guid;
					if (!Directory.Exists(folderpath))
					{
						Directory.CreateDirectory(folderpath);
					}
					folderpath += "/icon";
					if (!Directory.Exists(folderpath))
					{
						Directory.CreateDirectory(folderpath);
					}
					scn.icon.save(folderpath + "/" + scn.guid + ".png", System.Drawing.RotateFlipType.Rotate270FlipNone);
					scn.icon.Dispose();
					scn.Dispose();
				}
				else if (ext.IsTextureExtension())
				{
					this.name = Path.GetFileNameWithoutExtension(filePath);
					this.type = RessourceType.Texture;

					Texture tmp = new Texture(filePath);
					this.guid = tmp.guid;
					//tmp.generateIcon();
					if (!Directory.Exists("Meta"))
					{
						Directory.CreateDirectory("Meta");
					}
					string folderpath = "Meta/" + tmp.guid;
					if (tmp.guid.Length >= 2)
					{
						folderpath = "Meta/" + tmp.guid.Substring(0, 2);
					}


					if (!Directory.Exists(folderpath))
					{
						Directory.CreateDirectory(folderpath);
					}
					folderpath = folderpath + "/" + tmp.guid;
					if (!Directory.Exists(folderpath))
					{
						Directory.CreateDirectory(folderpath);
					}
					folderpath += "/icon";
					if (!Directory.Exists(folderpath))
					{
						Directory.CreateDirectory(folderpath);
					}
					tmp.save(folderpath + "/" + tmp.guid + ".png", System.Drawing.RotateFlipType.RotateNoneFlipNone);
					//tmp.icon.Dispose();
					tmp.Dispose();
					//MappedRessourceInfo tmp = new MappedRessourceInfo(filePath);
				}
				else if (ext.IsAudioExtension())
				{
					this.name = Path.GetFileNameWithoutExtension(filePath);
					this.type = RessourceType.Sound;
					AudioClip tmp = new AudioClip(filePath);
					this.guid = tmp.guid;
					tmp.Dispose();
				}
				else if (ext.IsScriptExtension())
				{
					this.name = Path.GetFileNameWithoutExtension(filePath);
					this.type = RessourceType.Script;
					UserScript tmp = new UserScript(filePath);
					this.guid = tmp.guid;
					tmp.Dispose();
					//UserScript tmp = new UserScript(filePath);
				}
				string metaFile = filePath + ".meta";
				if (!File.Exists(metaFile) && this.type != RessourceType.None)
				{
					Serializer.Serialize(metaFile, this);
					//Debug.Log(Serializer.CustomProperty.logPath, "guid for:" + this.type.ToString() + "=" + this.guid + ";" + " (filepath:" + this.filePath + ")");
					//Debug.Log(Serializer.CustomProperty.logPath, "guid for:" + this.type.ToString() + "=" + this.guid + ";");
					if (this.guid == "DA")
					{
						Console.WriteLine("Hey new da");
					}
					//this.Register();
				}
				if (this.type != RessourceType.None)
				{
					this.Register();
				}
			}
			public bool generated
			{
				get
				{
					return (SharedResource.getRessource(this.guid) != null);
				}
			}
			public SharedResource Regenerate()
			{
				SharedResource res = null;
				if (this.generated)
				{
					Console.WriteLine("generated allready");
					StackTrace trace = new StackTrace(true);
					//for (int i = 0; i < trace.FrameCount; i++)
					//{
					// Note that high up the call stack, there is only
					// one stack frame.
					string console = "";
					try
					{
						if (trace.GetFrame(1) != null && trace.GetFrame(1).GetMethod() != null)
						{
							Type type = trace.GetFrame(1).GetMethod().DeclaringType;
							if (type.Assembly == Assembly.GetExecutingAssembly())
							{
								console += "\n";
								console += trace.GetFrame(1).GetFileName() + "; l";
								console += trace.GetFrame(1).GetFileLineNumber() + " - c";
								console += trace.GetFrame(1).GetFileColumnNumber() + " : ";
							}
						}
					}
					catch (Exception e)
					{
						;
					}
					Console.WriteLine("console:" + console);
					if (this.type == RessourceType.Texture)
					{
						Console.WriteLine("guid:" + this.guid);

						//Console.WriteLine(SharedResource.getRessource(this.guid).ToString());
					}
					return (SharedResource.getRessource(this.guid));
				}
				Console.WriteLine("regenerating" + this.root.name);
				if (this.parent != null && !this.parent.generated)
				{
					this.root.Regenerate();
					return (SharedResource.getRessource(this.guid));
				}
				else if (this.parent != null && this.parent.generated)
				{
					return (SharedResource.getRessource(this.guid));
				}
				bool getFromParent = false;
				if (this.parent != null)
				{
					getFromParent = true;
				}
				if (this.type == RessourceType.Material)
				{

					//gerer le parent existant, ou non, ici, avant d'aller creer un nouveau quoi que ce soit.
					return (new Material(this));
				}
				else if (this.type == RessourceType.Mesh)
				{
					// gerer le parent existant, ou non, ici.
					return (new Mesh(this));
				}
				else if (this.type == RessourceType.Scene)
				{

					//gerer le parent existant, ou non, ici, avant d'aller creer un nouveau quoi que ce soit.
					return (new Scene(this, 0));
				}
				else if (this.type == RessourceType.Script)
				{
					//gerer le parent existant, ou non, ici, avant d'aller creer un nouveau quoi que ce soit.
					return (new UserScript(this));
				}
				else if (this.type == RessourceType.Sound)
				{
					//gerer le parent existant, ou non, ici, avant d'aller creer un nouveau quoi que ce soit.
					return (new AudioClip(this));
				}
				else if (this.type == RessourceType.Texture)
				{
					if (this.guid == "DA")
					{
						Console.WriteLine("regenerating texture");
					}
					//gerer le parent existant, ou non, ici, avant d'aller creer un nouveau quoi que ce soit.
					return (new Texture(this));
				}
				return (res);
			}

			public void UnPack()
			{

				if (!SharedResource.RessourceDictionary.ContainsKey(this.guid))
				{
					this.Regenerate();
				}
				else
				{
					return;
				}
				//this.UnPackRecursively();
			}
			public void update()
			{
				this.LastWriteSeconds = DateTime.Now.Second;
				string ext = Path.GetExtension(filePath);
				//Bitmap bitmap = new Bitmap();
				this.filePath = filePath;
				//	bool check = false;
				if (ext.IsMeshExtension())
				{
					this.type = RessourceType.Scene;
					//Scene scn = new Scene(filePath);
					//for
					/*
					 * faut:
					 * quand on clique sur un truc, voir le guid contenu dans la meta pour debug.
					 * 
					 */

					//Scene
					//Transform tmp = Transform.ImportFromFile()
					//MappedRessourceInfo tmp = new MappedRessourceInfo(filePath);
				}
				else if (ext.IsTextureExtension())
				{
					this.type = RessourceType.Texture;
					//Texture tmp = new Texture(filePath);
					//MappedRessourceInfo tmp = new MappedRessourceInfo(filePath);
				}
				else if (ext.IsAudioExtension())
				{
					this.type = RessourceType.Sound;
					//Sound tmp = new Sound(filePath);
				}
				else if (ext.IsScriptExtension())
				{
					this.type = RessourceType.Script;
					//UserScript tmp = new UserScript(filePath);
				}
				string metaFile = filePath + ".meta";
				if (!File.Exists(metaFile) && this.type != RessourceType.None)
				{
					Serializer.Serialize(metaFile, this);
				}
			}
		}



		public static void MapRessourcesRecursively(string directoryPath)
		{
			string[] filePaths = Directory.GetFiles(directoryPath);
			for (int i = 0; i < filePaths.Length; i++)
			{
				Console.WriteLine("accessing:" + filePaths[i]);
				string meta = filePaths[i] + ".meta";
				if (File.Exists(meta) && Path.GetExtension(filePaths[i]) != ".meta")
				{
					FileInfo info = new FileInfo(filePaths[i]);
					MappedRessourceInfo metaInfo = (MappedRessourceInfo)Serializer.UnSerialize(meta);
					if (metaInfo.LastWriteSeconds < info.LastWriteTime.Second)
					{
						if (metaInfo.guid == "DA")
						{
							Console.WriteLine("DAAAAAAA");
							Console.WriteLine("here0.0:" + ((Texture)SharedResource.RessourceDictionary[metaInfo.guid]).width);
						}
						Console.WriteLine("updating");
						metaInfo.update();
					}
					if (metaInfo.guid == "DA")
					{
						Console.WriteLine("DAAAAAAA");
						Console.WriteLine("here0:" + ((Texture)SharedResource.RessourceDictionary[metaInfo.guid]).width);
						metaInfo.Regenerate();
						Console.WriteLine("here1:" + ((Texture)SharedResource.RessourceDictionary[metaInfo.guid]).width);
						if (((Texture)SharedResource.RessourceDictionary[metaInfo.guid]).disposed)
						{
							Console.WriteLine("has been disposed");
						}
					}
				}
				else
				{
					if (Path.GetFileName(filePaths[i]) == "alien2.dae")
					{
						Console.WriteLine("found that fuckin alien");
					}

					MappedRessourceInfo metaInfo = new MappedRessourceInfo(filePaths[i]);

				}
			}
			foreach (string directory in Directory.GetDirectories(directoryPath))
			{
				MapRessourcesRecursively(directory);
			}
		}

		public static void MapRessources()
		{
			//File.WriteAllText(Serializer.CustomProperty.logPath, "");
			cleanMeta();
			string directoryPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Assets");
			Console.WriteLine("Directory path:" + directoryPath);
			MapRessourcesRecursively(directoryPath);
		}
		public bool loaded = true;
		public static bool isEnvironnementLoaded = false;
		[System.NonSerialized]
		public Texture icon;
		[System.NonSerialized]
		public RessourceInfo info = null;
		private static int _index = 0;
		private int _id = 0;
		public string guid = "";
		public string filePath = "";
		public class DictionaryCollection<T, T2>
		{
			private Dictionary<T, T2> dictionary = new Dictionary<T, T2>();
			public DictionaryCollection()
			{
				;
			}
			public void Add(T key, T2 value)
			{
				if ((string)((object)key) == "DA")
				{
					Console.WriteLine("adding da");
				}
				dictionary.Add(key, value);
			}
			public bool ContainsKey(T key)
			{
				return (dictionary.ContainsKey(key));
			}
			public T2 this[T index]
			{
				get
				{
					return (dictionary[index]);
				}
				set
				{
					if ((string)((object)index) == "DA")
					{
						Console.WriteLine("accessing da");
					}
					dictionary[index] = value;
				}
			}
			public void Remove(T key)
			{
				if ((string)((object)key) == "DA")
				{
					Console.WriteLine("removing da");
				}
				dictionary.Remove(key);
			}
			public bool TryGetValue(T key, out T2 value)
			{
				return (dictionary.TryGetValue(key, out value));
			}
		}
		private static Dictionary<string, SharedResource> RessourceDictionary = new Dictionary<string, SharedResource>();
		public virtual void Dispose()
		{
			if (RessourceDictionary.ContainsKey(this.guid))
			{
				RessourceDictionary[this.guid] = null;
				RessourceDictionary.Remove(this.guid);
				SharedRessources.Remove(this);
				if (this.guid == "DA")
				{
					if (RessourceDictionary.ContainsKey(this.guid))
					{
						Console.WriteLine("IM STILL HERE WTFFFF?");
					}
				}
			}
		}
		public static void addRessource(SharedResource ressource)
		{
			if (ressource.guid != "")
			{
				RessourceDictionary.Add(ressource.guid, ressource);
			}
		}
		public void updateDictionary()
		{
			if (getRessource(this.guid) != null)
				RessourceDictionary[this.guid] = this;
			else
			{
				RessourceDictionary.Add(this.guid, this);
			}
		}
		public static SharedResource getRessource(string sharedRessourceName)
		{
			SharedResource res = null;
			if (RessourceDictionary.ContainsKey(sharedRessourceName))
			{
				return (RessourceDictionary[sharedRessourceName]);
			}
			return (null);
			/*
			if (RessourceDictionary.TryGetValue(sharedRessourceName, out res))
				return (res);
			return (null);*/
			//			RessourceDictionary.TryGetValue(sharedRessourceName, out res);
			//		return (res);
		}
		public static string startIndex
		{
			get
			{
				if (!Directory.Exists(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "../Library/")))
				{
					Directory.CreateDirectory(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "../Library/"));
				}
				if (!File.Exists(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "../Library/metaStartFile")))
					File.WriteAllText(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "../Library/metaStartFile"), "");
				return (File.ReadAllText(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "../Library/metaStartFile")));
			}
			private set
			{
				string last = generateGuid();
				File.WriteAllText(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "../Library/metaStartFile"), last);
			}
		}
		public static List<SharedResource> SharedRessources = new List<SharedResource>();
		public SharedResource()
		{
			this._id = SharedResource._index;
			SharedResource.SharedRessources.Add(this);
			this.guid = generateGuid();
			SharedResource.RessourceDictionary.Add(this.guid, this);
		}
		public string name
		{
			get;
			protected set;
		}
		public SharedResource(MappedRessourceInfo info)
		{
			this.guid = info.guid;
			this.name = info.name;
			if (info.guid == "DA")
				Console.WriteLine("new ressource 0");
			if (!SharedResource.RessourceDictionary.ContainsKey(info.guid))
			{
				if (info.guid == "DA")
				{
					Console.WriteLine("adding new");
				}
				SharedResource.RessourceDictionary.Add(info.guid, this);
			}
			else
			{
				if (info.guid == "DA")
				{
					Console.WriteLine("rewriting old");
				}
				SharedResource.RessourceDictionary[info.guid] = this;
			}
			SharedResource.SharedRessources.Add(this);
			if (info.guid == "DA")
			{
				Console.WriteLine("new ressource");
			}
		}
		public virtual void generateIcon()
		{
			this.icon = null;
		}
		public static string generateGuid()
		{
			string hexValue = startIndex;
			hexValue = _index.ToString("X");
			_index++;
			return (hexValue);
		}
	}
}
