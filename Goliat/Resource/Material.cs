﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Platform;
using System.Reflection;
using System.Drawing;
using System.Drawing.Imaging;
using TextureType = Assimp.TextureType;
using TextureSlot = Assimp.TextureSlot;
using PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web.Script.Serialization;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Color = Goliat.Color;

namespace Goliat
{
	public class Material : SharedResource
	{
		public override string ToString()
		{
			string res = "Material:\n";
			res += "diffuse:" + this.diffuseColor.ToString() + "\n";
			res += "specular:" + this.specularColor.ToString() + "\n";
			res += "shininess:" + this.shininess + "\n";
			res += "strengh:" + this.strength + "\n";
			return (res);

		}
		public Texture[] textures = new Texture[6] { null, null, null, null, null, null };
		public Material(MappedRessourceInfo info)
			: base(info)
		{
			;
		}
		private Color FromColor(Assimp.Color4D color)
		{
			Color c = new Color(0, 0, 0, 0);
			c.R = color.R;
			c.G = color.G;
			c.B = color.B;
			c.A = color.A;
			return c;
		}
		public override void Dispose()
		{
			base.Dispose();
			/*for (int i = 0; i < this.textures.Length; i++)
			{
				if (this.textures[i] != null)
				{
					this.textures[i].Dispose();
					this.textures[i] = null;
				}
			}*/
		}
		public Material(MappedRessourceInfo info, Assimp.Material mat, int index) // au choix, on donne un index de materiaux ici, ou bien on 
			: base(info)
		{
			TextureSlot tex;
			if (mat.GetMaterialTexture(TextureType.Diffuse, index, out tex))
			{
				diffuseMap = new Texture(info.textures[0]);
			}

			if (mat.HasColorDiffuse)
			{
				diffuseColor = FromColor(mat.ColorDiffuse);
			}

			if (mat.HasColorSpecular)
			{
				specularColor = FromColor(mat.ColorSpecular);
			}

			if (mat.HasColorAmbient)
			{
				ambientColor = FromColor(mat.ColorAmbient);
			}

			if (mat.HasColorEmissive)
			{
				emissiveColor = FromColor(mat.ColorEmissive);
			}

			shininess = 1;
			strength = 1;
			if (mat.HasShininess)
			{
				shininess = mat.Shininess;
			}
			if (mat.HasShininessStrength)
			{
				strength = mat.ShininessStrength;
			}
			//generateIcon();
		}
		private Color4 FromColor(Color color)
		{
			Color4 c;
			c.R = color.R;
			c.G = color.G;
			c.B = color.B;
			c.A = color.A;
			return c;
		}
		public Texture diffuseMap
		{
			get
			{
				return (this.textures[0]);
			}
			set
			{
				this.textures[0] = value;
			}
		}
		public Texture normalMap
		{
			get
			{
				return (this.textures[1]);
			}
			set
			{
				this.textures[1] = value;
			}
		}
		public Texture alphaMap
		{
			get
			{
				return (this.textures[2]);
			}
			set
			{
				this.textures[2] = value;
			}
		}
		public Texture specularMap
		{
			get
			{
				return (this.textures[3]);
			}
			set
			{
				this.textures[3] = value;
			}
		}
		public Texture reflectiveMap
		{
			get
			{
				return (this.textures[4]);
			}
			set
			{
				this.textures[4] = value;
			}
		}
		public Texture emissiveMap
		{
			get
			{
				return (this.textures[5]);
			}
			set
			{
				this.textures[5] = value;
			}
		}
		public Color diffuseColor = new Color(0.6f, .6f, .6f, 1.0f);
		public Color specularColor = new Color(0, 0, 0, 1.0f);
		public Color ambientColor = new Color(.6f, .6f, .6f, 1.0f);
		public Color emissiveColor = new Color(0, 0, 0, 1.0f);
		public float shininess = 0.3f;
		public float strength = 1f;
		public MaterialType type;
		public override void generateIcon()
		{
			/*this.Bind();
			Mesh tmp = Primitive.Sphere;
			tmp.generateIcon();
			this.icon = tmp.icon;
			this.UnBind();*/
		}
		public void Bind()
		{
			if (diffuseMap != null)
			{
				GL.Enable(EnableCap.Texture2D);
				diffuseMap.Bind();
			}
			else
			{
				GL.BindTexture(TextureTarget.Texture2D, 0);
				GL.Disable(EnableCap.Texture2D);
			}
			//GL.Enable(EnableCap.ColorMaterial);
			if (diffuseColor.A != 1)
			{
				Debug.Log("TRANSPARENCY");
				GL.Enable(EnableCap.Blend);
				GL.DepthMask(false);
			}
			else
			{
				//Debug.Log("NON");
				GL.Disable(EnableCap.Blend);
				GL.DepthMask(true);
			}
			GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Diffuse, diffuseColor.gl());
			GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Specular, specularColor.gl());
			GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Ambient, ambientColor.gl());
			GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Emission, emissiveColor.gl());
			GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Shininess, shininess * strength);
		}
		public void UnBind()
		{
			this.diffuseMap.UnBind();
		}
		public Material()
		{
			;
		}
		public static Material standard
		{
			get
			{
				Material res = new Material();
				return (res);
			}
		}
		public Material(Assimp.Material mat, int index)
		{
			TextureSlot tex;
			if (mat.GetMaterialTexture(TextureType.Diffuse, index, out tex))
			{
				diffuseMap = new Texture(tex.FilePath);
			}

			if (mat.HasColorDiffuse)
			{
				diffuseColor = FromColor(mat.ColorDiffuse);
			}

			if (mat.HasColorSpecular)
			{
				specularColor = FromColor(mat.ColorSpecular);
			}

			if (mat.HasColorAmbient)
			{
				ambientColor = FromColor(mat.ColorAmbient);
			}

			if (mat.HasColorEmissive)
			{
				emissiveColor = FromColor(mat.ColorEmissive);
			}

			shininess = 1;
			strength = 1;
			if (mat.HasShininess)
			{
				shininess = mat.Shininess;
			}
			if (mat.HasShininessStrength)
			{
				strength = mat.ShininessStrength;
			}
			//generateIcon();
		}
	}
}
